//
//  wbFeedSelectView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 05/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbFeedSelectView : UIView <UITextFieldDelegate>
{
    UIImageView *feedArrow;
}

#define kICONPADDING 4
#define kALPHASELECT [UIColor colorWithRed:67.0/255.0 green:73.0/255.0 blue:87.0/255.0 alpha:0.25]

@property (nonatomic,retain) id delegate;
@property (nonatomic,retain) UITextField *searchField;
@property (nonatomic) NSInteger selectedId;
@property (nonatomic,retain) NSDictionary *searchFeed;
@property BOOL showFeedArrow;
@property BOOL showSearch;

@property (nonatomic,retain) NSMutableDictionary *feeds;

-(void)updateFeed:(id)selector;
-(void)cancelSearch:(id)selector;
-(void)open;
-(void)close;
-(void)close:(BOOL)animate;

@end
