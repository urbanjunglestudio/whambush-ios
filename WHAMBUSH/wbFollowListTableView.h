//
//  wbFollowListTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 15/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseTableView.h"
#import "wbFollowListCell.h"

@interface wbFollowListTableView : wbBaseTableView<UITableViewDelegate,UITableViewDataSource>
{
    BOOL isMe;
    UIActivityIndicatorView *ai;
    UIRefreshControl *refreshController;
}

@property (nonatomic) BOOL isFollowers;
@property (nonatomic,retain) wbUser *user;

@end
