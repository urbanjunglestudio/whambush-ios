//
//  wbNewMissionTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseTableView.h"
#import "wbNewMissionCell.h"

@interface wbNewMissionTableView : wbBaseTableView <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>
{
    UIRefreshControl *refreshController;
    CGFloat prevY;
    NSIndexPath *currentRow;
    BOOL refreshing;
}

-(void)refreshRow;


@end
