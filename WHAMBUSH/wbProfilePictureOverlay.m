//
//  wbProfilePictureOverlay.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 17/02/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbProfilePictureOverlay.h"

@implementation wbProfilePictureOverlay

@synthesize editorMode;
@synthesize cropRect;
@synthesize delegate;


- (id)initWithFrame:(CGRect)frame
    {
        self = [super initWithFrame:frame];
        if (self) {
            [self setBackgroundColor:kTRANSPARENTUICOLOR];
            overlay = [self overlayLayer:frame.size];
        }
        return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    UIImageView *ol = [[UIImageView alloc] initWithFrame:self.frame];
    [ol setBackgroundColor:kTRANSPARENTUICOLOR];
    [ol setImage:overlay];
    [self addSubview:ol];
    
    if (!editorMode) {
        UIButton *camera = [UIButton buttonWithType:UIButtonTypeSystem];
        [camera setBackgroundImage:[UIImage ch_imageNamed:@"profile_camera_button.png"] forState:UIControlStateNormal];
        [camera addTarget:delegate action:@selector(cameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [camera setFrame:CGRectMake(0, 0, [camera backgroundImageForState:UIControlStateNormal].size.width, [camera backgroundImageForState:UIControlStateNormal].size.height)];
        [camera setTintColor:kTRANSPARENTUICOLOR];
        [camera setCenter:CGPointMake(ol.frame.size.width/2, ol.frame.size.height-50)];
        
        UIImage *backImg = [UIImage ch_imageNamed:@"capture_cancel.png"];
        wbButton *back  = [wbButton buttonWithType:UIButtonTypeSystem];
        [back addTarget:delegate action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [back setFrame:CGRectMake(0, 0, backImg.size.width*2, backImg.size.height*2)];
        [back setButtonImage:backImg];
        [back setTag:0];
        [back setCenter:CGPointMake(ol.frame.size.width/4-10, camera.center.y)];
        
        flipCamera = [UIButton buttonWithType:UIButtonTypeSystem];
        [flipCamera setBackgroundImage:[UIImage ch_imageNamed:@"capture_cam2back.png"] forState:UIControlStateNormal];
        [flipCamera setBackgroundImage:[UIImage ch_imageNamed:@"capture_cam2front.png"] forState:UIControlStateSelected];
        [flipCamera addTarget:delegate action:@selector(flipCameraButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [flipCamera setSelected:NO];
        [flipCamera setFrame:CGRectMake(0, 0, [flipCamera backgroundImageForState:UIControlStateNormal].size.width, [flipCamera backgroundImageForState:UIControlStateNormal].size.height)];
        [flipCamera setTintColor:kTRANSPARENTUICOLOR];
        [flipCamera setCenter:CGPointMake(ol.frame.size.width/2+ol.frame.size.width/4+10, camera.center.y)];
        
        [self addSubview:back];
        [self addSubview:camera];
        [self addSubview:flipCamera];
    } else {
        UIButton *back  = [UIButton buttonWithType:UIButtonTypeSystem];
        [back setBackgroundImage:[UIImage ch_imageNamed:@"cancel_profile_pic.png"] forState:UIControlStateNormal];
        [back addTarget:delegate action:@selector(backButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [back setFrame:CGRectMake(0, 0, [back backgroundImageForState:UIControlStateNormal].size.width, [back backgroundImageForState:UIControlStateNormal].size.height)];
        [back setTintColor:kTRANSPARENTUICOLOR];
        [back setTag:1];
        [back setCenter:CGPointMake(ol.frame.size.width/3, ol.frame.size.height-55)];
        
        UIButton *done  = [UIButton buttonWithType:UIButtonTypeSystem];
        [done setBackgroundImage:[UIImage ch_imageNamed:@"accept_profile_pic.png"] forState:UIControlStateNormal];
        [done addTarget:delegate action:@selector(doneButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [done setFrame:CGRectMake(0, 0, [done backgroundImageForState:UIControlStateNormal].size.width, [done backgroundImageForState:UIControlStateNormal].size.height)];
        [done setTintColor:kTRANSPARENTUICOLOR];
        [done setCenter:CGPointMake(ol.frame.size.width-back.center.x, back.center.y)];
        
        wbLabel *help = [[wbLabel alloc] init];
        [help setText:NSLocalizedString(@"USER_SETTINGS_PROFILE_PICTURE_POSITION", @"")];
        [help setFont:kFONTHelvetica(20)];
        [help setTextColor:kWHITEUICOLOR];
        [help sizeToFit];
        [help setCenter:CGPointMake(ol.frame.size.width/2, CGRectGetMinY(back.frame)-30)];

        [self addSubview:help];
        [self addSubview:back];
        [self addSubview:done];
    }
}


-(UIImage*)overlayLayer:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width,size.height),0.0,0.0);
    //// Bezier Drawing
    float height = size.height;
    float width = size.width;
    
    float fromLeft = 1;
    float diameter = (size.width-(fromLeft*2));
    float fromTop = size.height/2.5-diameter/2;
    float radius = diameter/2;
    float rr = radius * 4 *(sqrtf(2)-1)/3;
    
    cropRect = CGRectMake(0, fromTop, size.width, size.width);
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    
    float X1 = fromLeft+radius;
    float Y1 = fromTop;
    float X2 = fromLeft;
    float Y2 = fromTop+radius;
    float X3 = fromLeft+radius;
    float Y3 = fromTop+diameter;
    float X4 = fromLeft+diameter;
    float Y4 = fromTop+radius;
    
    [bezierPath moveToPoint: CGPointMake(X1, Y1)];
    [bezierPath addCurveToPoint: CGPointMake(X2, Y2)
                  controlPoint1: CGPointMake(X1-rr, Y1)
                  controlPoint2: CGPointMake(X2, Y2-rr)];
    
    [bezierPath addCurveToPoint: CGPointMake(X3, Y3)
                  controlPoint1: CGPointMake(X2, Y2+rr)
                  controlPoint2: CGPointMake(X3-rr, Y3)];
    
    [bezierPath addCurveToPoint: CGPointMake(X4, Y4)
                  controlPoint1: CGPointMake(X3+rr, Y3)
                  controlPoint2: CGPointMake(X4, Y4+rr)];
    
    [bezierPath addCurveToPoint: CGPointMake(X1, Y1)
                  controlPoint1: CGPointMake(X4, Y4-rr)
                  controlPoint2: CGPointMake(X1+rr, Y1)];
    
    [bezierPath closePath];
    
    [bezierPath moveToPoint: CGPointMake(0, 0)];
    [bezierPath addLineToPoint: CGPointMake(width, 0)];
    [bezierPath addLineToPoint: CGPointMake(width, height)];
    [bezierPath addLineToPoint: CGPointMake(0, height)];
    [bezierPath addLineToPoint: CGPointMake(0, 0)];
    [bezierPath closePath];
    
    [kBKGUICOLORAlpha setFill];
    [bezierPath fill];
    
    UIImage *thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return thumbnailImage;
    
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [super hitTest:point withEvent:event];
    
    // If the hitView is THIS view, return nil and allow hitTest:withEvent: to
    // continue traversing the hierarchy to find the underlying view.
    if (hitView == self) {
        return nil;
    }
    // Else return the hitView (as it could be one of this view's buttons):
    return hitView;
}


@end
