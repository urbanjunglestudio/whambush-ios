//
//  wbNewMissionViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"
#import "wbNewMissionTableView.h"
#import "wbMissionCountrySelectViewController.h"

@interface wbNewMissionViewController : wbBaseViewController

@property (retain,nonatomic,readonly) wbMissionCountrySelectViewController *countrySelect;

@end
