//
//  wbBaseTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 31/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbBaseTableView.h"

@implementation wbBaseTableView

@synthesize endpoint;
@synthesize currentFeed;
@synthesize hideHeader;
@synthesize allFeeds;
@synthesize showFeedSelectSearch;

-(id)initWithFrame:(CGRect)frame

{
    self = [super initWithFrame:frame];
    if (self) {
        refresh = NO;
        hideHeader = NO;
        deltaY = 0;
        gettingMoreResults = NO;
        showFeedSelectSearch = YES;
        origY = 0;
        prevContentOffset = 0;
        onGoing = YES;
    }
    
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)refresh:(UIRefreshControl *)refreshControl
{
    if (refreshControl != nil) {
        [refreshControl beginRefreshing];
    }
    if ([[wbAPI sharedAPI] checkIfNetworkAvailable] && !onGoing) {
        kSHOWWAIT;
        [[wbHeader sharedHeader] setCountryChangeActive:NO];
       
        dataArray = nil;
        resultArray = nil;
        refresh = YES;
        
        
        NSMutableArray *removeRows = [[NSMutableArray alloc] init];
        for (int i = 0; i < [self numberOfRowsInSection:0]; i++) {
            NSIndexPath *ip = [NSIndexPath indexPathForRow:i inSection:0];
            [removeRows addObject:ip];
            ip = nil;
        }
        [self beginUpdates];
        numberOfResults = 0;
        [self deleteRowsAtIndexPaths:removeRows withRowAnimation:UITableViewRowAnimationFade];
        [self endUpdates];
        
        if ([self respondsToSelector:@selector(executeRefresh)]) {
            [self performSelector:@selector(executeRefresh)];
        } else {
            [[wbAPI sharedAPI] getDataWithEndpoint:endpoint delegate:self];
        }
    }
    if (refreshControl != nil) {
        [refreshControl endRefreshing];
    }
}

-(void)dataReady:(id)data
{
    if (dataArray == nil || refresh == YES) {
        onGoing = YES;
        dataArray = (NSMutableDictionary*)data;
        
        if (resultArray == nil || refresh == YES) {
            resultArray = [NSMutableArray arrayWithArray:[dataArray objectForKey:@"results"]];
        } else {
            [resultArray addObjectsFromArray:[dataArray objectForKey:@"results"]];
        }
        
        numberOfResults = [resultArray count];
        totalNumberOfResults = [[dataArray valueForKey:@"count"] integerValue];
        
        nextUrl = [dataArray objectForKey:@"next"];
        
        [self performSelector:@selector(resultsReady)];
        if (!refresh) {
            UIView *tableHeader =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kTXTBOXH)];
            //[tableHeader setBackgroundColor:[feedSelect backgroundColor]];
            [tableHeader setBackgroundColor:kTRANSPARENTUICOLOR];
            
            if (!hideHeader) {
                [self createFeedSelect];
                if ([currentFeedType isEqualToString:@"mission"]) {
                    [feedSelectButton setUserInteractionEnabled:NO];
                } else {
                    [feedSelectButton setUserInteractionEnabled:YES];
                }
            } else {
                [tableHeader setFrame:CGRectMake(0, 0, self.frame.size.width, 1)];
            }
            if ([self tableHeaderView] == nil) {
                [self setTableHeaderView:tableHeader];
            }
        }
        
    } else {
        DMSG;
        kHIDEWAIT;
    }
    refresh = NO;
    onGoing = NO;
    [[wbHeader sharedHeader] setCountryChangeActive:YES];
    
}

-(void)getMoreResults
{
    if([[wbAPI sharedAPI] checkIfNetworkAvailable]){
        if (!gettingMoreResults) {
            if (nextUrl != nil) {
                dataArray = nil;
                [[wbAPI sharedAPI] getDataWithURL:nextUrl delegate:self];
            }
        }
    }
}

-(void)showAskToLoginRegisterOnView
{
    [[wbLoginRegisterViewController sharedLRVC] showAskToLoginRegisterOnView:self];
}

-(void)scrollTableTop
{
    [self closeFeed];
    [self setContentOffset:CGPointZero animated:YES];
}

-(void)updateFeed:(NSDictionary*)feed
{
    //if ([[wbAPI sharedAPI] checkIfNetworkAvailable]) {
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
        [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
    }];
    [op setCompletionBlock:^(void){
        [self performSelectorOnMainThread:@selector(updateFeedMainThread:) withObject:feed waitUntilDone:NO];
    }];
    [op start];
    //}
}

/////////////////////
-(void)setCurrentFeed:(NSDictionary *)_currentFeed
{
    currentFeedType = [_currentFeed objectForKey:@"type"];
    currentFeed = _currentFeed;
    endpoint = [_currentFeed objectForKey:@"endpoint"];
    refresh = NO;
//    [self setGAIViewName];
}

-(void)updateFeedMainThread:(NSDictionary*)feed
{
    if ([[feed valueForKey:@"id"] integerValue] != [[currentFeed valueForKey:@"id"] integerValue] || [[feed valueForKey:@"id"] integerValue] == 0) {
        
        [self setCurrentFeed:feed];
        currentFeedType = [currentFeed objectForKey:@"type"];
        
        //[self setGAIViewName];
        
        
        NSMutableArray *ips = [[NSMutableArray alloc] init];
        NSInteger count = [self numberOfRowsInSection:0];
        for (int i = 0; i < count; i++) {
            [ips addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        }
        dataArray = nil;
        resultArray = nil;
        totalNumberOfResults = 0;
        numberOfResults = 0;
        
        deltaY = 0;
        [feedSelect setFrame:CGRectMake(0, origY, self.frame.size.width, kTXTBOXH)];
        [feedSelectButton setFrame:feedSelect.frame];
        
        [[self tableFooterView] setHidden:YES];
        
        [self beginUpdates];
        [self deleteRowsAtIndexPaths:ips withRowAnimation:UITableViewRowAnimationBottom];
        [self endUpdates];
        
        
        if ([currentFeedType isEqualToString:@"normal"]) {
            [[wbData sharedData] getFeedData:[currentFeed objectForKey:@"name"] delegate:self];
        } else if ([currentFeedType isEqualToString:@"missions"]) {
            [[wbData sharedData] getMissionsData:[currentFeed objectForKey:@"name"] delegate:self];
        } else {
            if ([currentFeedType isEqualToString:@"search"]) {
                [feedSelect setSearchFeed:currentFeed];
            }
            [[wbAPI sharedAPI] getDataWithEndpoint:[currentFeed objectForKey:@"endpoint"] delegate:self];
        }
        
    } else {
        kHIDEWAIT;
    }
    
    [feedSelect setSelectedId:[[currentFeed valueForKey:@"id"] integerValue]];
    [feedSelect setShowFeedArrow:YES];
    [feedSelect close];
    [feedSelectButton setHidden:NO];
    
}
-(void)createFeedSelect
{
    origY = 0;
    if (feedSelect == nil) {
        feedSelect  = [[wbFeedSelectView alloc] initWithFrame:CGRectMake(0, origY, self.frame.size.width, kTXTBOXH)];
        feedSelectButton = [[UIButton alloc] initWithFrame:feedSelect.frame];
    }
    [feedSelectButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [feedSelectButton addTarget:self action:@selector(openFeed) forControlEvents:UIControlEventTouchUpInside];
    
    [feedSelect setDelegate:self];
    if ([[currentFeed objectForKey:@"type"] isEqualToString:@"mission"]) {
        [feedSelect setSearchFeed:currentFeed];
        [feedSelect setShowFeedArrow:NO];
    } else {
        [feedSelect setFeeds:allFeeds];
    }
    DCMSG(currentFeed);
    [feedSelect setSelectedId:[[currentFeed valueForKey:@"id"] integerValue]];
    [feedSelect setShowSearch:showFeedSelectSearch];
    
    [self addSubview:feedSelect];
    [self addSubview:feedSelectButton];
}

-(void)openFeed
{
    if (feedSelect.frame.size.height == kTXTBOXH) {
        [feedSelect open];
        [feedSelectButton setHidden:YES];
    }
}

-(void)closeFeed
{
    if (feedSelect.frame.size.height > kTXTBOXH) {
        [feedSelect setShowFeedArrow:YES];
        [feedSelect close:NO];
        [feedSelectButton setHidden:NO];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!refresh) {
        CGRect oldFrame = feedSelect.frame;
        float newY = origY + scrollView.contentOffset.y;
        
        if (scrollView.contentOffset.y > prevContentOffset) {
            deltaY += scrollView.contentOffset.y - prevContentOffset;
            if (deltaY > (float)oldFrame.size.height) {
                [self closeFeed];
                oldFrame = feedSelect.frame;
                deltaY = (float)oldFrame.size.height;
            }
            newY = newY - deltaY;
        }
        else {
            deltaY -= prevContentOffset - scrollView.contentOffset.y;
            if (deltaY < 0) {
                deltaY = 0;
            }
            newY = newY - deltaY;
        }
        if (scrollView.contentOffset.y <= 0) {
            newY = origY;
            deltaY = 0;
        }
        
        //DNSLog(@"%.f : %.f : %.f",scrollView.contentOffset.y,prevContentOffset,deltaY);
        CGRect newFrame = CGRectMake(oldFrame.origin.x, newY, oldFrame.size.width, oldFrame.size.height);
        [feedSelect setFrame:newFrame];
        [feedSelectButton setFrame:feedSelect.frame];
        prevContentOffset = scrollView.contentOffset.y;
    }
}


@end
