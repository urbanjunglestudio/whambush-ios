//
//  wbTagsView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 10/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbTagsView.h"

@implementation wbTagsView

@synthesize tagStrings;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //tagStrings = [NSMutableArray arrayWithObjects:@"+ Add Tag",nil];
        addTags = [[NSMutableArray alloc] init];//[NSMutableArray arrayWithObjects:nil];
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    NSUInteger numOfTags = [tagStrings count];
    NSUInteger numberOfButtons;
    DNSLog(@"tags: %ld",(long)numOfTags);
    for (int i = 0; i < numOfTags; i++) {
        DMSG;
        UIButton *addTag = [UIButton buttonWithType:UIButtonTypeSystem];
        if (i == 0) {
            if (numOfTags < 6) {
                [addTag setFrame:CGRectMake(0, 0, self.frame.size.width/numOfTags, kTXTBOXH)];
                numberOfButtons = numOfTags;
            } else {
                [addTag setFrame:CGRectMake(0, 0, 0, kTXTBOXH)];
                numberOfButtons = numOfTags-1;
            }
        } else {
               [addTag setFrame:CGRectMake(CGRectGetMaxX([[addTags objectAtIndex:i-1] frame])+2, 0, self.frame.size.width/numberOfButtons, kTXTBOXH)];
        }
        [addTag setTitle:[tagStrings objectAtIndex:i] forState:UIControlStateNormal];
        [addTag setBackgroundColor:kBOTTOMUICOLOR];
        [addTag setContentHorizontalAlignment:UIControlContentHorizontalAlignmentFill];
        [addTag setTitleColor:kWHITEUICOLOR forState:UIControlStateNormal];
        [addTag.titleLabel setFont:kFONTHelvetica(18)];
        [addTag setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
        [addTag setTag:i];
        [addTag addTarget:delegate action:@selector(modifyTag:) forControlEvents:UIControlEventTouchUpInside];
        [addTags setObject:addTag atIndexedSubscript:i];
        [self addSubview:addTag];
    }
}

@end
