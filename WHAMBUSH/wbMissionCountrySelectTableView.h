//
//  wbMissionCountrySelectTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 14/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseTableView.h"

@interface wbMissionCountrySelectTableView : UITableView <UITableViewDelegate, UITableViewDataSource>

{
    NSInteger numberOfResults;
    NSMutableArray *supportedCountries;
}

@property id missionView;

@end
