//
//  wbCameraView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/17/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbCameraView.h"

@implementation wbCameraView


@synthesize doneButton;
@synthesize previewButton;
@synthesize backButton;
@synthesize cameraRollButton;
@synthesize cameraButton;
@synthesize toggleCameraButton;
@synthesize toggleCameraImgView;
@synthesize helpLabel;
@synthesize timerView;
@synthesize missionData;
@synthesize recording;
@synthesize firstVideoDone;
@synthesize fileUrl;
@synthesize previewUrl;


- (id)initWithFrame:(CGRect)frame controller:(UIViewController*)_controller
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        recording = NO;
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        X = self.frame.size.height/6.0;
        Y = self.frame.size.width/10.0;
        count = kVIDEOMAXTIME;
        controller = _controller;
        firstVideoDone = NO;
        toolBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, Y, self.frame.size.height)];
        [toolBar setBackgroundColor:kBOTTOMUICOLOR];
        [toolBar setAlpha:0.75];

        cameraRollImgView = [[UIImageView alloc] init];
        [cameraRollImgView setImage:[UIImage ch_imageNamed:@"capture_no_access_camroll.png"]];
        [self createCameraRollImg];

    }
    return self;
}

-(void)resetView
{
    recording = NO;
    count = kVIDEOMAXTIME;
    doneButton.hidden = YES;
    cameraRollButton.hidden = recording;
    backButton.hidden = recording;
    helpLabel.hidden = recording;
    [timerView setRecording:recording];
    [timerView setSeconds:count];
    [timerView setNeedsDisplay];
    //[self setNeedsDisplay];

}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    if (blurView == nil) {
        blurView = [[wbPortraitOverlayView alloc] initWithFrame:self.frame];
    }
    [blurView setHidden:YES];
    
    //Toggle start Camera "button"
    if (toggleRecordButton == nil) {
        toggleRecordButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    }
    [toggleRecordButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [toggleRecordButton addTarget:controller action:@selector(toggleVideoCapture) forControlEvents:UIControlEventTouchUpInside];

    
    //Toggle camera type button (front/back)
    UIImage* toggleCameraFrontImg = [UIImage ch_imageNamed:@"capture_cam2back.png"];
    UIImage* toggleCameraBackImg = [UIImage ch_imageNamed:@"capture_cam2front.png"];
    if (toggleCameraImgView == nil) {
        toggleCameraImgView = [[UIImageView alloc] initWithImage:toggleCameraFrontImg highlightedImage:toggleCameraBackImg];
    }
    [toggleCameraImgView setFrame:CGRectMake(0, 0, Y/2, toggleCameraFrontImg.size.height/(toggleCameraBackImg.size.width/(Y/2)))];
    [toggleCameraImgView setHighlighted:NO];

    if (toggleCameraButton == nil) {
        toggleCameraButton = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [toggleCameraButton setFrame:CGRectMake(0, 0, Y, toggleCameraFrontImg.size.height/(toggleCameraFrontImg.size.width/(Y)))];
    [toggleCameraButton setCenter:CGPointMake(toolBar.frame.size.width/2,self.frame.size.height/2)];
    [toggleCameraButton setShowsTouchWhenHighlighted:YES];
    [toggleCameraButton addTarget:controller action:@selector(toggleCamera:) forControlEvents:UIControlEventTouchUpInside];
    [toggleCameraImgView setCenter:CGPointMake(toggleCameraButton.frame.size.width/2, toggleCameraButton.frame.size.height/2)];
    [toggleCameraButton addSubview:toggleCameraImgView];
    
    //Back button
    UIImage *backButtonImg = [UIImage ch_imageNamed:@"capture_cancel.png"];
    UIImageView *backButtonImgView = [[UIImageView alloc] initWithImage:backButtonImg ];
    [backButtonImgView setFrame:CGRectMake(0, 0, Y/3, Y/3)];
    
    if (backButton == nil) {
        backButton = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [backButton setShowsTouchWhenHighlighted:YES];
    [backButton setFrame:CGRectMake(0,Y/2,Y,Y)];
    CGPoint centerPoint = CGPointMake(toolBar.frame.size.width/2, backButton.center.y);
    [backButton setCenter:centerPoint];
    [backButton addTarget:controller action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [backButtonImgView setCenter:CGPointMake(backButton.frame.size.width/2,backButton.frame.size.height/2)];
    [backButton addSubview:backButtonImgView];
    
    
    if (timerView == nil) {
        timerView = [[wbTimerSubView alloc] initWithFrame:CGRectMake(self.bounds.size.width-(X+(2*X/3)),X/2, X, X)];
        [timerView setLineWidth:X/8];
    }
    [timerView setSeconds:count ];
    
    UIImage *undoButtonImg = [UIImage ch_imageNamed:@"capture_segment.png"];
    UIImage *undoTrashButtonImg = [UIImage ch_imageNamed:@"capture_segment_erase.png"];
    if (undoButtonImgView == nil) {
        undoButtonImgView = [[UIImageView alloc] initWithImage:undoButtonImg highlightedImage:undoTrashButtonImg];
    }
    [undoButtonImgView setFrame:CGRectMake(0, 0, X/2, X/2)];
    [undoButtonImgView setHighlighted:NO];
    
    if (undoButton == nil) {
        undoButton = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [undoButton setFrame:CGRectMake(0,CGRectGetMaxY(timerView.frame), X/2, X/2)];
    [undoButton setCenter:CGPointMake(CGRectGetMaxX(timerView.frame), undoButton.center.y)];
    CGPoint undoCenterPoint = undoButton.center;
    [undoButton setFrame:CGRectMake(0, 0, X/1.5, X/1.5)];
    [undoButton setCenter:undoCenterPoint];
    [undoButton addTarget:self action:@selector(undoClip:) forControlEvents:UIControlEventTouchUpInside];
    [undoButton setTintColor:kTRANSPARENTUICOLOR];
    [undoButton setShowsTouchWhenHighlighted:YES];
    [undoButton setHidden:YES];
    [undoButtonImgView setCenter:CGPointMake(undoButton.frame.size.width/2, undoButton.frame.size.height/2)];
    [undoButton addSubview:undoButtonImgView];
    
    
    //Help labels
    if (helpLabel == nil) {
        helpLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    }
    [helpLabel setText:[NSLocalizedString(@"CAMERA_HELP",@"") uppercaseString]];
    [helpLabel setFont:kFONT(30)];
    [helpLabel setTextAlignment:NSTextAlignmentCenter];
    [helpLabel setTextColor:kWHITEUICOLOR];
    [helpLabel setBackgroundColor:kTRANSPARENTUICOLOR];
    [helpLabel setNumberOfLines:2];
    
    //Camera roll button
    [cameraRollImgView setFrame:CGRectMake(0, 0, Y/2, Y/2)];
    
    if (cameraRollButton == nil) {
        cameraRollButton = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [cameraRollButton setShowsTouchWhenHighlighted:YES];
    [cameraRollButton setFrame:CGRectMake(0,self.frame.size.height-Y,Y,Y)];
    [cameraRollButton setCenter:CGPointMake(toolBar.frame.size.width/2, cameraRollButton.center.y)];
    [cameraRollButton addTarget:self action:@selector(openCameraRoll) forControlEvents:UIControlEventTouchUpInside];
    [cameraRollImgView setCenter:CGPointMake(cameraRollButton.frame.size.width/2, cameraRollButton.frame.size.height/2)];
    [cameraRollButton addSubview:cameraRollImgView];
    
    //Done button
    if (doneButton == nil) {
        doneButton = [wbRoundedButton buttonWithType:UIButtonTypeSystem];
    }
    [doneButton setFilled:YES];
    [doneButton setShowsTouchWhenHighlighted:YES];
    [doneButton setFontSize:20];
    [doneButton setHelvetica:NO];
    [doneButton setTitleColor:kBKGUICOLOR forState:UIControlStateNormal];
    [doneButton setTitle:[NSLocalizedString(@"GENERAL_DONE",@"Done") uppercaseString] forState:UIControlStateNormal];
    [doneButton setHidden:YES];
    [doneButton setTitleEdgeInsets:UIEdgeInsetsMake(7, 5, 5, 5)];
    [doneButton sizeToFit];
    [doneButton setFrame:CGRectMake(self.frame.size.width-(X/2 + doneButton.frame.size.width), self.frame.size.height-(X/2 + doneButton.frame.size.height-3), doneButton.frame.size.width, doneButton.frame.size.height)];
    [doneButton addTarget:controller action:@selector(captureDone) forControlEvents:UIControlEventTouchUpInside];

    //preview
    UIImage *previewImg = [UIImage ch_imageNamed:@"play_preview.png"];
    UIImageView *previewImgView = [[UIImageView alloc] initWithImage:previewImg];
    [previewImgView setFrame:CGRectMake(0, 0, X/2, X/2)];
   
    if (previewButton == nil) {
        previewButton = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [previewButton setHidden:YES];
    [previewButton setShowsTouchWhenHighlighted:YES];
    [previewButton setFrame:CGRectMake(CGRectGetMinX(undoButton.frame), CGRectGetMaxY(undoButton.frame), X/1.5, X/1.5)];
    [previewButton addTarget:self action:@selector(openPreview) forControlEvents:UIControlEventTouchUpInside];
    [previewImgView setCenter:CGPointMake(previewButton.frame.size.width/2, previewButton.frame.size.height/2)];
    [previewButton addSubview:previewImgView];
    
    
    [self addSubview:helpLabel];
    [self addSubview:blurView];
    [self addSubview:timerView];
    [self addSubview:toggleRecordButton];
    [self addSubview:toolBar];
    [self addSubview:toggleCameraButton];
    [self addSubview:backButton];
    //[self addSubview:undoButton];
    [self addSubview:cameraRollButton];
    [self addSubview:doneButton];
    //[self addSubview:previewButton];
    
    DMSG;

}

-(void)openCameraRoll
{
    UIAlertView *ask = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"GENERAL_WARNING_TITLE", @"") message:NSLocalizedString(@"CAMERA_WARNING_LOAD_FROM_GALLERY",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"GENERAL_YES",@"") otherButtonTitles:NSLocalizedString(@"GENERAL_NO",@""), nil];
    if (firstVideoDone) {
        [ask show];
    } else {
        [self alertView:ask clickedButtonAtIndex:0];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [kROOTVC performSelector:@selector(startCameraRoll:) withObject:nil];
    }
}

-(void)createCameraRollImg
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    
    // Enumerate just the photos and videos group by using ALAssetsGroupSavedPhotos.
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        
        // Within the group enumeration block, filter to enumerate just photos.
        //        [group setAssetsFilter:[ALAssetsFilter allPhotos]];
        [group setAssetsFilter:[ALAssetsFilter allVideos]];
        
        if ([group numberOfAssets] > 0) {
            
            // Chooses the photo at the last index
            [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *alAsset, NSUInteger index, BOOL *innerStop) {
                
                // The end of the enumeration is signaled by asset == nil.
                if (alAsset) {
                    CGImageRef thumbImg = [alAsset thumbnail];
                    UIImage *latestPhoto = [UIImage imageWithCGImage:thumbImg];
                    
                    // Stop the enumerations
                    *stop = YES; *innerStop = YES;
                    
                    // Do something interesting with the AV asset.
//                    [cameraRollButton setBackgroundImage:latestPhoto forState:UIControlStateNormal];
                    [cameraRollImgView setImage:latestPhoto];
                }
            }];
        }
    } failureBlock: ^(NSError *error) {

        // Typically you should handle an error more gracefully than this.
        DNSLog(@"No groups");
    }];
}

-(void)setFileUrl:(NSURL*)_fileUrl
{
    fileUrl = _fileUrl;
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:_fileUrl options:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:AVURLAssetPreferPreciseDurationAndTimingKey]];
    
    DNSLog(@"**duration %lld, %d %@",[asset duration].value,[asset providesPreciseDurationAndTiming],[fileUrl absoluteString]);
    if (clipTimes == nil) {
        clipTimes = [[NSMutableArray alloc] init];
    }
    [clipTimes addObject:[NSNumber numberWithFloat:CMTimeGetSeconds([asset duration])]];
    count = oldCount - CMTimeGetSeconds([asset duration]);
}

-(void)toggleCamera
{
    if (firstVideoDone) {
        if ([[[wbAPI sharedAPI] iPhoneVersion] integerValue] < 4) {
            [toggleCameraButton removeFromSuperview];
        }
    }
    doneButton.hidden = recording||!firstVideoDone;
    previewButton.hidden = recording||!firstVideoDone;
    cameraRollButton.hidden = recording;
    backButton.hidden = recording;
    toggleCameraButton.hidden = recording;
    toolBar.hidden = recording;
    undoButton.hidden = recording||!firstVideoDone;
    [undoButton setSelected:NO];
    helpLabel.hidden = YES;
    [timerView setRecording:recording];
    //[timerView setNeedsDisplay];
    if (recording) {
        oldCount = count;
    }
    if (!recording) {
        [timerView setClipPoint];
    }
    [blurView setHidden:YES];
    //if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait || [[UIDevice currentDevice] orientation] == UIDeviceOrientationPortraitUpsideDown) {
        //[blurView setHidden:!recording];
    //}
}

-(void)doTimer
{
    count = count - 0.1 ;
    [timerView setSeconds:count];
    if(count <= 0) {
        count = 0;
        [timerView setSeconds:count];
        [controller performSelector:@selector(toggleVideoCapture)];
    }
}

-(void)undoClip:(id)sender
{
    if ([sender isSelected]) {
        [sender setSelected:NO];
        [undoButtonImgView setHighlighted:NO];
        [timerView removeClipPoint];
        count = count + [[clipTimes lastObject] floatValue];
        [timerView setSeconds:count];
        [clipTimes removeLastObject];
        [controller performSelector:@selector(undoClip)];
        [toggleRecordButton removeTarget:self action:@selector(undoUndoClip) forControlEvents:UIControlEventTouchUpInside];
        [toggleRecordButton addTarget:controller action:@selector(toggleVideoCapture) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [sender setSelected:YES];
        [undoButtonImgView setHighlighted:YES];
        [previewButton setHidden:YES];
        [toggleRecordButton removeTarget:controller action:@selector(toggleVideoCapture) forControlEvents:UIControlEventTouchUpInside];
        [toggleRecordButton addTarget:self action:@selector(undoUndoClip) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)undoUndoClip
{
    [undoButton setSelected:NO];
    [undoButtonImgView setHighlighted:NO];
    [previewButton setHidden:NO];
    [toggleRecordButton removeTarget:self action:@selector(undoUndoClip) forControlEvents:UIControlEventTouchUpInside];
    [toggleRecordButton addTarget:controller action:@selector(toggleVideoCapture) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)openPreview
{
    if (previewUrl != nil) {
        if (preview == nil) {
            preview = [[wbVideoPreviewView alloc] initWithFrame:self.frame];
            [preview setDelegate:self];
        }
        [preview setHidden:NO];
        [preview setVideoUrl:previewUrl];
        [self addSubview:preview];
    }
}
-(void)hidePreview
{
    [preview setHidden:YES];
    preview = nil;
}
@end
