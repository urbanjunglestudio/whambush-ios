//
//  wbFlagSelectionView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 27/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbFlagSelectionView.h"

@implementation wbFlagSelectionView

@synthesize videoId;

- (id)initWithFrame:(CGRect)frame
{
    DMSG;
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setUserInteractionEnabled:NO];
    }
    DPRINTCLASS;
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    DMSG;
//    hideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    [hideView setBackgroundColor:[UIColor blackColor]];
//    [hideView setAlpha:0];
    if (hideView == nil) {
        hideView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    [hideView setBackgroundColor:kTRANSPARENTUICOLOR];
    [hideView setAlpha:0.0];
    [hideView setBarStyle:UIBarStyleBlack];

    theBox = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 182+40)];
    [theBox setBackgroundColor:kSEPRATORUICOLORAlpha];
    
    UIPickerView *selection = [[UIPickerView alloc] init];
    [selection setFrame:CGRectMake(0, 40, self.frame.size.width, 180)];
    [selection setBackgroundColor:kSEPRATORUICOLORAlpha];
    [selection setDelegate:self];
    [selection setDataSource:self];
    [selection setShowsSelectionIndicator:YES];

    UIImage *flagImg = [UIImage ch_imageNamed:@"flag_default.png"];
    UIImageView *flagImgView  = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"flag_default.png"]];
    [flagImgView setFrame:CGRectMake(2, 2/*CGRectGetMinY(selection.frame)*/, flagImg.size.width-5, flagImg.size.height-5)];
    
    cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setFrame:hideView.frame];
    [cancelBtn setBackgroundColor:[UIColor clearColor]];
    [cancelBtn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [hideView addSubview:cancelBtn];
    
    UIImage *sendImg = [UIImage ch_imageNamed:@"done_comment_default.png"];
    UIImage *sendImgActive = [UIImage ch_imageNamed:@"done_comment_active.png"];
    sendBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [sendBtn setFrame:CGRectMake(self.frame.size.width-(sendImg.size.width+2), 2, sendImg.size.width-5, sendImg.size.height-5)];
    [sendBtn setBackgroundColor:[UIColor clearColor]];
    [sendBtn setBackgroundImage:sendImg forState:UIControlStateNormal];
    [sendBtn setBackgroundImage:sendImgActive forState:UIControlStateHighlighted];
    [sendBtn setShowsTouchWhenHighlighted:YES];
    [sendBtn addTarget:self action:@selector(reportVideo) forControlEvents:UIControlEventTouchUpInside];
    [theBox addSubview:selection];
    [theBox addSubview:flagImgView];
    [theBox addSubview:sendBtn];
    [self addSubview:hideView];
    [self addSubview:theBox];
}

// Handle the selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    selectedRow = row;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger numRows = 8;
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    int sectionWidth = 320;
    
    return sectionWidth;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
 {
     UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 60)];
     [label setTextColor:kWHITEUICOLOR];
     if (row == 0) {
         [label setText:NSLocalizedString(@"SINGLE_FLAG_TITLE",@"")];
         [label setFont:kFONTHelvetica(24)];
     } else {
         NSString *flagStr = [NSString stringWithFormat:@"SINGLE_FLAG_REASON[%ld]",(long)row];
         [label setText:NSLocalizedString(flagStr, @"")];
         [label setFont:kFONTHelvetica(20)];
     }
     return label;
 }

//show/hide
-(void)show
{
    [self setUserInteractionEnabled:YES];
    // get a rect for the textView frame
	CGRect containerFrame = theBox.frame;
    containerFrame.origin.y =  self.bounds.size.height - theBox.frame.size.height;
	
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
	
	// set views with new info
    hideView.alpha = 0.97;
    theBox.alpha = 0.90;
	theBox.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
}

-(void)hide
{
    DMSG;
    [self setUserInteractionEnabled:NO];
    // get a rect for the textView frame
	CGRect containerFrame = theBox.frame;
    containerFrame.origin.y = self.bounds.size.height;
	
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
	
	// set views with new info
    hideView.alpha = 0.0;
    theBox.alpha = 0.0;
	theBox.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
    
}

-(void)reportVideo
{
    if(selectedRow > 0) {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        
        [op setCompletionBlock:^(void){
            [[wbAPI sharedAPI] reportVideo:videoId reasonCode:selectedRow];
        }];
        [op start];
    }
    [self hide];
}

@end
