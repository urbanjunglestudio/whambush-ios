//
//  wbVideoFeedViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 19/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbVideoFeedViewController.h"

@interface wbVideoFeedViewController ()

@end

@implementation wbVideoFeedViewController

//@synthesize backButton;
@synthesize startFeed;
@synthesize isTv;
@synthesize hideHeader;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //backButton = NO;
        hideHeader = NO;
    }
    DPRINTCLASS;
    return self;
}

//-(void)loadView
//{
//    wbVideoFeedView *videoFeedView = [[wbVideoFeedView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect] controller:self];
//    //[videoFeedView setBackButton:backButton];
//    [videoFeedView setCurrentFeed:startFeed];
//    [self setView:videoFeedView];
//}
-(void)loadView
{
    wbVideoFeedTableView *videoFeedView = [[wbVideoFeedTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    //[videoFeedView setBackButton:backButton];
    if (startFeed != nil) {
        [videoFeedView setCurrentFeed:startFeed];
        if ([[startFeed objectForKey:@"type"] isEqualToString:@"tv"]) {
            [videoFeedView setIsTv:YES];
        }
    }
    [videoFeedView setHideHeader:hideHeader];
    [self setView:videoFeedView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    DMSG;
    [self.view performSelector:@selector(setGAIViewName)];

    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [super viewDidAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarOrientation:1];
    
    [[NSNotificationCenter defaultCenter] addObserver:self.view
                                             selector:@selector(afterDelete)
                                                 name:@"deleteVideoDone"
                                               object:nil];
    
    kHIDEWAIT
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self.view];

}

-(void)setIsTv:(BOOL)_isTv
{
    [(wbVideoFeedTableView*)[self view] setIsTv:_isTv];
}

//show video
-(void)showVideo:(id)sender
{
    [kROOTVC performSelectorOnMainThread:@selector(startSingleVideo:) withObject:self waitUntilDone:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
