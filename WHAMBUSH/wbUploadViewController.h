//
//  wbUploadViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/27/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbAPI.h"
#import "wbDefines.h"
#import "wbUploadView.h"
#import "wbBaseViewController.h"

@interface wbUploadViewController : wbBaseViewController
{
    BOOL scrolled;
}
//@property (nonatomic) wbUploadView *uploadView;

@property (nonatomic,retain) NSDictionary *uploadSetupData;

@end
