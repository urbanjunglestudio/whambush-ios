//
//  wbRootViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/28/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbRootViewController.h"

@interface wbRootViewController ()

@end

@implementation wbRootViewController

@synthesize defaultTvChannel;
@synthesize missionController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[wbAPI sharedAPI] setRootViewController:self];
        [self setEdgesForExtendedLayout:UIRectEdgeNone];

        tvHomeIndex = 0;
    }
    DPRINTCLASS;
    return self;
}

- (void)viewDidLoad
{
    kSHOWWAIT
    DMSG;
    [super viewDidLoad];
    [self.view setBackgroundColor:kTRANSPARENTUICOLOR];
    //[[wbAPI sharedAPI] setRootViewController:self];
    
    //[self initViewControllers];
    
    viewStack = homeViewStack;
    UIView *rootView = [[UIView alloc] initWithFrame:[[wbAPI sharedAPI] portraitFrame]];
    [rootView setBackgroundColor:kBKGUICOLOR];
    [self setView:rootView];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [self setNeedsStatusBarAppearanceUpdate];
    //kHIDEWAIT
    [super viewDidAppear:animated];
    DMSG;
    if (containerView == nil) {
        containerView = [[UIView alloc] initWithFrame:CGRectMake(0,/*[[wbAPI sharedAPI] headerBarHeight]+1*/0, [[wbAPI sharedAPI] portraitFrame].size.width,[[wbAPI sharedAPI] portraitFrame].size.height-(/*[[wbAPI sharedAPI] headerBarHeight]+*/[[wbAPI sharedAPI] footerBarHeight]-1))];
        [containerView setBackgroundColor:kTRANSPARENTUICOLOR];
        [self.view addSubview:containerView];
        //[self.view addSubview:[wbHeader sharedHeader]];
        [self.view addSubview:[wbFooter sharedFooter]];
    }
    [self.view addSubview:[wbHeader sharedHeader]];
    [self.view bringSubviewToFront:[wbHeader sharedHeader]];
    
    if ([viewStack count] > 1) {
        [[wbHeader sharedHeader] setHeaderBackButton:YES];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scrollToTop:)
                                                 name:@"StatusBarTappedNotification"
                                               object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"StatusBarTappedNotification" object:nil];
}

-(void)launchFromPush //or URl
{
    DCMSG(@"Here I am");
    DCMSG([[wbData sharedData] pushData]);
    
    NSDictionary *dictionary = [[wbData sharedData] pushData];
    DCMSG(dictionary);
    
    if ([dictionary objectForKey:@"id"] == nil) { // url
        if ([dictionary objectForKey:@"video"] != nil) {
            NSString *endpoint = [NSString stringWithFormat:kVIDEOSLUGAPI,[dictionary objectForKey:@"video"]];
            [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:endpoint] response:^(id data){
                DCMSG(data);
                [[wbData sharedData] setSingleVideoData:data];
                [kROOTVC performSelectorOnMainThread:@selector(startSingleVideo:) withObject:nil waitUntilDone:NO];
            }];
        } else if ([dictionary objectForKey:@"user"] != nil) {
            wbUser *user = [[wbData sharedData] getWhambushUserWithUsername:[dictionary objectForKey:@"user"]];
            if (user != nil) {
                DCMSG(user);
                [kROOTVC performSelectorOnMainThread:@selector(startUserPage:) withObject:user waitUntilDone:NO];
            }
        } else if ([dictionary objectForKey:@"mission"] != nil) {
            //
            wbMission *mission = [[wbMission alloc] initWithSlug:[dictionary objectForKey:@"mission"]];
            [kROOTVC performSelectorOnMainThread:@selector(startSingleMission:) withObject:mission waitUntilDone:NO];
        }
    } else { // push notification
        if ([[dictionary valueForKey:@"video"] integerValue] > 0) {
            NSString *endpoint = [NSString stringWithFormat:@"%@%ld/",kVIDEOAPI,(long)[[dictionary valueForKey:@"video"] integerValue]];
            [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:endpoint] response:^(id data){
                DCMSG(data);
                [[wbData sharedData] setSingleVideoData:data];
                [kROOTVC performSelectorOnMainThread:@selector(startSingleVideo:) withObject:nil waitUntilDone:NO];
            }];
        } else if ([[dictionary valueForKey:@"user"] integerValue] > 0) {
            wbUser *user = [[wbData sharedData] getWhambushUserWithId:[[dictionary valueForKey:@"user"] integerValue]];
            if (user != nil) {
                DCMSG(user);
                [kROOTVC performSelectorOnMainThread:@selector(startUserPage:) withObject:user waitUntilDone:NO];
            }
        } else if ([[dictionary valueForKey:@"mission"] integerValue] > 0) {
            // mission
            wbMission *mission = [[wbData sharedData] getMissionWithId:[NSNumber numberWithInteger:[[dictionary valueForKey:@"mission"] integerValue]]];
            [kROOTVC performSelectorOnMainThread:@selector(startSingleMission:) withObject:mission waitUntilDone:NO];
        }

    }
    
    [[wbData sharedData] setLaunchedFromPush:NO];
}

-(void)scrollToTop:(id)sender
{
    if ([[[viewStack lastObject] view] respondsToSelector:@selector(scrollTableTop)]) {
        [[[viewStack lastObject] view] performSelector:@selector(scrollTableTop)];
    } else {
        if ([[[[[viewStack lastObject] view] subviews] firstObject] respondsToSelector:@selector(scrollTableTop)]) {
            [[[[[viewStack lastObject] view] subviews] firstObject] performSelector:@selector(scrollTableTop)];
        }
    }
}

-(void)loginOK
{
    DNSLog(@"Logged");
    [[wbData sharedData] clearUserData];
    if (videoFeedController == nil) {
        //[self startVideoFeed:self];
        [self initViewControllers];
        [self changeViewStack:@"mission"];
    } else {
        [userViewController setUser:[wbUser sharedUser]];
        if ([[[viewStack lastObject] view] respondsToSelector:@selector(forceRefresh)]) {
            [[[viewStack lastObject] view] performSelector:@selector(forceRefresh)];
        }
        if ([videoFeedController.view respondsToSelector:@selector(forceRefresh)]) {
            [videoFeedController.view performSelector:@selector(forceRefresh)];
        }
        if ([missionController.view respondsToSelector:@selector(forceRefresh)]) {
            [missionController.view performSelector:@selector(forceRefresh)];
        }
        if ([[missionController countrySelect].view respondsToSelector:@selector(forceRefresh)]) {
            [[missionController countrySelect].view performSelector:@selector(forceRefresh)];
        }

        if ([tvChannelsController.view respondsToSelector:@selector(forceRefresh)]) {
            [tvChannelsController.view performSelector:@selector(forceRefresh)];
        }
        if ([[[tvViewStack firstObject] view] respondsToSelector:@selector(forceRefresh)]) {
            [[[tvViewStack firstObject] view] performSelector:@selector(forceRefresh)];
        }
        kHIDEWAIT;
    }
    
    [[wbLoginRegisterViewController sharedLRVC] closeAlert];
    [[wbFooter sharedFooter] setNeedsDisplay];

    if ([[[viewStack lastObject] view] respondsToSelector:@selector(userLoggedIn)]) {
        [[[viewStack lastObject] view] performSelector:@selector(userLoggedIn)];
    }
    DCMSG(viewStack);
    
//    [[wbData sharedData] performSelectorInBackground:@selector(getAllFeeds) withObject:nil];
//    [[wbData sharedData] performSelectorInBackground:@selector(getAllMissionFeeds) withObject:nil];
//    [[wbData sharedData] performSelectorInBackground:@selector(getAllActivity) withObject:nil];

}

-(void)askCountry
{
    wbStartViewController *startView = [[wbStartViewController alloc] init];
    [self.view addSubview:startView.view];
}

-(void)initViewControllers
{
    videoFeedController = [[wbVideoFeedViewController alloc] init];
    [videoFeedController setHeaderLogo:@"_wb_"];
    homeViewStack = [[NSMutableArray alloc] init];
    [homeViewStack addObject:videoFeedController];
    
    missionController = [[wbNewMissionViewController alloc] init];
    [missionController setHeaderLogo:@"_wb_"];
    missionViewStack = [[NSMutableArray alloc] init];
    [missionViewStack addObject:missionController];
    
    tvChannelsController = [[wbTvViewController alloc] init];
    [tvChannelsController setOpenChannelWithId:0];
    [tvChannelsController setHeaderLogo:@"_tv_"];
    tvViewStack = [[NSMutableArray alloc] init];
    [tvViewStack addObject:tvChannelsController];
    defaultTvViewController = tvChannelsController;
    
    
    userViewController = [[wbUserViewController alloc] init];
    if (![[wbAPI sharedAPI] is_guest]) {
        [userViewController setUser:[wbUser sharedUser]];
        [userViewController setHeaderLogo:[[wbUser sharedUser] username]];
    }
    userViewStack = [[NSMutableArray alloc] init];
    [userViewStack addObject:userViewController];
}

-(void)setDefaultTvChannel:(NSNumber*)_defaultTvChannel
{
    defaultTvChannel = _defaultTvChannel;
    if ([defaultTvChannel integerValue] > 0) {
        wbTvViewController *newTvChannelsController = [[wbTvViewController alloc] init];
        [newTvChannelsController setOpenChannelWithId:[_defaultTvChannel integerValue]];
        [newTvChannelsController setHeaderLogo:@"_tv_"];
        [tvViewStack addObject:newTvChannelsController];
        tvHomeIndex = [tvViewStack count]-1;
        defaultTvViewController = newTvChannelsController;
    }
    tvHomeIndex = [tvViewStack count]-1;
}

-(void)startNewVideoFeed:(id)sender feed:(NSDictionary*)feed
{
    wbVideoFeedViewController *newVideoFeedController = [[wbVideoFeedViewController alloc] init];
    
    [[wbHeader sharedHeader] setHeaderBackButton:YES];
    [newVideoFeedController setHasBack:YES];
    
    [newVideoFeedController setStartFeed:feed];
    
    [viewStack addObject:newVideoFeedController];
    
    [containerView addSubview:newVideoFeedController.view];
}

-(void)startUserPage:(wbUser*)user
{
    [self startUserPage:nil user:user];
}

-(void)startUserPage:(id)sender user:(wbUser*)user
{
    wbUserViewController *newUserViewController = [[wbUserViewController alloc] init];
    
    [[wbHeader sharedHeader] setHeaderBackButton:YES];
    [newUserViewController setHasBack:YES];
    
    [newUserViewController setUser:user];
    
    [viewStack addObject:newUserViewController];
    
    [containerView addSubview:newUserViewController.view];
}

-(void)startSingleMission:(wbMission*)mission
{
    wbSingleMissionViewController *singleMissionController = [[wbSingleMissionViewController alloc] init];
    [singleMissionController setHeaderLogo:@"_mission_"];
    [[wbHeader sharedHeader] setHeaderBackButton:YES];
    [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
    [singleMissionController setHasBack:YES];
    [singleMissionController setMission:mission];
    
    [viewStack addObject:singleMissionController];
    
    [containerView addSubview:singleMissionController.view];

}

-(void)startSettings:(id)sender
{
    if (settingsController == nil || ![[wbAPI sharedAPI] is_guest]) {
        settingsController = [[wbSettingsViewController alloc] init];
    }
    [settingsController setHeaderLogo:[NSLocalizedString(@"USER_SETTINGS", @"") capitalizedString] ];
    
    if (sender == nil) {
        [[wbHeader sharedHeader] setHeaderBackButton:YES];
        [settingsController setHasBack:YES];
        [viewStack addObject:settingsController];
    }
    [containerView addSubview:settingsController.view];
}

-(void)startSingleVideo:(id)sender
{
    wbSingleVideoViewController *singleVideoController = [[wbSingleVideoViewController alloc] init];
    [singleVideoController setHeaderLogo:@"_wb_"];
    [[wbHeader sharedHeader] setHeaderBackButton:YES];
    [singleVideoController setHasBack:YES];
    
    [viewStack addObject:singleVideoController];
    
    [containerView addSubview:singleVideoController.view];
}

-(void)startTvChannelView:(NSNumber*)channelId
{
    wbTvViewController *tvViewController = [[wbTvViewController alloc] init];
    [tvViewController setHeaderLogo:@"_tv_"];
    [tvViewController setOpenChannelWithId:[channelId integerValue]];
    [viewStack addObject:tvViewController];
    if ([viewStack count] > tvHomeIndex+1) {
        [[wbHeader sharedHeader] setHeaderBackButton:YES];
        [tvViewController setHasBack:YES];
    } else {
        [[wbHeader sharedHeader] setHeaderBackButton:NO];
        [tvViewController setHasBack:NO];
    }
    
    [containerView addSubview:tvViewController.view];
    
}

-(void)startMissionCountrySelect:(id)sender
{
    wbMissionCountrySelectViewController *viewC = [missionController countrySelect];
    
    [viewStack addObject:viewC];
    [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
    [containerView addSubview:viewC.view];
}

-(void)startCamera:(id)sender
{
    if ([[viewStack lastObject] respondsToSelector:@selector(startCamera)] && sender != nil) {
        [[viewStack lastObject] performSelector:@selector(startCamera)];
    } else {
        cameraViewController = nil;
        DCMSG(sender);
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(showLandscape) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            
            [self performSelectorOnMainThread:@selector(startCameraInLocal:) withObject:sender waitUntilDone:NO];
        }];
        [op start];
    }
}

-(void)startCameraInLocal:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
        if (cameraViewController == nil) {
            cameraViewController = [[wbPBJCameraViewController alloc] init];
        }
        [cameraViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        
        [self presentViewController:cameraViewController animated:YES completion:NULL];
    } else {
        wbCameraRollViewController *cameraRollViewController = [[wbCameraRollViewController alloc] init];
        [cameraRollViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [self presentViewController:cameraRollViewController animated:YES completion:NULL];
    }
}

-(void)closeCamera:(id)sender
{
    NSError *error;
    DNSLog(@"%@",[SSKeychain allAccounts]);
    [SSKeychain setAccessibilityType:kSecAttrAccessibleAlways];
    DNSLog(@"%@",[SSKeychain passwordForService:kAPPNAME account:@"token" error:&error]);
    DNSLog(@"%@",error);
    DNSLog(@"%@",kAPPNAME);
    
    [sender dismissViewControllerAnimated:YES completion:^(void){
        [containerView addSubview:[[viewStack lastObject] view]];
        DNSLog(@"%@",kWHAMBUSHTOKEN);

    }];
}



-(void)startCameraRoll:(id)sender
{
    wbCameraRollViewController *cameraRollViewController = [[wbCameraRollViewController alloc] init];
    [cameraRollViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    
    
    if (cameraViewController == nil) {
        [self presentViewController:cameraRollViewController animated:YES completion:NULL];
    } else {
        [cameraViewController presentViewController:cameraRollViewController animated:YES completion:NULL];
    }
}

-(void)startProfilePicGallery:(id)sender
{
    wbProfilePicPickerViewController *profilePicCameraViewController = [[wbProfilePicPickerViewController alloc] init];
    
    [profilePicCameraViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [profilePicCameraViewController setIsCamera:NO];
    [profilePicCameraViewController setParent:sender];
    [self presentViewController:profilePicCameraViewController animated:YES completion:NULL];
}

-(void)startProfilePicCamera:(id)sender
{
    wbProfilePicPickerViewController *profilePicCameraViewController = [[wbProfilePicPickerViewController alloc] init];
    
    [profilePicCameraViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [profilePicCameraViewController setIsCamera:YES];
    [profilePicCameraViewController setParent:sender];
    
    [self presentViewController:profilePicCameraViewController animated:YES completion:NULL];
}

-(void)startUpload:(id)sender
{
    DCMSG(sender);
    [self.view setHidden:YES];

    if ([sender isKindOfClass:[wbCameraRollViewController class]]) {
        [sender dismissViewControllerAnimated:YES completion:^(void){
            [cameraViewController dismissViewControllerAnimated:YES completion:^(void){
                [self.view setHidden:NO];
                wbUploadViewController *uploadViewController = [[wbUploadViewController alloc] init];
                if ([sender isKindOfClass:[NSDictionary class]]) {
                    [uploadViewController setUploadSetupData:sender];
                }
                [[wbHeader sharedHeader] setLogo:@"_upload_"];
                [[wbHeader sharedHeader] setHeaderBackButton:YES];
                [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
                [viewStack addObject:uploadViewController];
                [containerView addSubview:uploadViewController.view];
                [[wbFooter sharedFooter] setEmptyFooter:YES];
                kHIDEWAIT;
                
            }];
        }];
        DNSLog(@"VC Class");
    } else if ([sender isKindOfClass:[wbPBJCameraViewController class]]) {
        [sender dismissViewControllerAnimated:YES completion:^(void){
            [self.view setHidden:NO];
            wbUploadViewController *uploadViewController = [[wbUploadViewController alloc] init];
            if ([sender isKindOfClass:[NSDictionary class]]) {
                [uploadViewController setUploadSetupData:sender];
            }
            [[wbHeader sharedHeader] setLogo:@"_upload_"];
            [[wbHeader sharedHeader] setHeaderBackButton:YES];
            [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
            [viewStack addObject:uploadViewController];
            [containerView addSubview:uploadViewController.view];
            [[wbFooter sharedFooter] setEmptyFooter:YES];
            kHIDEWAIT;
            
        }];
    } else {
        [self.view setHidden:NO];
        wbUploadViewController *uploadViewController = [[wbUploadViewController alloc] init];
        if ([sender isKindOfClass:[NSDictionary class]]) {
            [uploadViewController setUploadSetupData:sender];
        }
        [[wbHeader sharedHeader] setLogo:@"_upload_"];
        [[wbHeader sharedHeader] setHeaderBackButton:YES];
        [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
        [viewStack addObject:uploadViewController];
        [containerView addSubview:uploadViewController.view];
        [[wbFooter sharedFooter] setEmptyFooter:YES];
    }
    
}

-(void)startUploadWithData:(NSDictionary*)data
{
    DCMSG(data);
    [self startUpload:data];
}


-(void)logout:(id)sender //TODO: FIX
{
    kSHOWWAIT;
    [[containerView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    homeViewStack = nil;
    missionViewStack = nil;
    tvViewStack = nil;
    userViewStack = nil;
    
    videoFeedController = nil;
    cameraViewController = nil;
    missionController = nil;
    userViewController = nil;
    tvFeedController = nil;
    
    [[wbAPI sharedAPI] tryLogin];
}


-(void)goHome:(id)sender
{
    BOOL scroll = NO;
    if ([viewStack count] == 1) {
        scroll = YES;
    }
    
    for (NSInteger i = [viewStack count]-1; i > 0; i--) {
        [self goBack:nil];
    }
    if (sender == nil) {
        DCMSG(viewStack);
        [[wbHeader sharedHeader] setHeaderBackButton:NO];
    } else {
        [self changeViewStack:sender];
    }
    
    if ([[[viewStack lastObject] view] respondsToSelector:@selector(scrollTableTop)] && scroll) {
        [[[viewStack lastObject] view] performSelector:@selector(scrollTableTop)];
    }
    kHIDEWAIT;
    
}

-(void)goTvHome:(id)sender
{
    NSAssert(defaultTvViewController != nil, @"defaultTvViewController is null");
    
//    BOOL scroll = NO;
//    if ([viewStack count] == tvHomeIndex) {
//        scroll = YES;
//    }
    
    for (NSInteger i = [viewStack count]-1; i > 0; i--) {
        [self goBack:nil];
    }
    [tvViewStack addObject:defaultTvViewController];

    [self changeViewStack:@"tv"];
    [[wbHeader sharedHeader] setHeaderBackButton:NO];

    
    if ([[[viewStack lastObject] view] respondsToSelector:@selector(scrollTableTop)]/* && scroll*/) {
        [[[viewStack lastObject] view] performSelector:@selector(scrollTableTop)];
    }
    
}

-(void)hideSettings
{
    if ([[wbAPI sharedAPI] is_guest] && settingsController != nil) {
        [settingsController.view removeFromSuperview];
        [[wbHeader sharedHeader] setLogo:[(wbBaseViewController*)[viewStack lastObject] headerLogo]];
        [[(UIViewController*)[viewStack lastObject] view] setNeedsDisplay];
        settingsController = nil;
    }
}

-(void)goBack:(id)sender
{
    [[wbFooter sharedFooter] setEmptyFooter:NO];
    [[[viewStack lastObject] view] removeFromSuperview];
    
    DNSLog(@"Back from %@",[[viewStack lastObject] description]);
    if (![sender isKindOfClass:[wbMissionCountrySelectTableView class]])
    {
        kHIDEWAIT
    }
    //    kHIDEWAIT
    
    [viewStack removeLastObject];
    
    DCMSG(viewStack);
    [[wbHeader sharedHeader] setLogo:[[viewStack lastObject] headerLogo]];
    DNSLog(@"headerlogo: %@",[[viewStack lastObject] headerLogo]);
    
    NSInteger viewStackHome = 1;
    if ([[viewStack lastObject] isKindOfClass:[wbTvViewController class]]) {
        viewStackHome = 2;
    }
    
    if ([viewStack count] > viewStackHome) {
        [[wbHeader sharedHeader] setHeaderBackButton:YES];
    } else {
        [[wbHeader sharedHeader] setHeaderBackButton:NO];
    }
    wbHeader *tmp = [wbHeader sharedHeader];
    [tmp setGreen:YES];
    
    [containerView addSubview:[[viewStack lastObject] view]];
    if ([[viewStack lastObject] isKindOfClass:[wbUserViewController class]]) {
        if ([[[viewStack lastObject] view] respondsToSelector:@selector(refreshTabs)]) {
            [[[viewStack lastObject] view] performSelector:@selector(refreshTabs)];
        }
    }

    if ([[[viewStack lastObject] view] respondsToSelector:@selector(refreshRow)]) {
        [[[viewStack lastObject] view] performSelector:@selector(refreshRow)];
    }
    
    if ([[viewStack lastObject] respondsToSelector:@selector(viewDidAppear:)]) {
        [[viewStack lastObject] performSelector:@selector(viewDidAppear:) withObject:nil];
    }
    //DCMSG([containerView subviews]);
    //DCMSG(viewStack);

}

-(void)changeViewStack:(NSString*)stack
{
    [self hideSettings];
    BOOL noBack = NO;
    [[containerView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
    if ([stack isEqualToString:@"mission"]) {
        [[wbHeader sharedHeader] setLogo:@"_wb_"];
        [[wbHeader sharedHeader] setShowChangeCountryButton:YES];
        [[[wbFooter sharedFooter] homeButton]setSelected:NO];
        [[[wbFooter sharedFooter] missionButton]setSelected:YES];
        [[[wbFooter sharedFooter] tvButton]setSelected:NO];
        [[[wbFooter sharedFooter] userButton]setSelected:NO];
        viewStack = missionViewStack;
    } else if ([stack isEqualToString:@"tv"]) {
        [[wbHeader sharedHeader] setLogo:@"_tv_"];
        [[wbHeader sharedHeader] setShowChangeCountryButton:YES];
        if ([tvViewStack count] <= tvHomeIndex+1) {
            noBack = YES;
        }
        [[[wbFooter sharedFooter] homeButton]setSelected:NO];
        [[[wbFooter sharedFooter] missionButton]setSelected:NO];
        [[[wbFooter sharedFooter] tvButton]setSelected:YES];
        [[[wbFooter sharedFooter] userButton]setSelected:NO];
        viewStack = tvViewStack;
    } else if ([stack isEqualToString:@"user"]) {
        [[wbHeader sharedHeader] setLogo:[[wbUser sharedUser] username]];
        [[[wbFooter sharedFooter] homeButton]setSelected:NO];
        [[[wbFooter sharedFooter] missionButton]setSelected:NO];
        [[[wbFooter sharedFooter] tvButton]setSelected:NO];
        [[[wbFooter sharedFooter] userButton]setSelected:YES];
        viewStack = userViewStack;
    } else {
        [[wbHeader sharedHeader] setLogo:@"_wb_"];
        [[[wbFooter sharedFooter] homeButton]setSelected:YES];
        [[[wbFooter sharedFooter] missionButton]setSelected:NO];
        [[[wbFooter sharedFooter] tvButton]setSelected:NO];
        [[[wbFooter sharedFooter] userButton]setSelected:NO];
        viewStack = homeViewStack;
    }
    if ([viewStack count] > 1 && !noBack) {
        [[wbHeader sharedHeader] setHeaderBackButton:YES];
    } else {
        [[wbHeader sharedHeader] setHeaderBackButton:NO];
    }
    DNSLog(@"%@",[[viewStack lastObject] headerLogo]);
    DNSLog(@"%@",[[wbHeader sharedHeader] logo]);
    [[wbHeader sharedHeader] setLogo:[[viewStack lastObject] headerLogo]];
    [containerView addSubview:[[viewStack lastObject] view]];

}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if ([[viewStack lastObject] isKindOfClass:[wbSingleVideoViewController class]] || [[viewStack lastObject] isKindOfClass:[wbSingleMissionViewController class]]) {
        [[viewStack lastObject] willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL) shouldAutorotate
{
    if ([[viewStack lastObject] isKindOfClass:[wbSingleVideoViewController class]] || [[viewStack lastObject] isKindOfClass:[wbSingleMissionViewController class]]) {
        return [[viewStack lastObject] shouldAutorotate];
        //return YES;
    } else {
        return NO;
    }
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
