//
//  wbRankTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 16/02/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseCell.h"

@interface wbRankTableView : UITableView <UITableViewDataSource, UITableViewDelegate>
{
    UIActivityIndicatorView *ai;
    NSInteger nextPageNumber;
    NSInteger totalNumberOfResults;
    NSInteger numberOfResults;
    NSMutableArray *resultArray;
}
@end
