//
//  wbBaseTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 31/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbFeedSelectView.h"
#import <AVFoundation/AVFoundation.h>

@interface wbBaseTableView : UITableView
{
    NSMutableDictionary* dataArray;
    NSMutableArray *resultArray;
    BOOL refresh;
    BOOL gettingMoreResults;
    NSInteger numberOfResults;
    NSInteger totalNumberOfResults;
    NSString *nextUrl;
    
    wbFeedSelectView *feedSelect;
    UIButton *feedSelectButton;
    float prevContentOffset;
    float origY;
    float deltaY;

    NSString *currentFeedType;
    BOOL onGoing;
}

@property (nonatomic,retain) NSString *endpoint;
@property (nonatomic,retain) NSMutableDictionary *allFeeds;
@property (nonatomic,retain) NSDictionary *currentFeed;
@property (nonatomic) BOOL hideHeader;
@property (nonatomic) BOOL showFeedSelectSearch;

-(void)refresh:(UIRefreshControl *)refreshControl;
-(void)dataReady:(id)data;
-(void)getMoreResults;

@end
