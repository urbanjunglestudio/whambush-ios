//
//  wbNewMissionCell.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/11/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbNewMissionCell : UITableViewCell
{
    wbUser *user;
    CGFloat y;
    UIImageView *checked;
    UIImageView *profilePicture;
    wbButton *username;
    wbLabel *title;
    wbLabel *ends;
    UIImageView *bOrS;
    wbLabel *count;
    
    UIActivityIndicatorView *ai;
    
}
@property (retain,nonatomic) UIImageView *cellBkg;
@property (retain,nonatomic) wbMission *mission;

-(void)resetBkg;

@end
