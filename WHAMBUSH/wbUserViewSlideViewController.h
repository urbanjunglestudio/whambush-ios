//
//  wbUserViewSlideViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbVideoFeedViewController.h"
#import "wbFollowListViewController.h"
#import "wbUserViewSlideTabsView.h"
#import "wbActivityViewController.h"
#import "wbRankViewController.h"

@interface wbUserViewSlideViewController : UIViewController <UIPageViewControllerDataSource,UIPageViewControllerDelegate,UIScrollViewDelegate>
{
    UIPageViewController *slidePageViewController;
    NSArray *viewControllers;
    NSInteger viewControllerIndex;
    
    wbUserViewSlideTabsView *tabs;
    
    NSInteger numberOfViews;
}

@property (nonatomic,retain) wbUser *user;
@property (nonatomic,retain) UIView *touchView;

-(void)update;

@end
