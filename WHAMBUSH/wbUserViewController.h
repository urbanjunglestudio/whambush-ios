//
//  wbUserViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 21/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseViewController.h"
#import "wbNewUserView.h"

@interface wbUserViewController : wbBaseViewController

@property (nonatomic,retain) wbUser *user;

@end
