#define UIColorFromHexRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define kTOPUICOLOR [UIColor colorWithRed:49.0/255.0 green:51.0/255.0 blue:63.0/255.0 alpha:1.0]
#define kTOPUICOLORAlpha [UIColor colorWithRed:49.0/255.0 green:51.0/255.0 blue:63.0/255.0 alpha:0.75]
#define kBKGUICOLOR [UIColor colorWithRed:41.0/255.0 green:43.0/255.0 blue:53.0/255.0 alpha:1.0]
//#define kBKGUICOLOR UIColorFromHexRGB(0x292b35)


#define kBKGUICOLORAlpha [UIColor colorWithRed:41.0/255.0 green:43.0/255.0 blue:53.0/255.0 alpha:0.95]
#define kBOTTOMUICOLOR [UIColor colorWithRed:67.0/255.0 green:73.0/255.0 blue:87.0/255.0 alpha:1.0]
#define kBOTTOMUICOLORAlpha [UIColor colorWithRed:67.0/255.0 green:73.0/255.0 blue:87.0/255.0 alpha:0.95]
#define kBUTTONUICOLOR [UIColor colorWithRed:179.0/255.0 green:201.0/255.0 blue:70.0/255.0 alpha:1.0]
#define kGREENUICOLOR [UIColor colorWithRed:173.0/255.0 green:194.0/255.0 blue:52.0/255.0 alpha:1.0]
#define kREDUICOLOR [UIColor colorWithRed:204.0/255.0 green:85.0/255.0 blue:74.0/255.0 alpha:1.0]
#define kYELLOWUICOLOR [UIColor colorWithRed:245.0/255.0 green:212.0/255.0 blue:42.0/255.0 alpha:1.0]

#define kUSERUICOLOR [UIColor colorWithRed:40.0/255.0 green:53.0/255.0 blue:68.0/255.0 alpha:1.0]

#define kWHITEUICOLOR [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0]
#define kLIGHTGRAYUICOLOR [UIColor colorWithRed:0.67 green:0.67 blue:0.67 alpha:1.0]
#define kNOTSOLIGHTGRAYUICOLOR [UIColor colorWithRed:0.4 green:0.4 blue:0.4 alpha:1.0]
#define kBLACKUICOLOR [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0]
#define kTRANSPARENTUICOLOR [UIColor clearColor]

#define kTITLEUICOLOR kWHITEUICOLOR
#define kSEPRATORUICOLOR [UIColor colorWithRed:31.0/255.0 green:33.0/255.0 blue:40.0/255.0 alpha:1.0]
#define kSEPRATORUICOLORAlpha [UIColor colorWithRed:31.0/255.0 green:33.0/255.0 blue:40.0/255.0 alpha:0.95]

#define kTESTBOX(BOX)  CGContextRef context__ = UIGraphicsGetCurrentContext();\
    CGContextSetRGBFillColor(context__, 1,1,1,1);\
    CGContextFillRect(context__, BOX);

//#define kToken [[wbAPI sharedAPI] token]
//#define kUsername  [[wbUser sharedUser] username]
//#define kEmail  [[wbUser sharedUser] email]

// Used to specify the application used in accessing the Keychain.
#define kAPPNAME [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]

#define kTXTBOXW 250
#define kTXTBOXH 35

#define kSTATUSBARHEIGHT [GUI statusBarHeight]
#define kTOPBARHEIGHT [GUI headerBarHeight]
#define kBOTTOMBARHEIGHT [GUI footerBarHeight]

#define kFONTLeague(_size) [UIFont fontWithName:@"LeagueGothic-Regular" size:_size]
#define kFONT(_size) [UIFont fontWithName:@"AkzidenzGroteskBQ-MdCnd" size:_size]
//#define kFONTHelvetica(_size) [UIFont fontWithName:@"HelveticaNeue-Light" size:_size]
//#define kFONTHelveticaReg(_size) [UIFont fontWithName:@"HelveticaNeue" size:_size]
//#define kFONTHelveticaBold(_size) [UIFont fontWithName:@"HelveticaNeue-Bold" size:_size]
#define kFONTHelvetica(_size) [UIFont fontWithName:@"SFUIDisplay-Thin" size:_size]
#define kFONTHelveticaReg(_size) [UIFont fontWithName:@"SFUIDisplay-Regular" size:_size]
#define kFONTHelveticaBold(_size) [UIFont fontWithName:@"SFUIDisplay-Bold" size:_size]

//#define printCGRect(frame) DDLogVerbose(@"Rect -> %f,%f,%f,%f ",frame.origin.x,frame.origin.y,frame.size.width,frame.size.height);

//#define DPRINTCLASS NSLog(@"%@",[self description]);
#define DPRINTCLASS

#define kVIDEOMAXTIME 30
#define kVIDEOMAXTIMEf 30.00


#define kROOTVC [[wbAPI sharedAPI] rootViewController]

#define iOSv [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue]

#define iOS7 (iOSv >= 7) 
#define iOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

#define kVZAARAPI @"https://vzaar.com/api"
#define kBITRATE @"1024"
#define kVIDEOHEIGHT 480

//#define kWHAMBUSHTOKEN [SSKeychain passwordForService:kAPPNAME account:@"token"]
//#define kVZAARSECRET [SSKeychain passwordForService:kAPPNAME account:@"vzaar_secret"]
//#define kVZAARTOKEN [SSKeychain passwordForService:kAPPNAME account:@"vzaar_key"]

#define kWHAMBUSHTOKEN [[wbAPI sharedAPI] token]
#define kVZAARSECRET [[wbAPI sharedAPI] vzaar_secret]
#define kVZAARTOKEN [[wbAPI sharedAPI] vzaar_key]

#define kSELECTEDIMAGEHEIGHT 2

//#define kSHOWTOUCH

#define kFOLLOWLINEH 35
#define kTVCELLH 128
#define kDOTSIZE 10

#ifdef PRODUCTION
    #define kGOOGLEANALYTICS @"UA-49596640-5"
    #define kWHAMBUSHAPIVERSION @"2.1"
    #define kWHAMBUSHAPI @"https://api.whambush.com"
    #define kWHAMBUSHAPIURL [NSString stringWithFormat:@"%@/v%@",kWHAMBUSHAPI,kWHAMBUSHAPIVERSION]
    #define kWHAMBUSHDOTNET @"whambush.com"
    #define kRESETPASSWORDLINK @"LOGIN_RESET_PASSWORD_LINK_PRODUCTION"
    #define kRESENDACTIVATIONLINK @"LOGIN_RESEND_ACTIVATION_LINK_PRODUCTION"
    #define kTERMSCONDLINK @"REGISTER_TERMS_CONDITIONS_LINK_PRODUCTION"
    #define kPROFILEPICPATH @"https://api.whambush.com/media/%@"
    #define kCOUNTRYPICPATH @"%@"
    #define kZZBANNERPATH @"https://api.whambush.com/static/img/country_flags/ZZ_banner.jpg"
    #define kFAQPAGELINK @"LOGIN_FAQ_LINK_PRODUCTION"
    #define DNSLog DDLogVerbose
    #define kFLURRYKEY @"87PXD7QXGXX77FR8HFDW"
    #undef DEBUG
#endif

#ifdef DEBUG
    #define kGOOGLEANALYTICS @"UA-49596640-3"
    #define kWHAMBUSHAPIVERSION @"2.1"
    #define kWHAMBUSHAPI @"http://api.whambush.net"
    #define kWHAMBUSHAPIURL [NSString stringWithFormat:@"%@/v%@",kWHAMBUSHAPI,kWHAMBUSHAPIVERSION]
    #define kWHAMBUSHDOTNET @"whambush.net"
    #define kRESETPASSWORDLINK @"LOGIN_RESET_PASSWORD_LINK"
    #define kRESENDACTIVATIONLINK @"LOGIN_RESEND_ACTIVATION_LINK"
    #define kTERMSCONDLINK @"REGISTER_TERMS_CONDITIONS_LINK"
    #define kPROFILEPICPATH @"%@"
    #define kCOUNTRYPICPATH @"%@"
    #define kZZBANNERPATH @"http://api.whambush.net/static/img/country_flags/ZZ_banner.jpg"
    #define kFAQPAGELINK @"LOGIN_FAQ_LINK"
    #define DNSLog NSLog
    #define kFLURRYKEY @"87PXD7QXGXX77FR8HFDW"
    #define DEBUGMSG
#endif

#define DMSG DNSLog(@"-> %s:%d", __FILE__, __LINE__);
#define DCMSG(CL) DNSLog(@"-> %@  -> %s:%d",CL, __FILE__, __LINE__);

//#define kGAMEAPI @"http://178.62.88.135/1/code/"
#define kGAMEAPI @"http://tofuhead.eu/1/code/"

#define kSHOWWAIT [[wbWaitView sharedWait] show];
#define kSHOWWAITLAND [[wbWaitView sharedWait] showLandscape];

#define kSHOWWAITSELF 
//#define kHIDEWAIT [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(hide) withObject:nil waitUntilDone:NO];
#define kHIDEWAIT [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(hide) withObject:nil waitUntilDone:NO];DNSLog(@"hide wait -> %s:%d", __FILE__, __LINE__);

#define kRESETGUESTID @"reset_id"
#define kRESETALL @"reset_all"
#define kRESETCOUNTRY @"reset_country"
#define kFORCEDEBUG @"force_debug"

// TODO: API ENDPOINTS
#define kAUTHENTICATE @"login/"
#define kFEEDSAPI [NSString stringWithFormat:@"feeds/?version=%@",kWHAMBUSHAPIVERSION]
#define kACTIVEMISSIONAPI @"missions/"
#define kOLDMISSIONAPI @"missions/?type=old"
#define kACTIVITYAPI @"activities/"
#define kACTIVITYREADAPI @"activities/read/"
#define kVIDEOAPI @"videos/"
#define kCOMMENTAPI @"comments/"
#define kCHANNELSAPI @"channels/"
#define kCOUNTRIESAPI @"countries/"
#define kUSERSEARCHAPI @"search/users/?query=%@&fields=username"
#define kMISSIONSLUGAPI @"missions/slug/%@/"
#define kVIDEOSLUGAPI @"videos/slug/%@/"
#define kMISSIONVIDEOSAPI @"search/videos/?mission=%@&order=rank"
