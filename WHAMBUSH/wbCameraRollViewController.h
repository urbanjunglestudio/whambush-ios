//
//  wbCameraRollViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/26/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbDefines.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>
#import "wbAPI.h"

@interface wbCameraRollViewController : UIImagePickerController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@end
