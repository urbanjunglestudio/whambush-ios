//
//  wbSettingsTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 12/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbSettingsTableView.h"

@implementation wbSettingsTableView

@synthesize selectedCountry;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        [self setHidden:NO];
        
        UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 2*kFOLLOWLINEH)];
        [footer setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setTableFooterView:footer];
        
        if (![[wbAPI sharedAPI] is_guest]) {
            
            selectBirthdayView = [[wbBirthdaySelectView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            [selectBirthdayView setBirthday:[[wbUser sharedUser] birthday]];
            [selectBirthdayView setDelegate:self];
            
            pickCountry = [[wbCountryPickerView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            [pickCountry setDelegate:self];
            [pickCountry setSelectedCountry:[[wbUser sharedUser] countryDictionary]];
            
            insertTxtView = [[wbInsertTextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
            [insertTxtView setDelegate:self];
            [insertTxtView setMaxCharCount:90];
            [insertTxtView setMaxLinesCount:1];
            [insertTxtView setHeaderText:NSLocalizedString(@"SETTINGS_EMAIL",@"")];
            [[wbUser sharedUser] updateUserData];
        }
        
    }
    DPRINTCLASS;
    return self;
}

#define kNUMSECTIONS 4

#define kSETTINGS 0
//#define kNOTIFICATIONS 1
#define kSUPPORT 1
#define kSOME 2

#define kALLOWEMAIL

#define kACCOUNTEMAIL 0
#define kACCOUNTBIRTHDAY 1
#define kACCOUNTCOUNTRY 2
#define kACCOUNTPASSWORD 3

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    if ([[wbUser sharedUser] userReady] || [[wbAPI sharedAPI] is_guest]) {
        // Drawing code
        kHIDEWAIT;
        if (insertTxtView != nil) {
            [insertTxtView setInitialText:[[wbUser sharedUser] email]];
            [self addSubview:insertTxtView];
        }
        if (selectBirthdayView != nil) {
            [selectBirthdayView setBirthday:[[wbUser sharedUser] birthday]];
            [self addSubview:selectBirthdayView];
            [self bringSubviewToFront:selectBirthdayView];
        }
        if (pickCountry != nil) {
            [pickCountry setSelectedCountry:[[wbUser sharedUser] countryDictionary]];
            [self addSubview:pickCountry];
            [self bringSubviewToFront:pickCountry];
        }
        [self inactiveEmailPrompt];
        [self reloadData];
    } else {
        kSHOWWAIT;
        [self performSelector:@selector(setNeedsDisplay) withObject:nil afterDelay:1];
    }
}

-(void)hideKB
{
    [insertTxtView hideTextView];
    [selectBirthdayView hide];
    [pickCountry hide];
}

-(UIView*)accountRow:(NSInteger)row
{
    wbLabel *line = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-(kFOLLOWLINEH/2+6), kFOLLOWLINEH)];
    [line setBackgroundColor:kTOPUICOLOR];
    [line setTextColor:kNOTSOLIGHTGRAYUICOLOR];
    [line setFont:kFONTHelvetica(15)];
    [line setEdgeInsets:UIEdgeInsetsMake(2, 12, 2, 0)];

    NSMutableAttributedString *beginningOfLine;
    NSMutableAttributedString *endOfLine;
    NSMutableAttributedString *stringLine;
    
    UIImage *lineImg;
    
    switch (row) {
        case kACCOUNTEMAIL: //email
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_EMAIL", @"")];
            [beginningOfLine addAttribute:NSForegroundColorAttributeName value:kNOTSOLIGHTGRAYUICOLOR range:(NSRange){0,[beginningOfLine length]}];
            
            endOfLine = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[[wbUser sharedUser] email]]];
            if ([[[wbUser sharedUser] activation_state] isEqualToString:@"NEW"]) {
                [endOfLine addAttribute:NSForegroundColorAttributeName value:kREDUICOLOR range:(NSRange){0,[endOfLine length]}];
            } else {
                [endOfLine addAttribute:NSForegroundColorAttributeName value:kWHITEUICOLOR range:(NSRange){0,[endOfLine length]}];
            }
            
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            [stringLine appendAttributedString:endOfLine];
            
#ifdef kALLOWEMAIL
            lineImg = [UIImage ch_imageNamed:@"settings_edit.png"];
#endif
            break;
        case kACCOUNTBIRTHDAY: //birthday
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_BIRTHDAY", @"")];
            [beginningOfLine addAttribute:NSForegroundColorAttributeName value:kNOTSOLIGHTGRAYUICOLOR range:(NSRange){0,[beginningOfLine length]}];
            if ([[[wbUser sharedUser] birthdayString] length] > 0) {
                endOfLine = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",[[wbUser sharedUser] birthdayString]]];
            } else {
                endOfLine = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",NSLocalizedString(@"SETTINGS_BIRTHDAY_EMPTY", @"")]];
            }
            [endOfLine addAttribute:NSForegroundColorAttributeName value:kWHITEUICOLOR range:(NSRange){0,[endOfLine length]}];
            
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            [stringLine appendAttributedString:endOfLine];
            
            lineImg = [UIImage ch_imageNamed:@"settings_edit.png"];
            
            break;
        case kACCOUNTPASSWORD: // password
            
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_PASSWORD_CHANGE", @"")];
 
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];

            lineImg = [UIImage ch_imageNamed:@"settings_link.png"];

            break;
        case kACCOUNTCOUNTRY:
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_COUNTRY", @"")];
            [beginningOfLine addAttribute:NSForegroundColorAttributeName value:kNOTSOLIGHTGRAYUICOLOR range:(NSRange){0,[beginningOfLine length]}];
            
             endOfLine = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",NSLocalizedString([[[wbUser sharedUser] countryDictionary] objectForKey:@"name"],@"")]];
            [endOfLine addAttribute:NSForegroundColorAttributeName value:kWHITEUICOLOR range:(NSRange){0,[endOfLine length]}];
            
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            [stringLine appendAttributedString:endOfLine];
            
            lineImg = [UIImage ch_imageNamed:@"settings_edit.png"];
            
            break;
        default:
            break;
    }

    [line addSubview:[self setImage:lineImg]];
    
    [line setAttributedText:stringLine];

    return line;
}

-(UIView*)notificationRow:(NSInteger)row
{
    return nil;
}

-(UIView*)supportRow:(NSInteger)row
{
    wbLabel *line = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-(kFOLLOWLINEH/2+6), kFOLLOWLINEH)];
    [line setBackgroundColor:kTRANSPARENTUICOLOR];
    [line setTextColor:kNOTSOLIGHTGRAYUICOLOR];
    [line setFont:kFONTHelvetica(15)];
    [line setEdgeInsets:UIEdgeInsetsMake(2, 12, 2, 0)];
    
    NSMutableAttributedString *beginningOfLine;
    NSMutableAttributedString *stringLine;
    
    UIImage *lineImg;
    
    switch (row) {
        case 0: //FAQ
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_SUPPORT_FAQ", @"")];

            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            
            lineImg = [UIImage ch_imageNamed:@"settings_link.png"];
            
            break;
        case 1: //SUPPORT MAIL
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_SUPPORT_LABEL", @"")];
            
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            
            lineImg = [UIImage ch_imageNamed:@"settings_mail.png"];
            
            break;
        default:
            break;
    }
    
    [line addSubview:[self setImage:lineImg]];
    
    [line setAttributedText:stringLine];
    
    return line;
}

-(UIView*)someRow:(NSInteger)row
{
    //FACEBOOK
    //INSTAGRAM
    //TWITTER
    wbLabel *line = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-(kFOLLOWLINEH/2+6), kFOLLOWLINEH)];
    [line setBackgroundColor:kTRANSPARENTUICOLOR];
    [line setTextColor:kNOTSOLIGHTGRAYUICOLOR];
    [line setFont:kFONTHelvetica(15)];
    [line setEdgeInsets:UIEdgeInsetsMake(2, 12, 2, 0)];
    
    NSMutableAttributedString *beginningOfLine;
    NSMutableAttributedString *stringLine;
    
    UIImage *lineImg;
    
    switch (row) {
        case 0: //FACEBOOK
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_SOME_FACEBOOK", @"")];
 
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            
            lineImg = [UIImage ch_imageNamed:@"settings_link.png"];
            
            break;
        case 1: //INSTAGRAM
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_SOME_INSTAGRAM", @"")];
            
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            
            lineImg = [UIImage ch_imageNamed:@"settings_link.png"];
            
            break;
        case 2: //TWITTER
            beginningOfLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"SETTINGS_SOME_TWITTER", @"")];
            
            stringLine = [[NSMutableAttributedString alloc] initWithAttributedString:beginningOfLine];
            
            lineImg = [UIImage ch_imageNamed:@"settings_link.png"];
            
            break;
        default:
            break;
    }
    
    [line addSubview:[self setImage:lineImg]];
    
    [line setAttributedText:stringLine];
    
    return line;
}

-(UIView*)logoutRow
{
    wbRoundedButton  *logoutButton = [wbRoundedButton buttonWithType:UIButtonTypeSystem];
    [logoutButton setButtonColor:kREDUICOLOR];
    [logoutButton setFilled:NO];
    [logoutButton setHelvetica:NO];
    [logoutButton setShowsTouchWhenHighlighted:YES];
    [logoutButton setTitle:NSLocalizedString(@"GENERAL_LOGOUT",@"") forState:UIControlStateNormal];
    [logoutButton sizeToFit];
    [logoutButton setCenter:CGPointMake(self.frame.size.width/2, logoutButton.center.y)];
    [logoutButton addTarget:self action:@selector(logout:) forControlEvents:UIControlEventTouchUpInside];
    return logoutButton;
}


-(UIImageView*)setImage:(UIImage *)_image
{
    
    if (_image != nil) {
        
        float width = kFOLLOWLINEH/2;
        float height = kFOLLOWLINEH/2;
        
        if (_image.size.height > _image.size.width) {
            if (height > _image.size.height) {
                height = _image.size.height;
            }
            width  = (_image.size.width)/(_image.size.height/height);
        } else {
            if (width > _image.size.width) {
                width = _image.size.width;
            }
            height  = (_image.size.height)/(_image.size.width/width);
        }
        
        UIImageView *buttonImageView = [[UIImageView alloc] init];
        [buttonImageView setImage:_image];
        [buttonImageView setFrame:CGRectMake(self.frame.size.width-(width+6), 0, width,height)];
        [buttonImageView setCenter:CGPointMake(buttonImageView.center.x, kFOLLOWLINEH/2)];
        
        return buttonImageView;
    } else {
        return nil;
    }
}

/////

-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([[wbAPI sharedAPI] is_guest] && section == kSETTINGS) {
        return nil;
    }

    wbLabel *header = [[wbLabel alloc] init];
    [header setBackgroundColor:kBKGUICOLOR];
    [header setTextColor:kWHITEUICOLOR];
    [header setFont:kFONT(24)];
    [header setEdgeInsets:UIEdgeInsetsMake(12, 12, 0, 0)];
    
    if (section == kSETTINGS) {
        [header setText:[NSLocalizedString(@"SETTINGS_ACCOUNT", @"") uppercaseString]];
//    } else if (section == kNOTIFICATIONS) {
//        [header setText:@"NOTIFICATIONS"];
    } else if (section == kSUPPORT) {
        [header setText:[NSLocalizedString(@"SETTINGS_SUPPORT", @"") uppercaseString]];
    } else if (section == kSOME) {
        [header setText:[NSLocalizedString(@"SETTINGS_SOME", @"") uppercaseString]];
    } else {
        [header setText:@""];
    }
    [header sizeToFit];

    return header;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == kSETTINGS) {
        if ([[wbAPI sharedAPI] is_guest]) {
            return 0;
        } else {
            return 4;
        }
//    } else if (section == kNOTIFICATIONS) {
//        return 0;
    } else if (section == kSUPPORT) {
        return 2;
    } else if (section == kSOME) {
        return 3;
    } else {
        if ([[wbAPI sharedAPI] is_guest]) {
            return 0;
        } else {
            return 1;
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return kNUMSECTIONS;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([[wbAPI sharedAPI] is_guest] && section == kSETTINGS) {
        return 0;
    } else {
        return (float)kFOLLOWLINEH+10;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[wbAPI sharedAPI] is_guest] && [indexPath section] == kSETTINGS) {
        return 0;
    } else {
        return kFOLLOWLINEH+1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *identifier = [NSString stringWithFormat:@"%ld%ld",(long)[indexPath row],(long)[indexPath section]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];

    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSeparatorInset:UIEdgeInsetsZero];
        [cell setBackgroundColor:kTOPUICOLOR];
        [cell setClipsToBounds:NO];
    }
    
    
    if ([indexPath section] == kSETTINGS) {
        if (![[wbAPI sharedAPI] is_guest]) {
            [cell addSubview:[self accountRow:[indexPath row]]];
        }
    } else if ([indexPath section] == kSUPPORT) {
        [cell addSubview:[self supportRow:[indexPath row]]];
    } else if ([indexPath section] == kSOME) {
        [cell addSubview:[self someRow:[indexPath row]]];
    } else {
        [cell addSubview:[self logoutRow]];
        [cell setBackgroundColor:kTRANSPARENTUICOLOR];
    }
    
    [self bringSubviewToFront:selectBirthdayView];
    [self bringSubviewToFront:pickCountry];
    [self bringSubviewToFront:insertTxtView];
    
    [self setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];

    NSArray *subs = [cell subviews];
    DNSLog(@"sc: %lu",(long)[subs count]);
    DNSLog(@"%@",subs);
    DNSLog(@"%@",cell);

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch ([indexPath section]) {
        case 0:
            switch ([indexPath row]) {
                case kACCOUNTEMAIL: //email
                    #ifdef kALLOWEMAIL
                        [insertTxtView showTextView];
                    #endif
                    break;
                case kACCOUNTBIRTHDAY: //birthday
                    [selectBirthdayView show];
                    break;
                case kACCOUNTPASSWORD: //password
                    [self openLink:NSLocalizedString(@"SETTINGS_PASSWORD_CHANGE_LINK", @"")];
                    break;
                case kACCOUNTCOUNTRY: //country
                    [pickCountry show];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch ([indexPath row]) {
                case 0:
                    [self openLink:NSLocalizedString(@"SETTINGS_SUPPORT_FAQ_LINK", @"")];
                    break;
                case 1:
                    [self sendSupportMail];
                    break;
                default:
                    break;
            }
            break;
        case 2:
            switch ([indexPath row]) {
                case 0:
                    [self openLink:NSLocalizedString(@"SETTINGS_SOME_FACEBOOK_FI", @"")];
                    break;
                case 1:
                    [self openLink:NSLocalizedString(@"SETTINGS_SOME_INSTAGRAM_FI", @"")];
                    break;
                case 2:
                    [self openLink:NSLocalizedString(@"SETTINGS_SOME_TWITTER_FI", @"")];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
    
}

-(void)openLink:(NSString*)link
{
    linkToBeOpen = [NSURL URLWithString:link];
    NSString *string = NSLocalizedString(@"GENERAL_WARNING_TITLE", @"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string
                                                    message:NSLocalizedString(@"SETTINGS_CONFIRM_BROWSER", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"GENERAL_CANCEL",@"")
                                          otherButtonTitles:NSLocalizedString(@"GENERAL_OK",@""),nil];
    [alert setTag:0];
    [alert show];
}


-(void)changePassword
{
    NSString *string = NSLocalizedString(@"GENERAL_WARNING_TITLE", @"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string
                                                    message:NSLocalizedString(@"SETTINGS_PASSWORD_CHANGE_CONFIRM", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"GENERAL_CANCEL",@"")
                                          otherButtonTitles:NSLocalizedString(@"GENERAL_OK",@""),nil];
    [alert setTag:1];
    [alert show];
}

-(void)logout:(id)sender
{
    NSString *string = NSLocalizedString(@"GENERAL_WARNING_TITLE", @"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string
                                                    message:NSLocalizedString(@"SETTINGS_CONFIRM_LOGOUT", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"GENERAL_CANCEL",@"")
                                          otherButtonTitles:NSLocalizedString(@"GENERAL_OK",@""),nil];
    [alert setTag:2];
    [alert show];
    
}

-(void)inactiveEmailPrompt
{
    if ([[[wbUser sharedUser] activation_state] isEqualToString:@"NEW"]) {
        NSString *string = NSLocalizedString(@"GENERAL_WARNING_TITLE", @"");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string
                                                        message:[NSString stringWithFormat:NSLocalizedString(@"SETTINGS_EMAIL_NOT_ACTIVATED", @""),[[wbUser sharedUser] email]]
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"SETTINGS_EMAIL_RESEND_ACTIVATION",@"")
                                              otherButtonTitles:NSLocalizedString(@"GENERAL_OK",@""),nil];
        [alert setTag:3];
        [alert show];
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView tag] == 0) {
        if (buttonIndex == 1) {
            [[UIApplication sharedApplication]  openURL:linkToBeOpen];
        }
    }
    if ([alertView tag] == 1) {

    }
    if ([alertView tag] == 2) {
        if (buttonIndex == 1) {
            [[wbAPI sharedAPI] logout];
            [kROOTVC performSelector:@selector(logout:) withObject:NULL];
        }
    }
    if ([alertView tag] == 3) {
        if (buttonIndex == 0) {
            [[UIApplication sharedApplication]  openURL:[NSURL URLWithString:NSLocalizedString(@"SETTINGS_RESEND_ACTIVATION_LINK", @"")]];
        }
    }
}

//////////////// mail stuff

-(void)sendSupportMail
{
    if ([MFMailComposeViewController canSendMail]) {
        mail = [[MFMailComposeViewController alloc] init];
        [mail setMailComposeDelegate:self];
        [mail setSubject:NSLocalizedString(@"SETTINGS_SUPPORT_MAIL_SUBJECT", @"")];
        [mail setMessageBody:[[wbData sharedData] supportMessage] isHTML:NO];
        [mail setToRecipients:@[NSLocalizedString(@"SETTINGS_SUPPORT_MAIL", @"")]];
        
        [kROOTVC presentViewController:mail animated:YES completion:NULL];
    } else {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"SETTINGS_SUPPORT_MAIL_FAIL",@"")];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result) {
        case MFMailComposeResultSent:
            [[wbAPI sharedAPI] showNoteWithTxt:NSLocalizedString(@"SETTINGS_SUPPORT_MAIL_DONE",@"")];
           break;
        case MFMailComposeResultSaved:
            DNSLog(@"draft saved");
            break;
        case MFMailComposeResultCancelled:
            DNSLog(@"mail cancel");
            break;
        case MFMailComposeResultFailed:
            [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"SETTINGS_SUPPORT_MAIL_FAIL",@"")];
            break;
        default:
            [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"SETTINGS_SUPPORT_MAIL_FAIL",@"")];
            break;
    }
    
    [kROOTVC performSelector:@selector(closeCamera:) withObject:mail];
}

-(void)updateBirthday:(NSString*)bday
{
    DCMSG(bday);
    if (![bday isEqualToString:[[wbUser sharedUser] birthday]]) {
        DMSG;
        NSDictionary *updateUserDetails = [NSDictionary dictionaryWithObjectsAndKeys:bday,@"birthday", nil];
        [[wbAPI sharedAPI] updateUserDetails:updateUserDetails];
        //[[wbUser sharedUser] updateUserData];
        [[wbUser sharedUser] setNewBirthday:bday];
        [selectBirthdayView setBirthday:bday];
        DMSG;
        [self beginUpdates];
        [self reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self endUpdates];
//        [self reloadData];
    }
    [selectBirthdayView hide];
    kHIDEWAIT
}

-(void)newText:(NSString*)text  //delegate method for insetTxtView
{
    DCMSG(text);
    if (![text isEqualToString:[insertTxtView initialText]]) {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            [self performSelectorOnMainThread:@selector(updateTxt:) withObject:text waitUntilDone:YES];
        }];
        
        [op start];
    }
}

-(void)updateTxt:(NSString*)text
{
    if ([[wbAPI sharedAPI] validateEmailWithString:text]) {
        NSDictionary *updateUserDetails = [NSDictionary dictionaryWithObjectsAndKeys:text,@"email", nil];
        [[wbAPI sharedAPI] updateUserEmail:updateUserDetails delegate:self];
    } else {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"REGISTER_ERROR_BROKEN_EMAIL",@"")];
        [insertTxtView showTextView];
        kHIDEWAIT
    }
}

-(void)emailSetDone:(id)sender
{
    DCMSG(sender);
    
    [insertTxtView setInitialText:[[wbUser sharedUser] email]];
    
    kHIDEWAIT
    [[wbAPI sharedAPI] showNoteWithTxt:[NSString stringWithFormat:NSLocalizedString(@"SETTINGS_EMAIL_CHANGE_DONE", @""),[[wbUser sharedUser] email]]];
    
    [self beginUpdates];
    [self reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:kACCOUNTEMAIL inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self endUpdates];
}

-(void)setSelectedCountry:(NSDictionary *)_selectedCountry
{
    selectedCountry = _selectedCountry;
    [[wbUser sharedUser] setCountryDictionary:_selectedCountry];
    DCMSG(_selectedCountry);
    NSDictionary *updateUserDetails = [NSDictionary dictionaryWithObjectsAndKeys:[selectedCountry objectForKey:@"country"],@"country", nil];
    [[wbAPI sharedAPI] updateUserDetails:updateUserDetails];

    [[wbAPI sharedAPI] setCurrentCountry:selectedCountry];

    [self beginUpdates];
    [self reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:kACCOUNTCOUNTRY inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self endUpdates];
    
    [[wbData sharedData] performSelectorInBackground:@selector(getAllMissionFeeds) withObject:nil];
    [kROOTVC performSelector:@selector(loginOK)];
    
}

@end
