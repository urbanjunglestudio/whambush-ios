//
//  UIImage+AutoCheck.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30.05.15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "UIImage+AutoCheck.h"

@implementation UIImage (AutoCheck)

+(UIImage *)ch_imageNamed:(NSString *)name
{
    UIImage *tmp = [self imageNamed:name];
    NSAssert(tmp != nil, @"image file missing");
    return tmp;
}


@end
