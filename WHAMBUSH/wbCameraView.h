//
//  wbCameraView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/17/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbAPI.h"
#import "wbDefines.h"
#import "wbBaseView.h"
#import "wbTimerSubView.h"
#import "wbRoundedButton.h"
#import "AssetsLibrary/AssetsLibrary.h"
#import "wbVideoPreviewView.h"
#import "wbPortraitOverlayView.h"

@interface wbCameraView : wbBaseView <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    float count;
    UIViewController *controller;
    float oldCount;
    UIView *toolBar;
    UIImage* galleryImg;
    UIButton *undoButton;
    NSMutableArray *clipTimes;
    wbVideoPreviewView *preview;
    float X;
    float Y;
    //UIImageView* toggleCameraImgView;
    UIImageView *cameraRollImgView;
    UIImageView *undoButtonImgView;
    UIButton *toggleRecordButton;
    
    wbPortraitOverlayView *blurView;
}

@property (nonatomic,retain) wbRoundedButton *doneButton;
@property (nonatomic,retain) UIButton *previewButton;
@property (nonatomic,retain) UIButton *backButton;
@property (nonatomic,retain) UIButton *cameraRollButton;
@property (nonatomic,retain) UIButton *cameraButton;
@property (nonatomic,retain) UIButton *toggleCameraButton;
@property (nonatomic,retain) UIImageView* toggleCameraImgView;
@property (nonatomic,retain) UILabel *helpLabel;
@property (nonatomic,retain) wbTimerSubView *timerView;
@property (nonatomic,retain) NSArray *missionData;
@property BOOL recording;
@property BOOL firstVideoDone;
@property (nonatomic,retain) NSURL *fileUrl;
@property (nonatomic,retain) NSURL *previewUrl;

-(void)toggleCamera;
-(void)doTimer;
- (id)initWithFrame:(CGRect)frame controller:(UIViewController*)_controller;
-(void)resetView;

@end
