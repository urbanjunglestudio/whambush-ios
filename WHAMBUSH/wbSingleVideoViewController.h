//
//  wbSingleVideoViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 21/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbSingleVideoView.h"
#import "wbData.h"
#import "wbBaseViewController.h"
#import "wbVideoPlayerViewController.h"
@interface wbSingleVideoViewController : wbBaseViewController
{
    wbVideoPlayerViewController *videoPlayerController;
    wbSingleVideoView *singleVideoView;
    BOOL statusBarHidden;
}

@end
