//
//  wbFooter.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 04/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbFooter.h"

@implementation wbFooter

@synthesize uploadView;
@synthesize homeButton;
@synthesize missionButton;
@synthesize cameraButton;
@synthesize tvButton;
@synthesize userButton;
@synthesize emptyFooter;
@synthesize footerImage;
//@synthesize fixVal;

#define kCAMERABUTTONWIDTH 80
#define kFOOTERBUTTONWIDTH (320/5)
//#define kFOOTERBUTTONWIDTH ((320-kCAMERABUTTONWIDTH)/4)

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        emptyFooter = NO;
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        
        homeButtonImg = [UIImage ch_imageNamed:@"main_feed.png"];
        homeButton = [wbButton buttonWithType:UIButtonTypeSystem];

        tvButtonImg = [UIImage ch_imageNamed:@"tv.png"];
        tvButton = [wbButton buttonWithType:UIButtonTypeSystem];
        
        missionButtonImg = [UIImage ch_imageNamed:@"mission.png"];
        missionButton = [wbButton buttonWithType:UIButtonTypeSystem];
        
        cameraButtonImg = [UIImage ch_imageNamed:@"camera.png"];
        cameraButton = [wbButton buttonWithType:UIButtonTypeSystem];

        userButtonImg = [UIImage ch_imageNamed:@"profile.png"];
        userGuestButtonImg = [UIImage ch_imageNamed:@"profile_guest.png"];
        userButton = [wbButton buttonWithType:UIButtonTypeSystem];

    }
    DPRINTCLASS;
    return self;
}


+ (id)sharedFooter{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] initWithFrame:[[wbAPI sharedAPI] portraitFrame]];
    });
    return _sharedObject;
}

-(NSString*)description
{
    return [super description];
}

-(float)footerBarHeight
{
    return [[wbAPI sharedAPI] footerBarHeight];
}

-(float)statusBarHeight
{
    return  [[wbAPI sharedAPI] statusBarHeight];
}

-(void)setEmptyFooter:(BOOL)_emptyFooter
{
    emptyFooter = _emptyFooter;
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    // Drawing code
    float statusBarHeightLoc = [UIApplication sharedApplication].statusBarFrame.size.height;
    float fixVal = 0;
    if (statusBarHeightLoc > 20) {
        fixVal = 20;
    }
    footerBarRect = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-([self footerBarHeight]+fixVal/*+[self statusBarHeight]-[[wbAPI sharedAPI] statusBarHeight]*/), [[UIScreen mainScreen] bounds].size.width, [self footerBarHeight]);
    UIView *footerBar = [[UIView alloc] initWithFrame:footerBarRect];
    //if (footerImage == nil) {
        [footerBar setBackgroundColor:kTOPUICOLOR];
    //} else {
    //    [footerBar setBackgroundColor:[UIColor colorWithPatternImage:footerImage]];
    //}
    
    if (!emptyFooter) {

        [self createHomeButton];
        
        [self createMissionButton];
        //[missionButton setCenter:CGPointMake(75/2+CGRectGetWidth(missionButton.frame)/2, [self footerBarHeight]/2)];
        
        [self createCameraButton];
        //[cameraButton setCenter:CGPointMake(CGRectGetMidX(footerBarRect), [self footerBarHeight]/2-10)];
        
        [self createTvButton];
        
        [self createUserButton];
        //[userButton setCenter:CGPointMake(CGRectGetWidth(footerBarRect)-(userButton.frame.size.width/2+66.5/2), [self footerBarHeight]/2)];
 
        [footerBar addSubview:homeButton];
        
        [footerBar addSubview:missionButton];
        [footerBar addSubview:[self activeMissionsDot]];
        
        [footerBar addSubview:cameraButton];
        
        [footerBar addSubview:tvButton];
        
        [footerBar addSubview:userButton];
        [footerBar addSubview:[self incompleteProfileDot]];

    } else {
        if (uploadView != nil) {
            UIImage *uploadImg = [UIImage ch_imageNamed:@"upload.png"];
            UIButton *uploadButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [uploadButton setFrame:CGRectMake(0,0,uploadImg.size.width,uploadImg.size.height)];
            [uploadButton setCenter:CGPointMake(CGRectGetMidX(footerBarRect), [self footerBarHeight]/2)];
            [uploadButton setBackgroundImage:uploadImg forState:UIControlStateNormal];
            [uploadButton addTarget:uploadView action:@selector(checkMissionUploadErrors) forControlEvents:UIControlEventTouchUpInside];
            [footerBar addSubview:uploadButton];
        }
    }

    if ([[wbAPI sharedAPI] uploading]) {
        if (uploadProgress == nil) {
            uploadProgress = [[wbAPI sharedAPI] getUploadProgressView];
            [uploadProgress setCenter:cameraButton.center];//CGPointMake(CGRectGetMidX(footerBarRect), [self footerBarHeight]/2-10)];
        }
        [footerBar addSubview:uploadProgress];
    }
    //one line
    UIView *oneLine = [[UIView alloc] initWithFrame:CGRectMake(0, footerBar.frame.origin.y-1, self.frame.size.width, 1)];
    [oneLine setBackgroundColor:kBKGUICOLOR];
    [oneLine setAlpha:0.25];
    
    [self addSubview:oneLine];
    [self addSubview:footerBar];
}


//home button
-(void) createHomeButton
{
    [homeButton setFrame:CGRectMake(0,0,kFOOTERBUTTONWIDTH,[self footerBarHeight])];
    [homeButton setBackgroundImage:[self selectedImage] forState:UIControlStateSelected];
    [homeButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [homeButton setTintColor:kTRANSPARENTUICOLOR];
    [homeButton setShowsTouchWhenHighlighted:YES];
    [homeButton setAdjustsImageWhenHighlighted:NO];
    [homeButton setButtonImage:homeButtonImg];
    //[homeButton setPayload:delegate];
    [homeButton addTarget:self action:@selector(selectHome:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)selectHome:(id)sender
{
    if ([homeButton isSelected]) {
        [kROOTVC performSelector:@selector(goHome:) withObject:@"home"];
    } else {
        [kROOTVC performSelector:@selector(changeViewStack:) withObject:@"home"];
    }
}

//missons button
-(void) createMissionButton
{
    [missionButton setFrame:CGRectMake(CGRectGetMaxX(homeButton.frame),0,kFOOTERBUTTONWIDTH,[self footerBarHeight])];
    [missionButton setBackgroundImage:[self selectedImage] forState:UIControlStateSelected];
    [missionButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [missionButton setTintColor:kTRANSPARENTUICOLOR];
    [missionButton setShowsTouchWhenHighlighted:YES];
    [missionButton setAdjustsImageWhenHighlighted:NO];
    [missionButton setButtonImage:missionButtonImg];
    //[missionButton setPayload:delegate];
    [missionButton addTarget:self action:@selector(selectMissions:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)selectMissions:(id)sender
{
    if ([missionButton isSelected]) {
        [kROOTVC performSelector:@selector(goHome:) withObject:@"mission"];
        //[kROOTVC performSelector:@selector(scrollToTop:) withObject:@"mission"];
    } else {
        [kROOTVC performSelector:@selector(changeViewStack:) withObject:@"mission"];
    }
}

//camera button
-(void) createCameraButton
{
//    [cameraButton setFrame:CGRectMake(CGRectGetMaxX(missionButton.frame),[self footerBarHeight]-kCAMERABUTTONWIDTH,kCAMERABUTTONWIDTH,kCAMERABUTTONWIDTH)];
    [cameraButton setFrame:CGRectMake(CGRectGetMaxX(missionButton.frame),0,kFOOTERBUTTONWIDTH,[self footerBarHeight])];
    [cameraButton setButtonImage:cameraButtonImg];
//    [cameraButton setBackgroundImage:cameraButtonImg forState:UIControlStateNormal];
    //[cameraButton setBackgroundImage:cameraButtonImg forState:UIControlStateHighlighted];
    [cameraButton setTintColor:kBOTTOMUICOLOR];
    [cameraButton addTarget:kROOTVC action:@selector(startCamera:) forControlEvents:UIControlEventTouchUpInside];
}


//tv button
-(void) createTvButton
{
    [tvButton setFrame:CGRectMake(CGRectGetMaxX(cameraButton.frame),0,kFOOTERBUTTONWIDTH,[self footerBarHeight])];
    [tvButton setButtonImage:tvButtonImg];
    [tvButton setBackgroundImage:[self selectedImage] forState:UIControlStateSelected];
    [tvButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [tvButton setTintColor:kTRANSPARENTUICOLOR];
    [tvButton setShowsTouchWhenHighlighted:YES];
    [tvButton setAdjustsImageWhenHighlighted:NO];
    [tvButton addTarget:self action:@selector(selectTv:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)selectTv:(id)sender
{
    if ([tvButton isSelected]) {
        //[kROOTVC performSelector:@selector(scrollToTop:) withObject:@"tv"];
        [kROOTVC performSelector:@selector(goTvHome:) withObject:@"tv"];
    } else {
        [kROOTVC performSelector:@selector(changeViewStack:) withObject:@"tv"];
    }
}

//userButton
-(void)createUserButton
{
    [userButton setFrame:CGRectMake(CGRectGetMaxX(tvButton.frame),0,kFOOTERBUTTONWIDTH,[self footerBarHeight])];
    if ([[wbAPI sharedAPI] is_guest]) {
        [userButton setButtonImage:userGuestButtonImg];
    } else {
        [userButton setButtonImage:userButtonImg];
    }
    [userButton setBackgroundImage:[self selectedImage] forState:UIControlStateSelected];
    [userButton setShowsTouchWhenHighlighted:YES];
    [userButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [userButton setTintColor:kTRANSPARENTUICOLOR];
    //[userButton setPayload:delegate];
    [userButton addTarget:self action:@selector(selectUser:) forControlEvents:UIControlEventTouchUpInside];
}

 -(void)selectUser:(id)sender
{
    if ([[wbAPI sharedAPI] is_guest]) {
        [[wbLoginRegisterViewController sharedLRVC] setShowSettings:YES];
        [[wbLoginRegisterViewController sharedLRVC] showAskToLoginRegisterOnView:[[[UIApplication sharedApplication] delegate] window]];
    } else if ([userButton isSelected] ) {
        [kROOTVC performSelector:@selector(goHome:) withObject:@"user"];
    } else {
        [kROOTVC performSelector:@selector(changeViewStack:) withObject:@"user"];
    }
}

-(UIImage*)doDot
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(kDOTSIZE,kDOTSIZE),0.0,0.0);
    UIBezierPath *circle = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, kDOTSIZE, kDOTSIZE)];
    [kREDUICOLOR setFill];
    [circle fill];
    //[kTOPUICOLOR setStroke];
    //[circle setLineWidth:0.5];
    //[circle stroke];
    UIImage *bezierImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return bezierImage;
}


-(UIImageView*)activeMissionsDot
{
    DNSLog(@"%@",[[wbAPI sharedAPI] num_active_missions]);
    if ([[[wbAPI sharedAPI] num_active_missions] integerValue] > 0) {
        UIImageView *activeMissionsLabel = [[UIImageView alloc] initWithImage:[self doDot]];
        [activeMissionsLabel setCenter:CGPointMake(CGRectGetMidX(missionButton.frame)+[missionButton buttonImageView].frame.size.width/2, 11)];
        return activeMissionsLabel;
    } else {
        return nil;
    }
}

-(UIImageView*)incompleteProfileDot
{
    DNSLog(@"%@",[[wbAPI sharedAPI] num_incomplete_profile]);
    if ([[[wbAPI sharedAPI] num_unread_activities] integerValue] > 0) {
        UIImageView *incompleteProfileLabel = [[UIImageView alloc] initWithImage:[self doDot]];
        [incompleteProfileLabel setCenter:CGPointMake(CGRectGetMidX(userButton.frame)+[userButton buttonImageView].frame.size.width/2, 11)];
        return incompleteProfileLabel;
    } else {
        return nil;
    }
}

-(UIImage*)selectedImage
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(kFOOTERBUTTONWIDTH,[self footerBarHeight]),0.0,0.0);
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: CGRectMake(0, [self footerBarHeight]-kSELECTEDIMAGEHEIGHT, kFOOTERBUTTONWIDTH, kSELECTEDIMAGEHEIGHT)];
    [kGREENUICOLOR setFill];
    [rectanglePath fill];
    UIImage *bezierImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return bezierImage;
}


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [super hitTest:point withEvent:event];
    
    // If the hitView is THIS view, return nil and allow hitTest:withEvent: to
    // continue traversing the hierarchy to find the underlying view.
    if (hitView == self) {
        return nil;
    }
    // Else return the hitView (as it could be one of this view's buttons):
    return hitView;
}

-(void)dealloc
{
    userButtonImg = nil;
    missionButtonImg = nil;
    cameraButtonImg = nil;
    cameraButtonPressImg = nil;
    missionButton = nil;
    cameraButton = nil;
    userButton = nil;
    footerImage = nil;
}

@end
