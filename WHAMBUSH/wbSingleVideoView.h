//
//  wbSingleVideoView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/6/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbBaseView.h"
#import "wbDefines.h"
#import "wbBaseCell.h"
#import "wbCommentCell.h"
#import "wbFlagSelectionView.h"
#import "wbInsertTextView.h"
#import "wbVideoPlayerViewController.h"
#import "wbBaseViewController.h"
#import "wbMission.h"

@interface wbSingleVideoView : wbBaseView <UITableViewDelegate, UITableViewDataSource,UIAlertViewDelegate>
{
    float ccellHeight;
    
    NSMutableDictionary *contentArray;
    NSInteger likeCount;
    NSInteger dislikeCount;
    NSInteger commentCount;
    BOOL has_liked;
    BOOL has_disliked;
    float fclRowHeight;
    NSMutableArray *commentArray;
    NSInteger charCount;
    UIButton *likeBtn;
    UIImage *likeBtnImg;
    UIButton *dislikeBtn;
    UIImage *dislikeBtnImg;
    wbLabel *likeDislikeCount;
    UIViewController *controller;
    wbFlagSelectionView *flagSelect;
    wbInsertTextView *insertTxtView;
    BOOL videoWebLoaded;
    
    UIView *descriptionRow;
    float descriptionRowHeight;
    wbButton *postedButton;
    
    float moreRowHeight;
    BOOL moreRowOpen;
    
    wbUser *user;
    NSString *nextUrl;
    
    float videoRowHeight;
    //NSMutableURLRequest* request;
    NSString *videoHtmlString;
    
    //UIWebView *videoWeb;
    UITableView *content;
    UIButton *moreButton;
    UIButton *deleteButton;
    UIButton *shareButton;
    UIButton *flagButton;
    wbButton *addCommentButton;
    UIActivityIndicatorView *ai;
    
    UIActivityViewController *activityViewController;
    
    //float contentHeight;
    BOOL gettingMoreResults;
    
    //wbVideoPlayerViewController *videoPlayerController;
    UIMenuController *menu;
    NSDictionary* commentDictionary;
    wbMission *mission;
}

@property (nonatomic,strong) wbVideoPlayerViewController *videoPlayerController;

- (id)initWithFrame:(CGRect)frame controller:(UIViewController*)_controller content:(NSMutableDictionary*) contentA;
-(void)setGAIViewName;
-(void)likeDone:(NSNumber*)who count:(NSNumber*)count;

//-(void)clean;

@end
