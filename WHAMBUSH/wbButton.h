//
//  wbButton.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 04/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbButton : UIButton
{
    //UIImageView* buttonImageView;
}
//@property (nonatomic,retain) id payload;
@property BOOL preserveWidth;
@property (retain,nonatomic) UIImage* buttonImage;
@property (retain,nonatomic,readonly) UIImageView* buttonImageView;
@property (nonatomic) BOOL cropButtonImage;
@property (nonatomic) CGFloat imageScale;
@end
