////
////  wbVideo.m
////  WHAMBUSH
////
////  Created by Jari Kalinainen on 05/03/15.
////  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
////
//
//#import "wbVideo.h"
//
//
//@implementation wbVideo
//
//@synthesize added_by;
//@synthesize comment_count;
//@synthesize dislike_count;
//@synthesize like_count;
//@synthesize has_liked;
//@synthesize has_disliked;
//@synthesize video_id;
//@synthesize is_active;
//@synthesize is_mission_winner;
//@synthesize is_processed;
//@synthesize mission_id;
//@synthesize rank;
//@synthesize video_type;
//@synthesize view_count;
//@synthesize created_at;
//@synthesize video_description;
//@synthesize external_id;
//@synthesize external_url;
//@synthesize modified_at;
//@synthesize video_name;
//@synthesize published_at;
//@synthesize tags;
//@synthesize thumbnail_url;
//@synthesize web_url;
//@synthesize deleted;
//@synthesize videoCell;
//
//-(id) init
//{
//    self = [super init];
//    if (self) {
//    }
//    DPRINTCLASS;
//    return self;
//}
//
//- (NSString *)description
//{
//    return [super description];
//}
//
//-(void)setVideoData:(NSDictionary *)data
//{
//    if (data != nil) {
//        [[wbData sharedData] saveWhambushUser:[data objectForKey:@"added_by"]];
//        added_by = [[wbData sharedData] getWhambushUser:[[data objectForKey:@"added_by"] objectForKey:@"url"]];
//        
//        comment_count = [data objectForKey:@"comment_count"];
//        dislike_count = [data objectForKey:@"dislike_count"];
//        like_count = [data objectForKey:@"like_count"];
//        has_liked = [data objectForKey:@"has_liked"];
//        has_disliked = [data objectForKey:@"has_disliked"];
//        video_id = [data objectForKey:@"id"];
//        is_active = [data objectForKey:@"is_active"];
//        is_mission_winner = [data objectForKey:@"is_mission_winner"];
//        is_processed = [data objectForKey:@"is_processed"];
//        mission_id = [data objectForKey:@"mission_id"];
//        rank = [data objectForKey:@"rank"];
//        video_type = [data objectForKey:@"video_type"];
//        view_count = [data objectForKey:@"view_count"];
//        
//        created_at = [data objectForKey:@"created_at"];
//        video_description = [data objectForKey:@"description"];
//        external_id = [data objectForKey:@"external_id"];
//        external_url = [NSURL URLWithString:[data objectForKey:@"external_url"]];
//        modified_at = [data objectForKey:@"modified_at"];
//        video_name = [data objectForKey:@"video_name"];
//        published_at = [data objectForKey:@"published_at"];
//        tags = [data objectForKey:@"tags"];
//        thumbnail_url = [NSURL URLWithString:[data objectForKey:@"thumbnail_url"]];
//        web_url = [NSURL URLWithString:[data objectForKey:@"web_url"]];
//    }
//}
//
//-(void)updateVideoData
//{
//    if (video_id != nil) {
//        NSURL *url = [[wbAPI sharedAPI] urlWithEndpoint:[NSString stringWithFormat:@"%@%@/",kVIDEOAPI,video_id]];
//        [[wbAPI sharedAPI] getDataWithURL:url delegate:self selectorName:@"setVideoData:"];
//    }
//}
//
//+(id)initVideoObjectWithId:(NSInteger)videoId
//{
//    id this = [[self alloc] init];
//    NSURL *url = [[wbAPI sharedAPI] urlWithEndpoint:[NSString stringWithFormat:@"%@%@/",kVIDEOAPI,[NSNumber numberWithInteger:videoId]]];
//    [[wbAPI sharedAPI] getDataWithURL:url delegate:this selectorName:@"setVideoData:"];
//    return this;
//}
//
//+(id)initVideoObjectWithData:(NSDictionary*)videoData
//{
//    id this = [[self alloc] init];
//    [this setVideoData:videoData];
//    return this;
//}
//
//-(wbVideoCell*)videoCell
//{
//    if (videoCell == nil) {
//        videoCell = [[wbVideoCell alloc] init];
//    }
//    return nil;
//}
//
//@end
