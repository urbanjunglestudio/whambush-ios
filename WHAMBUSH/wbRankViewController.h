//
//  wbRankViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 16/02/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"
#import "wbRankTableView.h"
@interface wbRankViewController : wbBaseViewController

@end
