//
//  wbBirthdaySelectView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 10/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbBirthdaySelectView : UIView
<UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSInteger selectedRow;
    //UIView *hideView;
    UIToolbar *hideView;
    UIButton *cancelBtn;
    UIView *theBox;
    UIButton *sendBtn;
    
    NSInteger bDay;
    NSInteger bMonth;
    NSInteger bYear;
    
    NSInteger startYear;
}

@property (nonatomic,retain) NSString *birthday;
@property id delegate;

-(void)show;
-(void)hide;

@end
