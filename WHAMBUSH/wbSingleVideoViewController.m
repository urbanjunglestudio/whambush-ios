//
//  wbSingleVideoViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 21/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbSingleVideoViewController.h"

@interface wbSingleVideoViewController ()

@end

@implementation wbSingleVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    DPRINTCLASS;
    return self;
}

//-(void)loadView
//{
//    wbSingleVideoView *singleVideoView = [[wbSingleVideoView alloc] initWithFrame:[[UIScreen mainScreen] bounds] controller:self content:[[wbData sharedData] singleVideoData]];
//    [self setView:singleVideoView];
//}

- (void)viewDidLoad
{
    statusBarHidden = NO;
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    singleVideoView = [[wbSingleVideoView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect] controller:self content:[[wbData sharedData] singleVideoData]];
    [self.view addSubview:singleVideoView];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidAppear:animated];
    //[self.view performSelector:@selector(hideKB)];
    [singleVideoView performSelector:@selector(hideKB)];
    kHIDEWAIT
    //videoPlayerController = [(wbSingleVideoView*)self.view videoPlayerController];
    videoPlayerController = [singleVideoView videoPlayerController];
    [singleVideoView performSelector:@selector(setGAIViewName)];
    [[wbHeader sharedHeader] setLogo:[super headerLogo]];
}

-(void)viewDidDisappear:(BOOL)animated
{
    //[[(wbSingleVideoView*)self.view videoWeb] reload];
    //[self.view performSelector:@selector(hideKB)];
    [singleVideoView performSelector:@selector(hideKB)];
    [super viewDidDisappear:animated];
}

//-(void)goBack:(id)sender
//{
//    //[[(wbSingleVideoView*)self.view videoWeb] reload];
//    //[(wbSingleVideoView*)self.view clean];
//    [singleVideoView clean];
//    [super goBack:sender];
//    self.view = nil;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        if (iOS8) {
            [[[[UIApplication sharedApplication] delegate] window] addSubview:videoPlayerController.view];
        } else {
            [self.view addSubview:videoPlayerController.view];
        }
        //[singleVideoView setFrame:CGRectMake(0, 0, [[wbAPI sharedAPI] landscapeFrame].size.width, [[wbAPI sharedAPI] landscapeFrame].size.height)];
        //[singleVideoView setNeedsDisplay];
        [singleVideoView setHidden:YES];
        [videoPlayerController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
        statusBarHidden = YES;
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        [videoPlayerController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
        [videoPlayerController.view removeFromSuperview];
        //[singleVideoView setFrame:[[wbAPI sharedAPI] contentRect]];
        //[singleVideoView setNeedsDisplay];
        [singleVideoView setHidden:NO];
        [singleVideoView performSelector:@selector(refreshVideoCell)];
        statusBarHidden = NO;
    }
}


//-(BOOL)prefersStatusBarHidden
//{
//    DMSG;
//    return statusBarHidden;
//}

-(void)statusBarChange:(id)sender
{
    DMSG;
}

-(BOOL)shouldAutorotate
{
    DMSG;
    return YES;
}
@end
