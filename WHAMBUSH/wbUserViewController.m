//
//  wbUserViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 21/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbUserViewController.h"

@interface wbUserViewController ()

@end

@implementation wbUserViewController

@synthesize user;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    DPRINTCLASS;
    return self;
}

//-(void)loadView
//{
//    wbUserView *userView = [[wbUserView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect] controller:self];
//    [self setView:userView];
//}
-(void)loadView
{
    wbNewUserView   *userView = [[wbNewUserView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [self setView:userView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[wbFooter sharedFooter] setNeedsDisplay];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    //[[wbData sharedData] setHasUserpage:YES];
    [super viewDidAppear:animated];

    //[self setNeedsStatusBarAppearanceUpdate];
    [[UIApplication sharedApplication] setStatusBarOrientation:1];

    [[(wbNewUserView*)self.view insertTxtView] hideTextView];

    [[wbAPI sharedAPI] registerGoogleAnalytics:@"Profile"];

    //[self.view setNeedsDisplay];
    [[wbAPI sharedAPI] setNum_unread_activities:@"0"];
    [[wbFooter sharedFooter] setNeedsDisplay];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [[(wbNewUserView*)self.view insertTxtView] hideTextView];
    [user updateUserData];
}

-(void)setUser:(wbUser *)_user
{
    [(wbNewUserView*)self.view setUser:_user];
    //[(wbUserView*)self.view setFullView:NO];

}


//-(void)goBack:(id)sender
//{
//    [[wbData sharedData] setHasUserpage:NO];
//    [[wbData sharedData] setHasMissions:NO];
//    [super goBack:sender];
//}

-(void)logout:(id)sender
{
    [[wbAPI sharedAPI] logout];
    [kROOTVC performSelector:@selector(logout:) withObject:NULL];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
