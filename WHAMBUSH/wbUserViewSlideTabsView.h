//
//  wbUserViewSlideTabsView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 09/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseViewController.h"

@interface wbUserViewSlideTabsView : UIView
{
    UIView *slider;
    NSInteger currentTab;
    UIView *greenLine;
    NSMutableArray *tabLabels;
}

@property (nonatomic,retain) NSArray *titleStrings;
@property (nonatomic,retain) UIColor *titleColor;
@property (nonatomic,retain) UIFont *titleFont;
@property (nonatomic,retain) UIViewController *currentViewController;

@property NSInteger defaultTab;
@property (nonatomic,retain) NSArray *views;

-(void)moveToTab:(NSInteger)index;
-(void)moveToTab:(NSInteger)index animate:(BOOL)animate;

@end
