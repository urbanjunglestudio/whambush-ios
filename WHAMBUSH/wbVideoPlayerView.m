//
//  wbVideoPlayerView
//
//  Created by Jari Kalinainen on 24/03/14.
//  Copyright (c) 2014 TofuHead. All rights reserved.
//

#import "wbVideoPlayerView.h"

@implementation wbVideoPlayerView

@synthesize controller;
@synthesize portrait;
@synthesize progress;
@synthesize playing;
@synthesize buffered;
@synthesize readyToPlay;
@synthesize duration;
@synthesize showShare;
@synthesize isRed;
@synthesize thumbUrl;
@synthesize thumbView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
//        [self setBackgroundColor:kBLACKUICOLOR];
        playImg = [UIImage ch_imageNamed:@"play.png"];
        playRedImg = [UIImage ch_imageNamed:@"play_upload.png"];
        pauseImg = [UIImage ch_imageNamed:@"pause.png"];
        pauseRedImg = [UIImage ch_imageNamed:@"pause_upload.png"];
        sliderImg = [UIImage ch_imageNamed:@"sliderbutton.png"];
        shareImg = [UIImage ch_imageNamed:@"share_btn.png"];
        playing = NO;
        portrait = YES;
        buffered = 0.0;
        progress = 0.0;
        readyToPlay = NO;
        showShare = NO;
        isRed = NO;
        //[self setAlpha:0.5];
    };
    return self;
}


-(void)setProgress:(float)_progress
{
    progress = _progress;
    [progressBar setProgress:_progress animated:NO];
    [progressSlider setValue:_progress animated:NO];
}

-(void)setPlaying:(BOOL)_playing
{
    playing = _playing;
    if (!playing) {
        if ([hideTimer isValid]) {
            [hideTimer invalidate];
        }
        [playPauseImgView setAlpha:0.9];
        if (isRed) {
            [playPauseImgView setImage:playRedImg];
        } else {
            [playPauseImgView setImage:playImg];
        }
        [progressBar setAlpha:0.9];
        [progressSlider setAlpha:0.9];
        [shareButton setAlpha:0.9];
    } else {
        [thumbView setHidden:YES];
        hideTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(hideControls:) userInfo:nil repeats:NO];
        if (portrait) {
            [playPauseImgView setAlpha:0.0];
            [shareButton setAlpha:0.0];
        } else {
            [playPauseImgView setAlpha:0.4];
        }
        if (isRed) {
            [playPauseImgView setImage:pauseRedImg];
        } else {
            [playPauseImgView setImage:pauseImg];
        }
        [progressBar setAlpha:0.6];
        [progressSlider setAlpha:0.4];
    }
}

-(void)hideControls:(id)sender
{
    if ([hideTimer isValid]) {
        [hideTimer invalidate];
    }
	
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.7];
    
    [progressBar setAlpha:0.2];
    [progressSlider setAlpha:0.0];
    [playPauseImgView setAlpha:0.0];
    [shareButton setAlpha:0.0];
	
	[UIView commitAnimations];
}

-(void)setBuffered:(float)_buffered
{
    buffered = _buffered;
    NSString *bufferStatus = [NSString stringWithFormat:@"%.f%%",_buffered*100];
    [bufferLabel setText:bufferStatus];
    [bufferLabel sizeToFit];
    [bufferBar setProgress:_buffered animated:NO];
    
    if (_buffered >= 0.99) {
        [bufferLabel setHidden:YES];
        [bufferBar setHidden:YES];
    }
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    //if (portrait) {
    //    [thumbView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    //} else {
    //    [thumbView setFrame:CGRectMake(0, 0, 0, 0)];
    //}
    
    if (bufferBar == nil) {
        bufferBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    }
    [bufferBar setTintColor:kBOTTOMUICOLOR];
    [bufferBar setFrame:CGRectMake(0, self.frame.size.height-2, self.frame.size.width, 2)];
    [bufferBar setAlpha:0.7];
    [self addSubview:bufferBar];
    if  (playPauseImgView == nil) {
        playPauseImgView = [[UIImageView alloc] init];
        [playPauseImgView setAlpha:0.9];
    }
    if (portrait) {
        [playPauseImgView setFrame:CGRectMake(0,0, playImg.size.width,playImg.size.height)];
        [playPauseImgView setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
    } else {
        [playPauseImgView setFrame:CGRectMake(10, self.frame.size.height-(playImg.size.height+10), playImg.size.width,playImg.size.height)];
    }
    if (playing) {
        if (isRed) {
            [playPauseImgView setImage:pauseRedImg];
        } else {
            [playPauseImgView setImage:pauseImg];
        }
    } else {
        if (isRed) {
            [playPauseImgView setImage:playRedImg];
        } else {
            [playPauseImgView setImage:playImg];
        }
    }
    if (!readyToPlay) {
        [playPauseImgView setHidden:YES];
    }
    if (!isRed) {
        if (thumbView == nil) {
            thumbView = [[UIImageView alloc] init];
            [thumbView setHidden:NO];
        }
        __weak typeof(self) weakSelf = self;
        
        [thumbView setImageWithURLRequest:[NSMutableURLRequest requestWithURL:thumbUrl] placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            [weakSelf.thumbView setImage:[weakSelf fixThumb:image]];
            [weakSelf.thumbView setFrame:CGRectMake(0, 0, weakSelf.frame.size.width, weakSelf.frame.size.height)];
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
        /*[thumbView setImageWithURL:thumbUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [thumbView setImage:[self fixThumb:image]];
            [thumbView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        }];*/
        [self addSubview:thumbView];
        [self bringSubviewToFront:thumbView];
    }
    [self addSubview:playPauseImgView];
    
    if (playPauseButton == nil) {
        playPauseButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [playPauseButton setAlpha:0.9];
    }
    [playPauseButton setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [playPauseButton setBackgroundColor:[UIColor clearColor]];
    [playPauseButton setTintColor:[UIColor clearColor]];
    [playPauseButton addTarget:controller action:@selector(togglePlay:) forControlEvents:UIControlEventTouchUpInside];
    if (!readyToPlay) {
        [playPauseButton setHidden:YES];
    }
    //[playPauseButton setTintColor:kREDUICOLOR];
    [self addSubview:playPauseButton];

    if (shareButton == nil) {
        shareButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [shareButton setAlpha:0.9];
    }
    [shareButton setFrame:CGRectMake(10, self.frame.size.height-(shareImg.size.height/2+10), shareImg.size.width/2, shareImg.size.height/2)];
    [shareButton setBackgroundColor:[UIColor clearColor]];
    [shareButton setBackgroundImage:shareImg forState:UIControlStateNormal];
    [shareButton setTintColor:[UIColor clearColor]];
    [shareButton addTarget:controller action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    if (!readyToPlay) {
        [shareButton setHidden:YES];
    }
    if (showShare && portrait) {
        [self addSubview:shareButton];
    } else {
        [shareButton removeFromSuperview];
    }
    
    if (bufferLabel == nil) {
        bufferLabel = [[UILabel alloc] init];
        [bufferLabel setAlpha:0.9];
    }
    [bufferLabel setFont:kFONT(18)];
    if (isRed) {
        [bufferLabel setTextColor:kREDUICOLOR];
    } else {
        [bufferLabel setTextColor:kGREENUICOLOR];
    }
    [bufferLabel setTextAlignment:NSTextAlignmentRight];
    [bufferLabel setFrame:CGRectMake(self.frame.size.width-30, 2, 0,0)];
    [self addSubview:bufferLabel];
    
    if (progressBar == nil) {
        progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    }
    if (isRed) {
        [progressBar setTintColor:kREDUICOLOR];
    } else {
        [progressBar setTintColor:kGREENUICOLOR];
    }
    if (portrait) {
        [progressBar setFrame:CGRectMake(0, self.frame.size.height-2, self.frame.size.width, 2)];
        [self addSubview:progressBar];
    } else {
        [progressBar removeFromSuperview];
    }
    
    if (progressSlider == nil) {
        progressSlider = [[UISlider alloc] init];
        [progressSlider setAlpha:0.9];
        [progressSlider setMinimumValue:0.0];
        [progressSlider setMaximumValue:1.0];
        [progressSlider setMaximumTrackTintColor:kBKGUICOLOR];
        [progressSlider setMinimumTrackTintColor:kGREENUICOLOR];
        [progressSlider setContinuous:YES];
        [progressSlider setThumbImage:sliderImg forState:UIControlStateNormal];
        [progressSlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    [progressSlider setFrame:CGRectMake(CGRectGetMaxX(playPauseImgView.frame)+10, CGRectGetMinY(playPauseImgView.frame), self.frame.size.width-(CGRectGetMaxX(playPauseImgView.frame)+20), playImg.size.height)];
    
    if (durationZero == nil) {
        durationZero = [[UILabel alloc] init];
        [progressSlider addSubview:durationZero];
    }
    [durationZero setText:@"0"];
    [durationZero setFont:kFONT(8)];
    [durationZero setTextColor:kBKGUICOLOR];
    [durationZero setFrame:CGRectMake(0, 17, 0,0)];
    [durationZero sizeToFit];
    
    if (durationFull == nil) {
        durationFull = [[UILabel alloc] init];
        [progressSlider addSubview:durationFull];
    }
    [durationFull setFont:kFONT(8)];
    [durationFull setTextColor:kBKGUICOLOR];
    [durationFull sizeToFit];
    [durationFull setFrame:CGRectMake(progressSlider.frame.size.width-(durationFull.frame.size.width+2), 17,durationFull.frame.size.width,durationFull.frame.size.height)];
    
    if (!portrait) {
        [self addSubview:progressSlider];
        [self bringSubviewToFront:progressSlider];
    } else {
        [progressSlider removeFromSuperview];
    }
    
   
}

-(void)setDuration:(NSTimeInterval)_duration
{
    duration = _duration;
    if (durationFull == nil) {
        durationFull = [[UILabel alloc] init];
    }
    [durationFull setText:[NSString stringWithFormat:@"%.f",duration]];
    [durationFull setNeedsDisplay];
}

-(void)setReadyToPlay:(BOOL)_readyToPlay
{
    readyToPlay = _readyToPlay;
    if (readyToPlay == YES) {
        [playPauseButton setHidden:NO];
        [playPauseImgView setHidden:NO];
        [shareButton setHidden:NO];
    }
}

-(void)sliderValueChanged:(UISlider*)sender
{
    [controller performSelector:@selector(moveProgress:) withObject:[NSNumber numberWithFloat:[sender value]]];
}

-(UIImage*)fixThumb:(UIImage*)image
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(self.frame.size.width,self.frame.size.height), 0.0, 0.0);
    UIBezierPath* p = [UIBezierPath bezierPathWithRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [[UIColor blackColor] setFill];
    [p fill];
    [image drawInRect:CGRectMake((self.frame.size.width-(image.size.width)/(image.size.height/self.frame.size.height))/2, 0, (image.size.width)/(image.size.height/self.frame.size.height), self.frame.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end



