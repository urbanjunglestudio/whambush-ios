//
//  wbCountryPickerView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 01/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbCountryPickerView.h"

@implementation wbCountryPickerView

@synthesize delegate;
@synthesize selectedCountry;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setUserInteractionEnabled:NO];
        [self setHidden:YES];
        
        numberOfRows = 3;
        picked = NO;
        [[wbData sharedData] getCountriesData:self];
    }
    DPRINTCLASS;
    return self;
}

-(void)gotCountries:(NSArray*)countries
{
    //allCountries = countries;
    supportedCountries = [[NSMutableArray alloc] init];
    otherCountries  = [[NSMutableArray alloc] init];
    
    numberOfRows = [countries count];
    for (int i = 0; i < numberOfRows; i++) {
        if ([[[countries objectAtIndex:i] objectForKey:@"supported"] boolValue]) {
            NSMutableDictionary *toBeLocalized = [[NSMutableDictionary alloc] initWithDictionary:[countries objectAtIndex:i]];
            NSString *localizedName = NSLocalizedString([toBeLocalized objectForKey:@"name"], @"");
            [toBeLocalized setObject:localizedName forKey:@"name"];

            [supportedCountries addObject:toBeLocalized];
        } else {
            [otherCountries addObject:[countries objectAtIndex:i]];
        }
    }
    //TODO: sort arrays with localization
    
    allCountries = [NSMutableArray arrayWithArray:supportedCountries];
    if ([otherCountries count] > 0) {
        [allCountries addObject:@{@"name":NSLocalizedString(@"GENERAL_COUNTRY_SEPARATOR", @"")}];
        [allCountries addObjectsFromArray:otherCountries];
    }
    //DCMSG(allCountries);
    [self setNeedsDisplay];
    if ([delegate respondsToSelector:@selector(gotCountries)]) {
        [delegate performSelector:@selector(gotCountries)];
    }
}

-(void)setSelectedCountry:(NSDictionary *)_selectedCountry
{
    selectedCountry = _selectedCountry;
    if (allCountries != nil) {
        for (int i = 0; i < [allCountries count]; i++) {
            if ([[[allCountries objectAtIndex:i] objectForKey:@"country"] isEqualToString:[_selectedCountry objectForKey:@"country"]]) {
                selectedRowNumber = i+1;
                break;
            }
        }
    } else {
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self setSelectedCountry:_selectedCountry];
        });
    }
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    //    if (![birthday isEqualToString:@"<null>"]) {
    
    // Drawing code
    if (hideView == nil) {
        hideView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    [hideView setBackgroundColor:kTRANSPARENTUICOLOR];
    [hideView setAlpha:0.0];
    [hideView setBarStyle:UIBarStyleBlack];
    
    //    hideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    //    [hideView setBackgroundColor:[UIColor blackColor]];
    //    [hideView setAlpha:0];
    
    theBox = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 182+40)];
    [theBox setBackgroundColor:kSEPRATORUICOLORAlpha];
    
    UIPickerView *selection = [[UIPickerView alloc] init];
    [selection setFrame:CGRectMake(0, 40, self.frame.size.width, 180)];
    [selection setBackgroundColor:kSEPRATORUICOLORAlpha];
    [selection setDelegate:self];
    [selection setDataSource:self];
    [selection setShowsSelectionIndicator:YES];
    [selection setTintColor:kWHITEUICOLOR];
    if (selectedCountry != nil) {
        [selection selectRow:selectedRowNumber inComponent:0 animated:NO];
    }
    
    cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setFrame:hideView.frame];
    [cancelBtn setBackgroundColor:[UIColor clearColor]];
    [cancelBtn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [hideView addSubview:cancelBtn];
    
    UIImage *sendImg = [UIImage ch_imageNamed:@"done_comment_default.png"];
    UIImage *sendImgActive = [UIImage ch_imageNamed:@"done_comment_active.png"];
    sendBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [sendBtn setFrame:CGRectMake(self.frame.size.width-(sendImg.size.width+2), 2, sendImg.size.width, sendImg.size.height)];
    [sendBtn setBackgroundColor:[UIColor clearColor]];
    [sendBtn setBackgroundImage:sendImg forState:UIControlStateNormal];
    [sendBtn setBackgroundImage:sendImgActive forState:UIControlStateHighlighted];
    [sendBtn setShowsTouchWhenHighlighted:YES];
    [sendBtn addTarget:self action:@selector(pickerAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [theBox addSubview:selection];
    [theBox addSubview:sendBtn];
    [self addSubview:hideView];
    [self addSubview:theBox];
}

-(void)pickerAction:(id)sender
{
    if (picked) {
        [delegate setSelectedCountry:country];
    }
    [self hide];
}

// Handle the selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    if (row > 0) {
        picked = YES;
        country = [allCountries objectAtIndex:row-1];
        if ([country count] < 3) {
            picked = NO;
        }
    } else {
        picked = NO;
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return numberOfRows+2; // countries plus empty first and supported separator
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{   
    return self.frame.size.width-10;
}

-(NSAttributedString*)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    NSAttributedString *attString;
    if (row == 0) {
        attString= [[NSAttributedString alloc] initWithString:@""];
    } else {
        if ([allCountries count] > row-1) {
            NSString* countryName = [[allCountries objectAtIndex:row-1] objectForKey:@"name"];
            attString= [[NSAttributedString alloc] initWithString:countryName attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        }
        
    }
    return attString;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return kTXTBOXH;
}

//show/hide
-(void)show
{
    picked = NO;
    //[delegate setScrollEnabled:NO];
    [self setHidden:NO];
    [self setUserInteractionEnabled:YES];
    // get a rect for the textView frame
    CGRect containerFrame = theBox.frame;
    containerFrame.origin.y = self.bounds.size.height - theBox.frame.size.height;//-20;//-50;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
    
    // set views with new info
    hideView.alpha = 0.97;
    theBox.alpha = 0.90;
    theBox.frame = containerFrame;
    
    // commit animations
    [UIView commitAnimations];
}

-(void)hide
{
    DMSG;
    [self setUserInteractionEnabled:NO];
    // get a rect for the textView frame
    CGRect containerFrame = theBox.frame;
    containerFrame.origin.y = self.bounds.size.height;
    
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
    
    // set views with new info
    hideView.alpha = 0.0;
    theBox.alpha = 0.0;
    theBox.frame = containerFrame;
    
    // commit animations
    [UIView commitAnimations];
    //[delegate setScrollEnabled:YES];
    [self setHidden:YES];
}

@end
