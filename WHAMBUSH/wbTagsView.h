//
//  wbTagsView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 10/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbTagsView : UIView
{
    UIView *tagView;
    NSMutableArray *addTags;
}

@property (nonatomic,strong,retain) NSMutableArray *tagStrings;
@property (nonatomic,retain) id delegate;

@end

