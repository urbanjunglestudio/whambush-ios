//
//  wbMissionsTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 06/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbMissionsTableView.h"

@implementation wbMissionsTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        //[self setCanCancelContentTouches:NO];
        [self setHidden:NO];
        firstGo = YES;
        
    }
    DPRINTCLASS;
    return self;
}

-(void)setGAIViewName
{
    [[wbAPI sharedAPI] registerGoogleAnalytics:[[self currentFeed] objectForKey:@"type"]];
}

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    if (refreshController == nil) {
        refreshController = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [refreshController addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[UIImage ch_imageNamed:@"refresh_bg_image"]]];
        [self addSubview:refreshController];
    }
    
    UIView *tableFooter =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 100)];
    [tableFooter setBackgroundColor:kTRANSPARENTUICOLOR];
    
    [self setTableFooterView:tableFooter];
    
    if(dataArray == nil) {
        [[wbData sharedData] getMissionsData:@"default" delegate:self];
    }
}

-(void)forceRefresh
{
    //[ai setHidden:NO];
    //[[self tableFooterView] setHidden:NO];
    //[self refresh:nil];
    dataArray = nil;
    resultArray = nil;
    numberOfResults = 0;
    refresh = NO;
    super.hideHeader = NO;
    [feedSelect removeFromSuperview];
    feedSelect = nil;
    firstGo = NO;
    [self reloadData];
    [self setNeedsDisplay];
    kSHOWWAIT;
}

-(void)executeRefresh
{
    deltaY = 0;
    if ([[[self currentFeed] objectForKey:@"type"] isEqualToString:@"missions"]) {
        [[wbData sharedData] refreshMissionData:[[self currentFeed] objectForKey:@"name"] delegate:self];
    } else {
        [self setCurrentFeed:[self currentFeed]];
        [[wbAPI sharedAPI] getDataWithEndpoint:[[self currentFeed] objectForKey:@"endpoint"] delegate:self];
    }
}

-(void)resultsReady
{
    [super setAllFeeds:[[wbData sharedData] missionFeeds]];
    [super setShowFeedSelectSearch:NO];
    
    NSMutableArray *newRows = [[NSMutableArray alloc] init];
    
    for (long i = [self numberOfRowsInSection:0]; i < numberOfResults; i++) {
        NSIndexPath *newReloadRows = [NSIndexPath indexPathForRow:i inSection:0];
        [newRows addObject:newReloadRows];
        newReloadRows = nil;
    }
    
    [self beginUpdates];
    [self insertRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationAutomatic];
    [self endUpdates];
    
    
    [self setHidden:NO];
    
    [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [self setGAIViewName];
    
    DMSG;
    kHIDEWAIT;
}

-(float)missionCellHeight:(NSString*)txt hasVideo:(BOOL)hasVideo
{
    float height;
    
    UITextView *missionTxtView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
    if ([txt length] > 0) {
        [missionTxtView setText:[NSString stringWithFormat:@"%@",txt]];
        [missionTxtView setFont:kFONTHelvetica(15)];
        [missionTxtView setTextContainerInset:UIEdgeInsetsMake(0, 7, 0, 16)];
        [missionTxtView sizeToFit];
    }
    
    height = missionTxtView.frame.size.height+120;
    missionTxtView  = nil;
    if (hasVideo) {
        height += 20;
    }
    if (height < 162) {
        height = 162;
    }
    return height;
}
-(BOOL)isActive
{
    //DCMSG([self currentFeed]);
    BOOL myCountry;
    if  ([[[[wbAPI sharedAPI] currentCountry] objectForKey:@"country"] isEqualToString:[[wbData sharedData] selectedMissionCountry]] || [[[wbData sharedData] selectedMissionCountry] isEqualToString:@"ZZ"]) {
        myCountry = YES;
    } else {
        myCountry = NO;
    }
    if ([[[self currentFeed] objectForKey:@"name"] isEqualToString:@"MISSIONS_ACTIVE_FEED"] && myCountry) {
        return YES;
    }
    return NO;
}
//Tableview stuff
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (resultArray == nil) {
        return 0;
    } else {
        return numberOfResults;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    wbBaseCell *cell;
    
    if (cell == nil) {
        cell = [[wbBaseCell alloc] init];
    }
    
    if ([resultArray count]-5 == [indexPath row] || [resultArray count] == [indexPath row]) {
        if (numberOfResults < totalNumberOfResults) {
            DMSG;
            [self performSelectorInBackground:@selector(getMoreResults) withObject:nil];
        }
    }
    
    
    wbMissionCell *missionCell = [[wbMissionCell alloc] initWithFrame:CGRectMake(0, 0, 320, [self missionCellHeight:[[resultArray objectAtIndex:[indexPath row]] objectForKey:@"description"] hasVideo:[[[resultArray objectAtIndex:[indexPath row]] objectForKey:@"linked_video"] isKindOfClass:[NSDictionary class]]])];
    [missionCell setCellArray:[resultArray objectAtIndex:[indexPath row]]];
    if ([self isActive]) {
        [missionCell setIsArchived:NO];
    } else {
        [missionCell setIsArchived:YES];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [cell addSubview:missionCell];
   
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    if (numberOfResults > [indexPath row]) {
        height = [self missionCellHeight:[[resultArray objectAtIndex:[indexPath row]] objectForKey:@"description"] hasVideo:[[[resultArray objectAtIndex:[indexPath row]] objectForKey:@"linked_video"] isKindOfClass:[NSDictionary class]]];
    } else {
        height = 0;
    }
    return height+1;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if ([indexPath row] == 0) {
//        if (feedOpen) {
//            selectionHeight = kTXTBOXH;
//        } else {
//            selectionHeight = 2*kTXTBOXH;
//        }
//        [tableView beginUpdates];
//        [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        [tableView endUpdates];
//    }
//}

@end
