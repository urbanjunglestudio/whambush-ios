//
//  wbActivityViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"
#import "wbActivityTableView.h"

@interface wbActivityViewController : wbBaseViewController

@end
