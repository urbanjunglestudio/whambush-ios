//
//  wbPBJCameraViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 08/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbPBJCameraViewController.h"

@interface wbPBJCameraViewController ()

@end

@implementation wbPBJCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    waitView = [[UIView alloc] initWithFrame:[[wbAPI sharedAPI] landscapeFrame]];
    [waitView setBackgroundColor:kBLACKUICOLOR];
    [waitView setAlpha:0.8];
    UIActivityIndicatorView *ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [ai setCenter:waitView.center];
    [ai startAnimating];
    [waitView addSubview:ai];
    
    [[self view] setBackgroundColor:kBLACKUICOLOR];
    // Do any additional setup after loading the view.
    _assetLibrary = [[ALAssetsLibrary alloc] init];
   
    // preview and AV layer
    _previewView = [[UIView alloc] initWithFrame:[[wbAPI sharedAPI] landscapeFrame]];
    _previewView.backgroundColor = [UIColor blackColor];
    //CGRect previewFrame = [[wbAPI sharedAPI] landscapeFrame];
    //_previewView.frame = previewFrame;
    _previewLayer = [[PBJVision sharedInstance] previewLayer];
    _previewLayer.frame = _previewView.bounds;
    _previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    _previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    _previewLayer.connection.automaticallyAdjustsVideoMirroring = NO;
    _previewLayer.connection.videoMirrored = NO;
    [_previewView.layer addSublayer:_previewLayer];
    
    
    _recordTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(_handleRecordTapGesterRecognizer:)];
    _recordTapGestureRecognizer.delegate = self;
    _recordTapGestureRecognizer.numberOfTapsRequired = 1;
    _recordTapGestureRecognizer.enabled = NO;
    [_previewView addGestureRecognizer:_recordTapGestureRecognizer];
    
    isFirstRecord = YES;
    isRecording = NO;
    exit = NO;
 
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [super viewWillAppear:animated];
    
    if (cameraOverlay == nil) {
        cameraOverlay  = [[wbCameraView alloc] initWithFrame:[[wbAPI sharedAPI] landscapeFrame] controller:self];
    }
    [self.view addSubview:cameraOverlay];
    [self.view bringSubviewToFront:cameraOverlay];
    
    [self _resetCapture];
    [[PBJVision sharedInstance] startPreview];
    
    kHIDEWAIT;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[PBJVision sharedInstance] stopPreview];
}

///////////////////////////////
- (void)_startCapture
{
    [[PBJVision sharedInstance] startVideoCapture];
//    [cameraOverlay setRecording:YES];
//    [cameraOverlay toggleCamera];
}

- (void)_pauseCapture
{
    [[PBJVision sharedInstance] pauseVideoCapture];
//    if ([[PBJVision sharedInstance] capturedVideoSeconds] > 1) {
//        [cameraOverlay setFirstVideoDone:YES];
//    }
//    [cameraOverlay setRecording:NO];
//    [cameraOverlay toggleCamera];
//    NSLog(@"recorded: %.f",[[PBJVision sharedInstance] capturedVideoSeconds]);
}

- (void)_resumeCapture
{
    [[PBJVision sharedInstance] resumeVideoCapture];
//    [cameraOverlay setFirstVideoDone:YES];
//    [cameraOverlay setRecording:YES];
//    [cameraOverlay toggleCamera];
}

//- (void)_endCapture
//{
//    [[PBJVision sharedInstance] endVideoCapture];
//    [cameraOverlay setFirstVideoDone:YES];
//    [cameraOverlay setRecording:NO];
//    [cameraOverlay toggleCamera];
//}

- (void)_resetCapture
{    
    PBJVision *vision = [PBJVision sharedInstance];
    vision.delegate = self;
    
    if ([vision isCameraDeviceAvailable:PBJCameraDeviceBack]) {
        vision.cameraDevice = PBJCameraDeviceBack;
    } else {
        vision.cameraDevice = PBJCameraDeviceFront;
    }
    
    vision.maximumCaptureDuration = CMTimeMake(kVIDEOMAXTIME, 1);
    vision.cameraMode = PBJCameraModeVideo;
    vision.cameraOrientation = PBJCameraOrientationLandscapeRight;
    vision.focusMode = PBJFocusModeContinuousAutoFocus;
    vision.captureSessionPreset = AVCaptureSessionPresetHigh;//AVCaptureSessionPreset1280x720;
    vision.videoRenderingEnabled = YES;
    vision.videoBitRate = 1500000;
    //vision.outputFormat = PBJOutputFormatWidescreen;
    vision.outputFormat = PBJOutputFormatCustom;
    vision.customVideoSize = CGSizeMake(ceilf(kVIDEOHEIGHT*16.0/9.0) , kVIDEOHEIGHT);
    //NSLog(@"videosize: %.f, %.f",vision.customVideoSize.width,vision.customVideoSize.height);
    //vision.additionalCompressionProperties = @{AVVideoProfileLevelKey : AVVideoProfileLevelH264Baseline30}; // AVVideoProfileLevelKey requires specific captureSessionPreset
}

///////////////////////////////


- (void)toggleCamera:(UIButton *)button
{
    PBJVision *vision = [PBJVision sharedInstance];
    vision.cameraDevice = vision.cameraDevice == PBJCameraDeviceBack ? PBJCameraDeviceFront : PBJCameraDeviceBack;

    _previewLayer.connection.automaticallyAdjustsVideoMirroring = NO;
    if (vision.cameraDevice == PBJCameraDeviceBack) {
        _previewLayer.connection.videoMirrored = NO;
        [[cameraOverlay toggleCameraImgView] setHighlighted:NO];
    } else {
        _previewLayer.connection.videoMirrored = YES;
        [[cameraOverlay toggleCameraImgView] setHighlighted:YES];
    }
}

-(void)toggleVideoCapture
{
    if (!isRecording) {
        if (isFirstRecord) {
            [self _startCapture];
        } else {
            [self _resumeCapture];
        }
        theTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:cameraOverlay selector:@selector(doTimer) userInfo:nil repeats:YES];
        isRecording = YES;
    } else {
        if (theTimer.isValid) {
            [theTimer invalidate];
        }
        [self _pauseCapture];
        isRecording = NO;
    }
}

-(void)captureDone
{
    kSHOWWAITLAND;
    if (theTimer.isValid) {
        [theTimer invalidate];
    }
    
    DCMSG([PBJVision sharedInstance]);
    
    [[PBJVision sharedInstance] endVideoCapture];
    //[cameraOverlay setRecording:NO];
    //[cameraOverlay toggleCamera];
}
///////////////////////////////


// session

- (void)visionSessionWillStart:(PBJVision *)vision
{
}

- (void)visionSessionDidStart:(PBJVision *)vision
{
    if (![_previewView superview]) {
        [self.view addSubview:_previewView];
    }
}

- (void)visionSessionDidStop:(PBJVision *)vision
{
    [_previewView removeFromSuperview];
}

// preview

- (void)visionSessionDidStartPreview:(PBJVision *)vision
{
    DCMSG(@"Camera preview did start");
    [self.view bringSubviewToFront:cameraOverlay];    
}

- (void)visionSessionDidStopPreview:(PBJVision *)vision
{
    DCMSG(@"Camera preview did stop");
}

// device

- (void)visionCameraDeviceWillChange:(PBJVision *)vision
{
    DCMSG(@"Camera device will change");
    [self.view addSubview:waitView];
}

- (void)visionCameraDeviceDidChange:(PBJVision *)vision
{
    DCMSG(@"Camera device did change");
    [waitView removeFromSuperview];
}

// mode

- (void)visionCameraModeWillChange:(PBJVision *)vision
{
    DCMSG(@"Camera mode will change");
}

- (void)visionCameraModeDidChange:(PBJVision *)vision
{
    DCMSG(@"Camera mode did change");
}


// video capture

- (void)visionDidStartVideoCapture:(PBJVision *)vision
{
    DCMSG(@"Start capture");
    isFirstRecord = NO;
    [cameraOverlay setRecording:YES];
    [cameraOverlay toggleCamera];
}

- (void)visionDidPauseVideoCapture:(PBJVision *)vision
{
    DCMSG(@"Pause capture");
    if ([[PBJVision sharedInstance] capturedVideoSeconds] > 1) {
        [cameraOverlay setFirstVideoDone:YES];
    }
    [cameraOverlay setRecording:NO];
    [cameraOverlay toggleCamera];
    DNSLog(@"recorded: %.f",[[PBJVision sharedInstance] capturedVideoSeconds]);
}

- (void)visionDidResumeVideoCapture:(PBJVision *)vision
{
    DCMSG(@"Resume capture");
    [cameraOverlay setFirstVideoDone:YES];
    [cameraOverlay setRecording:YES];
    [cameraOverlay toggleCamera];
}

-(void)visionDidEndVideoCapture:(PBJVision *)vision
{
    DCMSG(@"End capture");
}

- (void)vision:(PBJVision *)vision capturedVideo:(NSDictionary *)videoDict error:(NSError *)error
{
    if (!exit) {
        isFirstRecord = YES;
        DCMSG(videoDict);
        
        if (error && [error.domain isEqual:PBJVisionErrorDomain] && error.code == PBJVisionErrorCancelled) {
            DNSLog(@"recording session cancelled");
            return;
        } else if (error) {
            DNSLog(@"encounted an error in video capture (%@)", error);
            return;
        }
        
        _currentVideo = videoDict;
        
        NSString *videoPath = [_currentVideo  objectForKey:PBJVisionVideoPathKey];
        //    [_assetLibrary writeVideoAtPathToSavedPhotosAlbum:[NSURL URLWithString:videoPath] completionBlock:^(NSURL *assetURL, NSError *error1) {
        //        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Video Saved!" message: @"Saved to the camera roll."
        //                                                       delegate:self
        //                                              cancelButtonTitle:nil
        //                                              otherButtonTitles:@"OK", nil];
        //        [alert show];
        //    }];
        DCMSG(videoDict);
        [[wbAPI sharedAPI] setVideoURL:[NSURL URLWithString:[NSString stringWithFormat:@"file:%@",videoPath]]];
        //    [[self view] setHidden:YES];
        [kROOTVC performSelectorOnMainThread:@selector(startUpload:) withObject:self waitUntilDone:NO];
        //[kROOTVC performSelector:@selector(closeCamera:) withObject:self];
    } else {
        kHIDEWAIT;
    }
}

// progress

- (void)visionDidCaptureAudioSample:(PBJVision *)vision
{
    //    NSLog(@"captured audio (%f) seconds", vision.capturedAudioSeconds);
}

- (void)visionDidCaptureVideoSample:(PBJVision *)vision
{
        DNSLog(@"captured video (%f) seconds", vision.capturedVideoSeconds);
}

///////////////////////
-(void)goBack:(id)selector
{
    UIAlertView *ask = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"GENERAL_WARNING_TITLE", @"") message:NSLocalizedString(@"CAMERA_WARNING_VIDEO_NOT_UPLOADED",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"GENERAL_YES",@"") otherButtonTitles:NSLocalizedString(@"GENERAL_NO",@""), nil];
    if (!isFirstRecord) {
        [ask show];
    } else {
        [self alertView:ask clickedButtonAtIndex:0];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DNSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0) {
        exit = YES;
        
        [cameraOverlay resetView];
        [[wbAPI sharedAPI] clearUploadData];
        [[wbAPI sharedAPI] cleanTmpDir];
        
        [kROOTVC performSelector:@selector(closeCamera:) withObject:self];
    }
}
///////////////////////

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
//    return UIInterfaceOrientationPortrait;
}

- (BOOL) shouldAutorotate
{
    return NO;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


@end
