//
//  wbTvTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/03/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbTvTableView.h"

@implementation wbTvTableView


@synthesize channelId;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        
        [self setHidden:NO];

        super.hideHeader = YES;
        channelArray = [[NSMutableArray alloc] init];

    }
    DPRINTCLASS;
    return self;
}

-(void)setChannelId:(NSInteger)_channelId
{
    DCMSG(@"set channel");
    channelId = _channelId;
    [[wbData sharedData] getTvChannelWithId:channelId delegate:self];
    
}

-(void)gotTvChannel:(NSDictionary*)channelData
{
    DCMSG(@"got channels");
    
    if (channelId == 0) {
        numberOfChannels = [channelData count];
        for (id key in channelData) {
            //NSLog(@"%@ - %@",key,[key class]);
            [channelArray addObject:key];
        }
    } else {
        numberOfChannels = [[channelData objectForKey:@"children"] count];
        [channelArray addObjectsFromArray:[channelData objectForKey:@"children"]];
    }
}

-(void)forceRefresh
{
    [self reloadData];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfChannels;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *identifier = [NSString stringWithFormat:@"%ld%ld",(long)[indexPath row],(long)[indexPath section]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setFrame:CGRectMake(0, 0, self.frame.size.width, kTVCELLH)];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setSeparatorInset:UIEdgeInsetsZero];
        [cell setBackgroundColor:kTOPUICOLOR];
        [cell setClipsToBounds:NO];
        
        UILabel *textLabel = [[UILabel alloc] init];
        [textLabel setText:NSLocalizedString([[channelArray objectAtIndex:[indexPath row]]objectForKey:@"name"],@"")];
        [textLabel setTextColor:kWHITEUICOLOR];
        [textLabel setText:[[textLabel text] uppercaseString]];
        [textLabel setBackgroundColor:kTRANSPARENTUICOLOR];
        
        if ([[[channelArray objectAtIndex:[indexPath row]] objectForKey:@"country"] isKindOfClass:[NSString class]]
            && [[[channelArray objectAtIndex:[indexPath row]] valueForKey:@"channel_type"] integerValue] == 0
            && ![[[channelArray objectAtIndex:[indexPath row]] valueForKey:@"has_parent"] boolValue]) {
            channelType = kCOUNTRYCHANNEL;
        } else if ([[[channelArray objectAtIndex:[indexPath row]] valueForKey:@"channel_type"] integerValue] == 0 && [[[channelArray objectAtIndex:[indexPath row]] valueForKey:@"has_parent"] boolValue]) {
            channelType = kSUBCHANNEL;
        } else {
            channelType = kARTISTCHANNEL;
        }
        
        if (channelType == kCOUNTRYCHANNEL) {
            NSString *countryString = [NSString stringWithFormat:@"%@",[[channelArray objectAtIndex:[indexPath row]] objectForKey:@"country"]];
            BOOL countrySelect;
            if ([countryString isEqualToString:[[[wbAPI sharedAPI] currentCountry] objectForKey:@"country"]] && [[[channelArray objectAtIndex:[indexPath row]] valueForKey:@"channel_type"] integerValue] == 0) {
                countrySelect = YES;
            } else {
                countrySelect = NO;
            }
            [textLabel setFont:kFONT(30)];
            [textLabel sizeToFit];
            [textLabel setFrame:CGRectMake(10, cell.frame.size.height/2 - textLabel.frame.size.height/2, textLabel.frame.size.width, textLabel.frame.size.height)];
 
            UIImageView *right;
            if (countrySelect) {
                right = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"country_select_done.png"]];
            } else {
                right = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"country_select.png"]];
            }
            [cell setAccessoryView:right];
            
//        } else if (channelType == kSUBCHANNEL) {
//            [textLabel setFont:kFONT(20)];
//            [textLabel sizeToFit];
//            [textLabel setFrame:CGRectMake(10, cell.frame.size.height/2 - textLabel.frame.size.height/2, textLabel.frame.size.width, textLabel.frame.size.height)];
// 
//            UIImageView *right = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"country_select.png"]];
//            [cell setAccessoryView:right];
            
        } else { //artist
            [textLabel setFont:kFONT(20)];
            [textLabel sizeToFit];
            [textLabel setFrame:CGRectMake(10, cell.frame.size.height-(textLabel.frame.size.height+10), textLabel.frame.size.width, textLabel.frame.size.height)];
        }
        
        if ([[[channelArray objectAtIndex:[indexPath row]] valueForKey:@"channel_type"] integerValue] == 0) {
       }
        UIImageView *background = [[UIImageView alloc] init];
        [background setImageWithURL:[NSURL URLWithString:[[channelArray objectAtIndex:[indexPath row]] objectForKey:@"picture_url"]]
                      placeholderImage:[UIImage ch_imageNamed:@"placeholder_banner"]];
        [cell addSubview:textLabel];
        [cell setBackgroundView:background];
    }
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTVCELLH+1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *channel = [channelArray objectAtIndex:[indexPath row]];
    if ([[channel objectForKey:@"channel_type"] integerValue] == 0) {
        [kROOTVC performSelector:@selector(startTvChannelView:) withObject:(NSNumber*)[channel objectForKey:@"id"]];
    } else {
        DCMSG(@"open feed");
//        videos_endpoint
        //create feed dictionary
        //NSString *tmpEndpoint = [channel objectForKey:@"videos_endpoint"];
        NSString *endpoint = [channel objectForKey:@"videos_endpoint"];//[tmpEndpoint substringWithRange:NSMakeRange(1, [tmpEndpoint length]-1)];
        
        NSMutableDictionary *feed = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     endpoint ,@"endpoint",
                                     [NSNumber numberWithInteger:0],@"id",
                                     [NSNumber numberWithInteger:0],@"default",
                                     [channel objectForKey:@"name"],@"name",
                                     @"star",@"icon",
                                     @"tv",@"type",
                                     [channel objectForKey:@"picture_url"],@"picture_url",
                                     nil];
        [kROOTVC performSelector:@selector(startNewVideoFeed:feed:) withObject:self withObject:feed];
    }
    
}

@end

