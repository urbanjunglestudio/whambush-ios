//
//  wbLabel.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 18/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbLabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@property BOOL preserveWidth;

- (UIImage*)labelAsImage:(CGSize)_size;

@end
