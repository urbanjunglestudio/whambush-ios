//
//  wbBaseView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 8/29/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbBaseView.h"

@implementation wbBaseView

@synthesize activeField;

//@synthesize _GUI;

@synthesize header;
@synthesize footer;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //[self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setBackgroundColor:kBKGUICOLOR];
    }
    DPRINTCLASS;
    return self;
}

-(void)userLoggedIn
{
    //nothing here
}

//text field
-(UITextField *)giveTxtFieldWithRect:(CGRect)tfRect placeholder:(NSString*)placeholder secure:(BOOL)secure tag:(NSInteger)tag
{
    UITextField *tmp = [self giveTxtFieldWithRect:tfRect placeholder:placeholder secure:secure];
    [tmp setTag:tag];
    return tmp;
}

-(UITextField *)giveTxtFieldWithRect:(CGRect)tfRect placeholder:(NSString*)placeholder secure:(BOOL)secure
{

    UITextField *txtField = [[UITextField alloc]initWithFrame:tfRect];
    //[txtField setBackground:txtFieldImg];
    [txtField setTextColor:kWHITEUICOLOR];
    [txtField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    //[txtField setBackgroundColor:[UIColor yellowColor]];
    [txtField setBackgroundColor:kTOPUICOLORAlpha];
    //[txtField setPlaceholder:placeholder];
    [txtField setBorderStyle:UITextBorderStyleNone];
    [txtField setFont:kFONTHelvetica(18)];
    [txtField setUserInteractionEnabled:YES];
    [txtField setSecureTextEntry:secure];
    [txtField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [txtField setKeyboardType:UIKeyboardTypeEmailAddress];
    [txtField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtField setTintColor:kWHITEUICOLOR];
    [txtField setAutoresizesSubviews:YES];
    [txtField setReturnKeyType:UIReturnKeyNext];
    //UIView *leftView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 10, 1)];
    //[txtField setLeftView:leftView];
    //[txtField setLeftViewMode:UITextFieldViewModeAlways];
    [txtField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [txtField setTextAlignment:NSTextAlignmentCenter];
    [txtField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName:kNOTSOLIGHTGRAYUICOLOR}]];
    //[txtField setLeftViewMode:UITextFieldViewModeNever];
    return txtField;
}

@end
