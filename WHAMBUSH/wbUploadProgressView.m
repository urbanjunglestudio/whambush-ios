//
//  wbUploadProgressView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 23.05.14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbUploadProgressView.h"

@implementation wbUploadProgressView
@synthesize progress;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        progress = 0.01;
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
    }
    return self;
}

-(void)setProgress:(float)_progress
{
    progress = _progress;
    [progressBar setProgress:progress animated:YES];
    DNSLog(@"%f",progress);
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self setAlpha:1.0];
    
    if (cancelButton == nil) {
        cancelButton  = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [cancelButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [cancelButton setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [cancelButton addTarget:self action:@selector(cancelUpload) forControlEvents:UIControlEventTouchUpInside];
    
    if (rotatingCircle == nil) {
        rotatingCircle = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"camera_uploading"]];
    }
    [rotatingCircle setCenter:CGPointMake([[[wbFooter sharedFooter] cameraButton] buttonImageView].center.x-6, [[[wbFooter sharedFooter] cameraButton] buttonImageView].center.y)];
    [self startRotating];

    if (progressBar == nil) {
        progressBar = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    }
    [progressBar setFrame:CGRectMake(0, self.frame.size.height-kSELECTEDIMAGEHEIGHT, self.frame.size.width, kSELECTEDIMAGEHEIGHT)];
    [progressBar setTintColor:kREDUICOLOR];
    [progressBar setProgress:progress];

    [self addSubview:rotatingCircle];
    [self addSubview:progressBar];
    [self addSubview:cancelButton];
}

-(void)startRotating
{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2*M_PI)];
    rotation.duration = 1.1; // Speed
    rotation.repeatCount = INFINITY; // Repeat forever. Can be a finite number.
    [rotatingCircle.layer addAnimation:rotation forKey:@"Spin"];
}

-(void)stopRotating
{
    [rotatingCircle.layer removeAnimationForKey:@"Spin"];
}

-(void)cancelUpload
{
    UIAlertView *ask = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"GENERAL_WARNING_TITLE", @"") message:  NSLocalizedString(@"UPLOAD_CANCEL_VIDEO_UPLOAD",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"GENERAL_YES",@"") otherButtonTitles:NSLocalizedString(@"GENERAL_NO",@""), nil];
    [ask show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self hide];
        [delegate performSelector:@selector(cancelUpload) withObject:NULL];
    }
}

-(void)hide
{
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:1.0];
	
	// set views with new info
    self.alpha = 0.0;
    
	// commit animations
	[UIView commitAnimations];
    
    progress = 0.01;
    [progressBar setProgress:progress animated:YES];
    [self stopRotating];
}

@end
