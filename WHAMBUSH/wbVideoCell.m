//
//  wbVideoCell.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/5/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbVideoCell.h"
#import "AFNetworking.h"

@implementation wbVideoCell

@synthesize cellContent;
@synthesize rankNumber;
@synthesize isTV;

//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
- (id)initWithFrame:(CGRect)frame
{
    //self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        isTV = NO;
        [self setBounds:CGRectMake(0, 0, self.bounds.size.width, 100)];
        [self setBackgroundColor:kBKGUICOLOR];

        videoRect = CGRectMake(0,(self.bounds.size.height-(150.0/16.0)*9.0)/2.0, 150,(150.0/16.0)*9.0);
        
        [[wbData sharedData] setThumbSize:videoRect.size];
        
        if (ai == nil) {
            ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        }
        [ai setCenter:CGPointMake(CGRectGetWidth(videoRect)/2,CGRectGetHeight(self.frame)/2)];
        [ai setHidesWhenStopped:YES];
        [ai startAnimating];
     }
    DPRINTCLASS;
    return self;
}

-(void)setCellContent:(NSMutableDictionary *)_cellContent
{
    cellContent=_cellContent;
    [[wbData sharedData] saveWhambushUser:[_cellContent objectForKey:@"added_by"]];
}

-(void)showWait
{
    [ai setHidden:YES];
}

-(void)hideWait
{
    [ai stopAnimating];
    [ai removeFromSuperview];
}

-(void)setRankNumber:(NSInteger)_rankNumber
{
    rankLabel = [NSString stringWithFormat:@"%ld.",(long)_rankNumber];
    rankNumber = _rankNumber;
}

#define kTVW 20

-(void)drawMe
{
    if (cellContent == nil) {
        DCMSG(cellContent);
    }
    if([self cellContent] != nil){
        [aiEmpty removeFromSuperview];
        if ([[cellContent objectForKey:@"deleted"] boolValue]) {
            
        } else {
            UIImageView *videoThumbViewBkg = [[UIImageView alloc] initWithFrame:videoRect];
            [videoThumbViewBkg setBackgroundColor:kBLACKUICOLOR];
            
            if (videoThumbView == nil) {
                videoThumbView = [[UIImageView alloc] initWithFrame:videoRect];
                [videoThumbView setContentMode:UIViewContentModeScaleAspectFit];
                __weak typeof(self) weakSelf = self;
                
                NSURL *url  = [NSURL URLWithString:[[self cellContent] objectForKey:@"thumbnail_url"]];
                [videoThumbView setImageWithURLRequest:[NSMutableURLRequest requestWithURL:url] placeholderImage:nil success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                    __strong typeof(self) strongSelf = weakSelf;
                    /*if (image != nil) {
                        __strong typeof(self) strongSelf = weakSelf;
                        
                        float tbHeight = strongSelf->videoRect.size.height;
                        float tbWidth = (image.size.width)/(image.size.height/tbHeight);
                        if  (tbWidth == videoRect.size.width) {
                            [strongSelf->videoThumbView setFrame:CGRectMake(0, strongSelf->videoRect.origin.y, tbWidth,tbHeight)];
                        } else {
                            float newX = (videoRect.size.width/2)-(tbWidth/2);
                            [strongSelf->videoThumbView setFrame:CGRectMake(newX, strongSelf->videoRect.origin.y, tbWidth,tbHeight)];
                        }
                    }*/
                    //[videoThumbView setCenter:CGPointMake(videoThumbView.center.x, videoRect.size.height/2)];
                    [strongSelf->videoThumbView setImage:image];
                    [weakSelf hideWait];
                } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                    [weakSelf hideWait];
                }];
                
                /*[videoThumbView setImageWithURL:url completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                    if (image != nil) {
                        float tbHeight = videoRect.size.height;
                        float tbWidth = (image.size.width)/(image.size.height/tbHeight);
                        if  (tbWidth == videoRect.size.width) {
                            [videoThumbView setFrame:CGRectMake(0, videoRect.origin.y, tbWidth,tbHeight)];
                        } else {
                            float newX = (videoRect.size.width/2)-(tbWidth/2);
                            [videoThumbView setFrame:CGRectMake(newX, videoRect.origin.y, tbWidth,tbHeight)];
                        }
                    }
                    //[videoThumbView setCenter:CGPointMake(videoThumbView.center.x, videoRect.size.height/2)];
                    [self hideWait];
                    
                }];*/
            }
            
            UILabel *notProcessedLabel = [[UILabel alloc] initWithFrame:videoRect];
            [notProcessedLabel setText:NSLocalizedString(@"MAIN_VIDEO_IS_BEING_PROCESSED", @"")];
            [notProcessedLabel setTextAlignment:NSTextAlignmentCenter];
            [notProcessedLabel setFont:kFONT(16)];
            [notProcessedLabel setTextColor:kWHITEUICOLOR];
            [notProcessedLabel setNumberOfLines:0];
            //[notProcessedLabel setBackgroundColor:[UIColor yellowColor]];
#define marginal 6
            
            UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(160, 6, self.bounds.size.width-(160+marginal), self.bounds.size.height-12)];
            [containerView setBackgroundColor:kTRANSPARENTUICOLOR];
            [containerView setClipsToBounds:YES];
            
            if (videoTime == nil) {
                videoTime = [[UILabel alloc] initWithFrame:CGRectMake(0, (containerView.frame.size.height-11), containerView.frame.size.width, 10)];
            }
            [videoTime setFont:kFONTHelvetica(9)];
            [videoTime setTextColor:kLIGHTGRAYUICOLOR];
            [videoTime setBackgroundColor:kTRANSPARENTUICOLOR];
            [videoTime setText:[NSString stringWithFormat:@"%@",[[wbAPI sharedAPI] parsePostedTime:[[self cellContent] objectForKey:@"published_at"]]]];
            
            if (videoDescription == nil) {
                videoDescription = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(videoTime.frame), containerView.frame.size.width, 1)];
            }
            [videoDescription setText:[[self cellContent] objectForKey:@"description"]];
            [videoDescription setTextAlignment:NSTextAlignmentLeft];
            [videoDescription setFont:kFONTHelvetica(12)];
            [videoDescription setTextColor:kWHITEUICOLOR];
            [videoDescription setBackgroundColor:kTRANSPARENTUICOLOR];
            [videoDescription setNumberOfLines:2];
            [videoDescription sizeToFit];
            [videoDescription setFrame:CGRectMake(0, CGRectGetMinY(videoTime.frame)-videoDescription.frame.size.height, videoDescription.frame.size.width, videoDescription.frame.size.height)];
            
            if (videoUser == nil) {
                videoUser = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(videoDescription.frame)-12, containerView.frame.size.width, 12)];
            }
            [videoUser setFont:kFONTHelvetica(9)];
            [videoUser setTextColor:kLIGHTGRAYUICOLOR];
            [videoUser setBackgroundColor:kTRANSPARENTUICOLOR];
            [videoUser setText:[NSString stringWithFormat:@"%@ %@ ",[[[self cellContent] objectForKey:@"added_by"] objectForKey:@"username"],NSLocalizedString(@"MAIN_VIEWCOUNT_DELIM", @"")]];
            [videoUser sizeToFit];
            
            UIImageView *viewCountImg = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"viewcount_small.png"]];
            [viewCountImg setFrame:CGRectMake(CGRectGetMaxX(videoUser.frame), CGRectGetMinY(videoDescription.frame)-12, 10, 10)];
            if (viewCount == nil) {
                viewCount = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(viewCountImg.frame)+3, CGRectGetMinY(videoDescription.frame)-12, 1, 12)];
            }
            [viewCount setFont:kFONTHelvetica(9)];
            [viewCount setTextColor:kLIGHTGRAYUICOLOR];
            [viewCount setBackgroundColor:kTRANSPARENTUICOLOR];
            [viewCount setText:[NSString stringWithFormat:@"%@",[[self cellContent] objectForKey:@"view_count"]]];
            [viewCount sizeToFit];
            
            if (videoTitle == nil) {
                videoTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(videoUser.frame)-25, containerView.frame.size.width, 25)];
            }
            [videoTitle setFont:kFONT(20)];
            [videoTitle setTextColor:kTITLEUICOLOR];
            [videoTitle setBackgroundColor:kTRANSPARENTUICOLOR];
            //DCMSG([[[super cellContent] objectForKey:@"mission_id"] description]);
            //DCMSG([[[self cellContent] objectForKey:@"mission_id"] class]);
            
            if (![[[self cellContent] objectForKey:@"mission_id"] isKindOfClass:[NSNull class]]) {
                if (rankLabel == nil) {
                    [videoTitle setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"MAIN_MISSION_PREFIX",@""),[[[self cellContent] objectForKey:@"name"] uppercaseString]]];
                } else {
                    [videoTitle setText:[NSString stringWithFormat:@"%@ %@ %@",rankLabel,NSLocalizedString(@"MAIN_MISSION_PREFIX",@""),[[[self cellContent] objectForKey:@"name"] uppercaseString]]];
                }
                //            [videoTitle setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"MAIN_MISSION_PREFIX",@""),[[[self cellContent] objectForKey:@"name"] uppercaseString]]];
            } else {
                [videoTitle setText:[NSString stringWithFormat:@"%@",[[[self cellContent] objectForKey:@"name"] uppercaseString]]];
            }
            
            [videoTitle setNumberOfLines:1];
            [videoTitle sizeToFit];
            [videoTitle setFrame:CGRectMake(videoTitle.frame.origin.x, videoTitle.frame.origin.y, containerView.frame.size.width, videoTitle.frame.size.height+6)];
            
            //[videoDescription sizeToFit];
            
            //DCMSG([self cellContent]);
            
            [self addSubview:videoThumbViewBkg];
            
            BOOL processed = [[cellContent valueForKey:@"is_processed"] boolValue];
            if (processed) {
                [self addSubview:videoThumbView];
            } else {
                [self addSubview:notProcessedLabel];
                [self hideWait];
            }
            [self addSubview:ai];
            [containerView addSubview:videoTitle];
            [containerView addSubview:videoUser];
            [containerView addSubview:viewCountImg];
            [containerView addSubview:viewCount];
            [containerView addSubview:videoTime];
            [containerView addSubview:videoDescription];
            [self addSubview:containerView];
            
            if (isTV) {
                [videoTime sizeToFit];
                //DCMSG(cellContent);
                UIImage* tvImg = [UIImage ch_imageNamed:@"tv_feed.png"];
                UIImageView *tvImgView = [[UIImageView alloc] initWithImage:tvImg];
                [tvImgView setFrame:CGRectMake(videoRect.size.width-(kTVW+6), videoRect.size.height-(kTVW-2), kTVW, kTVW)];
                [tvImgView setBackgroundColor:kTRANSPARENTUICOLOR];
                [self addSubview:tvImgView];
            }
            
            UIView *comLike = [[UIView alloc] initWithFrame:CGRectMake(0, 6, 80, 16)];
            [comLike setBackgroundColor:kTRANSPARENTUICOLOR];
            
            UIImage *smallBubbleImg = [UIImage ch_imageNamed:@"comments_dark_small.png"];
            UIImageView *smallBubble = [[UIImageView alloc] initWithImage:smallBubbleImg];
            [smallBubble setFrame:CGRectMake(0, 0, 15, 15)];
            wbLabel *comCount = [[wbLabel alloc]initWithFrame:CGRectMake(20, 2, 20, 10)];
            [comCount setFont:kFONT(10)];
            [comCount setEdgeInsets:UIEdgeInsetsMake(3, 0, 0, 0)];
            
            NSString *comCountStr;
            NSInteger comCountInt = [[[self cellContent] valueForKey:@"comment_count"] integerValue];
            if (comCountInt > 999) {
                comCountStr = [NSString stringWithFormat:@"%ld",(long)ABS(comCountInt)/1000];
            } else {
                comCountStr = [NSString stringWithFormat:@"%ld",(long)ABS(comCountInt)];
            }
            [comCount setText:comCountStr];
            [comCount setTextColor:kBKGUICOLOR];
            
            NSInteger displayCount = [[[self cellContent] valueForKey:@"like_count"] integerValue] - [[[self cellContent] valueForKey:@"dislike_count"] integerValue];
            
            UIImageView *smallBanana;
            if (displayCount < 0) {
                UIImage *shitImg = [UIImage ch_imageNamed:@"shit_dark_small.png"];
                smallBanana = [[UIImageView alloc] initWithImage:shitImg];
            } else {
                UIImage *bananaImg = [UIImage ch_imageNamed:@"banana_dark_small.png"];
                smallBanana = [[UIImageView alloc] initWithImage:bananaImg];
            }
            [smallBanana setFrame:CGRectMake(35, 0, 15, 15)];
            
            NSString *countStr;
            if (displayCount > 999) {
                countStr = [NSString stringWithFormat:@"%ldk",(long)ABS(displayCount)/1000];
            } else {
                countStr = [NSString stringWithFormat:@"%ld",(long)ABS(displayCount)];
            }
            
            wbLabel *banCount = [[wbLabel alloc]initWithFrame:CGRectMake(55, 2, 20, 10)];
            [banCount setFont:kFONT(10)];
            [banCount setText:countStr];
            [banCount setTextColor:kBKGUICOLOR];
            [banCount setEdgeInsets:UIEdgeInsetsMake(3, 0, 0, 0)];
            
            [comLike addSubview:smallBubble];
            [comLike addSubview:comCount];
            [comLike addSubview:smallBanana];
            [comLike addSubview:banCount];
            
            [self addSubview:[self giveRibbon:comLike.frame]];
            [self addSubview:comLike];
            
            if (rankNumber > 0 && rankNumber < 4) {
                UIImage *medal = [UIImage ch_imageNamed:[NSString stringWithFormat:@"medal_%ld.png",(long)rankNumber]];
                UIImageView *medalView = [[UIImageView alloc] initWithImage:medal];
                [medalView setFrame:CGRectMake(CGRectGetMaxX(videoThumbView.frame)-(medal.size.width+5), CGRectGetMinY(videoThumbView.frame), medal.size.width, medal.size.height)];
                [medalView setBackgroundColor:kTRANSPARENTUICOLOR];
                [self addSubview:medalView];
            }
         }
    } else {
        DCMSG([self cellContent]);
        
        aiEmpty = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [aiEmpty setCenter:CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2)];
        [aiEmpty startAnimating];
        [self addSubview:aiEmpty];
    }
    
}


- (void)drawRect:(CGRect)rect
{
     [self performSelectorOnMainThread:@selector(drawMe) withObject:NULL waitUntilDone:YES];
}

-(UIImageView*)giveRibbon:(CGRect)frame
{
#define W frame.size.width
#define H frame.size.height
//#define offset 10
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(W,H),0.0,0.0);
    
    //this gets the graphic context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //you can stroke and/or fill
    UIBezierPath *comLikeBkg = [UIBezierPath bezierPath];
    [comLikeBkg moveToPoint:CGPointMake(0, 0)];
    [comLikeBkg addLineToPoint:CGPointMake(W, 0)];
    [comLikeBkg addCurveToPoint:CGPointMake(W-H, H) controlPoint1:CGPointMake(W, H/2) controlPoint2:CGPointMake(W-(H/2), H)];
    [comLikeBkg addLineToPoint:CGPointMake(0, H)];
    [comLikeBkg closePath];
    
    CGContextSetFillColorWithColor(context, kGREENUICOLOR.CGColor);
    [comLikeBkg fill];
    UIImage *bezierImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    UIImageView *bezierImageView = [[UIImageView alloc] initWithImage:bezierImage];
    [bezierImageView setFrame:frame];
    
    comLikeBkg = nil;
    context = nil;
    bezierImage = nil;
    
    return bezierImageView;
}

-(void)dealloc
{
    cellContent = nil;
    viewCount = nil;
    ai = nil;
    aiEmpty = nil;
    videoTitle = nil;
    videoUser = nil;
    videoDescription = nil;
    videoTime = nil;
    videoHash = nil;
    videoUserStr = nil;
    videoDescriptionStr = nil;
    videoTimeStr = nil;
    commentCount = nil;
    bananaCount = nil;
    disbananaCount = nil;
    videoThumbnail = nil;
    videoThumbView = nil;
}

@end
