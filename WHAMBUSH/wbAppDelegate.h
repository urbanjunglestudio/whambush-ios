//
//  wbAppDelegate.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 5/20/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h> 
#import <CoreTelephony/CTCarrier.h>
#import "wbRootViewController.h"
#import "SSKeychain.h"
#import "vzaarAPI.h"
#import <Google/Analytics.h>

@interface wbAppDelegate : UIResponder <UIApplicationDelegate>
{
    //NSString *logfilePath;
    NSString *currentGAIScreen;
    BOOL background;
}
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic) DDFileLogger *fileLogger;

@end
