//
//  wbMissionsTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 06/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseTableView.h"
//#import "wbBaseCell.h"
#import "wbMissionCell.h"

@interface wbMissionsTableView : wbBaseTableView <UITableViewDelegate, UITableViewDataSource,UIScrollViewDelegate>
{
    //BOOL activeMissions;
    UIRefreshControl *refreshController;
    //wbMissionCountrySelectTableView *countrySelect;
    BOOL firstGo;
}


- (id)initWithFrame:(CGRect)frame;


@end
