//
//  wbMission.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbMission.h"

@implementation wbMission

@synthesize delegate;

@synthesize mission_dictionary;
@synthesize created_by;
@synthesize mission_id;
@synthesize title;
@synthesize text;
@synthesize ends;
@synthesize endTimeAString;
@synthesize mission_type;
@synthesize countryCode;
@synthesize is_videos_ranked;
@synthesize bananas;
@synthesize prize;
@synthesize linked_video_id;
@synthesize linked_video_dictionary;
@synthesize submissions_left;
@synthesize max_video_length;
//@synthesize mission_image_1;
//@synthesize mission_image_2;
@synthesize mission_image_1_url;
@synthesize mission_image_2_url;
@synthesize mission_url;
@synthesize mission_rules_url;
@synthesize mission_rules_url_ios;
@synthesize like_count;
@synthesize like_count_string;
@synthesize number_of_submissions;
@synthesize number_of_submissions_string;
@synthesize max_submissions;
@synthesize max_submissions_string;
@synthesize ready;
@synthesize parsedEndTime;
@synthesize has_liked;
@synthesize has_disliked;
@synthesize mission_2_image;
@synthesize status;

-(id) init
{
    self = [super init];
    if (self) {
        ready = NO;
    }
    DPRINTCLASS;
    return self;
}

-(id) initWithId:(NSInteger)missionid
{
    self = [super init];
    if (self) {
        mission_id = [NSNumber numberWithInteger:missionid];
        [self performSelectorInBackground:@selector(updateMissionData) withObject:nil];
    }
    return self;
}

-(id) initWithSlug:(NSString*)missionslug
{
    self = [super init];
    if (self) {
        [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:[NSString stringWithFormat:kMISSIONSLUGAPI,missionslug]]
                                 response:^(id resultArray){ 
                                     [self setMissionWithDictionary:resultArray];
                                 }];
    }
    return self;
}

////////////////////
-(void)updateMissionData
{
    ready = NO;
    [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:[NSString stringWithFormat:@"%@%@/",kACTIVEMISSIONAPI,mission_id]]
                             response:^(id resultArray){
                                 [self setMissionWithDictionary:resultArray];
                             }];
    
}

////////////////////

-(void)setMissionWithDictionary:(NSDictionary*)missionDictionary
{
    //DCMSG(missionDictionary);
    
    mission_dictionary = missionDictionary;
    
    
    created_by = [[wbData sharedData] getWhambushUserWithId:[[[missionDictionary objectForKey:@"added_by"] objectForKey:@"id"] integerValue]];
    
    mission_id = (NSNumber*)[missionDictionary objectForKey:@"id"];
    title = (NSString*)[missionDictionary objectForKey:@"name"];
    text = (NSString*)[missionDictionary objectForKey:@"description"];
    
    ends = [[wbAPI sharedAPI] parseMissionEndTime:(NSString*)[missionDictionary objectForKey:@"end_at"]];
    if (apiTimeFormat == nil) {
        apiTimeFormat = [[NSDateFormatter alloc] init];
        [apiTimeFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [apiTimeFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    }
    endDate = [apiTimeFormat dateFromString:[missionDictionary objectForKey:@"end_at"]];
    startDate = [apiTimeFormat dateFromString:[missionDictionary objectForKey:@"start_at"]];
    
    NSDateFormatter *endFormat = [[NSDateFormatter alloc] init];
    [endFormat setTimeZone:[NSTimeZone localTimeZone]];
    [endFormat setDateFormat:@"HH:mm dd.MM.yyyy"];
    endTimeAString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"MISSION_ENDS_TIME", @""),[endFormat stringFromDate:endDate]] attributes:@{NSFontAttributeName:kFONTHelveticaReg(14),NSForegroundColorAttributeName:kGREENUICOLOR}];
    
    mission_type = (NSNumber*)[missionDictionary objectForKey:@"mission_type"];
    countryCode = (NSString*)[missionDictionary objectForKey:@"country"];
    
    is_videos_ranked = (NSNumber*)[missionDictionary objectForKey:@"is_videos_ranked"];
    
    bananas = (NSNumber*)[missionDictionary objectForKey:@"bananas"];
    prize = (NSString*)[missionDictionary objectForKey:@"prize"];
    
    if (![[missionDictionary objectForKey:@"linked_video"] isKindOfClass:[NSNull class]]) {
        linked_video_id = (NSNumber*)[[missionDictionary objectForKey:@"linked_video"] objectForKey:@"id"];
        linked_video_dictionary = [missionDictionary objectForKey:@"linked_video"];
    }
    
    NSInteger user_submissions = [[missionDictionary objectForKey:@"has_submitted"] integerValue];
    NSInteger max_user_submissions = [[missionDictionary objectForKey:@"max_user_submissions"] integerValue];
    submissions_left = [NSNumber numberWithInteger:ABS(max_user_submissions-user_submissions)];
    
    max_submissions = [missionDictionary objectForKey:@"max_submissions"];
    max_submissions_string = [[wbAPI sharedAPI] valueToString:(NSNumber*)[missionDictionary objectForKey:@"max_submissions"]];
    
    max_video_length = (NSNumber*)[missionDictionary objectForKey:@"max_video_length"];

    mission_image_1_url = [NSURL URLWithString:[missionDictionary objectForKey:@"mission_image_1"]];

    NSInteger like = [[missionDictionary objectForKey:@"like_count"] integerValue];
    NSInteger dislike = [[missionDictionary objectForKey:@"dislike_count"] integerValue];
    
    if ([[missionDictionary objectForKey:@"mission_url"] rangeOfString:@"http"].location == NSNotFound) {  //TODO fix once API works
        mission_url = [NSURL URLWithString:[NSString stringWithFormat:@"https://whambush.com%@",[missionDictionary objectForKey:@"mission_url"]]];
    } else {
        mission_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",[missionDictionary objectForKey:@"mission_url"]]];
    }
    
    mission_image_2_url = [NSURL URLWithString:[missionDictionary objectForKey:@"mission_image_1"]];
    if (linked_video_id != nil && [[mission_image_2_url absoluteString] length] == 0) {
        mission_image_2_url = [NSURL URLWithString:[linked_video_dictionary objectForKey:@"thumbnail_url"]];
        //mission_2_image = [[wbAPI sharedAPI] blur:[UIImage imageWithData:[NSData dataWithContentsOfURL:mission_image_2_url]]];
    }
    
    if ([[missionDictionary objectForKey:@"mission_url"] rangeOfString:@"http"].location == NSNotFound) {  //TODO fix once API works
        mission_rules_url = [NSURL URLWithString:[NSString stringWithFormat:@"https://whambush.com%@/rules",[missionDictionary objectForKey:@"mission_url"]]];
    } else {
        mission_rules_url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/rules",[missionDictionary objectForKey:@"mission_url"]]];
        mission_rules_url_ios = [NSURL URLWithString:[NSString stringWithFormat:@"%@/rules?iOS=true",[missionDictionary objectForKey:@"mission_url"]]];
    }
    
    like_count = [NSNumber numberWithInteger:(like-dislike)];
    
    like_count_string = [[wbAPI sharedAPI] valueToString:like_count];
    
    
    number_of_submissions = (NSNumber*)[missionDictionary objectForKey:@"number_of_submissions"];
    number_of_submissions_string = [[wbAPI sharedAPI] valueToString:(NSNumber*)[missionDictionary objectForKey:@"number_of_submissions"]];

    has_liked = [NSNumber numberWithBool:[[missionDictionary valueForKey:@"has_liked"] boolValue]];
    has_disliked = [NSNumber numberWithBool:[[missionDictionary valueForKey:@"has_disliked"] boolValue]];
    
    status = [[missionDictionary valueForKey:@"status"] uppercaseString];
    
    ready = YES;
}


/*-(NSString*)valueToString:(NSNumber*)value
{
    NSString* valueString;
    if (labs([value integerValue]) > 1000000) {
        valueString = [NSString stringWithFormat:@"%.1fM",(labs([value integerValue])/1000000.0)];
    } else if (labs([value integerValue]) > 1000) {
        valueString = [NSString stringWithFormat:@"%.1fk",(labs([value integerValue])/1000.0)];
    } else {
        valueString = [NSString stringWithFormat:@"%ld",labs([value integerValue])];
    }
    return valueString;
}*/

////////////////////

-(NSAttributedString*)getMissionEndString
{
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:now toDate:endDate options:NSCalendarSearchBackwards ];
    
    NSDictionary *attributes;
    if ([components day] == 0 && [components hour] == 0) {
        attributes = @{NSFontAttributeName:kFONTHelveticaReg(15),NSForegroundColorAttributeName:kREDUICOLOR};
    } else {
        attributes = @{NSFontAttributeName:kFONTHelveticaReg(15),NSForegroundColorAttributeName:kGREENUICOLOR};
    }
    
    if ([components day] > 30) {
        return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ENDS_NOT_ANY_TIME_SOON", @"") attributes:attributes];
    } else if (![self isActive]) {
        return [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ENDS_NOT_RUNNING", @"") attributes:@{NSFontAttributeName:kFONTHelveticaReg(14),NSForegroundColorAttributeName:kREDUICOLOR}];
    }
    
    NSAttributedString *endString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"MISSIONS_TIMER_TEXT", @""),
                           (long)[components day],
                           (long)[components hour],
                           (long)[components minute],
                           (long)[components second]] attributes:attributes];
    return endString;
    
}

-(BOOL)isActive
{
//    if ([submissions_left integerValue] == 0) {
//        return NO;
//    }
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *end = [calendar components:NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:now toDate:endDate options:NSCalendarSearchBackwards ];
    
    NSDateComponents *start = [calendar components:NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:now toDate:startDate options:NSCalendarSearchBackwards ];
    if ([start day] > 0 || [start hour] > 0 || [start minute] > 0 || [start second] > 0) {
        return NO;
    } else if ([end day] < 0 || [end hour] < 0 || [end minute] < 0 || [end second] < 0) {
        return NO;
    } else {
        return YES;
    }
}

////////////////////

-(void)likeMission
{
    [self doliking:@"like/mission/"];
}
-(void)dislikeMission
{
    [self doliking:@"dislike/mission/"];
}
-(void)unlikeMission
{
    [self doliking:@"unlike/mission/"];
}
-(void)undislikeMission
{
    [self doliking:@"undislike/mission/"];
}

-(void)doliking:(NSString*)endpoint //delegate:(id)delegate
{
    [[wbAPI sharedAPI] postJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:endpoint] paramenters:[[NSDictionary alloc] initWithObjectsAndKeys:[mission_id stringValue], @"mission_id", nil]
                              response:^(id data){
                                  [self updateMissionData];
                                  if (delegate != nil && [delegate respondsToSelector:@selector(updateReady)]) {
                                      [delegate performSelector:@selector(updateReady)];
                                  }
                              }
                         errorResponse:^(id errorResp) {
                             DCMSG(errorResp);
                         }];
}

@end
