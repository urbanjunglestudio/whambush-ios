//
//  wbStartView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 23/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbStartView.h"

@implementation wbStartView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:kBKGUICOLOR];
    }
    DPRINTCLASS;
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    if (done == nil) {
        done = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [done setBackgroundImage:[UIImage ch_imageNamed:@"accept_profile_pic"] forState:UIControlStateNormal];
    [done setBackgroundImage:[UIImage ch_imageNamed:@"country_sel_grey"] forState:UIControlStateDisabled];
    [done setEnabled:NO];
    [done setFrame:CGRectMake(0, 0, [done backgroundImageForState:UIControlStateNormal].size.width, [done backgroundImageForState:UIControlStateNormal].size.height)];
    [done setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height-(20+[done backgroundImageForState:UIControlStateNormal].size.height/2))];
    [done addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];

    pickCountry = [[wbCountryPickerView alloc] initWithFrame:[[wbAPI sharedAPI] portraitFrame]];
    [pickCountry setDelegate:self];
    
    if (country == nil) {
        country = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [[country titleLabel] setFont:kFONTHelvetica(15)];
    [country setTitleColor:kWHITEUICOLOR forState:UIControlStateNormal];
    [country setTitleColor:kTOPUICOLOR forState:UIControlStateDisabled];
    [country setTitle:NSLocalizedString(@"GENERAL_PICK_COUNTRY", @"") forState:UIControlStateNormal];
    [country setFrame:CGRectMake(0, 0, self.frame.size.width-60, 50)];
    [country setCenter:CGPointMake(self.frame.size.width/2, CGRectGetMinY(done.frame)-(20+country.frame.size.height/2))];
    [[country layer] setCornerRadius:10];
    [[country layer] setBorderColor:kGREENUICOLOR.CGColor];
    [[country layer] setBorderWidth:1.0];
    [country setClipsToBounds:YES];
    [country addTarget:pickCountry action:@selector(show) forControlEvents:UIControlEventTouchUpInside];
    [country setEnabled:NO];
    
    if (ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    }
    [ai setCenter:country.center];
    [ai setHidden:NO];
    [ai startAnimating];
    
    UIImageView *arrow = [[UIImageView alloc] init];
    [arrow setImage:[UIImage ch_imageNamed:@"arrow_down_white"]];
    [arrow setFrame:CGRectMake(country.frame.size.width-([arrow image].size.width/2 + country.frame.size.height/2), 0, [arrow image].size.width, [arrow image].size.height)];
    [arrow setCenter:CGPointMake(arrow.center.x, country.frame.size.height/2)];
    [country addSubview:arrow];
    
    wbLabel *txt = [[wbLabel alloc] init];
    [txt setFrame:CGRectMake(0, 0, self.frame.size.width-60, 100)];
    [txt setText:NSLocalizedString(@"GENERAL_WELCOME_CHOOSE_COUNTRY_TXT", @"")];
    [txt setFont:kFONTHelvetica(15)];
    [txt setNumberOfLines:0];
    [txt setBackgroundColor:kTRANSPARENTUICOLOR];
    [txt setTextColor:kWHITEUICOLOR];
    [txt setTextAlignment:NSTextAlignmentCenter];
    [txt setPreserveWidth:YES];
    [txt sizeToFit];
    [txt setCenter:CGPointMake(self.frame.size.width/2, CGRectGetMinY(country.frame)-(20+txt.frame.size.height/2))];
   
    UILabel *title = [[UILabel alloc] init];
    [title setText:[NSLocalizedString(@"GENERAL_WELCOME_CHOOSE_COUNTRY", @"") uppercaseString]];
    [title setFont:kFONT(22)];
    [title setNumberOfLines:1];
    [title setBackgroundColor:kTRANSPARENTUICOLOR];
    [title setTextColor:kWHITEUICOLOR];
    [title sizeToFit];
    [title setCenter:CGPointMake(self.frame.size.width/2, CGRectGetMinY(txt.frame)-(12+title.frame.size.height/2))];
    
    UIImageView *logo = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"logo"]];
    [logo setBackgroundColor:kTRANSPARENTUICOLOR];
    [logo setCenter:CGPointMake(self.frame.size.width/2, 20+[logo image].size.height/2)];
   
    UIImageView *gorilla = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"country_select_image"]];
    [gorilla setBackgroundColor:kTRANSPARENTUICOLOR];
    CGFloat width = [gorilla image].size.width;
    CGFloat height = [gorilla image].size.height;
    if (height > (CGRectGetMinY(title.frame)-CGRectGetMinY(logo.frame)-20)) {
        CGFloat heightNew = CGRectGetMinY(title.frame)-CGRectGetMinY(logo.frame)-20;
        CGFloat widthNew = heightNew * width/height;
        height = heightNew;
        width = widthNew;
    }
    [gorilla setFrame:CGRectMake(0, (CGRectGetMinY(title.frame)-20)-height, width, height)];
    [gorilla setCenter:CGPointMake(self.frame.size.width/2, gorilla.center.y)];
    
    [self addSubview:gorilla];
    //[self addSubview:logo];
    [self addSubview:title];
    [self addSubview:txt];
    [self addSubview:country];
    [self addSubview:ai];
    [self addSubview:done];
    [self addSubview:pickCountry];

    kHIDEWAIT;
}

-(void)gotCountries
{
    [country setEnabled:YES];
    [ai setHidden:YES];
    [ai stopAnimating];
}

-(void)setSelectedCountry:(NSDictionary*)selectedCountry
{
    countryDictionary = selectedCountry;
    [country setTitle:NSLocalizedString([selectedCountry objectForKey:@"name"],@"") forState:UIControlStateNormal];
    [done setEnabled:YES];
}

-(void)doneAction:(id)sender
{
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
        [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
    }];
    [op setCompletionBlock:^(void){
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self setHidden:YES];
        });
        [[wbAPI sharedAPI] saveSettingWithKey:@"country" andData:countryDictionary];
        [[wbAPI sharedAPI] setCurrentCountry:countryDictionary];

        [[wbAPI sharedAPI] performSelectorOnMainThread:@selector(tryLogin) withObject:nil waitUntilDone:NO];
    }];
    [op start];
}

@end
