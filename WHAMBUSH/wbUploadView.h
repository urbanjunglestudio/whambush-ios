//
//  wbUploadView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/27/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "wbBaseView.h"
#import "wbUser.h"
#import "wbInsertTextView.h"
#import "wbTagsView.h"
#import "wbUpload.h"
#import "wbUploadMissionSelectView.h"
#import "wbVideoPlayerViewController.h"

@interface wbUploadView : wbBaseView <UIAlertViewDelegate,UIScrollViewDelegate>
{
    UIViewController *controller;
    BOOL guestTriedToUploadMission;
    BOOL isMission;
    NSDictionary *missionData;
    UIScrollView *content;
    
    wbButton *chooseMission;
    NSString *chooseMissionString;
    wbUploadMissionSelectView *missionSelectView;
    
    NSDictionary *activeMissionData;
    
    wbButton *addTitle;
    BOOL titleSet;
    NSString *title;
    NSMutableArray *tagStrings; //strings
    NSInteger currentTag;
    wbTagsView *tagView;
    wbLabel *addDescription;
    UIButton *addDescriptionBtn;
    BOOL descriptionSet;
    NSString *description;
    UILabel *mandatory;
    NSString *whosTurn;
    wbInsertTextView *insertTxtView;
    
    //UIWebView *videoWeb;
    wbVideoPlayerViewController *videoPlayerView;
    
    UITextView *descriptionTxt;
    UITextField *tagTxt;
    UITextField *titleTxt;
    UIButton *descriptionButton;
    BOOL uploaded;
    BOOL saved;
    
    //UISwitch *partnerVideoSwitch;
    /*UIButton *partnerVideoSwitchButton;
    BOOL partnerVideoSwitchValue;
    wbLabel *partnerVideoLabel;
    NSString *partnerVideoLabelString;*/
}

- (id)initWithFrame:(CGRect)frame controller:(UIViewController*)_controller;

//@property (nonatomic,retain) UIWebView *videoWeb;
//@property (nonatomic,retain) UITextView *descriptionTxt;
//@property (nonatomic,retain) UITextField *tagTxt;
//@property (nonatomic,retain) UITextField *titleTxt;
//@property (nonatomic,retain) UIButton *descriptionButton;
//@property (readonly) BOOL uploaded;

//@property (nonatomic,retain) UITextField *activeField;

//-(void)upload:(id)selector;

@property (nonatomic,retain) NSDictionary *uploadData;

@end
