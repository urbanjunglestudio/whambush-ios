//
//  wbActivityTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseTableView.h"
#import "wbActivityCell.h"
#import "wbBaseCell.h"

@interface wbActivityTableView : wbBaseTableView <UITableViewDataSource,UITableViewDelegate>
{
    UIActivityIndicatorView *ai;
    UIRefreshControl *refreshController;
    BOOL allRead;
}
@end
