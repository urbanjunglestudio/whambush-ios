//
//  wbFollowListCell.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbFollowListCell.h"

@implementation wbFollowListCell

@synthesize delegate;
@synthesize indexPath;
@synthesize hideWhenUnfollowPressed;
@synthesize user;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        hideWhenUnfollowPressed = NO;
        [self setBackgroundColor:kBKGUICOLOR];
    }
    DPRINTCLASS;
    return self;
}

-(void)setUser:(wbUser *)_user
{
    user = _user;
    isFollowing = [user is_following];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if ([user userReady]) {
        [ai removeFromSuperview];
        ai = nil;
        // Drawing code
        //profile picture
        UIImageView *profilePicImageView = [[UIImageView alloc] init];//WithImage:[user profilePicture]];//[user profilePictureUserViewSmall];
        [profilePicImageView setFrame:CGRectMake(0, 0,kFOLLOWLINEH-2,kFOLLOWLINEH-2)];
        [user userProfilePictureImageView:profilePicImageView];
        
        [profilePicImageView setCenter:CGPointMake(self.frame.size.height/2.0, self.frame.size.height/2.0)];
        [profilePicImageView setBackgroundColor:kTRANSPARENTUICOLOR];
        [self addSubview:profilePicImageView];
        
        //username
        wbLabel *usernameLabel = [[wbLabel alloc] initWithFrame:CGRectMake(kFOLLOWLINEH+10, 0, self.frame.size.width+kFOLLOWLINEH, 0)];
        [usernameLabel setText:/*[*/[user username] /*uppercaseString]*/];
        [usernameLabel setFont:kFONTHelvetica(15)];
        [usernameLabel setBackgroundColor:kTRANSPARENTUICOLOR];
        [usernameLabel setTextColor:kWHITEUICOLOR];
        [usernameLabel setEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
        [usernameLabel setPreserveWidth:YES];
        [usernameLabel sizeToFit];
        [usernameLabel setCenter:CGPointMake(usernameLabel.center.x, self.frame.size.height/2)];
        [self addSubview:usernameLabel];
        wbButton *usernameLabelButton = [wbButton buttonWithType:UIButtonTypeSystem];
        [usernameLabelButton setFrame:usernameLabel.frame];
        [usernameLabelButton setBackgroundColor:kTRANSPARENTUICOLOR];
        [usernameLabelButton addTarget:self action:@selector(openFeed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:usernameLabelButton];
        
        //followButton
        if (followButton == nil) {
            followButton = [wbRoundedButton buttonWithType:UIButtonTypeSystem];
        }
        [followButton setFilled:YES];
        [followButton setFontSize:16];
        [followButton setHelvetica:NO];
        if (isFollowing) {
            [followButton setTitle:[NSLocalizedString(@"GENERAL_UNFOLLOW_BUTTON", @"") capitalizedString] forState:UIControlStateNormal];
            [followButton setButtonColor:kREDUICOLOR];
        } else {
            [followButton setTitle:[NSLocalizedString(@"GENERAL_FOLLOW_BUTTON", @"") capitalizedString] forState:UIControlStateNormal];
            [followButton setButtonColor:kGREENUICOLOR];
        }
        [followButton.titleLabel setNumberOfLines:1];
        [followButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [followButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [followButton sizeToFit];
        [followButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [followButton setCenter:CGPointMake(self.frame.size.width-(15+followButton.frame.size.width/2),(self.frame.size.height/2))];
        [followButton addTarget:self action:@selector(toggleFollow) forControlEvents:UIControlEventTouchUpInside];
        if (![[[wbUser sharedUser] username] isEqualToString:[user username]]) {
            [self addSubview:followButton];
        }
    } else {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [ai startAnimating];
        [ai setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
        [self addSubview:ai];
        [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(setNeedsDisplay) userInfo:nil repeats:NO];
    }
}

-(void)toggleFollow
{
    if ([[wbAPI sharedAPI] is_guest]) {
        [[wbLoginRegisterViewController sharedLRVC] showAskToLoginRegisterOnView:[[[wbAPI sharedAPI] currentViewController] view]];
    } else {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            [self performSelectorOnMainThread:@selector(toggleFollowMainThread) withObject:NULL waitUntilDone:YES];
            [self performSelectorOnMainThread:@selector(updateUserData) withObject:NULL waitUntilDone:YES];
            kHIDEWAIT
        }];
        
        [op start];
    }
}

-(void)toggleFollowMainThread
{
    
    [followButton setUserInteractionEnabled:NO];
    if (isFollowing) {
        isFollowing = NO;
        [[wbAPI sharedAPI] unfollowUser:user delegate:nil];
        if (hideWhenUnfollowPressed) {
            [delegate performSelector:@selector(removeLine:) withObject:indexPath];
        } else {
            [followButton setTitle:[NSLocalizedString(@"GENERAL_FOLLOW_BUTTON", @"") uppercaseString] forState:UIControlStateNormal];
            [followButton setButtonColor:kGREENUICOLOR];
        }
    } else {
        isFollowing = YES;
        [[wbAPI sharedAPI] followUser:user delegate:nil];
        [followButton setTitle:[NSLocalizedString(@"GENERAL_UNFOLLOW_BUTTON", @"") uppercaseString] forState:UIControlStateNormal];
        [followButton setButtonColor:kREDUICOLOR];
    }
    [user setIs_following:isFollowing];
    
    [followButton sizeToFit];
    [followButton setCenter:CGPointMake(self.frame.size.width-(15+followButton.frame.size.width/2),self.frame.size.height/2)];
    [followButton setNeedsDisplay];
    [followButton setUserInteractionEnabled:YES];
    
    [delegate performSelector:@selector(update)];
    
}

-(void)updateUserData
{
    [user updateUserData];
    //[delegate performSelectorOnMainThread:@selector(updateCounts) withObject:NULL waitUntilDone:YES];
}

-(void)openFeed:(id)sender
{
    if (![[user username] isEqualToString:[[wbUser sharedUser] username]]) {
        //[delegate performSelector:@selector(openFeed:) withObject:[user userFeed]];
        [kROOTVC performSelector:@selector(startUserPage:user:) withObject:nil withObject:user];
    }
}

@end
