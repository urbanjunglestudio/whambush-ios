//
//  wbSettingsViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 12/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseViewController.h"
#import "wbSettingsTableView.h"

@interface wbSettingsViewController : wbBaseViewController

@end
