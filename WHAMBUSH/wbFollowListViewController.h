//
//  wbFollowListViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseViewController.h"
#import "wbFollowListTableView.h"

@interface wbFollowListViewController : wbBaseViewController    

@property (nonatomic,retain) wbUser *user;
@property (nonatomic) BOOL isFollowers;

-(void)update;

@end
