//
//  wbTvViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/03/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"
#import "wbTvTableView.h"

@interface wbTvViewController : wbBaseViewController

@property NSInteger openChannelWithId;

@end
