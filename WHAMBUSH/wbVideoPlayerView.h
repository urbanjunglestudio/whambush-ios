//
//  wbVideoPlayerView
//  VideoApp
//
//  Created by Jari Kalinainen on 24/03/14.
//  Copyright (c) 2014 TofuHead. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbVideoPlayerView : UIView
{
    UIImage *playImg;
    UIImage *pauseImg;
    UIImage *playRedImg;
    UIImage *pauseRedImg;
    UIImage *sliderImg;
    UIButton *playPauseButton;
    UIImageView *playPauseImgView;
    UIProgressView *progressBar;
    UIProgressView *bufferBar;
    UILabel *bufferLabel;
    UISlider *progressSlider;
    NSTimer *hideTimer;
    UILabel *durationZero;
    UILabel *durationFull;
    UILabel *durationProgress;
    UIImage *shareImg;
    UIButton *shareButton;
//    UIImageView *thumbView;
    
}

@property id controller;
@property BOOL portrait;
@property (nonatomic) BOOL playing;
@property (nonatomic) float progress;
@property (nonatomic) float buffered;
@property (nonatomic) BOOL readyToPlay;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) BOOL showShare;
@property (nonatomic) BOOL isRed;
@property (nonatomic,retain) NSURL *thumbUrl;
@property (nonatomic,retain) UIImageView *thumbView;

@end
