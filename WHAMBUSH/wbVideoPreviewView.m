//
//  wbVideoPreviewView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 24.06.14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbVideoPreviewView.h"

@implementation wbVideoPreviewView

@synthesize videoUrl;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        // Initialization code
        hideView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [hideView setBackgroundColor:kTRANSPARENTUICOLOR];
        [hideView setAlpha:0.97];
        [hideView setBarStyle:UIBarStyleBlack];
        
        player = [[wbVideoPlayerViewController alloc] init];
#define kPREVIEWDIVIDER 1.5
        [player setViewRect:CGRectMake((self.frame.size.width/2)-(self.frame.size.width/kPREVIEWDIVIDER)/2, (self.frame.size.height/2)-(self.frame.size.height/kPREVIEWDIVIDER)/2, self.frame.size.width/kPREVIEWDIVIDER, self.frame.size.height/kPREVIEWDIVIDER)];
    
        UIImage *exitImg = [UIImage ch_imageNamed:@"cancel.png"];
        exitButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [exitButton setBackgroundImage:exitImg forState:UIControlStateNormal];
        [exitButton setFrame:CGRectMake(0, 0, exitImg.size.width, exitImg.size.height)];
        [exitButton setCenter:CGPointMake(CGRectGetMinX(player.viewRect), CGRectGetMinY(player.viewRect))];
        [exitButton addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return self;
}

-(void)setVideoUrl:(NSURL *)_videoUrl
{
    [player setVideoURL:_videoUrl];
}

- (void)drawRect:(CGRect)rect
{
    [self addSubview:hideView];
    [self addSubview:player.view];
    [self addSubview:exitButton];
}

-(void)hide
{
    [delegate performSelector:@selector(hidePreview)];
}


@end
