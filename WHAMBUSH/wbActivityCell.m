//
//  wbActivityCell.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbActivityCell.h"

@implementation wbActivityCell

@synthesize data;
@synthesize allRead;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        isGuest = NO;
        isRead = NO;
        allRead = NO;
    }
    DPRINTCLASS;
    return self;
}

-(void)setData:(NSDictionary *)_data
{
    data = _data;
    if ([[[data objectForKey:@"user"] objectForKey:@"user_type"] integerValue] == 10) {
        isGuest = YES;
        who = nil;
    } else {
        isGuest = NO;
        who = [[wbData sharedData] getWhambushUser:[[data objectForKey:@"user"] objectForKey:@"url"]];
    }
    
    if ([[data objectForKey:@"is_read"] boolValue] == YES || allRead == YES) {
        isRead = YES;
    } else {
        isRead = NO;
    }
    
    if ([[data objectForKey:@"action"] isEqualToString:@"liked"]) {
        action = 0;
        if (isGuest) {
            activityString = NSLocalizedString(@"USER_ACTIVITY_LIKED_GUEST", @"");
        } else {
            activityString = NSLocalizedString(@"USER_ACTIVITY_LIKED", @"");
        }
    } else if ([[data objectForKey:@"action"] isEqualToString:@"followed"]) {
        action = 1;
        activityString = NSLocalizedString(@"USER_ACTIVITY_FOLLOWED", @"");
    } else {
        action = 2;
        activityString = NSLocalizedString(@"USER_ACTIVITY_COMMENTED", @"");
    }
    
}

#define kACTIVITYMARGIN 20

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    if (profilePicture == nil) {
        profilePicture = [[UIImageView alloc] init];
    }
    [profilePicture setFrame:CGRectMake(kACTIVITYMARGIN, 0, 60, 60)];
    if (!isGuest) {
        //[profilePicture setImage:[who profilePicture]];
        [who userProfilePictureImageView:profilePicture];
    } else {
        [profilePicture setImage:[UIImage ch_imageNamed:@"sheep.png"]];
    }
    [profilePicture setCenter:CGPointMake(profilePicture.center.x, self.frame.size.height/2)];
    [self addSubview:profilePicture];
    
    
    if (userButton == nil) {
        userButton = [wbButton buttonWithType:UIButtonTypeSystem];
        [userButton setFrame:CGRectMake(0, 0, 0, 0)];
        if (!isGuest) {
            [who getUserButton:userButton fontSize:14];
            [userButton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 3, 0)];
            [userButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, -4, 1)];
            [userButton sizeToFit];
            [userButton setBackgroundColor:kBKGUICOLOR];
        }
    }
    
    if (activityLabel == nil) {
        activityLabel = [[wbLabel alloc] init];
    }
    if (isGuest) {
        [activityLabel setText:[NSString stringWithFormat:@"%@",activityString]];
        [activityLabel setTextColor:kWHITEUICOLOR];
        [activityLabel setFont:kFONTHelvetica(14)];
    } else {
        NSMutableAttributedString *username = [[NSMutableAttributedString alloc] initWithAttributedString:[userButton attributedTitleForState:UIControlStateNormal]];
        [username appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %@",activityString] attributes:[NSDictionary dictionaryWithObjectsAndKeys:kFONTHelvetica(14),NSFontAttributeName,kWHITEUICOLOR,NSForegroundColorAttributeName, nil]]];
        [activityLabel setAttributedText:username];
    }
    [activityLabel setFrame:CGRectMake(0, userButton.frame.origin.y, self.frame.size.width-(CGRectGetMaxX(profilePicture.frame)+kACTIVITYMARGIN*2), 0)];
    [activityLabel setEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [activityLabel setPreserveWidth:YES];
    [activityLabel setNumberOfLines:2];
    [activityLabel sizeToFit];

    if (postedLabel == nil) {
        postedLabel = [[wbLabel alloc] init];
    }
    [postedLabel setText:[[wbAPI sharedAPI] parsePostedTime:[data objectForKey:@"created_at"]]];
    [postedLabel setFont:kFONTHelvetica(12)];
    [postedLabel setTextColor:kNOTSOLIGHTGRAYUICOLOR];
    [postedLabel setFrame:CGRectMake(0,CGRectGetMaxY(activityLabel.frame)+2, 0, 0)];
    [postedLabel sizeToFit];
    
    if (textContainer == nil) {
        textContainer = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(profilePicture.frame)+kACTIVITYMARGIN, 0, self.frame.size.width-(CGRectGetMaxX(profilePicture.frame)+kACTIVITYMARGIN*2),CGRectGetMaxY(postedLabel.frame))];
    }
    [textContainer setClipsToBounds:YES];
    [textContainer addSubview:activityLabel];
    [textContainer addSubview:userButton];
    [textContainer addSubview:postedLabel];
    [textContainer setCenter:CGPointMake(textContainer.center.x, self.frame.size.height/2)];
    [self addSubview:textContainer];
    
    if (!isRead) {
        if (unreadDot == nil) {
            unreadDot = [[UIImageView alloc] initWithImage:[[wbAPI sharedAPI] doDotWithColor:kREDUICOLOR border:NO]];
        }
        [unreadDot setFrame:CGRectMake(CGRectGetMinX(profilePicture.frame)-profilePicture.frame.size.width/6, CGRectGetMinY(profilePicture.frame), profilePicture.frame.size.width/6, profilePicture.frame.size.width/6)];
        [unreadDot setBackgroundColor:kTRANSPARENTUICOLOR];
        [self addSubview:unreadDot];
    }
    
}

//-(UIImage*)doDotWithColor:(UIColor*)color border:(BOOL)border
//{
//    UIGraphicsBeginImageContextWithOptions(CGSizeMake(15,15),0.0,0.0);
//    UIBezierPath *circle = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 15, 15)];
//    [color setFill];
//    [circle fill];
//    if (border) {
//        [kTOPUICOLOR setStroke];
//        [circle setLineWidth:0.5];
//        [circle stroke];
//    }
//    UIImage *bezierImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    return bezierImage;
//}


@end
