//
//  wbSingleVideoView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/6/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbSingleVideoView.h"

@implementation wbSingleVideoView

@synthesize videoPlayerController;

#define kCOMMENTROW ([indexPath row]-4)

- (id)initWithFrame:(CGRect)frame controller:(UIViewController*)_controller content:(NSMutableDictionary*) contentA
{
    self = [super initWithFrame:frame];
    if (self) {
        controller = _controller;
        contentArray = contentA;
        [[wbData sharedData] saveWhambushUser:[contentArray objectForKey:@"added_by"]];
        
        if (![[contentArray objectForKey:@"mission_id"] isKindOfClass:[NSNull class]]) {
            mission = [[wbMission alloc] initWithId:[[contentArray objectForKey:@"mission_id"] integerValue]];
        }
        
        if ([[contentArray objectForKey:@"video_type"] integerValue] == 2) {
            [[wbHeader sharedHeader] setLogo:@"_tv_"];
            [(wbBaseViewController*)controller setHeaderLogo:@"_tv_"];
        }
        
        DCMSG(contentArray);

        charCount = 0;
        videoRowHeight = (self.bounds.size.width/16)*9;
        descriptionRowHeight = 50;
        moreRowHeight = 0;
        fclRowHeight = 40.0;
        videoWebLoaded = NO;
        
        commentCount = [[contentArray valueForKey:@"comment_count"] integerValue];
        
        likeCount = [[contentArray valueForKey:@"like_count"] integerValue];
        dislikeCount = [[contentArray valueForKey:@"dislike_count"] integerValue];
        
        has_liked = [[contentArray valueForKey:@"has_liked"] boolValue];
        has_disliked = [[contentArray valueForKey:@"has_disliked"] boolValue];
        
        content = [[UITableView alloc] initWithFrame:CGRectMake(0, 1, [self frame].size.width, [self frame].size.height)];
        [content setBackgroundColor:kTRANSPARENTUICOLOR];
        [content setDelegate:self];
        [content setDataSource:self];
        [content setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [content setCanCancelContentTouches:NO];
        UIView *tableFooter =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 50)];
        [tableFooter setBackgroundColor:kTRANSPARENTUICOLOR];
        UIActivityIndicatorView *commentai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [commentai setCenter:CGPointMake(tableFooter.frame.size.width/2, tableFooter.frame.size.height/2)];
        [commentai startAnimating];
        [tableFooter addSubview:commentai];
        [content setTableFooterView:tableFooter];

        flagSelect = [[wbFlagSelectionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];

        insertTxtView = [[wbInsertTextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [insertTxtView setDelegate:self];
        [insertTxtView setMaxCharCount:150];
        [insertTxtView setMaxLinesCount:6];
        [insertTxtView setHeaderText:NSLocalizedString(@"SINGLE_ADD_COMMENT", @"")];
        
        user = [[wbData sharedData] getWhambushUser:[[contentArray objectForKey:@"added_by"] objectForKey:@"url"]];

        //videoPlayerController = [[wbVideoPlayerViewController alloc] init];
        
        [self setGAIViewName];
        
        if (videoPlayerController == nil) {
            videoPlayerController = [[wbVideoPlayerViewController alloc] init];
        }
        [videoPlayerController setVideoURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://view.vzaar.com/%@/video",[contentArray valueForKey:@"external_id"]]]];
        [videoPlayerController setVideoThumbURL:[NSURL URLWithString:[contentArray valueForKey:@"thumbnail_url"]]];
        [videoPlayerController.view setHidden:NO];
        [(wbVideoPlayerView*)videoPlayerController.view setShowShare:YES];
        [videoPlayerController setDelegate:self];

        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            NSArray *excludedServices = [NSArray arrayWithObjects:
                                         //UIActivityTypePostToFacebook,
                                         //UIActivityTypePostToTwitter,
                                         //UIActivityTypePostToWeibo,
                                         //UIActivityTypeMessage,
                                         //UIActivityTypeMail,
                                         UIActivityTypePrint,
                                         //UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         //UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         //UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop,
                                         nil];
            
            NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"SINGLE_SHARE_MESSAGE", @""),[contentArray objectForKey:@"name"]];
            NSURL *shareURL = [NSURL URLWithString:[contentArray objectForKey:@"web_url"]];
            
            activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[shareString, shareURL] applicationActivities:nil];
            [activityViewController setExcludedActivityTypes:excludedServices];
            [activityViewController setValue:NSLocalizedString(@"SINGLE_SHARE_TITLE", @"") forKey:@"subject"];

    
            
            [self performSelectorOnMainThread:@selector(initialize) withObject:NULL waitUntilDone:YES];
        }];
        [op start];
     
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction:)];
        [self addGestureRecognizer:tapGesture];
        
    }
    DPRINTCLASS;
    return self;
}

-(void)setGAIViewName
{
    NSString *gaString = [NSString stringWithFormat:@"Single:video=%ld",(long)[[contentArray objectForKey:@"id"] integerValue]];
   [[wbAPI sharedAPI] registerGoogleAnalytics:gaString];

}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
- (void)initialize
{
    DMSG;
    // Drawing code

    // description start
    wbLabel *title = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    [title setText:[NSString stringWithFormat:@"%@",[[contentArray objectForKey:@"name"] uppercaseString]]];
    [title setFont:kFONT(22)];
    [title setTextColor:kWHITEUICOLOR];
    [title setEdgeInsets:UIEdgeInsetsMake(6, 6, 0, 0)];
    [title sizeToFit];
    descriptionRowHeight = CGRectGetHeight(title.frame);
    
    wbButton *username = [wbButton buttonWithType:UIButtonTypeSystem];
    [user userButton:username controller:controller txt:YES];
    [username setFrame:CGRectMake(0,CGRectGetMaxY(title.frame), 0, 0)];
    [username setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 2, 0)];
    [username sizeToFit];
    descriptionRowHeight += CGRectGetHeight(username.frame);
    
    UITextView *description = [[UITextView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(username.frame), self.frame.size.width, 0)];
    [description setBackgroundColor:kTRANSPARENTUICOLOR];
    if ([[contentArray objectForKey:@"description"] length] > 0) {
        [description setText:[NSString stringWithFormat:@"%@",[contentArray objectForKey:@"description"]]];
        [description setFont:kFONTHelvetica(15)];
        [description setTextColor:kWHITEUICOLOR];
        [description setDataDetectorTypes:UIDataDetectorTypeLink];
        [description setTextContainerInset:UIEdgeInsetsMake(2, 2, 2, 0)];
        [description setScrollEnabled:NO];
        [description setEditable:NO];
        [description setUserInteractionEnabled:YES];
        [description setTintColor:kGREENUICOLOR];
        [description sizeToFit];
    }
    descriptionRowHeight += CGRectGetHeight(description.frame);

    //tags
    wbLabel *tags = [[wbLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(description.frame), self.frame.size.width, 0)];
    if ([[contentArray objectForKey:@"tags"] length] > 0) {
        NSString *tagString = [[[contentArray objectForKey:@"tags"] uppercaseString] stringByReplacingOccurrencesOfString:@"," withString:@", "];
        [tags setText:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"SINGLE_TAG_PREFIX", @""),tagString]];
        [tags setFont:kFONT(12)];
        [tags setEdgeInsets:UIEdgeInsetsMake(8, 6, 2, 0)];
        [tags setTextColor:kWHITEUICOLOR];
        [tags setNumberOfLines:0];
        [tags sizeToFit];
    }
    descriptionRowHeight += CGRectGetHeight(tags.frame);
    
    wbLabel *posted = [[wbLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tags.frame), self.frame.size.width, 1)];
    [posted setText:[NSString stringWithFormat:@"%@",[[wbAPI sharedAPI] parsePostedTime:[contentArray objectForKey:@"published_at"]]]];
    [posted setFont:kFONTHelvetica(10)];
    [posted setTextColor:kWHITEUICOLOR];
    [posted setEdgeInsets:UIEdgeInsetsMake(2, 6, 6, 0)];
    [posted sizeToFit];
    descriptionRowHeight += CGRectGetHeight(posted.frame);
    
    if (postedButton == nil) {
        postedButton = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    [postedButton.titleLabel setFont:kFONTHelvetica(10)];
    [postedButton setTitleColor:kWHITEUICOLOR forState:UIControlStateNormal];
    [postedButton setTitleColor:kWHITEUICOLOR forState:UIControlStateSelected];
    [postedButton setTitle:[NSString stringWithFormat:@"%@",[[wbAPI sharedAPI] parsePostedTime:[contentArray objectForKey:@"published_at"]]] forState:UIControlStateNormal];
    [postedButton setTitle:[[wbAPI sharedAPI] beautifyTime:[contentArray objectForKey:@"published_at"]] forState:UIControlStateSelected];
    [postedButton setSelected:NO];
    [postedButton setTintColor:kTRANSPARENTUICOLOR];
    [postedButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [postedButton sizeToFit];
    [postedButton setFrame:CGRectMake(6, CGRectGetMaxY(tags.frame), self.frame.size.width, postedButton.frame.size.height)];
    [postedButton addTarget:self action:@selector(togglePosted) forControlEvents:UIControlEventTouchUpInside];
    
    if (descriptionRow == nil) {
        descriptionRow = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, descriptionRowHeight)];
    }
    [descriptionRow setBackgroundColor:kTOPUICOLORAlpha];
    [descriptionRow addSubview:title];
    [descriptionRow addSubview:username];
    [descriptionRow addSubview:description];
    [descriptionRow addSubview:tags];
    [descriptionRow addSubview:postedButton];
    // desription end
    
    [self addSubview:content];
    
//    super.header = [GUI headerBarWithBack:controller];
//    super.footer = [GUI footerBar:controller];
//    [self addSubview:super.header];
//    [self addSubview:super.footer];
    
    [self addSubview:flagSelect];
    [flagSelect setVideoId:[[contentArray valueForKey:@"id"] integerValue]];
    
    [self addSubview:insertTxtView];

    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [refreshControl setBackgroundColor:[UIColor colorWithPatternImage:[UIImage ch_imageNamed:@"refresh_bg_image"]]];
    [content addSubview:refreshControl];
   
    if (commentArray == nil) {
        if (commentCount > 0) {
            //[self performSelectorInBackground:@selector(getComments) withObject:NULL];
            [self performSelector:@selector(getComments) withObject:NULL];
        } else {
            [[[content tableFooterView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        }
    }

    
    DMSG;
    kHIDEWAIT
}

-(void)togglePosted
{
    [postedButton setSelected:![postedButton isSelected]];
}

- (void)refresh:(UIRefreshControl *)refreshControl
{
    if (refreshControl != nil) {
        [refreshControl beginRefreshing];
    }
    
    if ([[wbAPI sharedAPI] checkIfNetworkAvailable]) {
        
        DNSLog(@"1---- rows %ld",(long)[content numberOfRowsInSection:0]);

        //NSInteger count = [commentArray count];
        NSMutableArray *commentRows = [[NSMutableArray alloc] init];
        for (int i = 0; i< commentCount; i++) {
            NSIndexPath *ip = [NSIndexPath indexPathForItem:i+4 inSection:0];
            [commentRows addObject:ip];
        }
        
        DNSLog(@"1---- rows %ld",(long)[content numberOfRowsInSection:0]);
        commentCount = 0;
        commentArray = nil;
        
        [content beginUpdates];
        [content deleteRowsAtIndexPaths:commentRows withRowAnimation:UITableViewRowAnimationAutomatic];
        [content endUpdates];
        
        DCMSG(commentRows);
        
        commentRows = nil;
        DNSLog(@"2---- rows %ld",(long)[content numberOfRowsInSection:0]);
        
        [self getComments];
    }
        
    if (refreshControl != nil) {
        [refreshControl endRefreshing];
        [self setNewContentSize];
    }
}
-(void)refreshVideoCell
{
    [content beginUpdates];
    [content reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [content endUpdates];
}

-(void)saveCommentUsers:(NSArray*)userArray
{
    //DNSLog(@"%@",userArray);
    NSInteger count = [userArray count];
    for (int i = 0; i < count; i++) {
        [[wbData sharedData] saveWhambushUser:[[userArray objectAtIndex:i] objectForKey:@"user"]];
    }
}

-(void)dataReady:(id)data
{
    if (commentArray == nil) {
        commentArray = [NSMutableArray arrayWithArray:[data objectForKey:@"results"]];
        [self performSelectorInBackground:@selector(saveCommentUsers:) withObject:commentArray];
        commentCount = [[data valueForKey:@"count"] integerValue];
        
        [contentArray setObject:[NSNumber numberWithLong:commentCount] forKey:@"comment_count"];
        nextUrl = [data objectForKey:@"next"];
        DCMSG(nextUrl);
        NSMutableArray *newRows = [[NSMutableArray alloc] init];
        for (int i = 0; i < commentCount ; i++) {
            NSIndexPath *ip = [NSIndexPath indexPathForRow:i+4 inSection:0];
            [newRows addObject:ip];
        }
        DCMSG(newRows);
        [content beginUpdates];
        
        if ([content numberOfRowsInSection:0] > 4) {
            [content reloadRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationAutomatic];
        } else {
            DMSG;
            [content insertRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        [content endUpdates];
        [[[content tableFooterView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
    } else {
        NSArray *newResult = [data objectForKey:@"results"];
        
        NSInteger oldCount = [commentArray count];
        
        nextUrl = [data objectForKey:@"next"];
        
        NSMutableArray *newRows = [[NSMutableArray alloc] init];
        for (NSInteger i = (oldCount+4); i < (commentCount+4); i++) {
            NSIndexPath *newReloadRows = [NSIndexPath indexPathForRow:i inSection:0];
            [newRows addObject:newReloadRows];
        }
        
        DCMSG(newRows);
        
        [commentArray addObjectsFromArray:newResult];
        
        [content beginUpdates];
        [content deleteRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationNone];
        [content insertRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationAutomatic];
        [content endUpdates];
        
        [self setNewContentSize];
        
        gettingMoreResults = NO;
    }

    [self setNewContentSize];

}

-(void)getComments
{
    if ([[wbAPI sharedAPI] checkIfNetworkAvailable]) {
        [[wbAPI sharedAPI] getDataWithEndpoint:[NSString stringWithFormat:@"%@video/%ld/",kCOMMENTAPI,(long)[[contentArray valueForKey:@"id"] integerValue]] delegate:self];
    }
}

-(void)setNewContentSize
{
    float contentHeight = 100 + videoRowHeight + fclRowHeight + descriptionRowHeight + commentCount;

    for (int i = 0; i < [commentArray count] ; i++)
    {
        NSString *textString = [[commentArray objectAtIndex:i] objectForKey:@"comment"];
        contentHeight += [self commentHeight:textString];
    }
    
    CGSize newContentSize = CGSizeMake(content.contentSize.width, contentHeight);
    [content setContentSize:newContentSize];
}

-(float)commentHeight:(NSString*)textString
{
    wbLabel *text = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, 255, 700)];
    [text setNumberOfLines:0];
    [text setEdgeInsets:UIEdgeInsetsMake(4, 6, 2, 16)];
    [text setFont:kFONTHelvetica(15)];
    [text setText:textString];
    [text setPreserveWidth:YES];
    [text sizeToFit];
    
    return text.frame.size.height + 30;
    
}

-(void)getMoreComments
{
    if (!gettingMoreResults) {
        if ([[wbAPI sharedAPI] checkIfNetworkAvailable]) {
            gettingMoreResults = YES;
            [[wbAPI sharedAPI] getDataWithURL:nextUrl delegate:self];
        }
    }
}


-(void)addComment
{
    if ([[wbAPI sharedAPI] is_guest]) {
        [[wbLoginRegisterViewController sharedLRVC] showAskToLoginRegisterOnView:self];
    } else {
        [insertTxtView showTextView];
    }
}

-(void)hideKB
{
    [insertTxtView hideTextView];
}

-(void)newText:(NSString*)text  //delegate method for insetTxtView
{
    DMSG;
    if ([text length] > 0) {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            [[wbAPI sharedAPI] addComment:text videoid:[[contentArray valueForKey:@"id"] integerValue] delegate:self];
        }];
        
        [op start];
    }
}

//-(void)goBack
//{
//    insertTxtView = nil;
//    [controller performSelector:@selector(goBack:)];
//}

-(void)scrollTableTop
{
    [content setContentOffset:CGPointZero animated:YES];
}


//Tableview stuff
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4+commentCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == 0) { //video
        return videoRowHeight;
    } else if ([indexPath row] == 1) { // like comment
        return fclRowHeight;
    } else if ([indexPath row] == 2) { // more
        return moreRowHeight;
    } else if ([indexPath row] == 3) { // description
        return descriptionRowHeight;
//    } else if ([indexPath row] == [self tableView:content numberOfRowsInSection:0] - 1) {
//        return 100;
    } else {  //comments
        if  ([commentArray count] > kCOMMENTROW) {
            NSString *textString = [[commentArray objectAtIndex:kCOMMENTROW] objectForKey:@"comment"];
            return [self commentHeight:textString];
        } else {
            return 0;
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *identifier = [NSString stringWithFormat:@"sv_%ld_%ld",(long)indexPath.section,(long)indexPath.row];
    
    //wbBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    //if (cell == nil) {
    wbBaseCell *cell = [[wbBaseCell alloc] init];//WithStyle:UITableViewCellStyleDefault];// reuseIdentifier:identifier];
    //}
    
    if ([commentArray count]-5 == kCOMMENTROW) {
        if ([commentArray count] < commentCount) {
            //[self performSelectorInBackground:@selector(getMoreComments) withObject:NULL];
            [self performSelectorOnMainThread:@selector(getMoreComments) withObject:NULL waitUntilDone:NO];
        }
    }

    if ([indexPath row] == 0) { //video
        //[cell setClipsToBounds:NO];
        [cell addSubview:videoPlayerController.view];
        [cell bringSubviewToFront:videoPlayerController.view];
        [cell setClipsToBounds:YES];
        [cell setHidden:NO];
        DCMSG(cell);
    } else if ([indexPath row] == 1) { // flag like comment
        cell = nil;
        cell = [[wbBaseCell alloc] init];
        [self fclCell:cell];
    } else if ([indexPath row] == 2) { // more
        cell = nil;
        cell = [[wbBaseCell alloc] init];
        [self moreCell:cell];
    } else if ([indexPath row] == 3) { // description
        DCMSG(descriptionRow);
        [cell addSubview:descriptionRow];
        [cell bringSubviewToFront:descriptionRow];
    } else {  //comments
        cell = nil;
        cell = [[wbBaseCell alloc] init];
        [cell setBackgroundColor:kTRANSPARENTUICOLOR];
        if  ([commentArray count] > kCOMMENTROW) {
            wbCommentCell *ccell = [[wbCommentCell alloc] init];
            [ccell setParentView:self];
            [ccell setCellContent:[commentArray objectAtIndex:kCOMMENTROW]];
            [ccell setController:controller];
            [ccell setTotalNumberOfComments:commentCount];
            [ccell setCurrentCommentNumber:kCOMMENTROW+1];
            NSString *textString = [[commentArray objectAtIndex:kCOMMENTROW] objectForKey:@"comment"];
            float height = [self commentHeight:textString];
            [ccell setFrame:CGRectMake(0, 0, self.frame.size.width, height)];
            
            UILongPressGestureRecognizer *longTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longTapGesture:)];
            [ccell addGestureRecognizer:longTap];
            
            [cell addSubview:ccell];
            

        }
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
}


// cells

// like,comment
//-(wbBaseCell*)fclCell:(wbBaseCell *)cell
-(void)fclCell:(wbBaseCell *)cell
{
    [cell setBackgroundColor:kBKGUICOLORAlpha];
    //[cell setSelected:NO];
    //[cell setClipsToBounds:NO];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    UIImage *moreImg = [UIImage ch_imageNamed:@"options.png"];
    if (moreButton == nil) {
        moreButton = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [moreButton setBackgroundImage:moreImg forState:UIControlStateNormal];
    [moreButton setBackgroundImage:[UIImage ch_imageNamed:@"options_active.png"] forState:UIControlStateSelected];
    [moreButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [moreButton setFrame:CGRectMake(cell.frame.size.width-moreImg.size.width, 0, fclRowHeight, fclRowHeight)];
    [moreButton addTarget:self action:@selector(toggleMoreRow) forControlEvents:UIControlEventTouchUpInside];
    [moreButton setTintColor:kTRANSPARENTUICOLOR];
     
    UIImage *addCommentImg = [UIImage ch_imageNamed:@"comment.png"];
    if (addCommentButton == nil) {
        addCommentButton = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    [addCommentButton setFrame:CGRectMake(6, 0, fclRowHeight, fclRowHeight)];
    //[addCommentButton setBackgroundImage:addCommentImg forState:UIControlStateNormal];
    [addCommentButton setCropButtonImage:YES];
    [addCommentButton setButtonImage:addCommentImg];
    [addCommentButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [addCommentButton addTarget:self action:@selector(addComment) forControlEvents:UIControlEventTouchUpInside];
    
    [cell addSubview:addCommentButton];
    [cell addSubview:moreButton];
    
    if(!has_liked) {
        likeBtnImg = [UIImage ch_imageNamed:@"banana_inactive.png"];
    } else {
        likeBtnImg = [UIImage ch_imageNamed:@"banana_active.png"];
    }
    if (likeBtn == nil) {
        likeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [likeBtn setBackgroundImage:likeBtnImg forState:UIControlStateNormal];
    [likeBtn setFrame:CGRectMake(CGRectGetMaxX(addCommentButton.frame)+25, 0, fclRowHeight, fclRowHeight)];
    [likeBtn addTarget:self action:@selector(like) forControlEvents:UIControlEventTouchUpInside];
    
    if(!has_disliked) {
        dislikeBtnImg = [UIImage ch_imageNamed:@"shit_inactive.png"];
    } else {
        dislikeBtnImg = [UIImage ch_imageNamed:@"shit_active.png"];
    }
    if (dislikeBtn == nil) {
        dislikeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [dislikeBtn setBackgroundImage:dislikeBtnImg forState:UIControlStateNormal];
    [dislikeBtn setFrame:CGRectMake(CGRectGetMinX(moreButton.frame)-dislikeBtnImg.size.width-25, 0, fclRowHeight, fclRowHeight)];
    [dislikeBtn addTarget:self action:@selector(dislike) forControlEvents:UIControlEventTouchUpInside];
    
    NSInteger count = likeCount - dislikeCount;
    
    NSString *countStr;
    if (count > 999) {
        countStr = [NSString stringWithFormat:@"%ldk",(long)ABS(count)/1000];
    } else {
        countStr = [NSString stringWithFormat:@"%ld",(long)(ABS(count))];
    }

    UIImageView *likeDislikeCountBubble;
    if (likeDislikeCount == nil) {
        likeDislikeCount = [[wbLabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(likeBtn.frame), 0, CGRectGetMaxX(dislikeBtn.frame)-CGRectGetMinX(likeBtn.frame), fclRowHeight)];
    }
    [likeDislikeCount setBackgroundColor:kTRANSPARENTUICOLOR];
    [likeDislikeCount setTextAlignment:NSTextAlignmentCenter];
    [likeDislikeCount setEdgeInsets:UIEdgeInsetsMake(5, 0, 0, 0)];
    [likeDislikeCount setFont:kFONT(24)];
    [likeDislikeCount setText:countStr];
    if (count < 0) {
        [likeDislikeCount setTextColor:kREDUICOLOR];
        likeDislikeCountBubble = [[wbAPI sharedAPI] giveBubleWithBanana:NO frame:likeDislikeCount.frame];
    } else {
        [likeDislikeCount setTextColor:kGREENUICOLOR];
        likeDislikeCountBubble = [[wbAPI sharedAPI] giveBubleWithBanana:YES frame:likeDislikeCount.frame];
    }
    
    [cell addSubview:likeDislikeCount];
    if (!(likeCount == 0 && dislikeCount == 0)) {
        [cell addSubview:likeDislikeCountBubble];
    } else {
        [likeDislikeCount setTextColor:UIColorFromHexRGB(0x8F8F8F)];
    }
    [cell addSubview:likeBtn];
    [cell addSubview:dislikeBtn];
    if (ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    }
    [ai setFrame:CGRectMake(0, 0, fclRowHeight, fclRowHeight)];
    [ai setHidden:YES];
    [cell addSubview:ai];
    //DCMSG(cell);
    //return cell;
}

-(void)moreCell:(wbBaseCell*)cell
{
    [cell setBackgroundColor:kSEPRATORUICOLORAlpha];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    UIImage *shareImg = [UIImage ch_imageNamed:@"share_inactive.png"];
    shareButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [shareButton setBackgroundImage:shareImg forState:UIControlStateNormal];
    [shareButton setBackgroundColor:kTRANSPARENTUICOLOR];
    //[shareButton setBackgroundColor:kYELLOWUICOLOR];
    [shareButton setFrame:CGRectMake((cell.frame.size.width)-(moreRowHeight), 0,moreRowHeight, moreRowHeight)];
    [shareButton addTarget:self action:@selector(showShareSelect) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *flagImg = [UIImage ch_imageNamed:@"flag_inactive.png"];
    flagButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [flagButton setBackgroundImage:flagImg forState:UIControlStateNormal];
    [flagButton setBackgroundColor:kTRANSPARENTUICOLOR];
    //[flagButton setBackgroundColor:kREDUICOLOR];
    [flagButton setFrame:CGRectMake(CGRectGetMinX(shareButton.frame)-(moreRowHeight+1), 0,moreRowHeight, moreRowHeight)];
    [flagButton addTarget:flagSelect action:@selector(show) forControlEvents:UIControlEventTouchUpInside];

    UIImage *trashImg = [UIImage ch_imageNamed:@"remove_video.png"];
    deleteButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [deleteButton setBackgroundImage:trashImg forState:UIControlStateNormal];
    [deleteButton setBackgroundColor:kTRANSPARENTUICOLOR];
    //[deleteButton setBackgroundColor:kYELLOWUICOLOR];
    [deleteButton setFrame:CGRectMake(CGRectGetMinX(flagButton.frame)-(moreRowHeight+1), 0,moreRowHeight, moreRowHeight)];
    [deleteButton addTarget:self action:@selector(deleteVideo) forControlEvents:UIControlEventTouchUpInside];

    if([[[wbUser sharedUser] userId] integerValue] == [[user userId] integerValue]){
        if ([self showDelete]) {
            [cell addSubview:deleteButton];
        }
    }
    [cell addSubview:shareButton];
    [cell addSubview:flagButton];
}

-(void)showShareSelect
{
    if ([[contentArray valueForKey:@"is_processed"] boolValue]) {
        [controller presentViewController:activityViewController animated:YES completion:NULL/*^{ ...}*/];
        
    }
}

-(BOOL)showDelete
{
    if (mission == nil) {
        return YES;
    } else if ([mission ready]) {
        if ([[mission status] isEqualToString:@"OLD"] && [[mission is_videos_ranked] boolValue]) {
            return YES;
        }
    }
    return NO;
}

-(void)deleteVideo
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"GENERAL_WARNING_TITLE", @"")
                                                    message:NSLocalizedString(@"SINGLE_DELETE_VIDEO", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"GENERAL_NO",@"")
                                          otherButtonTitles:NSLocalizedString(@"GENERAL_YES",@""),nil];
     [alert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            [[wbAPI sharedAPI] removeVideo:[[contentArray objectForKey:@"id"] integerValue] delegate:self];
        }];
        [op start];
        
    }
}

-(void)deleteDone
{
    [[wbUser sharedUser] updateUserData];
    [contentArray setObject:[NSNumber numberWithBool:YES] forKey:@"deleted"];
    //[[[[wbData sharedData] videoCells] objectForKey:(NSNumber*)[contentArray objectForKey:@"id"]] setNeedsDisplay];
    //[[wbData sharedData] refreshFeedData:@"MAIN_FEED_MY_VIDEOS" delegate:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"deleteVideoDone" object:self];
    [kROOTVC performSelectorOnMainThread:@selector(goBack:) withObject:self waitUntilDone:NO];
}


-(void)likeDone:(NSNumber*)who count:(NSNumber*)count
{
    switch ([who integerValue]) {
        case 1: //like
            likeCount = [count integerValue];
            has_liked = YES;
            [contentArray setObject:[NSNumber numberWithInt:1] forKey:@"has_liked"];
            [contentArray setObject:[NSNumber numberWithLong:likeCount] forKey:@"like_count"];
            if (has_disliked) {
                [self dislike];
            }
            break;
        case 2: //unlike
            likeCount = [count integerValue];
            has_liked = NO;
            [contentArray setObject:[NSNumber numberWithInt:0] forKey:@"has_liked"];
            [contentArray setObject:[NSNumber numberWithLong:likeCount] forKey:@"like_count"];
            break;
        case 3: //dislike
            dislikeCount = [count integerValue];
            has_disliked = YES;
            [contentArray setObject:[NSNumber numberWithInt:1] forKey:@"has_disliked"];
            [contentArray setObject:[NSNumber numberWithLong:dislikeCount] forKey:@"dislike_count"];
            if (has_liked) {
                [self like];
            }
            break;
        case 4: //undislike
            dislikeCount = [count integerValue];
            has_disliked = NO;
            [contentArray setObject:[NSNumber numberWithInt:0] forKey:@"has_disliked"];
            [contentArray setObject:[NSNumber numberWithLong:dislikeCount] forKey:@"dislike_count"];
            break;
        default:
            break;
    }
    [content reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    [likeBtn setEnabled:YES];
    [dislikeBtn setEnabled:YES];
    [ai stopAnimating];
    [ai setHidden:YES];
}

-(void)like
{
    DCMSG(@"like");
    if (!has_liked) {
//        likeCount = [[wbAPI sharedAPI] like:YES videoid:[NSString stringWithFormat:@"%@",[contentArray valueForKey:@"id"]]];
        [[wbAPI sharedAPI] like:YES videoid:[[contentArray valueForKey:@"id"] stringValue] delegate:self];
        [likeBtn setEnabled:NO];
        [ai setHidden:NO];
        [ai startAnimating];
        [ai setCenter:likeBtn.center];
    } else {
        //likeCount = [[wbAPI sharedAPI] unlike:YES videoid:[NSString stringWithFormat:@"%@",[contentArray valueForKey:@"id"]]];
        [[wbAPI sharedAPI] unlike:YES videoid:[[contentArray valueForKey:@"id"] stringValue] delegate:self];
        [likeBtn setEnabled:NO];
        [ai setHidden:NO];
        [ai startAnimating];
        [ai setCenter:likeBtn.center];
    }
//    [content reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];

}

-(void)dislike
{
    DCMSG(@"dislike");
    if (!has_disliked) {
        [dislikeBtn setEnabled:NO];
        [ai setHidden:NO];
        [ai startAnimating];
        [ai setCenter:dislikeBtn.center];
//        dislikeCount = [[wbAPI sharedAPI] like:NO videoid:[NSString stringWithFormat:@"%@",[contentArray valueForKey:@"id"]]];
        [[wbAPI sharedAPI] like:NO videoid:[[contentArray valueForKey:@"id"] stringValue] delegate:self];
//        has_disliked = YES;
//        [contentArray setObject:[NSNumber numberWithInt:1] forKey:@"has_disliked"];
//        [contentArray setObject:[NSNumber numberWithLong:dislikeCount] forKey:@"dislike_count"];
//        if (has_liked) {
//            [self like];
//        }
    } else {
//        dislikeCount = [[wbAPI sharedAPI] unlike:NO videoid:[NSString stringWithFormat:@"%@",[contentArray valueForKey:@"id"]]];
        [dislikeBtn setEnabled:NO];
        [ai setHidden:NO];
        [ai startAnimating];
        [ai setCenter:dislikeBtn.center];
        [[wbAPI sharedAPI] unlike:NO videoid:[[contentArray valueForKey:@"id"] stringValue] delegate:self];
//        has_disliked = NO;
//        [contentArray setObject:[NSNumber numberWithInt:0] forKey:@"has_disliked"];
//        [contentArray setObject:[NSNumber numberWithLong:dislikeCount] forKey:@"dislike_count"];
    }
//    [content reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}

-(void)toggleMoreRow
{
    if (moreRowOpen) {
        moreRowHeight = 0;
        moreRowOpen = NO;
        [moreButton setSelected:NO];
        [moreButton setBackgroundColor:kTRANSPARENTUICOLOR];
    } else {
        moreRowHeight = fclRowHeight;
        moreRowOpen = YES;
        [moreButton setSelected:YES];
        [moreButton setBackgroundColor:kSEPRATORUICOLORAlpha];
    }
    [content reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
}



/*-(UIImageView*)giveBubleWithBanana:(BOOL)banana frame:(CGRect)frame
{
    // variables to make things happend
    float width = frame.size.width-25-frame.size.height;
    float height = frame.size.height-6;
    float xx = 25+frame.size.height;
    float yy = 6;
    float rr = height/3.0;
    //DNSLog(@"%f, %f, %f, %f, %f",width,height,xx,yy,rr);
    //start graphic context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(frame.size.width,frame.size.height),0.0,0.0);
    
    //this gets the graphic context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //you can stroke and/or fill
    UIBezierPath *countBkg = [UIBezierPath bezierPath];
    
    //draw "buble"
    [countBkg moveToPoint:CGPointMake(xx+rr, yy)];
    [countBkg addLineToPoint:CGPointMake(width-rr, yy)];
    [countBkg addCurveToPoint:CGPointMake(width, yy+rr) controlPoint1:CGPointMake(width-rr/2,yy) controlPoint2:CGPointMake(width, yy+rr/2)];
    [countBkg addLineToPoint:CGPointMake(width, height-rr)];
    [countBkg addCurveToPoint:CGPointMake(width-rr, height) controlPoint1:CGPointMake(width,height-rr/2) controlPoint2:CGPointMake(width-rr/2, height)];
    [countBkg addLineToPoint:CGPointMake(xx+rr, height)];
    [countBkg addCurveToPoint:CGPointMake(xx, height-rr) controlPoint1:CGPointMake(xx+rr/2,height) controlPoint2:CGPointMake(xx, height-rr/2)];
    [countBkg addLineToPoint:CGPointMake(xx, yy/2+height/2)];
    [countBkg addLineToPoint:CGPointMake(xx-(25+yy), yy/2+height/2)]; // point
    [countBkg addLineToPoint:CGPointMake(xx, yy/2+height/2)];
    [countBkg addLineToPoint:CGPointMake(xx, yy+rr)];
    
    [countBkg addCurveToPoint:CGPointMake(xx+rr, yy) controlPoint1:CGPointMake(xx,yy+rr/2) controlPoint2:CGPointMake(xx+rr/2, yy)];
    [countBkg closePath];
    
    //draw circle
    UIBezierPath *circle = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(yy, yy, frame.size.height-yy*2, frame.size.height-yy*2)];
    UIBezierPath *circleEmpty = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(frame.size.width-(yy+(frame.size.height-yy*2)), yy, frame.size.height-yy*2, frame.size.height-yy*2)];
    
    //define color
    if (banana) {
        CGContextSetStrokeColorWithColor(context, kGREENUICOLOR.CGColor);
    } else {
        CGContextSetStrokeColorWithColor(context, kREDUICOLOR.CGColor);
    }
    [countBkg setLineWidth:1.0];
    [countBkg stroke];
    [circle setLineWidth:1.0];
    [circle stroke];
    CGContextSetStrokeColorWithColor(context, UIColorFromHexRGB(0x8F8F8F).CGColor);//kBOTTOMUICOLOR.CGColor);
    [circleEmpty setLineWidth:1.0];
    [circleEmpty stroke];
    
    //create image
    UIImage *bezierImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    //image to view
    UIImageView *bezierImageView = [[UIImageView alloc] initWithImage:bezierImage];
    
    // turn image view 180 decree if shit ;)
    if (!banana) [bezierImageView setTransform:CGAffineTransformRotate([bezierImageView transform],M_PI)];

    //set to frame
    [bezierImageView setFrame:frame];
    
    return bezierImageView;
}*/

//-(void)dealloc
//{
//    [self clean];
//}
//
//-(void)clean
//{
//    videoWebLoaded = NO;
//    contentArray = nil;
//    commentArray = nil;
//    likeBtn = nil;
//    likeBtnImg = nil;
//    dislikeBtn = nil;
//    dislikeBtnImg = nil;
//    likeDislikeCount = nil;
//    controller = nil;
//    flagSelect = nil;
//    insertTxtView = nil;
//    descriptionRow = nil;
//    user = nil;
//    nextUrl = nil;
//    videoHtmlString = nil;
//    content = nil;
//    flagButton = nil;
//    addCommentButton = nil;
//    [videoPlayerController clean];
//    videoPlayerController = nil;
//}

-(void)tapGestureAction:(UITapGestureRecognizer*)gesture
{
    if ([[UIMenuController sharedMenuController] isMenuVisible]) {
        [[UIMenuController sharedMenuController] setMenuVisible:NO];
    }
}

-(void)longTapGesture:(UILongPressGestureRecognizer*)gesture
{
    if ([gesture state] == UIGestureRecognizerStateBegan) {
        
        commentDictionary = [(wbCommentCell*)[gesture view] cellContent];

        [self becomeFirstResponder];
        
        DNSLog(@"longTapGesture");
        DNSLog(@"%@",gesture);
        
        menu = [UIMenuController sharedMenuController];
        
        CGRect targetRectangle = CGRectMake([gesture locationInView:self].x-50, [gesture locationInView:self].y, 100, 100);
        
        [menu setTargetRect:targetRectangle inView:self];
        
        UIMenuItem *menuItem1 = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"SINGLE_FLAG_COMMENT_LABEL", @"") action:@selector(reportAction:)]; //TODO: Comment flagging
        DCMSG(menuItem1);
        UIMenuItem *menuItem2;
        if ([[[commentDictionary objectForKey:@"user"] objectForKey:@"id"] integerValue] == [[[wbUser sharedUser] userId] integerValue] ) {
            menuItem2 = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"SINGLE_DELETE_COMMENT_LABEL", @"") action:@selector(deleteAction:)];
        }
        [menu setMenuItems:[[NSArray alloc] initWithObjects:/*menuItem1,*/menuItem2, nil]];
        [menu update];
        
        //    [menu setMenuVisible:YES animated:YES];
        [menu setMenuVisible:YES];
        DCMSG(menu);
        
        DNSLog(@"menuframe: %.f %.f %.f %.f",menu.menuFrame.origin.x,menu.menuFrame.origin.y,menu.menuFrame.size.width,menu.menuFrame.size.height);
        
    }
}


- (BOOL)canBecomeFirstResponder
{
    return YES;
}


- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    BOOL result = NO;
    if(@selector(copy:) == action || @selector(deleteAction:) == action || @selector(reportAction:) == action) {
        result = YES;
    }
    return result;
}

// UIMenuController Methods

// Default copy method
- (void)copy:(id)sender
{
    DCMSG(@"copy");
    [[UIPasteboard generalPasteboard] setString:[commentDictionary valueForKey:@"comment"] ];
}

// Our custom method
- (void)deleteAction:(id)sender
{
    DCMSG(@"deleteAction");
    DCMSG(commentDictionary);
    NSNumber *commentId = [NSNumber numberWithInteger:[[commentDictionary valueForKey:@"id"] integerValue]];
    [[wbAPI sharedAPI] removeComment:commentId delegate:self];
}

-(void)deleteCommentDone
{
    [self refresh:nil];
}

- (void)reportAction:(id)sender
{
    DCMSG(@"reportAction");
    DCMSG(commentDictionary);
}

// scroll
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [[UIMenuController sharedMenuController] setMenuVisible:NO];
}

@end
