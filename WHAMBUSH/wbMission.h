//
//  wbMission.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface wbMission : NSObject
{
    NSDateFormatter* apiTimeFormat;
    NSDate* endDate;
    NSDate* startDate;
}

@property id delegate;

@property (nonatomic,retain,readonly) NSDictionary   *mission_dictionary; // old school remove when possible
@property (nonatomic,retain,readonly) wbUser   *created_by;
@property (nonatomic,retain,readonly) NSNumber *mission_id;
@property (nonatomic,retain,readonly) NSString *title;
@property (nonatomic,retain,readonly) NSString *text;
@property (nonatomic,retain,readonly) NSString *ends;
@property (nonatomic,retain,readonly) NSAttributedString *endTimeAString;
@property (nonatomic,retain,readonly) NSNumber *mission_type;
@property (nonatomic,retain,readonly) NSString *countryCode;
@property (nonatomic,retain,readonly) NSNumber *is_videos_ranked;
@property (nonatomic,retain,readonly) NSNumber *bananas;
@property (nonatomic,retain,readonly) NSString *prize;
@property (nonatomic,retain,readonly) NSNumber *linked_video_id; // should be wbVideo TODO?
@property (nonatomic,retain,readonly) NSDictionary *linked_video_dictionary; // should be wbVideo TODO?
@property (nonatomic,retain,readonly) NSNumber *submissions_left;
@property (nonatomic,retain,readonly) NSNumber *max_video_length;
//@property (nonatomic,retain,readonly) UIImage  *mission_image_1;
//@property (nonatomic,retain,readonly) UIImage  *mission_image_2;
@property (nonatomic,retain,readonly) NSURL  *mission_image_1_url;
@property (nonatomic,retain,readonly) NSURL  *mission_image_2_url;
@property (nonatomic,retain,readonly) NSURL    *mission_url;
@property (nonatomic,retain,readonly) NSURL    *mission_rules_url;
@property (nonatomic,retain,readonly) NSURL    *mission_rules_url_ios;
@property (nonatomic,retain,readonly) NSNumber *like_count;
@property (nonatomic,retain,readonly) NSString *like_count_string;
@property (nonatomic,retain,readonly) NSNumber *number_of_submissions;
@property (nonatomic,retain,readonly) NSNumber *max_submissions;
@property (nonatomic,retain,readonly) NSString *max_submissions_string;
@property (nonatomic,retain,readonly) NSString *number_of_submissions_string;
@property (nonatomic,retain,readonly) NSString *parsedEndTime;
@property (nonatomic,readonly) BOOL ready;
@property (nonatomic,retain,readonly) NSNumber *has_liked;
@property (nonatomic,retain,readonly) NSNumber *has_disliked;
@property (nonatomic,retain,readonly) UIImage* mission_2_image;
@property (nonatomic,retain,readonly) NSString* status;

-(id) initWithId:(NSInteger)missionid;
-(id) initWithSlug:(NSString*)missionslug;

-(void)updateMissionData;

-(void)setMissionWithDictionary:(NSDictionary*)missionDictionary;
-(NSAttributedString*)getMissionEndString;

-(BOOL)isActive;

-(void)likeMission;
-(void)dislikeMission;
-(void)unlikeMission;
-(void)undislikeMission;


@end
