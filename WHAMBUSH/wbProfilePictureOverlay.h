//
//  wbProfilePictureOverlay.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 17/02/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbProfilePictureOverlay : UIView
{
    UIButton *flipCamera;
    UIImage* overlay;
}
@property BOOL editorMode;
@property CGRect cropRect;
@property id delegate;

@end
