//
//  wbBubbleCell.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/6/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbMissionCell.h"

@implementation wbMissionCell

@synthesize cellArray;
@synthesize isArchived;
@synthesize delegate;

//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
-(id)initWithFrame:(CGRect)frame
{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor colorWithRed:67.0/255.0 green:73.0/255.0 blue:87.0/255.0 alpha:0.475]];
        isArchived = NO;
        doneTickImg = [UIImage ch_imageNamed:@"done_comment_active.png"];
        //gorillaHeadImg = [UIImage ch_imageNamed:@"half_faced_wb_DS.png"];
        iconImg = [UIImage ch_imageNamed:@"iconMission_icon.png"];
        playImg = [UIImage ch_imageNamed:@"play.png"];
        
        
    }
    return self;
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated
//{
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}

#define bubleW (self.frame.size.width+10)

- (void)drawRect:(CGRect)rect
{
    wbMission *mission = [[wbMission alloc] init];
    [mission setMissionWithDictionary:cellArray];
    
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    sb = [[wbSpeechBubbleView alloc] initWithFrame:CGRectMake(0, 0, bubleW, self.frame.size.height-6)];
    [sb setCenter:CGPointMake(sb.center.x, self.frame.size.height/2)];
    [sb setBubbleAlpha:0.475];
    [sb setBubbleColor:kBOTTOMUICOLOR];
    //[self addSubview:sb];
    
    if (missionTitleLabel == nil) {
        missionTitleLabel = [[wbLabel alloc] initWithFrame:CGRectMake(0, 6, bubleW-doneTickImg.size.width, 0)];
    }
    [missionTitleLabel setTextColor:kWHITEUICOLOR];
    [missionTitleLabel setBackgroundColor:kTRANSPARENTUICOLOR];
    [missionTitleLabel setFont:kFONT(22)];
    [missionTitleLabel setText:[[cellArray objectForKey:@"name"] uppercaseString]];
    [missionTitleLabel setEdgeInsets:UIEdgeInsetsMake(12, 12, 0, 6)];
    [missionTitleLabel setNumberOfLines:0];
    [missionTitleLabel sizeToFit];
    
    [self addSubview:missionTitleLabel]; 
    
//    if (missionTxtLabel == nil) {
//        missionTxtLabel = [[wbLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(missionTitleLabel.frame), bubleW, 0)];
//    }
//    [missionTxtLabel setTextColor:kWHITEUICOLOR];
//    [missionTxtLabel setBackgroundColor:kTRANSPARENTUICOLOR];
//    [missionTxtLabel setFont:kFONTHelvetica(16)];
//    [missionTxtLabel setNumberOfLines:0];
//    [missionTxtLabel setText:[cellArray objectForKey:@"description"]];
//    [missionTxtLabel setEdgeInsets:UIEdgeInsetsMake(0, 12, 0, 16)];
//    [missionTxtLabel setPreserveWidth:YES];
//    [missionTxtLabel sizeToFit];
    //[self addSubview:missionTxtLabel];
    
    if (missionTxtView == nil) {
        missionTxtView = [[UITextView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(missionTitleLabel.frame), bubleW, 0)];
    }
    [missionTxtView setBackgroundColor:kTRANSPARENTUICOLOR];
    if ([[cellArray objectForKey:@"description"] length] > 0) {
        [missionTxtView setText:[NSString stringWithFormat:@"%@",[cellArray objectForKey:@"description"]]];
        [missionTxtView setFont:kFONTHelvetica(15)];
        [missionTxtView setTextColor:kWHITEUICOLOR];
        [missionTxtView setDataDetectorTypes:UIDataDetectorTypeLink];
        [missionTxtView setTextContainerInset:UIEdgeInsetsMake(0, 7, 0, 16)];
        [missionTxtView setScrollEnabled:NO];
        [missionTxtView setEditable:NO];
        [missionTxtView setTintColor:kGREENUICOLOR];
        [missionTxtView sizeToFit];
    }
    [self addSubview:missionTxtView];
   
    /*if (missionEndTimeLabel == nil) {
        missionEndTimeLabel = [[wbLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(missionTxtView.frame), bubleW, 0)];
    }
    [missionEndTimeLabel setFont:kFONTHelvetica(10)];
    [missionEndTimeLabel setTextColor:kLIGHTGRAYUICOLOR];
    [missionEndTimeLabel setBackgroundColor:kTRANSPARENTUICOLOR];
    [missionEndTimeLabel setEdgeInsets:UIEdgeInsetsMake(6, 12, 0, 16)];
    //[missionEndTimeLabel setText:[NSString stringWithFormat:@"%@",[[wbAPI sharedAPI] parseMissionEndTime:[cellArray objectForKey:@"end_at"]]]];
    [missionEndTimeLabel setText:[mission getMissionEndCount]];
    [missionEndTimeLabel sizeToFit];
    if (!isArchived) {
        //[self addSubview:missionEndTimeLabel];
    }*/
    
    if (missionEndTimeButton == nil) {
        missionEndTimeButton = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    [missionEndTimeButton.titleLabel setFont:kFONTHelvetica(10)];
    [missionEndTimeButton setTintColor:kTRANSPARENTUICOLOR];
    [missionEndTimeButton setTitleColor:kLIGHTGRAYUICOLOR forState:UIControlStateNormal];
    [missionEndTimeButton setTitleColor:kLIGHTGRAYUICOLOR forState:UIControlStateSelected];
    //[missionEndTimeButton setTitle:[NSString stringWithFormat:@"%@",[[wbAPI sharedAPI] parseMissionEndTime:[cellArray objectForKey:@"end_at"]]] forState:UIControlStateNormal];
    [missionEndTimeButton setAttributedTitle:[mission getMissionEndString] forState:UIControlStateNormal];
    [missionEndTimeButton setTitle:[[wbAPI  sharedAPI] beautifyTime:[cellArray objectForKey:@"end_at"]] forState:UIControlStateSelected];
    [missionEndTimeButton setSelected:NO];
    [missionEndTimeButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [missionEndTimeButton setContentVerticalAlignment:UIControlContentVerticalAlignmentBottom];
    [missionEndTimeButton sizeToFit];
    [missionEndTimeButton setFrame:CGRectMake(12, 10+CGRectGetMaxY(missionTxtView.frame), /*bubleW-12-16*/missionEndTimeButton.frame.size.width+6, missionEndTimeButton.frame.size.height)];
    [missionEndTimeButton addTarget:self action:@selector(toggleEndTime) forControlEvents:UIControlEventTouchUpInside];
    if (!isArchived) {
        [self addSubview:missionEndTimeButton];
    }
    //[missionEndTimeButton setBackgroundColor:kYELLOWUICOLOR];
    
    if (playButton == nil) {
        playButton = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    [playButton setBackgroundImage:playImg forState:UIControlStateNormal];
    if (!isArchived) {
        [playButton setFrame:CGRectMake(CGRectGetMaxX(missionEndTimeButton.frame)+6, CGRectGetMaxY(missionEndTimeButton.frame)-playImg.size.height/2, playImg.size.width/2, playImg.size.height/2)];
    } else {
        [playButton setFrame:CGRectMake(12, CGRectGetMaxY(missionTxtView.frame)+6, playImg.size.width/2, playImg.size.height/2)];
    }
    [playButton addTarget:self action:@selector(openMissionVideo) forControlEvents:UIControlEventTouchUpInside];
    if ([[cellArray objectForKey:@"linked_video"] isKindOfClass:[NSDictionary class]]) {
        [self addSubview:playButton];
    }
    
    if (missionButton == nil) {
        missionButton = [wbRoundedButton buttonWithType:UIButtonTypeSystem];
    }
    [missionButton setHelvetica:NO];
    [missionButton setFontSize:20];
    [missionButton setAlpha:1.0];
    if ([[cellArray valueForKey:@"has_submitted"] boolValue] || isArchived) {
        [missionButton setTitle:NSLocalizedString(@"MISSIONS_VIEW_MISSION_VIDEOS",@"") forState:UIControlStateNormal];
        [missionButton setButtonColor:kGREENUICOLOR];
        [missionButton setEnabled:YES];
    } else {
        [missionButton setTitle:NSLocalizedString(@"MISSIONS_CREATE_MISSION_VIDEO",@"") forState:UIControlStateNormal];
        if ([[wbAPI sharedAPI] uploading]) {
            [missionButton setAlpha:0.3];
            [missionButton setEnabled:NO];
        } else {
            [missionButton setButtonColor:kREDUICOLOR];
            [missionButton setEnabled:YES];
        }
    }
    [missionButton setFilled:YES];
    [missionButton sizeToFit];
    [missionButton setCenter:CGPointMake(bubleW-16-missionButton.frame.size.width/2, self.frame.size.height-4-6-missionButton.frame.size.height/2)];
    [missionButton addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:missionButton];
    //[missionButton setBackgroundColor:kYELLOWUICOLOR];
    
    if (iconImgView == nil) {
        iconImgView = [[UIImageView alloc] initWithImage:iconImg];
    }
    [iconImgView setFrame:CGRectMake(12,CGRectGetMaxY(missionButton.frame)-(iconImg.size.height+3), iconImg.size.width, iconImg.size.height)];
    
    if ([[cellArray valueForKey:@"mission_type"] integerValue] == 1 ) {
        [self addSubview:iconImgView];
    }
    

    if (doneTickImageView == nil) {
        doneTickImageView = [[UIImageView alloc] initWithImage:doneTickImg];
    }
#define kTICKSIZE doneTickImg.size.width-10
    //[missionTitleLabel setBackgroundColor:kREDUICOLOR];
    [doneTickImageView setFrame:CGRectMake(CGRectGetMaxX(missionTitleLabel.frame),CGRectGetMinY(sb.frame)+5,kTICKSIZE,kTICKSIZE)];
    //[doneTickImageView setCenter:CGPointMake(doneTickImageView.center.x, missionButton.center.y-3)];
    
    if ([[cellArray valueForKey:@"has_submitted"] boolValue]) {
        [self addSubview:doneTickImageView];
    }

}

-(void)openMissionVideo
{
    [[wbData sharedData] setSingleVideoData:[cellArray objectForKey:@"linked_video"]];
    [kROOTVC performSelector:@selector(startSingleVideo:) withObject:NULL];
    [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
}

-(void)toggleEndTime
{
    [missionEndTimeButton setSelected:![missionEndTimeButton isSelected]];
}


-(void)buttonPressed
{
        //NSDictionary* missionArray = [resultsArray objectAtIndex:[indexPath row]-1];
        if ([[cellArray valueForKey:@"has_submitted"] integerValue] == 0 && !isArchived) {
            [[wbAPI sharedAPI] setMissionData:cellArray];
            //DCMSG(missionArray);
            [kROOTVC performSelector:@selector(startCamera:) withObject:nil];
        } else {
            //DMSG;
            DCMSG(cellArray);
            
            NSNumber* isRanked;
            if ([[cellArray objectForKey:@"is_videos_ranked"] boolValue] && [[cellArray objectForKey:@"mission_type"] integerValue] == 0) {
                isRanked = [NSNumber numberWithBool:YES];
            } else {
                isRanked = [NSNumber numberWithBool:NO];
            }
            NSString *endpoint;
            if ([isRanked boolValue]) {
                endpoint = [NSString stringWithFormat:@"search/videos/?mission=%@&order=rank",[cellArray valueForKey:@"id"]];
            } else {
                endpoint = [NSString stringWithFormat:@"search/videos/?mission=%@&order=random",[cellArray valueForKey:@"id"]];
            }
            NSDictionary *feed = [NSDictionary dictionaryWithObjectsAndKeys:
                                  endpoint,@"endpoint",
                                  [NSNumber numberWithInteger:0],@"id",
                                  [NSNumber numberWithInteger:0],@"default",
                                  [[cellArray objectForKey:@"name"] uppercaseString],@"name",
                                  @"star.png",@"icon",
                                  @"mission",@"type",
                                  [cellArray objectForKey:@"id"],@"missionId",
                                  isRanked,@"is_ranked",
                                  nil];
            [kROOTVC performSelector:@selector(startNewVideoFeed:feed:) withObject:nil withObject:feed];
        }

}

@end
