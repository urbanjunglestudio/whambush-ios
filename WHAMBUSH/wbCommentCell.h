//
//  wbCommentCell.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/10/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbBaseCell.h"
#import "wbDefines.h"
#import "wbSpeechBubbleView.h"


@interface wbCommentCell : UIView//wbBaseCell
{
    float height;
    wbUser *user;
    UIImageView *head;
    wbButton *headButton;
    wbLabel *comment;
    wbButton *author;
    wbLabel *when;
    
    wbLabel *numberOfCommentsLabel;
    
    UIActivityIndicatorView *ai;
    
    NSString *notificationObject;

    UIMenuController *menu;
    
    //BOOL userLoaded;
}

@property (nonatomic,retain) NSMutableDictionary* cellContent;
@property id controller;
//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;

@property NSInteger totalNumberOfComments;
@property NSInteger currentCommentNumber;

@property (nonatomic,retain) UIView *parentView;
//-(NSInteger)getHeight;
//-(NSInteger)getHeight:(NSMutableDictionary*)_cellContent;


@end
