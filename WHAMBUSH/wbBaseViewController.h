//
//  wbBaseViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 18/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseView.h"

@interface wbBaseViewController : UIViewController

//@property BOOL transitionAnimate;
//@property NSInteger viewStackIndex;
@property (nonatomic,retain) NSString *headerLogo;
@property (nonatomic) BOOL hasBack;

@property (nonatomic,retain) NSString *tabTitle;
@property (nonatomic,retain) wbLabel *tabLabel;

@property NSInteger index;

-(void)refreshView;
//-(void)goBack:(id)sender;

@end
