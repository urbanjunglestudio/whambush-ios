//
//  wbFollowListViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbFollowListViewController.h"

@interface wbFollowListViewController ()

@end

@implementation wbFollowListViewController

@synthesize user;
@synthesize isFollowers;

//-(void)loadView
//{
//    wbFollowListView *followView = [[wbFollowListView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
//    [self setView:followView];
//}
-(void)loadView
{
    wbFollowListTableView *followView = [[wbFollowListTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [self setView:followView];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [(wbFollowListTableView*)self.view setIsFollowers:isFollowers];
//    [(wbFollowListTableView*)self.view setUser:user];
}
 -(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [(wbFollowListTableView*)self.view setIsFollowers:isFollowers];
    [(wbFollowListTableView*)self.view setUser:user];
}

-(void)update
{
    [self.view performSelector:@selector(update)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
