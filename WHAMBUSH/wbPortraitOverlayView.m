//
//  wbPortraitOverlayView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 25.08.14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbPortraitOverlayView.h"

@implementation wbPortraitOverlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    float boxH = self.frame.size.height;
    float boxW = (9*boxH)/16;
    UIView *box = [[UIView alloc] initWithFrame:CGRectMake(0, 0, boxW, boxH)];
    [box setCenter:CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame))];
//    [box setBackgroundColor:[UIColor yellowColor]];
//    [box setAlpha:0.2];
    
    UIToolbar *topBlurView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetMinX(box.frame), boxH)];
    [topBlurView setBackgroundColor:kTRANSPARENTUICOLOR];
//    [topBlurView setAlpha:1];
    [topBlurView setBarStyle:UIBarStyleBlack];
    
    UIToolbar *bottomBlurView = [[UIToolbar alloc] initWithFrame:CGRectMake(CGRectGetMaxX(box.frame), 0, self.frame.size.width-CGRectGetMaxX(box.frame), boxH)];
    [bottomBlurView setBackgroundColor:kTRANSPARENTUICOLOR];
//    [bottomBlurView setAlpha:0.97];
    [bottomBlurView setBarStyle:UIBarStyleBlack];
    
    [self addSubview:topBlurView];
    [self addSubview:box];
    [self addSubview:bottomBlurView];
}


@end
