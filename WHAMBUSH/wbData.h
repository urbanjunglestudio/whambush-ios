//
//  wbData.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "wbMission.h"

@interface wbData : NSObject
{
    NSMutableDictionary *missionsData;
    NSMutableDictionary *whambushMissions;
    NSMutableDictionary *allFeedData;
    NSMutableDictionary *activityData;
    //NSMutableDictionary *fetching;
    NSMutableDictionary *whambushUsers;
    NSMutableDictionary *tvChannels;
    NSMutableDictionary *tvChannelsData;
    NSMutableArray *allCountriesData;
    NSInteger numberOfCountries;
}

+(id)sharedData;

@property NSInteger rootTVChannel;
@property (nonatomic,retain) NSString *viewMissionsFrom;
@property (nonatomic,strong) NSMutableDictionary *singleVideoData;

@property (nonatomic,strong) NSMutableDictionary *feeds;
@property (nonatomic,strong) NSMutableDictionary *missionFeeds;

//@property (nonatomic,strong) NSMutableDictionary *videoCells;

@property CGSize thumbSize;

@property (nonatomic) NSInteger rankNumber;

//@property BOOL hasUserpage;
//@property BOOL hasMissions;

@property BOOL launchedFromPush;
@property (nonatomic,retain) NSDictionary *pushData;


@property (nonatomic) NSNumber *num_of_unreadActivities;

@property (nonatomic,retain,readonly) NSString *selectedMissionCountry;

@property (retain,readonly) NSString* supportMessage;

@property (nonatomic,readonly,retain) NSString *activityString;
@property (nonatomic,readonly,retain) wbLabel *activityLabel;

@property (nonatomic, retain) wbMission *launchWithMission;

-(void)getTvChannels;
-(void)getTvChannelWithId:(NSInteger)channelId delegate:(id)delegate;


-(void)getAllCountries;
-(void)getCountriesData:(id)delegate;

-(void)getAllMissionFeeds;
-(void)getAllCountryMissionFeeds:(NSString*)country;

-(void)saveMission:(NSDictionary*)data;
-(void)refreshMissionData:(NSString*)name delegate:(id)delegate;
-(void)getMissionsData:(NSString*)name delegate:(id)delegate;
-(wbMission*)getMissionWithId:(NSNumber*)missionid;

-(void)getAllFeeds;
-(void)refreshFeedData:(NSString*)name delegate:(id)delegate;
-(void)getFeedData:(NSString*)name delegate:(id)delegate;

-(void)getAllActivity;
-(void)refreshActivityData:(id)delegate;
-(void)getActivityData:(id)delegate;


-(NSString*)parseIcon:(NSInteger)number;
-(void)saveWhambushUser:(NSDictionary*)userArray;
-(wbUser*)getWhambushUser:(NSString*)path;
-(wbUser*)getWhambushUserWithId:(NSInteger)id;
-(wbUser*)getWhambushUserWithUsername:(NSString*)username;


-(void)clearUserData;
-(void)clearData;
-(void)clearMemory;
-(NSDictionary*)tvFeed;

-(void)createSupportMessage:(NSDictionary*)loginInfo;

@end
