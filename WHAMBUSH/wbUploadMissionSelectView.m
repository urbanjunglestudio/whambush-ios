//
//  wbUploadMissionSelectView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07.06.14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbUploadMissionSelectView.h"

@implementation wbUploadMissionSelectView

@synthesize missions;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    DMSG;
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setUserInteractionEnabled:NO];
    }
    DPRINTCLASS;
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    DMSG;
    if (hideView == nil) {
        hideView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    [hideView setBackgroundColor:kTRANSPARENTUICOLOR];
    [hideView setAlpha:0.0];
    [hideView setBarStyle:UIBarStyleBlack];
    
    if (theBox == nil) {
        theBox = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 182+40)];
    }
    [theBox setBackgroundColor:kSEPRATORUICOLORAlpha];
    
    UIPickerView *selection = [[UIPickerView alloc] init];
    [selection setFrame:CGRectMake(0, 40, self.frame.size.width, 180)];
    [selection setBackgroundColor:kSEPRATORUICOLORAlpha];
    [selection setDelegate:self];
    [selection setDataSource:self];
    [selection setShowsSelectionIndicator:YES];
    
   
    if (cancelBtn == nil) {
        cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [cancelBtn setFrame:hideView.frame];
    [cancelBtn setBackgroundColor:kTRANSPARENTUICOLOR];
    [cancelBtn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    NSAssert(hideView != nil, @"something wrong here");
    [hideView addSubview:cancelBtn];
    
    UIImage *okImg = [UIImage ch_imageNamed:@"done_comment_default.png"];
    UIImage *okImgActive = [UIImage ch_imageNamed:@"done_comment_active.png"];
    if (okBtn == nil) {
        okBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [okBtn setFrame:CGRectMake(self.frame.size.width-(okImg.size.width+2), 2, okImg.size.width-5, okImg.size.height-5)];
    [okBtn setBackgroundColor:[UIColor clearColor]];
    [okBtn setBackgroundImage:okImg forState:UIControlStateNormal];
    [okBtn setBackgroundImage:okImgActive forState:UIControlStateHighlighted];
    [okBtn setShowsTouchWhenHighlighted:YES];
    [okBtn addTarget:self action:@selector(doneSelect) forControlEvents:UIControlEventTouchUpInside];
    [theBox addSubview:selection];
    [theBox addSubview:okBtn];
    [self addSubview:hideView];
    [self addSubview:theBox];
}

-(void)setMissions:(NSDictionary *)_missions
{
    //DCMSG(_missions);
    missionArray = [[NSMutableArray alloc] init];
    for (NSDictionary *tmp  in [_missions objectForKey:@"results"]) {
        if (![[tmp valueForKey:@"has_submitted"] boolValue]) {
            [missionArray addObject:tmp];
        }
    }
    //DCMSG(missionArray);
}

// Handle the selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    selectedRow = row;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger numRows = [missionArray count]+1;
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}


// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    int sectionWidth = 320;
    
    return sectionWidth;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    wbLabel *label = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 60)];
    [label setTextColor:kWHITEUICOLOR];
    [label setNumberOfLines:1];
    [label setEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 5)];
    
    if (row == 0) {
        [label setText:NSLocalizedString(@"UPLOAD_NORMAL_VIDEO", @"")];
        [label setFont:kFONTHelvetica(24)];
    } else {
        //if ([[[missionArray objectAtIndex:row-1] valueForKey:@"has_submitted"] boolValue]) {
        NSString *selectStr = [NSString stringWithFormat:@"%@: %@",[[missionArray objectAtIndex:row-1] objectForKey:@"name"],[[missionArray objectAtIndex:row-1] objectForKey:@"description"]];
        [label setText:NSLocalizedString(selectStr, @"")];
        [label setFont:kFONTHelvetica(24)];
        //}
    }
    return label;
}

//show/hide
-(void)show
{
    [self setUserInteractionEnabled:YES];
    // get a rect for the textView frame
	CGRect containerFrame = theBox.frame;
    containerFrame.origin.y = self.bounds.size.height - theBox.frame.size.height ;//-50;
	
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
	
	// set views with new info
    hideView.alpha = 0.97;
    theBox.alpha = 0.90;
	theBox.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
}

-(void)hide
{
    DMSG;
    [self setUserInteractionEnabled:NO];
    // get a rect for the textView frame
	CGRect containerFrame = theBox.frame;
    containerFrame.origin.y = self.bounds.size.height;
	
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
	
	// set views with new info
    hideView.alpha = 0.0;
    theBox.alpha = 0.0;
	theBox.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
    
}

-(void)doneSelect
{
    DMSG;
    if (selectedRow == 0) {
        [delegate performSelector:@selector(missionSelected:) withObject:nil];
    } else {
        [delegate performSelector:@selector(missionSelected:) withObject:[missionArray objectAtIndex:selectedRow-1]];
    }
    [self hide];
}

@end
