//
//  UIImage+AutoCheck.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30.05.15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (AutoCheck)

+(UIImage *)ch_imageNamed:(NSString *)name;

@end
