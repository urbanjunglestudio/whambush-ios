//
//  wbMissionCountrySelectTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 14/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbMissionCountrySelectTableView.h"

@implementation wbMissionCountrySelectTableView

@synthesize missionView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        //[self setCanCancelContentTouches:NO];
        [self setHidden:NO];
        [self setBackgroundColor:kBKGUICOLOR];
        
        supportedCountries = [[NSMutableArray alloc] initWithObjects:@{@"country":@"ZZ",
                                                                       @"flag_url":@"",
                                                                       @"name":@"Global",
                                                                       @"supported":@1,
                                                                       @"flag_banner_url":kZZBANNERPATH
                                                                       }, nil]; //TODO FIX ME
        [[wbData sharedData] getCountriesData:self];
    }
    DPRINTCLASS;
    return self;
}

-(void)setGAIViewName
{
    [[wbAPI sharedAPI] registerGoogleAnalytics:@"Mission:CountrySelect"];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
//    if ([supportedCountries count] < 1) {
        kSHOWWAIT;
//    }
}

-(void)forceRefresh
{
    [self reloadData];
}

-(void)gotCountries:(NSArray*)countries
{
    NSInteger count = [countries count];
    for (int i = 0; i < count; i++) {
        if ([[[countries objectAtIndex:i] valueForKey:@"supported"] boolValue]) {
            [supportedCountries addObject:[countries objectAtIndex:i]];
        } else {
            break;
        }
    }
    numberOfResults = [supportedCountries count];
    DCMSG(supportedCountries);
    [self reloadData];
    kHIDEWAIT;
}

//Tableview stuff
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfResults;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    kHIDEWAIT;
    NSString *identifier = [NSString stringWithFormat:@"%ld%ld",(long)[indexPath row],(long)[indexPath section]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        //[cell setFrame:CGRectMake(0, 0, self.frame.size.width, kTVCELLH)];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];//UITableViewCellSelectionStyleNone];
        [cell setSeparatorInset:UIEdgeInsetsZero];
        [cell setBackgroundColor:kTOPUICOLOR];
        [cell setClipsToBounds:NO];
    }
    NSString *txtString = NSLocalizedString([[supportedCountries objectAtIndex:[indexPath row]] objectForKey:@"name"], @"");
    [[cell textLabel] setText:txtString];
    NSString *countryString = [[supportedCountries objectAtIndex:[indexPath row]] objectForKey:@"country"];
    UIImageView *right;
    if ([countryString isEqualToString:[[[wbAPI sharedAPI] currentCountry] objectForKey:@"country"]] || ([countryString isEqualToString:@"ZZ"] && ![[[[wbAPI sharedAPI] currentCountry] valueForKey:@"supported"] boolValue])) {
        right = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"country_select_done.png"]];
    } else {
        right = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"country_select.png"]];
    }
    [cell setAccessoryView:right];

    [[cell textLabel] setTextColor:kWHITEUICOLOR];
    [[cell textLabel] setFont:kFONT(30)];
    [[cell textLabel] setText:[[[cell textLabel] text] uppercaseString]];
    [[cell textLabel] setBackgroundColor:kTRANSPARENTUICOLOR];
    UIImageView *background = [[UIImageView alloc] init];
    NSString *bannerEndpoint = [NSString stringWithFormat:kCOUNTRYPICPATH,[[supportedCountries objectAtIndex:[indexPath row]] objectForKey:@"flag_banner_url"]];
    [background setImageWithURL:[NSURL URLWithString:bannerEndpoint]
                  placeholderImage:[UIImage ch_imageNamed:@"placeholder_banner"]];
    [cell setBackgroundView:background];
    return cell;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kTVCELLH;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self deselectRowAtIndexPath:indexPath animated:NO];
    [[wbHeader sharedHeader] setShowChangeCountryButton:YES];
    if (![[[supportedCountries objectAtIndex:[indexPath row]] objectForKey:@"country"] isEqualToString:[[wbData sharedData] selectedMissionCountry]]) {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            DMSG;
            [missionView performSelectorOnMainThread:@selector(forceRefresh) withObject:nil waitUntilDone:NO];

            [[wbData sharedData] performSelectorInBackground:@selector(getAllCountryMissionFeeds:) withObject:[[supportedCountries objectAtIndex:[indexPath row]] objectForKey:@"country"]];
             [kROOTVC performSelectorOnMainThread:@selector(goBack:) withObject:self waitUntilDone:NO];
        }];
        [op start];
        
    } else {
        [kROOTVC performSelector:@selector(goBack:) withObject:nil];
    }
}


@end
