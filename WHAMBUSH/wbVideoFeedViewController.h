//
//  wbVideoFeedViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 19/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseViewController.h"
#import "wbVideoFeedTableView.h"

@interface wbVideoFeedViewController : wbBaseViewController

//@property BOOL backButton;
@property (nonatomic,retain) NSDictionary *startFeed;
@property (nonatomic) BOOL hideHeader;
@property (nonatomic) BOOL isTv;

@end
