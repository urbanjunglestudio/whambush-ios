////
////  wbVideo.h
////  WHAMBUSH
////
////  Created by Jari Kalinainen on 05/03/15.
////  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "wbDefines.h"
//#import "wbButton.h"
//#import "wbLabel.h"
//#import <SDWebImage/UIImageView+WebCache.h>
//#import "wbVideoCell.h"
//
//@interface wbVideo : NSObject
//{
//    
//}
//
//@property (nonatomic,retain) wbUser   *added_by;
//@property (nonatomic,retain) NSNumber *comment_count;
//@property (nonatomic,retain) NSNumber *dislike_count;
//@property (nonatomic,retain) NSNumber *like_count;
//@property (nonatomic,retain) NSNumber *has_liked;
//@property (nonatomic,retain) NSNumber *has_disliked;
//@property (nonatomic,retain) NSNumber *video_id;
//@property (nonatomic,retain) NSNumber *is_active;
//@property (nonatomic,retain) NSNumber *is_mission_winner;
//@property (nonatomic,retain) NSNumber *is_processed;
//@property (nonatomic,retain) NSNumber *mission_id;
//@property (nonatomic,retain) NSNumber *rank;
//@property (nonatomic,retain) NSNumber *video_type;
//@property (nonatomic,retain) NSNumber *view_count;
//
//@property (nonatomic,retain) NSString *created_at;
//@property (nonatomic,retain) NSString *video_description;
//@property (nonatomic,retain) NSString *external_id;
//@property (nonatomic,retain) NSURL *external_url;
//@property (nonatomic,retain) NSString *modified_at;
//@property (nonatomic,retain) NSString *video_name;
//@property (nonatomic,retain) NSString *published_at;
//@property (nonatomic,retain) NSString *tags;
//@property (nonatomic,retain) NSURL *thumbnail_url;
//@property (nonatomic,retain) NSURL *web_url;
//
//@property (nonatomic) BOOL *deleted;
//
//@property (nonatomic,retain,readonly) wbVideoCell *videoCell;
//
//
////-(void)setVideoData:(NSDictionary*)data;
//-(void)updateVideoData;
//
//+(id)initVideoObjectWithId:(NSInteger)videoId;
//+(id)initVideoObjectWithData:(NSDictionary*)videoData;
//
//@end
