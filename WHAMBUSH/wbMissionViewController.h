//
//  wbMissionViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 21/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseViewController.h"
#import "wbMissionsTableView.h"
#import "wbMissionCountrySelectViewController.h"

@interface wbMissionViewController : wbBaseViewController 

@property (retain,nonatomic,readonly) wbMissionCountrySelectViewController *countrySelect;

@end
