//
//  wbBaseView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 8/29/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbDefines.h"
#import "wbAPI.h"

@interface wbBaseView : UIScrollView
{
    //wbGUI *GUI;
}

@property (nonatomic) UITextField *activeField;

@property (nonatomic,strong,retain) UIView *header;
@property (nonatomic,strong,retain) UIView *footer;


-(UITextField *)giveTxtFieldWithRect:(CGRect)tfRect placeholder:(NSString*)placeholder secure:(BOOL)secure;

-(UITextField *)giveTxtFieldWithRect:(CGRect)tfRect placeholder:(NSString*)placeholder secure:(BOOL)secure tag:(NSInteger)tag;

-(void)userLoggedIn;

@end
