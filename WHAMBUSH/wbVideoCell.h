//
//  wbVideoCell.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/5/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbDefines.h"
#import "wbBaseCell.h"
#import "wbAPI.h"
#import "vzaarAPI.h"

//@interface wbVideoCell : wbBaseCell <UIWebViewDelegate>
@interface wbVideoCell : UIView
{
    UILabel *viewCount;
    UIActivityIndicatorView *ai;
    UIActivityIndicatorView *aiEmpty;
    UILabel   *videoTitle;
    UILabel   *videoUser;
    UILabel   *videoDescription;
    UILabel   *videoTime;
    NSString  *videoHash;
    NSString  *videoUserStr;
    NSString  *videoDescriptionStr;
    NSString  *videoTimeStr;
    NSString  *commentCount;
    NSString  *bananaCount;
    NSString  *disbananaCount;
    
    UIImage *videoThumbnail;
    UIImageView *videoThumbView;
    
    CGRect videoRect;
    NSData *thumbData;
    
    NSString *rankLabel;
}

@property (nonatomic,retain) NSDictionary* cellContent;
@property (nonatomic) NSInteger rankNumber;
@property BOOL isTV;
@end
