//
//  wbUserData.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 8/26/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbUser.h"


@implementation wbUser

@synthesize userDetails;

@synthesize birthday;
@synthesize birthdayString;
@synthesize country;
@synthesize countryDictionary;
@synthesize activation_state;
@synthesize descriptionTxt;
@synthesize email;
@synthesize userId;
@synthesize userscore;
@synthesize num_dislikes;
@synthesize num_followers;
@synthesize num_followings;
@synthesize num_likes;
@synthesize num_videos;
@synthesize profilePictureURL;
//@synthesize profilePicture;
@synthesize userRank;
@synthesize userURL;
@synthesize username;
//@synthesize profilePictureUserView;
//@synthesize profilePictureUserViewSmall;
@synthesize userFeed;
@synthesize userMyFeed;
@synthesize is_following;
@synthesize userType;
@synthesize userFollowerLabel;
@synthesize userFollowingLabel;
@synthesize rankLabel;
@synthesize userReady;
@synthesize videosLabel;
-(id) init
{
    self = [super init];
    if (self) {
        //
        userReady = NO;
        triedOnce = NO;
    }
    DPRINTCLASS;
    return self;
}

+(id)sharedUser
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init]; 
    });
    return _sharedObject;
}

- (NSString *)description
{
    return [super description];
}


-(void)setUserDetails:(NSDictionary *)_userDetails
{
    //DCMSG(_userDetails);
    if (_userDetails == nil) {
        DCMSG(_userDetails);
        if (userURL != nil) {
            DCMSG(userURL);
            [self updateUserData];
        }
    } else {
        //[[wbData sharedData] saveWhambushUser:_userDetails];

        triedOnce = NO;
        //DCMSG(_userDetails);
     
        birthday = [_userDetails objectForKey:@"birthday"];
        birthdayString = [self getBirthdaySting];
        countryDictionary = [_userDetails objectForKey:@"country"];
        if ([[countryDictionary valueForKey:@"supported"] boolValue]) {
            country = [countryDictionary objectForKey:@"country"];
        } else {
            country = @"ZZ";
        }
        activation_state = [_userDetails objectForKey:@"activation_state"];
        descriptionTxt = [_userDetails objectForKey:@"description"];
        if ([descriptionTxt isKindOfClass:[NSNull class]]) {
            descriptionTxt = @"";
        }
        email = [_userDetails objectForKey:@"email"];
        
        userId = [_userDetails objectForKey:@"id"];
        is_following = [[_userDetails objectForKey:@"is_following"] boolValue];
        
        userscore = [[wbAPI sharedAPI] valueToString:[_userDetails objectForKey:@"userscore"]];
        
        num_dislikes = [_userDetails objectForKey:@"num_dislikes"];
        num_followers = [_userDetails objectForKey:@"num_followers"];
        num_followings = [_userDetails objectForKey:@"num_followings"];
        num_likes = [_userDetails objectForKey:@"num_likes"];
        num_videos = [_userDetails objectForKey:@"num_videos"];
        
        profilePictureURL = [NSString stringWithFormat:@"%@",[_userDetails objectForKey:@"profile_picture"]];
        
        profilePicture = [UIImage ch_imageNamed:@"monkey.png"];

        userRank = [_userDetails objectForKey:@"rank"];
        if ([userRank isKindOfClass:[NSNull class]]) {
            userRank = @"RANK_PVT";
        }
        
        userURL = [_userDetails objectForKey:@"url"];
        username = [_userDetails objectForKey:@"username"];
        userType = [_userDetails objectForKey:@"user_type"];
        
        if (userFollowingLabel == nil) {
            userFollowingLabel = [[wbLabel alloc] init];
        }
        [userFollowingLabel setText:[NSString stringWithFormat:NSLocalizedString(@"USER_FOLLOWINGS_COUNT",@""),[NSString stringWithFormat:NSLocalizedString(@"USER_COUNT_STRING",@""),[[wbAPI sharedAPI] valueToString:num_followings] ]]];
        [userFollowingLabel sizeToFit];
 
        if (userFollowerLabel == nil) {
            userFollowerLabel = [[wbLabel alloc] init];
        }
        [userFollowerLabel setText:[NSString stringWithFormat:NSLocalizedString(@"USER_FOLLOWERS_COUNT",@""),[NSString stringWithFormat:NSLocalizedString(@"USER_COUNT_STRING",@""),[[wbAPI sharedAPI] valueToString:num_followers]]]];
        [userFollowerLabel sizeToFit];
        
        if (rankLabel == nil) {
            rankLabel = [[wbLabel alloc] init];
        }
        [rankLabel setText:[NSString stringWithFormat:NSLocalizedString(@"USER_RANK",@""),[NSString stringWithFormat:NSLocalizedString(@"USER_COUNT",@""),[[wbData sharedData] rankNumber]]]];
        [rankLabel sizeToFit];
        
        if (videosLabel == nil) {
            videosLabel = [[wbLabel alloc] init];
        }
        [videosLabel setText:[NSString stringWithFormat:NSLocalizedString(@"USER_VIDEO_COUNT",@""),[NSString stringWithFormat:NSLocalizedString(@"USER_COUNT_STRING",@""),[[wbAPI sharedAPI] valueToString:num_videos]]]];
        [videosLabel sizeToFit];
               
        userReady = YES;
        
     }
}

-(NSString*)getBirthdaySting
{
    if (![birthday isKindOfClass:[NSNull class]]) {
        NSDateFormatter *formatString = [[NSDateFormatter alloc] init];
        [formatString setDateFormat:@"yyyy-MM-dd"];
        [formatString setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        NSDate *monthDate = [formatString dateFromString:birthday];
        //DCMSG(monthDate);
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"d'.' MMMM yyyy"];
        NSString *txt = [dateFormat stringFromDate:monthDate];
        //DCMSG(txt);
        return [txt capitalizedString];
    } else {
        return @"";
    }
}

-(void)userProfilePictureImageView:(UIImageView*)imageView
{
    //    NSString *picUrl;
    //    if ([profilePictureURL rangeOfString:@"http"].location == NSNotFound) {
    //        picUrl = [NSString stringWithFormat:kPROFILEPICPATH,profilePictureURL];
    //    } else {
    NSString *picUrl = [NSString stringWithFormat:@"%@",profilePictureURL];
    //    }
    if ([profilePictureURL length] > 1) {
        [imageView setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:profilePicture];/*success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            
            profilePicture = image;
            [imageView setImage:image];
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];*/
        /*[imageView setImageWithURL:[NSURL URLWithString:picUrl] placeholderImage:profilePicture  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            profilePicture = image;
        }];*/
    } else {
        [imageView setImage:profilePicture];
    }
    imageView.layer.cornerRadius = imageView.frame.size.height/2;
    //imageView.layer.borderColor = kSEPRATORUICOLOR.CGColor;
    //imageView.layer.borderWidth = 1;
    imageView.clipsToBounds = YES;
    
}


-(void)updateProfilePicture:(UIImage*)image
{
    profilePicture = image;
}

-(void)getUserData:(NSString*)path
{
    userReady = NO;
    userURL = path;
    [self updateUserData];
}

-(void)dataReady:(id)data
{
    DMSG;
    //[self setUserDetails:data];
    [self performSelectorOnMainThread:@selector(setUserDetails:) withObject:data waitUntilDone:NO];
}

-(void)updateUserData
{
    userReady = NO;
    [[wbAPI sharedAPI] getDataWithURL:userURL delegate:self];
}

-(NSDictionary*)userFeed
{
    NSString *feedEndpoint;
    if ([[[wbUser sharedUser] userId] integerValue] == [userId integerValue]) {
        feedEndpoint = [NSString stringWithFormat:@"%@?type=my",kVIDEOAPI];
    } else {
        feedEndpoint = [NSString stringWithFormat:@"search/videos/?user=%@",userId];
    }
    
    NSMutableDictionary *feed = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 feedEndpoint,@"endpoint",
                                 [NSNumber numberWithInteger:0],@"id",
                                 [NSNumber numberWithInteger:0],@"default",
                                 username,@"name",
                                 @"monkey.png",@"icon",
                                 @"user",@"type",
                                 userURL,@"userUrl",
                                 userId,@"userId",
                                 nil];
    
    if ([profilePictureURL length] > 0) {
        [feed setObject:[NSString stringWithFormat:kPROFILEPICPATH,profilePictureURL] forKey:@"icon_url"];
    }
    
    return feed;
}

-(NSDictionary*)userMyFeed
{
    NSMutableDictionary *feed = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 [NSString stringWithFormat:@"%@?type=my",kVIDEOAPI],@"endpoint",
                                 [NSNumber numberWithInteger:0],@"id",
                                 [NSNumber numberWithInteger:0],@"default",
                                 @"MAIN_FEED_MY_VIDEOS",@"name",
                                 @"myvideos.png",@"icon",
                                 @"mission",@"type",
                                 userURL,@"userUrl",
                                 userId,@"userId",
                                 nil];
    return feed;
}

-(void)getUserButton:(wbButton*)userButton
{
    [self userButton:userButton controller:nil txt:YES fontSize:12];
}

-(void)getUserButton:(wbButton*)userButton fontSize:(NSInteger)fontSize
{
    [self userButton:userButton controller:nil txt:YES fontSize:fontSize];
}

-(void)userButton:(wbButton*)userButton controller:(id)controller txt:(BOOL)txtBool
{
    [self userButton:userButton controller:controller txt:txtBool fontSize:12];
}

-(void)userButton:(wbButton*)userButton controller:(id)controller txt:(BOOL)txtBool fontSize:(NSInteger)fontSize
{
    if (username != nil) {
        if (txtBool){
            NSMutableAttributedString *txt = [[NSMutableAttributedString alloc] initWithString:username];
            [txt addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"HelveticaNeue-Bold" size:fontSize] range:NSMakeRange(0, username.length)];
            [txt addAttribute:NSForegroundColorAttributeName value:kGREENUICOLOR range:NSMakeRange(0, username.length)];
            [userButton setAttributedTitle:txt forState:UIControlStateNormal];
            txt = nil;
            [userButton sizeToFit];
        }
        [userButton setBackgroundColor:kTRANSPARENTUICOLOR];
        //[userButton setBackgroundColor:[UIColor yellowColor]];
        //[userButton setPayload:controller];
        [userButton setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [userButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        [userButton setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        //if (![username isEqualToString:[[wbUser sharedUser] username]]) {
            [userButton addTarget:self action:@selector(openUserFeed:) forControlEvents:UIControlEventTouchUpInside];
        //}
    }
}

-(void)openUserFeed:(id)sender
{
    //[kROOTVC performSelector:@selector(startNewVideoFeed:feed:) withObject:nil withObject:[self userFeed]];
    [kROOTVC performSelector:@selector(startUserPage:user:) withObject:nil withObject:self];
}

-(void)printUser
{
    NSString *printString = @"\n*** user data: ***\n";
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* username: %@\n",username]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* email: %@\n",email]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* id: %@\n",userId]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* url: %@\n",userURL]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* profilepic: %@\n",profilePictureURL]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* videos: %@\n",num_videos]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* followers: %@\n",num_followers]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* followings: %@\n",num_followings]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* like/dislike: %@/%@\n",num_likes,num_dislikes]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* birthday: %@\n",birthday]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* country: %@\n",countryDictionary]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* description: %@\n",descriptionTxt]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* userReady: %d\n",userReady]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"* is_following: %d\n",is_following]];
    printString = [printString stringByAppendingString:[NSString stringWithFormat:@"*******************"]];
    DNSLog(@"%@",printString);
    printString = nil;
}
-(void)setCountryDictionary:(NSDictionary *)_countryDictionary
{
    countryDictionary = _countryDictionary;
    if ([[countryDictionary valueForKey:@"supported"] boolValue]) {
        country = [countryDictionary objectForKey:@"country"];
    } else {
        country = @"ZZ";
    }
}
-(void)setNewBirthday:(NSString*)_bday
{
    DCMSG(_bday);
    birthday = _bday;
    birthdayString = [self getBirthdaySting];

    [self performSelectorInBackground:@selector(updateUserData) withObject:nil];
}

-(void)setNewDescription:(NSString*)_desc
{
    descriptionTxt = _desc;
    [self performSelectorInBackground:@selector(updateUserData) withObject:nil];
}

-(void)setNewEmail:(NSString*)_email
{
    email = _email;
    [self performSelectorInBackground:@selector(updateUserData) withObject:nil];
}

-(void)flushUser
{
    birthday = nil;
    birthdayString = nil;
    country = nil;
    countryDictionary = nil;
    activation_state = nil;
    descriptionTxt = nil;
    email = nil;
    userId = nil;
    userscore = nil;
    num_dislikes = nil;
    num_followers = nil;
    num_followings = nil;
    num_likes = nil;
    num_videos = nil;
    profilePictureURL = nil;
    //profilePictureImageView = nil;
    userRank = nil;
    userURL = nil;
    username = nil;
    profilePicture = nil;
    userFeed = nil;
    userMyFeed = nil;
    userType = nil;
    userFollowerLabel = nil;
    userFollowingLabel = nil;
    userReady = NO;
}

@end
