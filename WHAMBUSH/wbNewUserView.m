//
//  wbNewUserView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbNewUserView.h"
#import "AFNetworking.h"

@implementation wbNewUserView

@synthesize user;
@synthesize insertTxtView;
@synthesize countryFlag;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:kBKGUICOLOR];
        isMe = NO;
   
        insertTxtView = [[wbInsertTextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [insertTxtView setDelegate:self];
        [insertTxtView setMaxCharCount:90];
        [insertTxtView setMaxLinesCount:3];
        [insertTxtView setHeaderText:NSLocalizedString(@"USER_ADD_DESCRIPTION",@"")];

        [self setDelegate:self];
        
        
    }
    DPRINTCLASS;
    return self;
}

//-(void)setUser:(wbUser *)_user
//{
//    user = _user;
//}

-(void)checkIsMe
{
    if ([[user userId] integerValue] == [[[wbUser sharedUser] userId] integerValue]) {
        isMe = YES;
    } else {
        isMe = NO;
    }
}

#define kMARGINAL 25
#define kPICTURESIZE 75
#define kPPBUTTONSIZE 20

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self checkIsMe];
    if ([user userReady] && user != nil) {
        kHIDEWAIT;
        [[wbHeader sharedHeader] setLogo:[user username]];
        
        if (profilePicture == nil) {
            profilePicture = [[UIImageView alloc] init];
        }
        [profilePicture setFrame:CGRectMake(kMARGINAL, kMARGINAL, kPICTURESIZE, kPICTURESIZE)];
        [user userProfilePictureImageView:profilePicture];
        
        [self addSubview:profilePicture];
        
        if (isMe) {
            if (profilePictureButton == nil) {
                profilePictureButton = [wbButton buttonWithType:UIButtonTypeSystem];
            }
            [profilePictureButton setFrame:profilePicture.frame];
            if ([[[wbUser sharedUser] profilePictureURL] length] < 1) {
                [profilePictureButton setBackgroundImage:[UIImage ch_imageNamed:@"add_profile_pic.png"] forState:UIControlStateNormal];
                //[profilePictureButton setAlpha:0.7];
            }
            [profilePictureButton addTarget:self action:@selector(showAskProfilePic:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:profilePictureButton];
        }
        
        if (countryFlag == nil) {
            countryFlag = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"profile_badge_global"]];
        }
        [countryFlag setFrame:CGRectMake(CGRectGetMaxX(profilePicture.frame)-countryFlag.image.size.width, CGRectGetMaxY(profilePicture.frame)-countryFlag.image.size.height, countryFlag.image.size.width, countryFlag.image.size.height)];
        NSString *flagUrl = [NSString stringWithFormat:kCOUNTRYPICPATH,[[user countryDictionary] objectForKey:@"flag_url"]];
        
        __weak typeof(self) weakSelf = self;
        
        [countryFlag setImageWithURLRequest:[NSMutableURLRequest requestWithURL:[NSURL URLWithString:flagUrl]] placeholderImage:[UIImage ch_imageNamed:@"profile_badge_global"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            if (image != nil) {
                [weakSelf.countryFlag setFrame:CGRectMake(CGRectGetMaxX(profilePicture.frame)-image.size.width/2, CGRectGetMaxY(profilePicture.frame)-image.size.height/2, image.size.width/2, image.size.height/2)];
                [weakSelf.countryFlag setImage:image];
            }
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
        
        /*[NSURL URLWithString:flagUrl] placeholderImage:[UIImage ch_imageNamed:@"profile_badge_global"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            if (image != nil) {
                [countryFlag setFrame:CGRectMake(CGRectGetMaxX(profilePicture.frame)-image.size.width/2, CGRectGetMaxY(profilePicture.frame)-image.size.height/2, image.size.width/2, image.size.height/2)];
            }
        }];*/
        [self addSubview:countryFlag];
        
        if (userDescription == nil) {
            userDescription = [[wbLabel alloc] init];
        }
        float udw = self.frame.size.width - (kMARGINAL + profilePicture.frame.size.width +  kMARGINAL/2 + kMARGINAL/2);
        [userDescription setFrame:CGRectMake(CGRectGetMaxX(profilePicture.frame)+kMARGINAL/2, profilePicture.frame.origin.y, udw ,kPICTURESIZE+20)];
        [userDescription setCenter:CGPointMake(userDescription.center.x, profilePicture.center.y)];
        if ([[user descriptionTxt] length] > 0) {
            [userDescription setText:[user descriptionTxt]];
            [insertTxtView setInitialText:[user descriptionTxt]];
        } else {
            if (isMe) {
                [userDescription setText:NSLocalizedString(@"USER_ADD_DESCRIPTION",@"")];
            } else {
                [userDescription setText:NSLocalizedString(@"USER_DESCRIPTION_EMPTY",@"")];
            }

        }
        [userDescription setTextColor:kWHITEUICOLOR];
        [userDescription setFont:kFONTHelvetica(15)];
        [userDescription setNumberOfLines:0];
        [userDescription setBackgroundColor:kTRANSPARENTUICOLOR];
        [userDescription setTextAlignment:NSTextAlignmentLeft];
        [self addSubview:userDescription];
        
        if (isMe) {
            UIButton *userDescriptionButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [userDescriptionButton setBackgroundColor:kTRANSPARENTUICOLOR];
            [userDescriptionButton setFrame:userDescription.frame];
            [userDescriptionButton addTarget:insertTxtView action:@selector(showTextView) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:userDescriptionButton];
        }
        
        if (settingsButton == nil) {
            settingsButton = [wbRoundedButton buttonWithType:UIButtonTypeSystem];
        }
        [settingsButton setFrame:CGRectMake(kMARGINAL, CGRectGetMaxY(profilePicture.frame)+kMARGINAL, 0, 0)];
        [settingsButton setButtonColor:kWHITEUICOLOR];
        [settingsButton setHelvetica:NO];
        [settingsButton setButtonText:NSLocalizedString(@"USER_SETTINGS", @"")];
        [settingsButton sizeToFit];
        [settingsButton addTarget:self action:@selector(showSettings:) forControlEvents:UIControlEventTouchUpInside];
        
        if (followButton == nil) {
            followButton = [wbRoundedButton buttonWithType:UIButtonTypeSystem];
        }
        [followButton setFrame:CGRectMake(kMARGINAL, CGRectGetMaxY(profilePicture.frame)+kMARGINAL, 0, 0)];
        if ([user is_following]) {
            [followButton setButtonColor:kREDUICOLOR];
            [followButton setButtonText:[NSLocalizedString(@"GENERAL_UNFOLLOW_BUTTON", @"") uppercaseString]];
        } else {
            [followButton setButtonColor:kGREENUICOLOR];
            [followButton setButtonText:[NSLocalizedString(@"GENERAL_FOLLOW_BUTTON", @"") uppercaseString]];
        }
        [followButton setHelvetica:NO];
        [followButton setFilled:YES];
        [followButton sizeToFit];
        [followButton addTarget:self action:@selector(toggleFollow) forControlEvents:UIControlEventTouchUpInside];
        
        if (bananaCount == nil) {
            bananaCount = [[wbLabel alloc] init];
        }
        [bananaCount setText:[[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"USER_BANANA_COUNT", @""),[user userscore]] uppercaseString]];
        [bananaCount setTextColor:kYELLOWUICOLOR];
        [bananaCount setFont:kFONT(20)];
        [bananaCount sizeToFit];
        [bananaCount setCenter:settingsButton.center];
        [bananaCount setFrame:CGRectMake(self.frame.size.width-(kMARGINAL+bananaCount.frame.size.width), bananaCount.frame.origin.y, bananaCount.frame.size.width, bananaCount.frame.size.height)];
        [self addSubview:bananaCount];
        
        
        
        UIImage *profileBkgImage = [UIImage ch_imageNamed:@"profile_background.jpg"];
        UIImageView *profileBkgImageView  = [[UIImageView alloc] initWithImage:profileBkgImage];
        [profileBkgImageView setFrame:CGRectMake(0, (CGRectGetMaxY(settingsButton.frame)+kMARGINAL)-profileBkgImage.size.height, self.frame.size.width, profileBkgImage.size.height)];
        
        [self insertSubview:profileBkgImageView atIndex:0];
        
        
        if (slideViewController == nil) {
            slideViewController = [[wbUserViewSlideViewController alloc] init];
        }
        [slideViewController setUser:user];
        [slideViewController.view setFrame:CGRectMake(0, CGRectGetMaxY(settingsButton.frame)+kMARGINAL/2, self.frame.size.width, self.frame.size.height)];
        //[slideViewController.view setBackgroundColor:kREDUICOLOR];
        [self addSubview:slideViewController.view];
        
        [self setContentSize:CGSizeMake(self.frame.size.width, CGRectGetMaxY(slideViewController.view.frame)-kMARGINAL/2)];
        
        if (isMe) {
            //enable edit buttons here
            [self addSubview:settingsButton];
            [self addSubview:insertTxtView];
        } else {
            [self addSubview:followButton];
        }
        
    } else {
        kSHOWWAIT;
        [self performSelector:@selector(setNeedsDisplay) withObject:nil afterDelay:1];
    }
}

-(void)showSettings:(id)sender
{
    [kROOTVC performSelector:@selector(startSettings:) withObject:nil];
}

-(void)toggleFollow
{
    if ([[wbAPI sharedAPI] is_guest]) {
        [[wbLoginRegisterViewController sharedLRVC] showAskToLoginRegisterOnView:[[[wbAPI sharedAPI] currentViewController] view]];
    } else {
        [followButton setUserInteractionEnabled:NO];
        
        if ([user is_following]) {
            DMSG;
            [[wbAPI sharedAPI] unfollowUser:user delegate:nil];
            [followButton setTitle:[NSLocalizedString(@"GENERAL_FOLLOW_BUTTON", @"") uppercaseString] forState:UIControlStateNormal];
            [followButton setButtonColor:kGREENUICOLOR];
        } else {
            DMSG;
            [[wbAPI sharedAPI] followUser:user delegate:nil];
            [followButton setTitle:[NSLocalizedString(@"GENERAL_UNFOLLOW_BUTTON", @"") uppercaseString] forState:UIControlStateNormal];
            [followButton setButtonColor:kREDUICOLOR];
        }
        [followButton setNeedsDisplay];
        [followButton sizeToFit];
        [followButton setUserInteractionEnabled:YES];
        
        [slideViewController update];
    }
}

-(void)newText:(NSString*)text  //delegate method for insetTxtView
{
    DCMSG(text);
    if (![text isEqualToString:[insertTxtView initialText]]) {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            [self performSelectorOnMainThread:@selector(updateTxt:) withObject:text waitUntilDone:YES];
        }];
        
        [op start];
    }
}

-(void)updateTxt:(NSString*)text
{
    NSDictionary *updateUserDetails = [NSDictionary dictionaryWithObjectsAndKeys:text,@"description", nil];
    [[wbAPI sharedAPI] updateUserDetails:updateUserDetails];
    [[wbUser sharedUser] setNewDescription:text];
    [insertTxtView setInitialText:[[wbUser sharedUser] descriptionTxt]];
    
    if ([text length] > 0) {
        [userDescription setText:[[wbUser sharedUser] descriptionTxt]];
    } else {
        [userDescription setText:NSLocalizedString(@"USER_ADD_DESCRIPTION",@"")];
    }
    kHIDEWAIT
}


-(void)scrollTableTop
{
    [self setContentOffset:CGPointZero animated:YES];
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (scrollView.contentOffset.y > CGRectGetMinY(slideViewController.view.frame)) {
//        NSLog(@"topped");
//    }
//}

-(void)showAskProfilePic:(id)view
{
    IBActionSheet *actionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"USER_SETTINGS_PROFILE_PICTURE_TITLE",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"GENERAL_CANCEL", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"USER_SETTINGS_PROFILE_PICTURE_GALLERY",@""),NSLocalizedString(@"USER_SETTINGS_PROFILE_PICTURE_CAMERA",@""), nil];
    
    [actionSheet setTitleBackgroundColor:kTOPUICOLOR];
    [actionSheet setTitleTextColor:kLIGHTGRAYUICOLOR];
    [actionSheet setButtonTextColor:kLIGHTGRAYUICOLOR];
    [actionSheet setButtonBackgroundColor:kTOPUICOLOR forButtonAtIndex:0];
    [actionSheet setButtonBackgroundColor:kTOPUICOLOR forButtonAtIndex:1];
    [actionSheet setButtonBackgroundColor:kREDUICOLOR forButtonAtIndex:2];
    [actionSheet setAlpha:1];
    [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
}

-(void)refreshProfilePicture
{
//    [profilePicture setNeedsDisplay];
    [user userProfilePictureImageView:profilePicture];
    [profilePictureButton setBackgroundImage:nil forState:UIControlStateNormal];

}

-(void)refreshTabs
{
    [slideViewController performSelector:@selector(refreshTabs)];
}

-(void)refreshRow
{
    [slideViewController performSelector:@selector(refreshRow)];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DNSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0) {
        [kROOTVC performSelector:@selector(startProfilePicGallery:) withObject:self];
    } else if (buttonIndex == 1) {
        [kROOTVC performSelector:@selector(startProfilePicCamera:) withObject:self];
    }
}

@end
