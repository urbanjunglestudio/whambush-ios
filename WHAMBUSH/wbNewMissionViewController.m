//
//  wbNewMissionViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbNewMissionViewController.h"

@interface wbNewMissionViewController ()

@end

@implementation wbNewMissionViewController

@synthesize countrySelect;

-(void)loadView
{
    wbNewMissionTableView *missionView = [[wbNewMissionTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [self setView:missionView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    countrySelect = [[wbMissionCountrySelectViewController alloc] init];
    [countrySelect setMissionView:self.view];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [super viewDidAppear:animated];
    //[[wbData sharedData] setHasMissions:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    [[UIApplication sharedApplication] setStatusBarOrientation:1];
    [self.view performSelector:@selector(setGAIViewName)];
    [[wbHeader sharedHeader] setShowChangeCountryButton:YES];
    
    [[wbAPI sharedAPI] setNum_active_missions:@"0"];
    [[wbFooter sharedFooter] setNeedsDisplay];
    [(wbNewMissionTableView*)[self view] refreshRow];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
