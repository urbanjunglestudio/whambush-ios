//
//  wbUploadView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/27/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbUploadView.h"

@implementation wbUploadView

//@synthesize videoWeb;
//@synthesize descriptionTxt;
//@synthesize tagTxt;
//@synthesize descriptionButton;
//@synthesize titleTxt;
//@synthesize uploaded;
//@synthesize activeField;
@synthesize uploadData;

- (id)initWithFrame:(CGRect)frame controller:(UIViewController*)_controller
{
    self = [super initWithFrame:frame];
    if (self) {
        
        DNSLog(@"w: %.2f, h: %.2f",frame.size.width,frame.size.height);
        // Initialization code
        [self setBackgroundColor:kBKGUICOLOR];
        
        missionData = [[[wbAPI sharedAPI] uploadData] objectForKey:@"missionData"];
        guestTriedToUploadMission = NO;
        
        DCMSG(missionData);
        if (missionData != nil) {
            isMission = YES;
            titleSet = YES;
            title = [missionData objectForKey:@"name"];
            [[wbData sharedData] refreshMissionData:@"default" delegate:self];
            [self missionSelected:missionData];
            chooseMissionString = NSLocalizedString(@"UPLOAD_MISSION_VIDEO", @"");
        } else {
            isMission = NO;
            titleSet = NO;
            title = NSLocalizedString(@"UPLOAD_ADD_TITLE",@"");
            [[wbData sharedData] refreshMissionData:@"default" delegate:self];
            chooseMissionString = NSLocalizedString(@"UPLOAD_NORMAL_VIDEO", @"");

        }
        kSHOWWAIT;
       
        videoPlayerView = [[wbVideoPlayerViewController alloc] init];
        [videoPlayerView setViewRect:CGRectMake(0, 0,self.bounds.size.width, (self.bounds.size.width/16)*9)];
        
        uploaded = NO;
        controller = _controller;
        
        content = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, [self frame].size.width, [self frame].size.height)];//-[[wbAPI sharedAPI] headerBarHeight])];
        [content setDelegate:self];
        [content setScrollEnabled:YES];
        [content setContentSize:CGSizeMake(CGRectGetWidth(frame), [self frame].size.height+[[wbAPI sharedAPI] footerBarHeight]/*(1136/2)-[[wbAPI sharedAPI] footerBarHeight]-[[wbAPI sharedAPI] headerBarHeight]*/)];
        [content setShowsHorizontalScrollIndicator:YES];
        
        if (!descriptionSet) {
            descriptionSet = NO;
            description = NSLocalizedString(@"UPLOAD_ADD_DESCRIPTION",@"n");
        }
        if (tagStrings == nil) {
            tagStrings = [NSMutableArray arrayWithObjects:NSLocalizedString(@"UPLOAD_ADD_TAG",@""),nil];
        }
        
        tagView = [[wbTagsView alloc] init];
        [tagView setTagStrings:tagStrings];
        [tagView setDelegate:self];

//        insertTxtView = [[wbInsertTextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//        [insertTxtView setDelegate:self];
       
        //[GUI setGreen:NO];
        saved = NO;
        //partnerVideoSwitchValue = YES;
        //partnerVideoLabelString = NSLocalizedString(@"UPLOAD_PARTNER_MISSION_VIDEO", @"");
        
//        if (missionSelectView == nil) {
//            missionSelectView = [[wbUploadMissionSelectView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//            [missionSelectView setUserInteractionEnabled:NO];
//        }
        //[missionSelectView setMissions:activeMissionData];
//        [missionSelectView setDelegate:self];
    }
    return self;
}

-(void)dataReady:(id)data
{
    DCMSG(data);
    activeMissionData = data;
    
    if (missionSelectView == nil) {
        missionSelectView = [[wbUploadMissionSelectView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    [missionSelectView setDelegate:self];
    [missionSelectView setMissions:activeMissionData];
    [missionSelectView setNeedsDisplay];
    [missionSelectView setUserInteractionEnabled:NO];
    
    [self addSubview:missionSelectView];
    [self bringSubviewToFront:missionSelectView];
    
    kHIDEWAIT;
}

-(void)setUploadData:(NSDictionary *)_uploadData
{
    DCMSG(@"setuploaddata");
    
    titleSet = YES;
    title = [_uploadData objectForKey:@"title"];
    
    if ([_uploadData objectForKey:@"description"] != nil) {
        description = [_uploadData objectForKey:@"description"];
    }
    if ([_uploadData objectForKey:@"tagStrings"] != nil) {
        tagStrings = [_uploadData objectForKey:@"tagStrings"];
        [tagView setTagStrings:tagStrings];
    }
    if ([_uploadData objectForKey:@"missionData"] != nil) {
        missionData = [_uploadData objectForKey:@"missionData"];
    }
    if ([_uploadData objectForKey:@"isMission"] != nil) {
        isMission  = [[_uploadData objectForKey:@"isMission"] boolValue];
    }
    if ([_uploadData objectForKey:@"descriptionSet"] != nil) {
        descriptionSet = [[_uploadData objectForKey:@"descriptionSet"] boolValue];
    }
    /*if ([_uploadData objectForKey:@"partnerVideo"] != nil) {
        [partnerVideoSwitchButton setSelected:[[_uploadData objectForKey:@"partnerVideo"] boolValue]];
        partnerVideoSwitchValue = [[_uploadData objectForKey:@"partnerVideo"] boolValue];
    }*/
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    NSURL *url = [[[wbAPI sharedAPI] uploadData]objectForKey:@"url"];
    [videoPlayerView setVideoURL:url];
    [(wbVideoPlayerView*)videoPlayerView.view setIsRed:YES];
    [content addSubview:videoPlayerView.view];
    
//choose mission
    if (chooseMission == nil) {
        chooseMission = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    [chooseMission setFrame:CGRectMake(0, CGRectGetMaxY(videoPlayerView.view.frame)+4, self.frame.size.width, kTXTBOXH)];
    [chooseMission setTitleEdgeInsets:UIEdgeInsetsMake(3, 6, 3, 0)];
    [chooseMission setBackgroundColor:kBOTTOMUICOLORAlpha];
    [chooseMission setAlpha:0.8];
    [chooseMission setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [chooseMission setTitleColor:kWHITEUICOLOR forState:UIControlStateNormal];
    [chooseMission addTarget:self action:@selector(chooseAMission:) forControlEvents:UIControlEventTouchUpInside];
    [chooseMission setTitle:chooseMissionString forState:UIControlStateNormal];
    [chooseMission.titleLabel setFont:kFONTHelvetica(18)];
    
    UIImage *arrowImg = [[wbAPI sharedAPI] changeColorForImage:[UIImage ch_imageNamed:@"feed_arrow.png"] toColor:kWHITEUICOLOR];
    UIImageView *arrow = [[UIImageView alloc] init];
    [arrow setImage:arrowImg];
    [arrow setFrame:CGRectMake(0, 0, arrowImg.size.width/1.5, arrowImg.size.height/1.5)];
    [arrow setCenter:CGPointMake(self.frame.size.width-20, chooseMission.center.y)];
    [arrow setBackgroundColor:kTRANSPARENTUICOLOR];
    
    //if (!isMission) {
        [content addSubview:chooseMission];
        [content addSubview:arrow];
    //}
    
//title
    if (addTitle == nil) {
        addTitle = [wbButton buttonWithType:UIButtonTypeSystem];
    }

    //[addTitle setFrame:CGRectMake(0, CGRectGetMaxY(videoWeb.frame)+4, self.frame.size.width, kTXTBOXH)];
    [addTitle setTitleEdgeInsets:UIEdgeInsetsMake(3, 6, 3, 0)];
    [addTitle setBackgroundColor:kBOTTOMUICOLORAlpha];
    [addTitle setAlpha:0.8];
    [addTitle setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [addTitle setTitleColor:kWHITEUICOLOR forState:UIControlStateNormal];
    [addTitle addTarget:self action:@selector(modifyTitle) forControlEvents:UIControlEventTouchUpInside];
    [addTitle setFrame:CGRectMake(0, CGRectGetMaxY(chooseMission.frame)+4, self.frame.size.width, kTXTBOXH)];
    if (isMission) {
        //[addTitle setFrame:CGRectMake(0, CGRectGetMaxY(videoPlayerView.view.frame)+4, self.frame.size.width, kTXTBOXH)];
        [addTitle setTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"MAIN_MISSION_PREFIX",@""),[title uppercaseString]] forState:UIControlStateNormal];
        [addTitle.titleLabel setFont:kFONTLeague(24)];
        [addTitle setEnabled:NO];
    } else {
        if (titleSet) {
            [addTitle setTitle:[title uppercaseString] forState:UIControlStateNormal];
            [addTitle.titleLabel setFont:kFONTLeague(24)];
        } else {
            [addTitle setTitle:title forState:UIControlStateNormal];
            [addTitle.titleLabel setFont:kFONTHelvetica(18)];
        }
    }
    [addTitle setPreserveWidth:YES];
    [content addSubview:addTitle];
    
    
//description
    if (addDescription == nil) {
        addDescription = [[wbLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(addTitle.frame)+4, self.frame.size.width, kTXTBOXH)];
    }
    [addDescription setEdgeInsets:UIEdgeInsetsMake(3, 6, 3, 6)];
    [addDescription setBackgroundColor:kBOTTOMUICOLORAlpha];
    [addDescription setAlpha:0.8];
    [addDescription setFont:kFONTHelvetica(18)];
    [addDescription setText:description];
    [addDescription setTextColor:kWHITEUICOLOR];
    [addDescription setNumberOfLines:0];
    
    addDescriptionBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [addDescriptionBtn setFrame:addDescription.frame];
    [addDescriptionBtn setBackgroundColor:kTRANSPARENTUICOLOR];
    [addDescriptionBtn addTarget:self action:@selector(modifyDescription) forControlEvents:UIControlEventTouchUpInside];
    
    [content addSubview:addDescription];
    [content addSubview:addDescriptionBtn];
   
//tags
    [tagView setFrame:CGRectMake(0, CGRectGetMaxY(addDescription.frame)+4, self.frame.size.width, kTXTBOXH)];
    [tagView setAlpha:0.8];
    [content addSubview:tagView];

    
//switch
    /*if (partnerVideoSwitchButton == nil) {
        partnerVideoSwitchButton = [[UIButton alloc] init];
        [partnerVideoSwitchButton setSelected:YES];
    }
    if (partnerVideoLabel == nil) {
        partnerVideoLabel = [[wbLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tagView.frame)+4, self.frame.size.width, kTXTBOXH)];
    }

    if ([[[wbUser sharedUser] userType] intValue] == 1) {
        [partnerVideoSwitchButton setFrame:CGRectMake(6, 1, kTXTBOXH-1, kTXTBOXH-1)];
        [partnerVideoSwitchButton setTintColor:kBKGUICOLOR];
        [partnerVideoSwitchButton setSelected:partnerVideoSwitchValue];
        [partnerVideoSwitchButton addTarget:self action:@selector(toggleSwitch:) forControlEvents:UIControlEventTouchUpInside];
        [partnerVideoSwitchButton setBackgroundImage:[UIImage ch_imageNamed:@"tickno.png"] forState:UIControlStateNormal];
        [partnerVideoSwitchButton setBackgroundImage:[UIImage ch_imageNamed:@"tickyes.png"] forState:UIControlStateSelected];
        
        [partnerVideoLabel setEdgeInsets:UIEdgeInsetsMake(3, CGRectGetWidth(partnerVideoSwitchButton.frame)+12, 3, 6)];
        [partnerVideoLabel setText:partnerVideoLabelString];
        [partnerVideoLabel setBackgroundColor:kBOTTOMUICOLORAlpha];
        [partnerVideoLabel setAlpha:0.8];
        [partnerVideoLabel setFont:kFONTHelvetica(18)];
        [partnerVideoLabel setTextColor:kWHITEUICOLOR];
        [partnerVideoLabel setNumberOfLines:0];
        [partnerVideoLabel setUserInteractionEnabled:YES];
        
        [content addSubview:partnerVideoLabel];
        [partnerVideoLabel addSubview:partnerVideoSwitchButton];
    }
    */
    [self addSubview:content];
    
    
//Draw bars and buttons
//    super.header = [GUI headerBarWithBack:self];
//    super.footer = [GUI uploadFooterBar:self];
//    [self addSubview:super.header];
//    [self addSubview:super.footer];
    

    if (insertTxtView == nil) {
        insertTxtView = [[wbInsertTextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    [self addSubview:insertTxtView];
    kHIDEWAIT;

}

-(void)toggleSwitch:(id)sender
{
//    if ([partnerVideoSwitch isOn]) {
/*    if (partnerVideoSwitchValue) {
        partnerVideoSwitchValue = NO;
        partnerVideoLabelString = NSLocalizedString(@"UPLOAD_PARTNER_NORMAL_VIDEO", @"");
    } else {
        partnerVideoSwitchValue = YES;
        partnerVideoLabelString = NSLocalizedString(@"UPLOAD_PARTNER_MISSION_VIDEO", @"");
    }
    [partnerVideoLabel setText:partnerVideoLabelString];
    [partnerVideoSwitchButton setSelected:partnerVideoSwitchValue];
*/
}

-(void)modifyTitle
{
    DMSG;
    whosTurn = @"title";
    [insertTxtView enableView];
    [insertTxtView setDelegate:self];
    [insertTxtView setMaxCharCount:32];
    [insertTxtView setMaxLinesCount:1];
    [insertTxtView setAllowEmojii:YES];
    [insertTxtView setHeaderText:NSLocalizedString(@"UPLOAD_ADD_TITLE",@"")];
    if (titleSet) {
        [insertTxtView showTextViewWithText:title];
    } else {
        [insertTxtView showTextViewWithText:@""];
    }
}

-(void)modifyDescription
{
    DMSG;
    whosTurn = @"description";
    [insertTxtView enableView];
    [insertTxtView setDelegate:self];
    [insertTxtView setMaxCharCount:150];
    [insertTxtView setMaxLinesCount:6];
    [insertTxtView setAllowEmojii:YES];
    [insertTxtView setHeaderText:NSLocalizedString(@"UPLOAD_ADD_DESCRIPTION",@"")];
    if (descriptionSet) {
        [insertTxtView showTextViewWithText:description];
    } else {
        [insertTxtView showTextViewWithText:@""];
    }
}

-(void)modifyTag:(id)sender
{
    DMSG;
    NSInteger index = [(UIButton*)sender tag];
    whosTurn = @"tag";
    currentTag = index;
    [insertTxtView enableView];
    [insertTxtView setDelegate:self];
    [insertTxtView setMaxCharCount:16];
    [insertTxtView setMaxLinesCount:1];
    [insertTxtView setAllowEmojii:NO];
    [insertTxtView setHeaderText:NSLocalizedString(@"UPLOAD_ADD_TAG",@"")];
    if (index > 0) {
        [insertTxtView showTextViewWithText:[tagStrings objectAtIndex:index]];
    } else {
        [insertTxtView showTextViewWithText:@""];
    }
}

-(void)newText:(NSString*)text  //delegate method for insetTxtView
{
    DMSG;
    DCMSG(text);
    if ([text length] > 0) {
        DMSG;
        if ([whosTurn isEqualToString:@"title"]) {
            title = [NSString stringWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [addTitle setTitle:[title uppercaseString] forState:UIControlStateNormal];
            [addTitle.titleLabel setFont:kFONTLeague(24)];
            [addTitle setFrame:CGRectMake(0, CGRectGetMaxY(chooseMission.frame)+4, self.frame.size.width, kTXTBOXH)];
            //[addTitle sizeToFit];
            titleSet = YES;
        }
    }
    if ([whosTurn isEqualToString:@"description"]) {
        if ([text length] > 0) {
            description = [NSString stringWithString:[text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [addDescription setFont:kFONTHelvetica(16)];
            [addDescription setNumberOfLines:6];
            descriptionSet = YES;
        } else {
            description = NSLocalizedString(@"UPLOAD_ADD_DESCRIPTION",@"");
            [addDescription setFont:kFONTHelvetica(18)];
            descriptionSet = NO;
        }
        [addDescription setText:description];
        [addDescription sizeToFit];
        float newHeight;
        if (addDescription.frame.size.height < kTXTBOXH) {
            newHeight = kTXTBOXH;
        } else {
            newHeight = addDescription.frame.size.height;
        }
        [addDescription setFrame:CGRectMake(addDescription.frame.origin.x, addDescription.frame.origin.y, self.frame.size.width, newHeight)];
        [addDescriptionBtn setFrame:addDescription.frame];
        [tagView setFrame:CGRectMake(0, CGRectGetMaxY(addDescription.frame)+4, self.frame.size.width, CGRectGetHeight(tagView.frame))];
        //[partnerVideoLabel setFrame:CGRectMake(0, CGRectGetMaxY(tagView.frame)+4, self.frame.size.width, CGRectGetHeight(partnerVideoLabel.frame))];
    }
    
    if ([whosTurn isEqualToString:@"tag"]) {
        DCMSG(tagStrings);
        
        NSString *tagS = [self parseTagString:text];
        
        if ([tagS length] > 0) {
            if  (currentTag == 0) {
                [tagStrings addObject:[NSString stringWithString:tagS]];
            } else {
                [tagStrings setObject:[NSString stringWithString:tagS] atIndexedSubscript:currentTag];
            }
        } else {
            if (currentTag > 0) {
                [tagStrings removeObjectAtIndex:currentTag];
            }
        }
        DCMSG(tagStrings);
        [[tagView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [tagView setNeedsDisplay];
        }
}

-(NSString*)parseTagString:(NSString*)string
{
    DCMSG(string);
    
    string = [string stringByReplacingOccurrencesOfString:@"#" withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"," withString:@""];
    string = [string stringByReplacingOccurrencesOfString:@"." withString:@""];
    DCMSG(string);
    return string;
}


-(void)goBack:(id)selector
{
    if(!uploaded) {
        UIAlertView *ask = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"GENERAL_WARNING_TITLE", @"") message:  NSLocalizedString(@"UPLOAD_WARNING_VIDEO_NOT_UPLOADED",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"GENERAL_YES",@"") otherButtonTitles:NSLocalizedString(@"GENERAL_NO",@""), nil];
        [ask show];
    } else {
        videoPlayerView = nil;
        [[wbUpload sharedUpload] cleanTmp];
        [[wbAPI sharedAPI] clearUploadData];
        [controller performSelector:@selector(goBack:) withObject:self];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DNSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 0) {
        videoPlayerView = nil;
        [[wbFooter sharedFooter] setUploadView:nil];
        [[wbHeader sharedHeader] setUploadView:nil];
        [[wbHeader sharedHeader] setGreen:YES];
        [[wbUpload sharedUpload] cleanTmp];
        [[wbAPI sharedAPI] clearUploadData];
        [controller performSelector:@selector(goBack:) withObject:self];
    }
}

-(void)checkMissionUploadErrors
{
    if (!titleSet) {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"UPLOAD_ERROR_NO_TITLE", @"")];
    } else {
            if ([[wbAPI sharedAPI] is_guest]) {
                [insertTxtView disableView];
                guestTriedToUploadMission = YES;
                [[wbLoginRegisterViewController sharedLRVC] showAskToLoginRegisterOnView:self];
            } else {
                if (!isMission) {
                    [self upload:nil];
//                } else if ([[missionData objectForKey:@"has_submitted"] integerValue] < 1 && guestTriedToUploadMission == NO) {
//                    [self upload:nil];
                } else {
                    if ([[wbAPI sharedAPI] checkIfNetworkAvailable]) {
                        kSHOWWAIT;
                        [[wbAPI sharedAPI] getJSONWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/missions/%@/",kWHAMBUSHAPIURL,[missionData objectForKey:@"id"]]]
                                             response:^(id data){
                                                 if ([[data objectForKey:@"has_submitted"] integerValue] >= [[data objectForKey:@"max_user_submissions"] integerValue]) {
                                                     [[wbUpload sharedUpload] saveVideo:[[[wbAPI sharedAPI] uploadData] objectForKey:@"url"]];
                                                     kHIDEWAIT;
                                                     [[wbAPI sharedAPI] performSelectorOnMainThread:@selector(showErrorWithTxt:) withObject:NSLocalizedString(@"UPLOAD_ERROR_MISSION_DONE", @"") waitUntilDone:NO];
                                                 } else {
                                                     missionData = data;
                                                     [self upload:nil];
                                                 }
                                             }];
                    } else {
                        [[wbUpload sharedUpload] saveVideo:[[[wbAPI sharedAPI] uploadData] objectForKey:@"url"]];
                    }
               }
            }
    }
}

-(void)upload:(id)selector
{
       NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            NSMutableDictionary *data= [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        title,@"title",
                                        description,@"description",
                                        tagStrings,@"tagStrings",
                                        [NSNumber numberWithBool:descriptionSet],@"descriptionSet",
                                        [NSNumber numberWithBool:isMission],@"isMission",
                                        //[NSNumber numberWithBool:partnerVideoSwitchValue],@"partnerVideo",
                                        nil];
            
            if (isMission) {
                [data setObject:missionData forKey:@"missionData"];
            }
            
            [[wbFooter sharedFooter] setUploadView:nil];
            [[wbHeader sharedHeader] setUploadView:nil];
            [[wbHeader sharedHeader] setGreen:YES];
            [[wbUpload sharedUpload] setUploadArray:data];
            [[wbUpload sharedUpload] setController:controller];
            [[wbUpload sharedUpload] saveVideo:[[[wbAPI sharedAPI] uploadData] objectForKey:@"url"]];
            [[wbUpload sharedUpload] performSelectorOnMainThread:@selector(doUpload) withObject:NULL waitUntilDone:NO];
            [insertTxtView disableView];
        }];
        [op start];
    
}

-(void)chooseAMission:(id)sender
{
    [self bringSubviewToFront:missionSelectView];
    [missionSelectView performSelector:@selector(show)];
}

-(void)missionSelected:(NSDictionary*)data
{
    if (data != nil) {
        DCMSG(data);
        isMission = YES;
        titleSet = YES;
        missionData = data;
        title = [data objectForKey:@"name"];
//        [addTitle setFrame:CGRectMake(0, CGRectGetMaxY(chooseMission.frame)+4, self.frame.size.width, kTXTBOXH)];
        [addTitle setTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"MAIN_MISSION_PREFIX",@""),[title uppercaseString]] forState:UIControlStateNormal];
        [addTitle.titleLabel setFont:kFONTLeague(24)];
        [addTitle setEnabled:NO];
        [chooseMission setTitle:NSLocalizedString(@"UPLOAD_MISSION_VIDEO", @"") forState:UIControlStateNormal];
    } else {
        isMission = NO;
        titleSet = NO;
        missionData = nil;
        title = NSLocalizedString(@"UPLOAD_ADD_TITLE",@"");
        [addTitle setTitle:title forState:UIControlStateNormal];
        [addTitle.titleLabel setFont:kFONTHelvetica(18)];
        [addTitle setEnabled:YES];
        [chooseMission setTitle:NSLocalizedString(@"UPLOAD_NORMAL_VIDEO", @"") forState:UIControlStateNormal];
    }
}

@end
