//
//  main.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 5/20/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "wbAppDelegate.h"
#import "QTouchposeApplication.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
#ifndef kSHOWTOUCH
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([wbAppDelegate class]));
#else
        return UIApplicationMain(argc, argv,
                                 NSStringFromClass([QTouchposeApplication class]),
                                 NSStringFromClass([wbAppDelegate class]));
#endif    
    }
}
