//
//  wbProfilePicCameraViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 16/02/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbProfilePicPickerViewController.h"

@interface wbProfilePicPickerViewController ()

@end

@implementation wbProfilePicPickerViewController

@synthesize isCamera;
@synthesize parent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setDelegate:self];
    }
    return self;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    DCMSG(info)
    UIImage* captured;
    
    if ([picker sourceType] == UIImagePickerControllerSourceTypeCamera) {
        if ([picker cameraDevice] == UIImagePickerControllerCameraDeviceFront) {
            UIImage *tmp = [info objectForKey:UIImagePickerControllerOriginalImage];
            captured = [UIImage imageWithCGImage:tmp.CGImage scale:tmp.scale orientation:UIImageOrientationLeftMirrored];
        } else {
            captured = [info objectForKey:UIImagePickerControllerOriginalImage];
        }
    } else {
        captured = [info objectForKey:UIImagePickerControllerOriginalImage];
    }
    
    editor = [DZImageEditingController new];
    [editor setImage:captured];
    wbProfilePictureOverlay *ol = [[wbProfilePictureOverlay alloc] initWithFrame:[[wbAPI sharedAPI] portraitFrame]];
    [ol setDelegate:self];
    [ol setEditorMode:YES];
    [editor setOverlayView:ol];
    [[editor overlayView] setUserInteractionEnabled:YES];
    [editor setCropRect:[ol cropRect]];
    [editor setDelegate:self];

    [self presentViewController:editor animated:YES completion:nil];
    
    //[[wbAPI sharedAPI] performSelector:@selector(updateUserProfilePicture:delegate:) withObject: [info valueForKey:UIImagePickerControllerEditedImage] withObject:self];
    //[kROOTVC performSelector:@selector(closeCamera:) withObject:self];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [kROOTVC performSelector:@selector(closeCamera:) withObject:self];
}

-(void)imageEditingControllerDidCancel:(DZImageEditingController *)editingController
{
    [editingController dismissViewControllerAnimated:YES completion:nil];
    //[kROOTVC performSelector:@selector(closeCamera:) withObject:self];
}

-(void)imageEditingController:(DZImageEditingController *)editingController didFinishEditingWithImage:(UIImage *)editedImage
{

    [editingController dismissViewControllerAnimated:NO completion:nil];
    
    UIImageWriteToSavedPhotosAlbum(editedImage,nil,nil,nil);

    [[wbAPI sharedAPI] performSelector:@selector(updateUserProfilePicture:delegate:) withObject: editedImage withObject:parent];
    [kROOTVC performSelector:@selector(closeCamera:) withObject:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (isCamera) {
        [self setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self setCameraDevice:UIImagePickerControllerCameraDeviceFront];
        frontCamera = YES;
        [self setCameraCaptureMode:UIImagePickerControllerCameraCaptureModePhoto];
        [self setShowsCameraControls:NO];
        wbProfilePictureOverlay *ol = [[wbProfilePictureOverlay alloc] initWithFrame:[self cameraOverlayView].frame];
        [ol setDelegate:self];
        [ol setEditorMode:NO];
        [self setCameraOverlayView:ol];
      //  [self setE]
        [[self cameraOverlayView] setUserInteractionEnabled:YES];
        //[self setModalPresentationStyle:UIModalPresentationFullScreen];
        
        CGSize screenBounds = [UIScreen mainScreen].bounds.size;
        CGFloat cameraAspectRatio = 4.0f/3.0f;
        CGFloat camViewHeight = screenBounds.width * cameraAspectRatio;
        CGFloat scale = screenBounds.height / camViewHeight;
        tmpScale = scale;
        self.cameraViewTransform = CGAffineTransformMakeTranslation(0, (screenBounds.height - camViewHeight) / 2.0);
        self.cameraViewTransform = CGAffineTransformScale(self.cameraViewTransform, scale, scale);
    
    } else {
        [self setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    }
    [self setAllowsEditing:NO];
    [self setMediaTypes:[[NSArray alloc] initWithObjects: (NSString *) kUTTypeImage, nil]];
    
    [[wbAPI sharedAPI] registerGoogleAnalytics:@"ProfilePicture"];
}

-(void)viewDidAppear:(BOOL)animated
{
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    
    [super viewDidAppear:animated];
    DMSG;
    //[self setNeedsStatusBarAppearanceUpdate];
    
    kHIDEWAIT
}


-(void)cameraButtonAction:(id)sender
{
    [self takePicture];
}

-(void)backButtonAction:(UIButton*)sender
{
    if ([sender tag] == 0) {
        [kROOTVC performSelector:@selector(closeCamera:) withObject:self];
    } else {
        [editor performSelector:@selector(cancelEditing)];
    }
}

-(void)flipCameraButtonAction:(id)sender
{
    if (frontCamera) {
        frontCamera = NO;
        [self setCameraDevice:UIImagePickerControllerCameraDeviceRear];
        [sender setSelected:YES];

    } else {
        frontCamera = YES;
        [self setCameraDevice:UIImagePickerControllerCameraDeviceFront];
        [sender setSelected:NO];
    }
    
}

-(void)doneButtonAction:(id)sender
{
    [editor performSelector:@selector(endedEditing)];
}



////#define rr radius*0.55228
//
//-(UIImage*)overlayLayer:(CGSize)size
//{
//    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width,size.height),0.0,0.0);
//    //// Bezier Drawing
//    float height = size.height;
//    float width = size.width;
//    
//    float fromLeft = 1;
//    float diameter = (size.width-(fromLeft*2));
//    float fromTop = size.height/2.5-diameter/2;
//    float radius = diameter/2;
//    float rr = radius * 4 *(sqrtf(2)-1)/3;
//    
//    cropRect = CGRectMake(0, fromTop, size.width, size.width);
//    
//    NSLog(@"dr: %.f %.f",diameter,radius);
//    
//    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
//    
//    float X1 = fromLeft+radius;
//    float Y1 = fromTop;
//    float X2 = fromLeft;
//    float Y2 = fromTop+radius;
//    float X3 = fromLeft+radius;
//    float Y3 = fromTop+diameter;
//    float X4 = fromLeft+diameter;
//    float Y4 = fromTop+radius;
//    
//    [bezierPath moveToPoint: CGPointMake(X1, Y1)];
//    [bezierPath addCurveToPoint: CGPointMake(X2, Y2)
//                  controlPoint1: CGPointMake(X1-rr, Y1)
//                  controlPoint2: CGPointMake(X2, Y2-rr)];
//
//    [bezierPath addCurveToPoint: CGPointMake(X3, Y3)
//                  controlPoint1: CGPointMake(X2, Y2+rr)
//                  controlPoint2: CGPointMake(X3-rr, Y3)];
//    
//    [bezierPath addCurveToPoint: CGPointMake(X4, Y4)
//                  controlPoint1: CGPointMake(X3+rr, Y3)
//                  controlPoint2: CGPointMake(X4, Y4+rr)];
//
//    [bezierPath addCurveToPoint: CGPointMake(X1, Y1)
//                  controlPoint1: CGPointMake(X4, Y4-rr)
//                  controlPoint2: CGPointMake(X1+rr, Y1)];
//    
//    
//    
//    [bezierPath closePath];
//    
//    [bezierPath moveToPoint: CGPointMake(0, 0)];
//    //[bezierPath addLineToPoint: CGPointMake(0, height)];
//    //[bezierPath addLineToPoint: CGPointMake(0, 0)];
//    [bezierPath addLineToPoint: CGPointMake(width, 0)];
//    [bezierPath addLineToPoint: CGPointMake(width, height)];
//    [bezierPath addLineToPoint: CGPointMake(0, height)];
//    [bezierPath addLineToPoint: CGPointMake(0, 0)];
//    //[bezierPath addLineToPoint: CGPointMake(fromLeft+radius, fromTop)];
//    [bezierPath closePath];
//    
//    [kBKGUICOLORAlpha setFill];
//    [bezierPath fill];
//    
//    UIImage *thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    
//    return thumbnailImage;
//
//}

/////////////////
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

-(UIViewController *) childViewControllerForStatusBarHidden {
    return nil;
}

- (BOOL) shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
