//
//  wbInsertTextView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 02/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
#import "wbSpeechBubbleView.h"

@interface wbInsertTextView : UIView <HPGrowingTextViewDelegate>
{
    UIView *insertTxtView;
    HPGrowingTextView *insertTxtHPView;
    UIToolbar *hideView;
    //UIToolbar *bgToolbar;
    NSInteger charCount;
    NSInteger lineCount;
    NSString *prevChar;
    BOOL backSpace;
    UILabel *insertTxtLabel;
    BOOL headerImgSet;
    //wbSpeechBubbleView *txtBkgImgView;
    UIKeyboardType keyboardType;
    NSMutableArray *allEmojis;
}

@property NSInteger maxCharCount;
@property NSInteger maxLinesCount;
@property id delegate;
@property (nonatomic,retain) NSString *headerText;
@property (nonatomic,retain) UIImage *headerImage;
@property (nonatomic,retain) NSString *initialText;
@property (nonatomic) BOOL allowEmojii;

-(void)showTextViewWithText:(NSString*)text;

-(void)showTextView;
-(void)hideTextView;

-(void)enableView;
-(void)disableView;

@end
