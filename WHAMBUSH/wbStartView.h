//
//  wbStartView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 23/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseView.h"
#import "wbCountryPickerView.h"

@interface wbStartView : wbBaseView
{
    wbCountryPickerView *pickCountry;
    UIButton *country;
    UIButton *done;
    NSDictionary *countryDictionary;
    UIActivityIndicatorView *ai;
}
@end
