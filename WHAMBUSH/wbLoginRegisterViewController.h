//
//  wbLoginRegisterViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 09/10/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "IBActionSheet.h"
#import "wbCountryPickerView.h"

@interface wbLoginRegisterViewController : NSObject <IBActionSheetDelegate,CustomIOS7AlertViewDelegate,UITextFieldDelegate>
{
    UITextField *emailField;
    UITextField *userField;
    UITextField *passField;
    UITextField *passCheckField;
    UITextField *activeField;
    CustomIOS7AlertView *currentAlertView;
    UIButton *countryButton;
    wbCountryPickerView *pickCountry;
}

@property (nonatomic,retain) NSDictionary *selectedCountry;
@property BOOL showSettings;

+(id)sharedLRVC;

-(void)showAskToLoginRegisterOnView:(id)view;
-(void)closeAlert;

@end
