//
//  wbMissionViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 21/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbMissionViewController.h"

@interface wbMissionViewController ()

@end

@implementation wbMissionViewController

@synthesize countrySelect;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)loadView
{
    wbMissionsTableView *missionsView = [[wbMissionsTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [self setView:missionsView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    countrySelect = [[wbMissionCountrySelectViewController alloc] init];
    [countrySelect setMissionView:self.view];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [super viewDidAppear:animated];
    //[[wbData sharedData] setHasMissions:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    [[UIApplication sharedApplication] setStatusBarOrientation:1];
    [self.view performSelector:@selector(setGAIViewName)];
    [[wbHeader sharedHeader] setShowChangeCountryButton:YES];
    
    [[wbAPI sharedAPI] setNum_active_missions:@"0"];
    [[wbFooter sharedFooter] setNeedsDisplay];


}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
