//
//  wbInsertTextView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 02/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbInsertTextView.h"

@implementation wbInsertTextView

//@synthesize insertTxtButton;

@synthesize delegate;
@synthesize maxCharCount;
@synthesize maxLinesCount;
@synthesize headerImage;
@synthesize headerText;
@synthesize initialText;
@synthesize allowEmojii;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setUserInteractionEnabled:NO];
        

        maxCharCount = 150;
        maxLinesCount = 10;
        backSpace = NO;
        headerImgSet = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShowIT:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillHideIT:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
        allowEmojii = YES;
        keyboardType = UIKeyboardTypeDefault;
        allEmojis = [[NSMutableArray alloc] initWithObjects:@"😄", @"😃", @"😀", @"😊", @"☺️", @"😉", @"😍", @"😘", @"😚", @"😗", @"😙", @"😜", @"😝", @"😛", @"😳", @"😁", @"😔", @"😌", @"😒", @"😞", @"😣", @"😢", @"😂", @"😭", @"😪", @"😥", @"😰", @"😅", @"😓", @"😩", @"😫", @"😨", @"😱", @"😠", @"😡", @"😤", @"😖", @"😆", @"😋", @"😷", @"😎", @"😴", @"😵", @"😲", @"😟", @"😦", @"😧", @"😈", @"👿", @"😮", @"😬", @"😐", @"😕", @"😯", @"😶", @"😇", @"😏", @"😑", @"👲", @"👳", @"👮", @"👷", @"💂", @"👶", @"👦", @"👧", @"👨", @"👩", @"👴", @"👵", @"👱", @"👼", @"👸", @"😺", @"😸", @"😻", @"😽", @"😼", @"🙀", @"😿", @"😹", @"😾", @"👹", @"👺", @"🙈", @"🙉", @"🙊", @"💀", @"👽", @"💩", @"🔥", @"✨", @"🌟", @"💫", @"💥", @"💢", @"💦", @"💧", @"💤", @"💨", @"👂", @"👀", @"👃", @"👅", @"👄", @"👍", @"👎", @"👌", @"👊", @"✊", @"✌️", @"👋", @"✋", @"👐", @"👆", @"👇", @"👉", @"👈", @"🙌", @"🙏", @"☝️", @"👏", @"💪", @"🚶", @"🏃", @"💃", @"👫", @"👪", @"👬", @"👭", @"💏", @"💑", @"👯", @"🙆", @"🙅", @"💁", @"🙋", @"💆", @"💇", @"💅", @"👰", @"🙎", @"🙍", @"🙇", @"🎩", @"👑", @"👒", @"👟", @"👞", @"👡", @"👠", @"👢", @"👕", @"👔", @"👚", @"👗", @"🎽", @"👖", @"👘", @"👙", @"💼", @"👜", @"👝", @"👛", @"👓", @"🎀", @"🌂", @"💄", @"💛", @"💙", @"💜", @"💚", @"❤️", @"💔", @"💗", @"💓", @"💕", @"💖", @"💞", @"💘", @"💌", @"💋", @"💍", @"💎", @"👤", @"👥", @"💬", @"👣", @"💭", @"🐶", @"🐺", @"🐱", @"🐭", @"🐹", @"🐰", @"🐸", @"🐯", @"🐨", @"🐻", @"🐷", @"🐽", @"🐮", @"🐗", @"🐵", @"🐒", @"🐴", @"🐑", @"🐘", @"🐼", @"🐧", @"🐦", @"🐤", @"🐥", @"🐣", @"🐔", @"🐍", @"🐢", @"🐛", @"🐝", @"🐜", @"🐞", @"🐌", @"🐙", @"🐚", @"🐠", @"🐟", @"🐬", @"🐳", @"🐋", @"🐄", @"🐏", @"🐀", @"🐃", @"🐅", @"🐇", @"🐉", @"🐎", @"🐐", @"🐓", @"🐕", @"🐖", @"🐁", @"🐂", @"🐲", @"🐡", @"🐊", @"🐫", @"🐪", @"🐆", @"🐈", @"🐩", @"🐾", @"💐", @"🌸", @"🌷", @"🍀", @"🌹", @"🌻", @"🌺", @"🍁", @"🍃", @"🍂", @"🌿", @"🌾", @"🍄", @"🌵", @"🌴", @"🌲", @"🌳", @"🌰", @"🌱", @"🌼", @"🌐", @"🌞", @"🌝", @"🌚", @"🌑", @"🌒", @"🌓", @"🌔", @"🌕", @"🌖", @"🌗", @"🌘", @"🌜", @"🌛", @"🌙", @"🌍", @"🌎", @"🌏", @"🌋", @"🌌", @"🌠", @"⭐️", @"☀️", @"⛅️", @"☁️", @"⚡️", @"☔️", @"❄️", @"⛄️", @"🌀", @"🌁", @"🌈", @"🌊", @"🎍", @"💝", @"🎎", @"🎒", @"🎓", @"🎏", @"🎆", @"🎇", @"🎐", @"🎑", @"🎃", @"👻", @"🎅", @"🎄", @"🎁", @"🎋", @"🎉", @"🎊", @"🎈", @"🎌", @"🔮", @"🎥", @"📷", @"📹", @"📼", @"💿", @"📀", @"💽", @"💾", @"💻", @"📱", @"☎️", @"📞", @"📟", @"📠", @"📡", @"📺", @"📻", @"🔊", @"🔉", @"🔈", @"🔇", @"🔔", @"🔕", @"📢", @"📣", @"⏳", @"⌛️", @"⏰", @"⌚️", @"🔓", @"🔒", @"🔏", @"🔐", @"🔑", @"🔎", @"💡", @"🔦", @"🔆", @"🔅", @"🔌", @"🔋", @"🔍", @"🛁", @"🛀", @"🚿", @"🚽", @"🔧", @"🔩", @"🔨", @"🚪", @"🚬", @"💣", @"🔫", @"🔪", @"💊", @"💉", @"💰", @"💴", @"💵", @"💷", @"💶", @"💳", @"💸", @"📲", @"📧", @"📥", @"📤", @"✉️", @"📩", @"📨", @"📯", @"📫", @"📪", @"📬", @"📭", @"📮", @"📦", @"📝", @"📄", @"📃", @"📑", @"📊", @"📈", @"📉", @"📜", @"📋", @"📅", @"📆", @"📇", @"📁", @"📂", @"✂️", @"📌", @"📎", @"✒️", @"✏️", @"📏", @"📐", @"📕", @"📗", @"📘", @"📙", @"📓", @"📔", @"📒", @"📚", @"📖", @"🔖", @"📛", @"🔬", @"🔭", @"📰", @"🎨", @"🎬", @"🎤", @"🎧", @"🎼", @"🎵", @"🎶", @"🎹", @"🎻", @"🎺", @"🎷", @"🎸", @"👾", @"🎮", @"🃏", @"🎴", @"🀄️", @"🎲", @"🎯", @"🏈", @"🏀", @"⚽️", @"⚾️", @"🎾", @"🎱", @"🏉", @"🎳", @"⛳️", @"🚵", @"🚴", @"🏁", @"🏇", @"🏆", @"🎿", @"🏂", @"🏊", @"🏄", @"🎣", @"☕️", @"🍵", @"🍶", @"🍼", @"🍺", @"🍻", @"🍸", @"🍹", @"🍷", @"🍴", @"🍕", @"🍔", @"🍟", @"🍗", @"🍖", @"🍝", @"🍛", @"🍤", @"🍱", @"🍣", @"🍥", @"🍙", @"🍘", @"🍚", @"🍜", @"🍲", @"🍢", @"🍡", @"🍳", @"🍞", @"🍩", @"🍮", @"🍦", @"🍨", @"🍧", @"🎂", @"🍰", @"🍪", @"🍫", @"🍬", @"🍭", @"🍯", @"🍎", @"🍏", @"🍊", @"🍋", @"🍒", @"🍇", @"🍉", @"🍓", @"🍑", @"🍈", @"🍌", @"🍐", @"🍍", @"🍠", @"🍆", @"🍅", @"🌽", @"🏠", @"🏡", @"🏫", @"🏢", @"🏣", @"🏥", @"🏦", @"🏪", @"🏩", @"🏨", @"💒", @"⛪️", @"🏬", @"🏤", @"🌇", @"🌆", @"🏯", @"🏰", @"⛺️", @"🏭", @"🗼", @"🗾", @"🗻", @"🌄", @"🌅", @"🌃", @"🗽", @"🌉", @"🎠", @"🎡", @"⛲️", @"🎢", @"🚢", @"⛵️", @"🚤", @"🚣", @"⚓️", @"🚀", @"✈️", @"💺", @"🚁", @"🚂", @"🚊", @"🚉", @"🚞", @"🚆", @"🚄", @"🚅", @"🚈", @"🚇", @"🚝", @"🚋", @"🚃", @"🚎", @"🚌", @"🚍", @"🚙", @"🚘", @"🚗", @"🚕", @"🚖", @"🚛", @"🚚", @"🚨", @"🚓", @"🚔", @"🚒", @"🚑", @"🚐", @"🚲", @"🚡", @"🚟", @"🚠", @"🚜", @"💈", @"🚏", @"🎫", @"🚦", @"🚥", @"⚠️", @"🚧", @"🔰", @"⛽️", @"🏮", @"🎰", @"♨️", @"🗿", @"🎪", @"🎭", @"📍", @"🚩", @"🇯🇵", @"🇰🇷", @"🇩🇪", @"🇨🇳", @"🇺🇸", @"🇫🇷", @"🇪🇸", @"🇮🇹", @"🇷🇺", @"🇬🇧", @"1⃣", @"2⃣", @"3⃣", @"4⃣", @"5⃣", @"6⃣", @"7⃣", @"8⃣", @"9⃣", @"0⃣", @"1️⃣", @"2️⃣", @"3️⃣", @"4️⃣", @"5️⃣", @"6️⃣", @"7️⃣", @"8️⃣", @"9️⃣", @"0️⃣", @"🔟", @"🔢", @"#️⃣", @"🔣", @"⬆️", @"⬇️", @"⬅️", @"➡️", @"🔠", @"🔡", @"🔤", @"↗️", @"↖️", @"↘️", @"↙️", @"↔️", @"↕️", @"🔄", @"◀️", @"▶️", @"🔼", @"🔽", @"↩️", @"↪️", @"ℹ️", @"⏪", @"⏩", @"⏫", @"⏬", @"⤵️", @"⤴️", @"🆗", @"🔀", @"🔁", @"🔂", @"🆕", @"🆙", @"🆒", @"🆓", @"🆖", @"📶", @"🎦", @"🈁", @"🈯️", @"🈳", @"🈵", @"🈴", @"🈲", @"🉐", @"🈹", @"🈺", @"🈶", @"🈚️", @"🚻", @"🚹", @"🚺", @"🚼", @"🚾", @"🚰", @"🚮", @"🅿️", @"♿️", @"🚭", @"🈷", @"🈸", @"🈂", @"Ⓜ️", @"🛂", @"🛄", @"🛅", @"🛃", @"🉑", @"㊙️", @"㊗️", @"🆑", @"🆘", @"🆔", @"🚫", @"🔞", @"📵", @"🚯", @"🚱", @"🚳", @"🚷", @"🚸", @"⛔️", @"✳️", @"❇️", @"❎", @"✅", @"✴️", @"💟", @"🆚", @"📳", @"📴", @"🅰", @"🅱", @"🆎", @"🅾", @"💠", @"➿", @"♻️", @"♈️", @"♉️", @"♊️", @"♋️", @"♌️", @"♍️", @"♎️", @"♏️", @"♐️", @"♑️", @"♒️", @"♓️", @"⛎", @"🔯", @"🏧", @"💹", @"💲", @"💱", @"©", @"®", @"™", @"❌", @"‼️", @"⁉️", @"❗️", @"❓", @"❕", @"❔", @"⭕️", @"🔝", @"🔚", @"🔙", @"🔛", @"🔜", @"🔃", @"🕛", @"🕧", @"🕐", @"🕜", @"🕑", @"🕝", @"🕒", @"🕞", @"🕓", @"🕟", @"🕔", @"🕠", @"🕕", @"🕖", @"🕗", @"🕘", @"🕙", @"🕚", @"🕡", @"🕢", @"🕣", @"🕤", @"🕥", @"🕦", @"✖️", @"➕", @"➖", @"➗", @"♠️", @"♥️", @"♣️", @"♦️", @"💮", @"💯", @"✔️", @"☑️", @"🔘", @"🔗", @"➰", @"〰", @"〽️", @"🔱", @"◼️", @"◻️", @"◾️", @"◽️", @"▪️", @"▫️", @"🔺", @"🔲", @"🔳", @"⚫️", @"⚪", @"🔴", @"🔵", @"🔻", @"⬜️", @"⬛️", @"🔶", @"🔷", @"🔸", @"🔹", nil];

    }
    DPRINTCLASS;
    return self;
}

-(void)setAllowEmojii:(BOOL)_allowEmojii
{
    allowEmojii = _allowEmojii;
    if (allowEmojii) {
        keyboardType = UIKeyboardTypeDefault;
    } else {
        keyboardType = UIKeyboardTypeASCIICapable;
    }
    [insertTxtHPView setKeyboardType:keyboardType];
}

-(void)showTextViewWithText:(NSString*)text
{
    DCMSG(headerText);
    if (!headerImgSet) {
        [insertTxtLabel setText:[headerText uppercaseString]];
    }
    [insertTxtHPView setText:text];
    [insertTxtHPView becomeFirstResponder];
}

-(void)enableView
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShowIT:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideIT:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}

-(void)disableView
{
    [insertTxtHPView resignFirstResponder];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)showTextView
{
    [self enableView];
    if (initialText == nil) {
        [self showTextViewWithText:@""];
    } else {
        [self showTextViewWithText:initialText];
    }
}

-(void)hideTextView
{
    
    DMSG;
    [insertTxtHPView setText:@""];
    //[insertTxtHPView resignFirstResponder];
    [self disableView];
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    if (hideView == nil) {
        hideView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    [hideView setBackgroundColor:kTRANSPARENTUICOLOR];
    [hideView setAlpha:0.0];
    [hideView setBarStyle:UIBarStyleBlack];
    
//    if (bgToolbar == nil) {
//        bgToolbar = [[UIToolbar alloc] initWithFrame:hideView.frame];
//    }
//    [bgToolbar setBarStyle:UIBarStyleBlack];
//    [hideView addSubview:bgToolbar];
    
    if (insertTxtView == nil) {
        insertTxtView = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 2*40+2)];
    }
    [insertTxtView setBackgroundColor:kSEPRATORUICOLOR];
    [insertTxtView setAlpha:0.0];
    
    if (headerImage != nil) {
        UIImage *insertTxtImg = headerImage;
        UIImageView *insertTxtImgView = [[UIImageView alloc] initWithImage:insertTxtImg];
        [insertTxtImgView setFrame:CGRectMake(0, 0, insertTxtImg.size.width, insertTxtImg.size.height)];
        [insertTxtImgView setBackgroundColor:kTRANSPARENTUICOLOR];
        [insertTxtView addSubview:insertTxtImgView];
        headerImgSet = YES;
    }
    if (!headerImgSet) {
        insertTxtLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, self.frame.size.width, 40)];
        [insertTxtLabel setText:[headerText uppercaseString]];
        [insertTxtLabel setFont:kFONT(24)];
        [insertTxtLabel setTextColor:kLIGHTGRAYUICOLOR];
        [insertTxtLabel setBackgroundColor:kTRANSPARENTUICOLOR];
        [insertTxtView addSubview:insertTxtLabel];
    }
     
    if (insertTxtHPView == nil) {
        insertTxtHPView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(2, 40+2, 250, 40-4)];
    }
    [insertTxtHPView setIsScrollable:NO];
    [insertTxtHPView setMinNumberOfLines:0];
    [insertTxtHPView setMaxNumberOfLines:6];
    [insertTxtHPView setReturnKeyType:UIReturnKeyDefault];
    [insertTxtHPView setFont:kFONTHelvetica(20)];
    [insertTxtHPView setTextColor:kWHITEUICOLOR];
    [insertTxtHPView setDelegate:self];
    [insertTxtHPView setBackgroundColor:kTRANSPARENTUICOLOR];
    //[insertTxtHPView setKeyboardType:UIKeyboardTypeAlphabet];
    [insertTxtHPView setKeyboardType:keyboardType];
    [insertTxtHPView setKeyboardAppearance:UIKeyboardAppearanceDark];
     
    UIView *txtBkgImgView = [[UIView alloc] initWithFrame:insertTxtHPView.frame];
    //txtBkgImgView = [[wbSpeechBubbleView alloc] initWithFrame:insertTxtHPView.frame];
    [txtBkgImgView setBackgroundColor:kTOPUICOLOR];
    //[txtBkgImgView setBubbleColor:kTOPUICOLOR];
    //[txtBkgImgView setRightBuble:YES];
    [txtBkgImgView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    
    
    UIImage *doneinsertTxtImg = [UIImage ch_imageNamed:@"done_comment_default.png"];
    UIImage *doneinsertTxtImgActive = [UIImage ch_imageNamed:@"done_comment_active.png"];
    UIButton *doneinsertTxtBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [doneinsertTxtBtn setBackgroundImage:doneinsertTxtImg forState:UIControlStateNormal];
    [doneinsertTxtBtn setBackgroundImage:doneinsertTxtImgActive forState:UIControlStateHighlighted];
    
    [doneinsertTxtBtn setFrame:CGRectMake(CGRectGetMaxX(insertTxtHPView.frame)+20, CGRectGetHeight(insertTxtView.frame)-40, doneinsertTxtImg.size.width, doneinsertTxtImg.size.height)];
    [doneinsertTxtBtn addTarget:self action:@selector(doneEdit) forControlEvents:UIControlEventTouchUpInside];
    [doneinsertTxtBtn setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin];
     
    UIButton *cancelinsertTxtBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelinsertTxtBtn setFrame:hideView.frame];
    [cancelinsertTxtBtn setBackgroundColor:kTRANSPARENTUICOLOR];
    [cancelinsertTxtBtn addTarget:self action:@selector(hideTextView) forControlEvents:UIControlEventTouchUpInside];
    [hideView addSubview:cancelinsertTxtBtn];
    
    //[insertTxtView addSubview:insertTxtLabel];
    [insertTxtView addSubview:txtBkgImgView];
    [insertTxtView addSubview:insertTxtHPView];
    [insertTxtView addSubview:doneinsertTxtBtn];
    
    [self addSubview:hideView];
    [self addSubview:insertTxtView];
    
}

-(void)doneEdit
{
    DMSG;
    if([delegate respondsToSelector:@selector(newText:)]) {
        NSString *text = [insertTxtHPView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [delegate performSelector:@selector(newText:) withObject:text];
        [self hideTextView];
    } else {
        DNSLog(@"ERROR: %s - No delegate method 'newText:' found",__FILE__);
    }
}

// keboard stuff
//Code from Brett Schumann
-(void) keyboardWillShowIT:(NSNotification *)note
{
    DNSLog(@"--> %@",note.userInfo);
    DNSLog(@"_--> %.f : %.f",[[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height);
    DNSLog(@"----> %.f : %.f",self.frame.size.width,self.frame.size.height);
    [self setUserInteractionEnabled:YES];
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    // Need to translate the bounds to account for rotation.
    DNSLog(@"kb--: %.f : %.f : %.f",keyboardBounds.origin.x,keyboardBounds.origin.y,keyboardBounds.size.height);
    keyboardBounds = [[[[UIApplication sharedApplication] delegate] window] convertRect:keyboardBounds toView:self];
    //keyboardBounds = [self convertRect:keyboardBounds toView:nil];
    DNSLog(@"kb**: %.f : %.f : %.f",keyboardBounds.origin.x,keyboardBounds.origin.y,keyboardBounds.size.height);
    
	// get a rect for the textView frame
	CGRect containerFrame = insertTxtView.frame;
    //NSInteger statusFix;
    //if ([UIApplication sharedApplication].statusBarFrame.size.height > 20) {
    //    statusFix = 20;
    //} else {
    //    statusFix = 20;
    //}
    
    //DNSLog(@"statusFix %.f",[UIApplication sharedApplication].statusBarFrame.size.height);
    
    //containerFrame.origin.y = (self.bounds.size.height+statusFix) - (keyboardBounds.size.height + containerFrame.size.height);
    
    //containerFrame.origin.y = ([[UIScreen mainScreen] bounds].size.height - (keyboardBounds.size.height+[[wbAPI sharedAPI] footerBarHeight])) - (containerFrame.size.height);
    
    containerFrame.origin.y = keyboardBounds.origin.y-containerFrame.size.height;
    
    DNSLog(@"kb-: %.f : %.f : %.f",keyboardBounds.origin.x,keyboardBounds.origin.y,keyboardBounds.size.height);
    
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	
	// set views with new info
    insertTxtView.alpha = 0.75;
	insertTxtView.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
    hideView.alpha = 0.97;

    DNSLog(@"kb+: %.f : %.f : %.f",keyboardBounds.origin.x,keyboardBounds.origin.y,keyboardBounds.size.height);

}

-(void) keyboardWillHideIT:(NSNotification *)note{
    [self setUserInteractionEnabled:NO];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	// get a rect for the textView frame
	CGRect containerFrame = insertTxtView.frame;
    containerFrame.origin.y = self.bounds.size.height;// - containerFrame.size.height;

	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
    insertTxtView.alpha = 0.0;
	insertTxtView.frame = containerFrame;
	
	// commit animations
	[UIView commitAnimations];
    hideView.alpha = 0.0;
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = insertTxtView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	insertTxtView.frame = r;
    //[txtBkgImgView setNeedsDisplay];
}

- (BOOL)growingTextView:(HPGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    lineCount = [[growingTextView.text componentsSeparatedByString:@"\n"] count];

    if (!allowEmojii) {
        for (NSInteger i = 0; i < [allEmojis count]; i++) {
            NSString *text2 = [text stringByReplacingOccurrencesOfString:[allEmojis objectAtIndex:i] withString:@""];
            if (![text2 isEqualToString:text]) {
                return NO;
            }
        }
    }
    
    if ([text length] > 1) {
        charCount += [text length];
    } else {
        charCount = [growingTextView.text length] + [text length];
    }
    DNSLog(@"c: %ld l: %ld",(long)charCount,(long)lineCount);
    
    prevChar = text;
    
    if (charCount > maxCharCount || lineCount > maxLinesCount) {
        NSRange stringRange = {0, MIN([growingTextView.text length], maxCharCount-1)};
        stringRange = [growingTextView.text rangeOfComposedCharacterSequencesForRange:stringRange];
        NSString *shortString = [growingTextView.text substringWithRange:stringRange];
        growingTextView.text = shortString;
        charCount = [growingTextView.text length];
        shortString = nil;
        if (lineCount > maxLinesCount && strcmp([text cStringUsingEncoding:NSUTF8StringEncoding], "\b") == -8) { //backspace detection hack :(
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}


@end
