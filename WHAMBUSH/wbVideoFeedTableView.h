//
//  wbVideoFeedTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 31/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseTableView.h"
#import "wbBaseCell.h"
#import "wbVideoCell.h"
#import "wbFeedSelectView.h"

@interface wbVideoFeedTableView : wbBaseTableView <UITableViewDelegate, UITableViewDataSource>
{
    wbButton *noVideos;
    
    UIActivityIndicatorView *ai;
    UIRefreshControl *refreshController;
    UILabel *tvHeaderLabel;
}

- (id)initWithFrame:(CGRect)frame;

@property (nonatomic) BOOL isTv;

@property (nonatomic,retain) NSIndexPath *currentSelection;


@end
