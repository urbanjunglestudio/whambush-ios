//
//  wbFeedSelectView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 05/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbFeedSelectView.h"

@implementation wbFeedSelectView

@synthesize delegate;
@synthesize searchField;
@synthesize selectedId;
@synthesize searchFeed;
@synthesize showFeedArrow;
@synthesize feeds;
@synthesize showSearch;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithRed:41.0/255.0 green:43.0/255.0 blue:53.0/255.0 alpha:1.0]];
        showFeedArrow  = YES;
        showSearch = YES;
        selectedId = 0;
    }
    DPRINTCLASS;
    return self;
}

-(void)setFeeds:(NSMutableDictionary *)_feeds
{
    [_feeds removeObjectForKey:@"default"];
    feeds = _feeds;
}

-(void)setSelectedId:(NSInteger)_selectedId
{
    selectedId = _selectedId;
}

-(void)open
{
    // animations settings
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationCurve:7];
    // set views with new info
    if (showSearch) {
        [self setFrame:CGRectMake(0, self.frame.origin.y, self.frame.size.width, kTXTBOXH*([feeds count]+1)+2)];
    } else {
        [self setFrame:CGRectMake(0, self.frame.origin.y, self.frame.size.width, kTXTBOXH*([feeds count])+2)];
    }
    
    // commit animations
    [UIView commitAnimations];
    //[self setUserInteractionEnabled:YES];
    [self setNeedsDisplay];
}

-(void)close
{
    [self close:YES];
}

-(void)close:(BOOL)animate
{
    if (animate) {
        // animations settings
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.1];
        [UIView setAnimationCurve:7];
    }
    
    [self setFrame:CGRectMake(0, self.frame.origin.y, self.frame.size.width, kTXTBOXH)];
    
    if (animate) {
        // commit animations
        [UIView commitAnimations];
    }
    
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1)];
    [topLine setBackgroundColor:kBKGUICOLOR];
    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-2, self.frame.size.width, 2)];
    [bottomLine setBackgroundColor:kBKGUICOLOR];
    
    if (feedArrow == nil) {
        feedArrow = [[UIImageView alloc] initWithImage:[UIImage ch_imageNamed:@"feed_arrow.png"]];
        [feedArrow setFrame:CGRectMake(0, 0, kTXTBOXH/2, kTXTBOXH/4)];
        [feedArrow setCenter:CGPointMake(self.frame.size.width-kTXTBOXH/1.5, kTXTBOXH/2)];
    }
    UIView *firstRow;
    // Drawing code
    if (CGRectGetHeight(self.frame) == kTXTBOXH) {
        if (selectedId == 0) {
            firstRow = [self giveLineNro:0 data:searchFeed enable:NO];
            [firstRow setBackgroundColor:kALPHASELECT];
        } else {
            for (NSString* key in feeds) {
                //for (int i = 0; i < [super.contentArray count]; i++) {
                if ([[[feeds objectForKey:key] valueForKey:@"id"] integerValue] == selectedId) {
                    if (showSearch) {
                        firstRow = [self giveLineNro:0 data:[feeds objectForKey:key] enable:NO];
                    } else {
                        firstRow = [self giveLineNro:1 data:[feeds objectForKey:key] enable:NO];
                    }
                    [firstRow setBackgroundColor:kALPHASELECT];
                    break;
                }
            }
        }
        [searchField resignFirstResponder];
        if (showFeedArrow) {
            [self addSubview:feedArrow];
        }
    } else {
        if (showSearch) {
            firstRow = [self giveSearchBarWithTxt:[searchFeed objectForKey:@"name"]];
        }
        NSInteger i = 0;
        for (NSString* key in feeds) {
            //for (int i = 0; i < [super.contentArray count]; i++) {
            UIView *nextRow = [self giveLineNro:i+1 data:[feeds objectForKey:key] enable:YES];
            if ([[[feeds objectForKey:key] valueForKey:@"id"] integerValue] == selectedId) {
                [nextRow setBackgroundColor:kALPHASELECT];
            }
            [self addSubview:nextRow];
            i++;
        }
        showFeedArrow = NO;
    }
    [self addSubview:firstRow];

    [self addSubview:topLine];
    [self addSubview:bottomLine];

}

-(UIView*)giveSearchBarWithTxt:(NSString*)txt
{
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(1, 0, self.frame.size.width-2, kTXTBOXH)];
    [line setBackgroundColor:kTRANSPARENTUICOLOR];
    UIImage *bkgImg = [[UIImage ch_imageNamed:@"txtborders.png"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kTXTBOXH, kTXTBOXH)];
    UIImage *iconImg = [UIImage ch_imageNamed:@"search.png"];
    UIImageView *icon = [[UIImageView alloc] initWithImage:iconImg];
    [icon setFrame:CGRectMake(kICONPADDING, kICONPADDING, kTXTBOXH-kICONPADDING*2, kTXTBOXH-kICONPADDING*2)];
    [icon setBackgroundColor:kTRANSPARENTUICOLOR];
    [leftView addSubview:icon];
    iconImg = nil;
    icon = nil;
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kTXTBOXH, kTXTBOXH)];
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setFrame:CGRectMake(kICONPADDING, kICONPADDING, kTXTBOXH-kICONPADDING*2, kTXTBOXH-kICONPADDING*2)];
    [cancelBtn setBackgroundColor:kTRANSPARENTUICOLOR];
    [cancelBtn setHighlighted:NO];
    [cancelBtn setImage:[UIImage ch_imageNamed:@"cancel.png"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelSearch:) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:cancelBtn];
    cancelBtn = nil;
    
    if (searchField == nil) {
        searchField = [[UITextField alloc] initWithFrame:CGRectMake(1, 0, self.frame.size.width-2, kTXTBOXH-3)];
    }
    [searchField setReturnKeyType:UIReturnKeySearch];
    [searchField setLeftView:leftView];
    [searchField setLeftViewMode:UITextFieldViewModeAlways];
    [searchField setRightView:rightView];
    [searchField setRightViewMode:UITextFieldViewModeWhileEditing];
    [searchField setFont:kFONTHelvetica(15)];
    [searchField setTextColor:kWHITEUICOLOR];
    [searchField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:NSLocalizedString(@"MAIN_FEED_SEARCH", @"") attributes:@{NSForegroundColorAttributeName:kBOTTOMUICOLOR}]];
    [searchField setTextColor:kWHITEUICOLOR];
    [searchField setUserInteractionEnabled:YES];
    [searchField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [searchField setKeyboardType:UIKeyboardTypeASCIICapable];
    [searchField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [searchField setDelegate:self];
    [searchField setBackground:bkgImg];
    [searchField setTintColor:kBOTTOMUICOLOR];
    [searchField setBackgroundColor:kTRANSPARENTUICOLOR];
    if (txt != nil) {
        [searchField setText:txt];
    }
    [line addSubview:searchField];
    
    leftView = nil;
    rightView = nil;
    bkgImg = nil;
    
    return line;
}

-(UIView*)giveLineNro:(NSInteger)nro data:(NSDictionary*)data enable:(BOOL)enable
{
    //DCMSG(data);
    if (!showSearch) {
        nro = nro - 1;
    }
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, nro*kTXTBOXH, self.frame.size.width, kTXTBOXH)];
    [line setBackgroundColor:kTRANSPARENTUICOLOR];
    NSString *iconUrlString = [data objectForKey:@"icon_url"];
    UIImageView *icon;
    UIImage *iconImg;
    
    if ([iconUrlString length] > 0) {
        iconImg = [UIImage ch_imageNamed:[[wbData sharedData] parseIcon:9]];
        NSURL *iconUrl = [NSURL URLWithString:[data objectForKey:@"icon_url"]];
        icon = [[UIImageView alloc] init];//WithImage:iconImg];
        [icon setImageWithURL:iconUrl placeholderImage:iconImg];
    } else {
        if (iconImg == nil) {
            iconImg = [UIImage ch_imageNamed:[data objectForKey:@"icon"]];
            icon = [[UIImageView alloc] initWithImage:iconImg];
        }
        if (iconImg == nil) {
            iconImg = [UIImage ch_imageNamed:[[wbData sharedData] parseIcon:0]];
            icon = [[UIImageView alloc] initWithImage:iconImg];
        }
    }
    [icon setFrame:CGRectMake(0, 0, iconImg.size.width/(iconImg.size.height/(kTXTBOXH-kICONPADDING*2)), kTXTBOXH-kICONPADDING*2)];
    [icon setCenter:CGPointMake(kICONPADDING+kTXTBOXH/2, kTXTBOXH/2)];
    [icon setBackgroundColor:kTRANSPARENTUICOLOR];
    
    UILabel *feedName = [[UILabel alloc] initWithFrame:CGRectMake(kTXTBOXH+5, 0, self.frame.size.width-kTXTBOXH, kTXTBOXH)];
    [feedName setText:[NSString stringWithFormat:@"%@",NSLocalizedString([data objectForKey:@"name"],@"")]];
    [feedName setFont:kFONTHelvetica(15)];
    [feedName setTextColor:kWHITEUICOLOR];
    
    UIButton *lineBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    if (enable) {
        [lineBtn setFrame:CGRectMake(0, 0, self.frame.size.width, kTXTBOXH)];
        [lineBtn setBackgroundColor:kTRANSPARENTUICOLOR];
        [lineBtn setTintColor:kWHITEUICOLOR];
        [lineBtn setTag:[[data valueForKey:@"id"] integerValue]];
        [lineBtn addTarget:self action:@selector(highlight:) forControlEvents:UIControlEventTouchDown];
        [lineBtn addTarget:self action:@selector(cancelHighlight:) forControlEvents:UIControlEventTouchUpOutside];
        [lineBtn addTarget:self action:@selector(updateFeed:) forControlEvents:UIControlEventTouchUpInside];
    }
    [line addSubview:icon];
    [line addSubview:feedName];
    if (enable) [line addSubview:lineBtn];
    
    iconUrlString = nil;
    icon = nil;
    iconImg = nil;
    feedName = nil;
    lineBtn = nil;
    
    return line;
}

////////////////////////
-(void)getIcon:(NSArray*)data
{
}

////////////////////////
-(void)highlight:(id)sender
{
    [sender setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.25]];
}
-(void)cancelHighlight:(id)sender
{
    [sender setBackgroundColor:kTRANSPARENTUICOLOR];
}
////////////////////////

-(void)cancelSearch:(id)selector
{
    searchFeed = nil;
    [searchField setText:@""];
    [searchField resignFirstResponder];
}
////////////////////////

-(void)updateFeed:(id)selector
{
   
    [self setUserInteractionEnabled:NO];
    
    [searchField resignFirstResponder];
    for (NSString* key in feeds) {
        
        if ([[[feeds objectForKey:key] valueForKey:@"id"] integerValue] == [(UIButton*)selector tag]) {
            if ([delegate respondsToSelector:@selector(updateFeed:)]) {
                NSMutableDictionary *feed = [NSMutableDictionary dictionaryWithDictionary:[feeds objectForKey:key]];
                [delegate performSelector:@selector(updateFeed:) withObject:feed];
            }
            break;
        }
    }
    [self setUserInteractionEnabled:YES];
    [selector setBackgroundColor:kTRANSPARENTUICOLOR];
}

//search
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([[textField text] length] > 0) {
        
        NSString *modifiedText = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]], NULL, CFSTR(" :/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8));
        
        if ([modifiedText  length] > 0) {
            
            NSMutableDictionary *feed = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         [NSString stringWithFormat:@"search/videos/?query=%@",modifiedText],@"endpoint",
                                         [NSNumber numberWithInteger:0],@"id",
                                         [NSNumber numberWithInteger:0],@"default",
                                         textField.text,@"name",
                                         @"search.png",@"icon",
                                         @"search",@"type",
                                         nil];
            if ([delegate respondsToSelector:@selector(updateFeed:)]) {
                [delegate performSelector:@selector(updateFeed:) withObject:feed];
            }
            
        }
        
    }
    
    [textField resignFirstResponder];
    return YES;
}

@end
