//
//  wbUserViewSlideViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbUserViewSlideViewController.h"


@implementation wbUserViewSlideViewController

@synthesize user;
@synthesize touchView;

//#define kNUMBEROFVIEWS 4

- (void)viewDidLoad {
    [super viewDidLoad];
//}
//
//-(void)viewDidAppear:(BOOL)animated
//{
//    [super viewDidAppear:animated];
//    
    viewControllerIndex = 0;
    
    // Do any additional setup after loading the view.
    slidePageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll/*UIPageViewControllerTransitionStylePageCurl*/navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    [slidePageViewController setDataSource:self];
    [slidePageViewController setDelegate:self];
    
    [slidePageViewController.view setFrame:CGRectMake(0, kFOLLOWLINEH, self.view.frame.size.width, self.view.frame.size.height)];
    
    wbVideoFeedViewController *myVideoFeed = [[wbVideoFeedViewController alloc] init];
    [myVideoFeed setStartFeed:[user userFeed]];
    [myVideoFeed setHideHeader:YES];
    [myVideoFeed setIndex:0];
    [myVideoFeed setTabLabel:[user videosLabel]];
    
    wbFollowListViewController *followersViewController = [[wbFollowListViewController alloc] init];
    [followersViewController setUser:user];
    [followersViewController setIsFollowers:YES];
    [followersViewController setIndex:1];
    [followersViewController setTabLabel:[user userFollowerLabel]];
    
    wbFollowListViewController *followViewController = [[wbFollowListViewController alloc] init];
    [followViewController setUser:user];
    [followViewController setIsFollowers:NO];
    [followViewController setIndex:2];
    [followViewController setTabLabel:[user userFollowingLabel]];
    
    wbActivityViewController *activityViewController;
    wbRankViewController *rankViewController;
    if ([[user username] isEqualToString:[[wbUser sharedUser] username]]) {
        activityViewController = [[wbActivityViewController alloc] init];
        [activityViewController setIndex:3];
        [activityViewController setTabLabel:[[wbData sharedData] activityLabel]];
        
        rankViewController = [[wbRankViewController alloc] init];
        [rankViewController setIndex:4];
        [rankViewController setTabLabel:[[wbUser sharedUser] rankLabel]];
    } else {
        activityViewController = nil;
        rankViewController = nil;
    }
    
    
    
    viewControllers = [[NSArray alloc] initWithObjects:myVideoFeed,followersViewController,followViewController,activityViewController,rankViewController,nil];

    tabs = [[wbUserViewSlideTabsView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, kFOLLOWLINEH)];
    [tabs setBackgroundColor:kTOPUICOLOR];
    [tabs setViews:viewControllers];
    [tabs setTitleColor:kGREENUICOLOR];
    [tabs setTitleFont:kFONTHelvetica(15)];
    
    
    DNSLog(@"%@",[[wbAPI sharedAPI] num_unread_activities]);
    
    
    
    [self addChildViewController:slidePageViewController];
    [self.view addSubview:slidePageViewController.view];
    [slidePageViewController didMoveToParentViewController:self];
    
//    [tabs setTitleStrings:[NSArray arrayWithObjects:videos,followers,followings,activity,nil]];
    [self.view addSubview:tabs];
    
    numberOfViews = [viewControllers count];
    
    if ([[[wbAPI sharedAPI] num_unread_activities] integerValue] > 0 && activityViewController != nil) {
        [slidePageViewController setViewControllers:[NSArray arrayWithObject:activityViewController] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished){}];
        [tabs setDefaultTab:[(wbBaseViewController*)activityViewController index]];
    } else {
        [slidePageViewController setViewControllers:[NSArray arrayWithObject:myVideoFeed] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished){}];
        [tabs setDefaultTab:[(wbBaseViewController*)myVideoFeed index]];
    }
    
}


-(void)refreshTabs
{
    [tabs setNeedsDisplay];
}

-(void)refreshRow
{
    if ([[[viewControllers objectAtIndex:0] view] respondsToSelector:@selector(refreshRow)]) {
        [[[viewControllers objectAtIndex:0] view] performSelector:@selector(refreshRow)];
    }
}

-(void)update
{
    [user updateUserData];
    for (int i = 0; i < [viewControllers count]; i++) {
        if ([[[viewControllers objectAtIndex:i] view] respondsToSelector:@selector(update)]) {
            [[[viewControllers objectAtIndex:i] view] performSelector:@selector(update)];
        }
    }
}


- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    DNSLog(@"<-- %@",viewController);
    NSInteger index = [(wbBaseViewController*)viewController index];
    index--;
    if (index < 0) {
        DNSLog(@"<%ld",(long)index);
        //[tabs moveToTab:[(wbBaseViewController*)viewController index]];
        //return nil;
        //NSLog(@"%@",[self.view subviews]);
        return [viewControllers objectAtIndex:numberOfViews-1];
    } else {
        DNSLog(@"<%ld",(long)index);
        //[tabs moveToTab:[(wbBaseViewController*)viewController index]];
        return [viewControllers objectAtIndex:index];
    }
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    DNSLog(@"--> %@",viewController);
    NSInteger index = [(wbBaseViewController*)viewController index];
    index++;
    if (index < numberOfViews) {
        DNSLog(@">%ld",(long)index);
        //[tabs moveToTab:[(wbBaseViewController*)viewController index]];
        return [viewControllers objectAtIndex:index];
    } else {
        DNSLog(@">%ld",(long)index);
        //[tabs moveToTab:[(wbBaseViewController*)viewController index]];
        //return nil;
        return [viewControllers objectAtIndex:0];
    }
    
}

-(void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers
{
    //[tabs moveToTab:[(wbBaseViewController*)[pendingViewControllers objectAtIndex:0] index] animate:NO];
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed) {
        [tabs moveToTab:[(wbBaseViewController*)[[pageViewController viewControllers] objectAtIndex:0] index] animate:NO];
    } else {
        [tabs moveToTab:[(wbBaseViewController*)[previousViewControllers objectAtIndex:0] index] animate:NO];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
