//
//  wbSettingsViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 12/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbSettingsViewController.h"

@interface wbSettingsViewController ()

@end

@implementation wbSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    wbSettingsTableView *settingsView = [[wbSettingsTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [self setView:settingsView];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[wbHeader sharedHeader] setLogo:[super headerLogo]];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(closeMe)
//                                                 name:@"NewSettingsView"
//                                               object:nil];
    if ([[wbAPI sharedAPI] is_guest]) {
        [[wbHeader sharedHeader] setHeaderBackButton:NO];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self.view performSelector:@selector(hideKB)];
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//-(void)closeMe
//{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//    [kROOTVC performSelector:@selector(goBack:) withObject:nil];
//}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
