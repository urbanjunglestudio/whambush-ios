//
//  wbAppDelegate.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 5/20/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbAppDelegate.h"
#import "QTouchposeApplication.h"
#import <sys/utsname.h>


@implementation wbAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DCMSG(launchOptions);
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    wbRootViewController *rootVC = [[wbRootViewController alloc] init];
    
    // Override point for customization after application launch.
    self.window.rootViewController = rootVC;
    
    [self.window makeKeyAndVisible];
    
    [self setVersion];

    _fileLogger = [[DDFileLogger alloc] init];
    _fileLogger.maximumFileSize = (1024 * 64); // 64 KByte
    _fileLogger.logFileManager.maximumNumberOfLogFiles = 1;
    [_fileLogger rollLogFileWithCompletionBlock:nil];
    [DDLog addLogger:_fileLogger];
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"debugSetting"] isEqualToString:kFORCEDEBUG]) {
        [DDLog addLogger:[DDASLLogger sharedInstance]];
        [DDLog addLogger:[DDTTYLogger sharedInstance]];
    }

#ifdef DEBUGMSG
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
#endif

    
#ifdef DEBUGMSG
  #ifdef kSHOWTOUCH
    // For demo purposes, show the touches even when not mirroring to an external display.
    QTouchposeApplication *touchposeApplication = (QTouchposeApplication *)application;
    [touchposeApplication setAlwaysShowTouches:NO];
  #endif
#endif
    
    [self setVersion];

    //[[wbAPI sharedAPI] saveCurrentCountry:nil];
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"debugSetting"] isEqualToString:kRESETALL]) {

        //settings to default
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"allowStatistics"];

        // clean keychain
        NSInteger count = [[SSKeychain accountsForService:kAPPNAME] count];
        NSArray *acct = [NSArray arrayWithArray:[SSKeychain accountsForService:kAPPNAME]];
        for (int i = 0; i < count; i++) {
            (void)[SSKeychain deletePasswordForService:kAPPNAME account:[[acct objectAtIndex:i] objectForKey:@"acct"]];
        }
        [[wbAPI sharedAPI] removeSettingWithKey:@"guest_id"];

        //remove country selection
        [[wbAPI sharedAPI] removeSettingWithKey:@"country"];
        [[wbAPI sharedAPI] setCurrentCountry:nil];
    }
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"debugSetting"] isEqualToString:kRESETCOUNTRY] || [[NSUserDefaults standardUserDefaults] boolForKey:@"resetCountry"]) {
        [[wbAPI sharedAPI] removeSettingWithKey:@"country"];
        [[wbAPI sharedAPI] setCurrentCountry:nil];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"resetCountry"];
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"allowStatistics"]) {
        DCMSG(@"YES statistics");
        
#ifndef DEBUGMSG
        //[Flurry startSession:kFLURRYKEY];
#endif
        NSError *configureError;
        [[GGLContext sharedInstance] configureWithError:&configureError];
        NSAssert(!configureError, @"Error configuring Google services: %@", configureError);

        // Optional: automatically send uncaught exceptions to Google Analytics.
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
        [GAI sharedInstance].dispatchInterval = 30;
        
        [[GAI sharedInstance] setOptOut:NO];
        
#ifdef DEBUGMSG
        //[[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
        [[GAI sharedInstance] setDryRun:YES];
#endif
        // Initialize tracker. Replace with your tracking ID.
        //[[GAI sharedInstance] trackerWithTrackingId:kGOOGLEANALYTICS];
        
    } else {
        DCMSG(@"NO statistics");
        [[GAI sharedInstance] setOptOut:YES];
    }
    
    NSDictionary *launchDictionary = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (launchDictionary != nil)
    {
        NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:launchDictionary];
        [dict setObject:@YES forKey:@"id"];
        [self launchedFromPush:dict running:NO];
    }
    [self clearNotifications];
    
    background = NO;
    
    kSHOWWAIT;
    
    
    [[wbAPI sharedAPI] tryLogin];


    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    [[wbAPI sharedAPI] setPushToken:[[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	DNSLog(@"Failed to get token, error: %@", error);
}

- (void) setVersion {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *appDefaults = [NSDictionary dictionaryWithObject:@"YES" forKey:@"allowStatistics"];
    [defaults registerDefaults:appDefaults];
    [defaults synchronize];
    
    NSString *version = [NSString stringWithFormat:@"%@.%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
    DCMSG(version);
    [defaults setObject:version forKey:@"version"];
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    DMSG;
    currentGAIScreen = [[[GAI sharedInstance] defaultTracker] get:kGAIScreenName];
    
//    if ([[[wbAPI sharedAPI] currentViewController] respondsToSelector:@selector(entersBackground)]) {
//        [[[wbAPI sharedAPI] currentViewController] performSelector:@selector(entersBackground)];
//    }
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
//    [[[GAI sharedInstance] defaultTracker] set:kGAIScreenName value:@"Inactive"];
//    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createAppView] build]];
    [[wbAPI sharedAPI] registerGoogleAnalytics:@"Inactive"];
    background = YES;
    
    DMSG;
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    DMSG;
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    DMSG;
/*    if (background) {
        [[wbHeader sharedHeader] setNeedsDisplay];
    }*/
//    [[[GAI sharedInstance] defaultTracker] set:kGAIScreenName value:currentGAIScreen];
//    [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createAppView] build]];
    [[wbAPI sharedAPI] registerGoogleAnalytics:currentGAIScreen];
    [kROOTVC viewDidAppear:NO];
 // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//    if ([[[wbAPI sharedAPI] currentViewController] respondsToSelector:@selector(refreshView) ]) {
//        DMSG;
//        [[[wbAPI sharedAPI] currentViewController] performSelector:@selector(refreshView)];
//    }
//    if ([[[wbAPI sharedAPI] currentViewController] respondsToSelector:@selector(entersForeground)]) {
//        [[[wbAPI sharedAPI] currentViewController] performSelector:@selector(entersForeground)];
//    }
    
    [self clearNotifications];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    DMSG;
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    DCMSG(sourceApplication);
    DCMSG([url scheme]);
    DCMSG([url query]);
    NSLog(@"open url1: %@",[url absoluteString]);
    NSLog(@"open url2: %@",[url query]);
    NSLog(@"open url3: %@",[url path]);
    
    NSString *query = [url query];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    if (query != nil) {
        for (NSString *param in [query componentsSeparatedByString:@"&"]) {
            NSArray *elts = [param componentsSeparatedByString:@"="];
            if([elts count] < 2) continue;
            [params setObject:[elts objectAtIndex:1] forKey:[elts objectAtIndex:0]];
        }
        NSDictionary *urlDic;
        if ([params objectForKey:@"video"] != nil) {
            urlDic = [[NSDictionary alloc] initWithObjectsAndKeys:[params objectForKey:@"video"],@"video", @YES,@"id", nil];
        } else if ([params objectForKey:@"user"] != nil) {
            urlDic = [[NSDictionary alloc] initWithObjectsAndKeys:[params objectForKey:@"user"],@"user", @YES,@"id", nil];
        } else if ([params objectForKey:@"mission"] != nil) {
            urlDic = [[NSDictionary alloc] initWithObjectsAndKeys:[params objectForKey:@"mission"],@"mission", @YES,@"id", nil];
        } else {
            return NO;
        }
        [self performSelectorInBackground:@selector(launchedFromPush:) withObject:urlDic];
        
        return YES;
    } else {
        if ([[url host] rangeOfString:@"whambush"].location != NSNotFound) {
            NSArray *path = [url pathComponents];
            NSString *detail;
            if ([path count] > 2) {
                detail = [path objectAtIndex:2];
            } else {
                return YES;
            }
            NSDictionary *pushDic;
            if ([[path objectAtIndex:1] isEqualToString:@"u"]) {
                // open user page
                pushDic = [[NSDictionary alloc] initWithObjectsAndKeys:detail, @"user", nil]; // FIX: Slug not ID
            } else if ([[path objectAtIndex:1] isEqualToString:@"v"]) {
                // open video page
                pushDic = [[NSDictionary alloc] initWithObjectsAndKeys:detail, @"video", nil]; // FIX: Slug not ID
            } else if ([[path objectAtIndex:1] isEqualToString:@"m"]) {
                // open mission page
                pushDic = [[NSDictionary alloc] initWithObjectsAndKeys:detail, @"mission", nil]; // FIX: Slug not ID
            } else {
                return NO;
            }
            [self performSelectorInBackground:@selector(launchedFromPush:) withObject:pushDic];
            
            return YES;
        }
    }
    return NO;
}

-(NSString*)getLogFilesContentWithMaxSize:(NSInteger)maxSize {
    NSMutableString *description = [NSMutableString string];
    
    NSArray *sortedLogFileInfos = [[_fileLogger logFileManager] sortedLogFileInfos];
    NSInteger count = [sortedLogFileInfos count];
    
    // we start from the last one
    for (NSInteger index = count - 1; index >= 0; index--) {
        DDLogFileInfo *logFileInfo = [sortedLogFileInfos objectAtIndex:index];
        
        
        NSData *logData = [[NSFileManager defaultManager] contentsAtPath:[logFileInfo filePath]];
        if ([logData length] > 0) {
            NSString *result = [[NSString alloc] initWithBytes:[logData bytes]
                                                        length:[logData length]
                                                      encoding: NSUTF8StringEncoding];
            
            [description appendString:result];
        }
    }
    
    if ([description length] > maxSize) {
        description = (NSMutableString *)[description substringWithRange:NSMakeRange([description length]-maxSize-1, maxSize)];
    }
    
    return description;
}


-(void)clearNotifications
{
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DCMSG(userInfo);
    if (userInfo != nil)
    {
        if (background) {
            NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[userInfo objectForKey:@"wb"]];
            [dict setObject:@YES forKey:@"id"];
            [self launchedFromPush:dict running:YES];
            background = NO;
        }
    }
    
    [self clearNotifications];
}

-(BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray * _Nullable))restorationHandler
{
    DCMSG([[userActivity webpageURL] pathComponents]);
    NSURL *userUrl = [[userActivity webpageURL] absoluteURL];
    
    if ([[userUrl host] rangeOfString:@"whambush"].location != NSNotFound) {
        NSArray *path = [userUrl pathComponents];
        NSString *detail;
        if ([path count] > 2) {
            detail = [path objectAtIndex:2];
        } else {
            return YES;
        }
        NSDictionary *pushDic;
        if ([[path objectAtIndex:1] isEqualToString:@"u"]) {
            // open user page
            pushDic = [[NSDictionary alloc] initWithObjectsAndKeys:detail, @"user", nil]; // FIX: Slug not ID
        } else if ([[path objectAtIndex:1] isEqualToString:@"v"]) {
            // open video page
            pushDic = [[NSDictionary alloc] initWithObjectsAndKeys:detail, @"video", nil]; // FIX: Slug not ID
        } else if ([[path objectAtIndex:1] isEqualToString:@"m"]) {
            // open mission page
            pushDic = [[NSDictionary alloc] initWithObjectsAndKeys:detail, @"mission", nil]; // FIX: Slug not ID
        } else {
            return NO;
        }
        [self performSelectorInBackground:@selector(launchedFromPush:) withObject:pushDic];
        return YES;
    }
    
    return NO;
}

-(void)launchedFromPush:(NSDictionary*)dictionary
{
    if(kWHAMBUSHTOKEN != nil) {
        [self launchedFromPush:dictionary running:YES];
    } else {
        [self launchedFromPush:dictionary running:NO];
    }
}

-(void)launchedFromPush:(NSDictionary*)dictionary running:(BOOL)running //or url
{
    DCMSG(@"Launched from push notification/url");
    DCMSG(dictionary);

    [[wbData sharedData] setPushData:dictionary];

    if (running) {
        [kROOTVC performSelector:@selector(launchFromPush)];
    } else {
        if (dictionary != nil) {
            [[wbData sharedData] setLaunchedFromPush:YES];
        }
    }
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    CGPoint location = [[[event allTouches] anyObject] locationInView:[self window]];
    CGRect statusBarFrame = [UIApplication sharedApplication].statusBarFrame;
    if (CGRectContainsPoint(statusBarFrame, location)) {
        [self statusBarTouchedAction];
    }
}

- (void)statusBarTouchedAction {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StatusBarTappedNotification"
                                                        object:nil];
}

@end

/*@implementation NSURLRequest(DataController)
+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host
{
    return YES;
}
@end*/
