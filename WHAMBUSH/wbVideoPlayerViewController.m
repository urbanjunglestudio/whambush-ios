//
//  wbVideoPlayerViewController
//
//  Created by Jari Kalinainen on 24/03/14.
//  Copyright (c) 2014 TofuHead. All rights reserved.
//

#import "wbVideoPlayerViewController.h"

@interface wbVideoPlayerViewController ()

@end

@implementation wbVideoPlayerViewController

@synthesize videoURL;
@synthesize videoThumbURL;
@synthesize viewRect;
@synthesize delegate;

-(void)loadView
{
    if ([self isSetViewRect]) {
        videoPlayerView = [[wbVideoPlayerView alloc] initWithFrame:viewRect];
    } else {
        videoPlayerView = [[wbVideoPlayerView alloc] initWithFrame:CGRectMake(0,0,[[UIScreen mainScreen] bounds].size.width, ([[UIScreen mainScreen] bounds].size.width/16)*9)];
    }
    oldFrame = [[UIScreen mainScreen] bounds];

    [videoPlayerView setController:self];
    [self setView:videoPlayerView];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:movieController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(readyToGo:)
                                                 name:MPMoviePlayerReadyForDisplayDidChangeNotification
                                               object:movieController];

}

-(BOOL)isSetViewRect
{
    if (viewRect.size.height > 0 && viewRect.size.width > 0) {
        return YES;
    }
    return NO;
}

-(void)setVideoThumbURL:(NSURL *)_videoThumbURL
{
    [(wbVideoPlayerView*)[self view] setThumbUrl:_videoThumbURL];
}

- (void)viewDidLoad {
    
    playing = NO;
    ready = NO;
    [super viewDidLoad];
    
//    if (videoPlayerView == nil) {
//        videoPlayerView = [[wbVideoPlayerView alloc] initWithFrame:CGRectMake(0,0,[[UIScreen mainScreen] bounds].size.width, ([[UIScreen mainScreen] bounds].size.width/16)*9)];
//        [videoPlayerView setController:self];
//        [videoPlayerView setHidden:YES];
//        [self.view addSubview:videoPlayerView];
//    }
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    if (movieController == nil) {
        movieController = [[MPMoviePlayerController alloc] init];
        [movieController setContentURL:videoURL];
        [movieController.view setFrame:CGRectMake(0, 0, videoPlayerView.frame.size.width, videoPlayerView.frame.size.height)];

        [movieController.view setBackgroundColor:[UIColor clearColor]];
        
        [self.view addSubview:movieController.view];
        [self.view sendSubviewToBack:movieController.view];
        
        
        [movieController setShouldAutoplay:NO];
        [movieController setControlStyle:MPMovieControlStyleNone];
    }
    
    if (videoThumbURL != nil) {
        /*if (thumb == nil) {
            thumb = [[UIImageView alloc] initWithFrame:videoPlayerView.frame];
        }
        [thumb setBackgroundColor:kBLACKUICOLOR];
        UIImageView *thumbImg = [[UIImageView alloc] initWithFrame:videoPlayerView.frame];
        [thumbImg af_setImageWithURL:videoThumbURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            if (image != nil) {
                float tbHeight = videoPlayerView.frame.size.height;
                float tbWidth = (image.size.width)/(image.size.height/tbHeight);
                if  (tbWidth == videoPlayerView.frame.size.width) {
                    [thumbImg setFrame:CGRectMake(0, videoPlayerView.frame.origin.y, tbWidth,tbHeight)];
                } else {
                    float newX = (videoPlayerView.frame.size.width/2)-(tbWidth/2);
                    [thumbImg setFrame:CGRectMake(newX, videoPlayerView.frame.origin.y, tbWidth,tbHeight)];
                }
            }
        }];
        [thumb addSubview:thumbImg];
        [self.view addSubview:thumb];*/
    }
}

- (void)viewDidAppear:(BOOL)animated {
    //    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    //    [self setNeedsStatusBarAppearanceUpdate];
    if (waitView == nil) {
        waitView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//        [waitView setFrame:CGRectMake(0,0,[[UIScreen mainScreen] bounds].size.width, ([[UIScreen mainScreen] bounds].size.width/16)*9)];
        [waitView setFrame:movieController.view.frame];
        [waitView startAnimating];
        [waitView setHidden:NO];
        [self.view addSubview:waitView];
        [self.view bringSubviewToFront:waitView];
    }

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:movieController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(readyToGo:)
                                                 name:MPMoviePlayerReadyForDisplayDidChangeNotification
                                               object:movieController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setDuration:)
                                                 name:MPMovieDurationAvailableNotification
                                               object:movieController];
    [movieController prepareToPlay];
    [self pauseVideo:NULL];
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
//    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
//    [self setNeedsStatusBarAppearanceUpdate];
    [super viewDidDisappear:animated];
    [self pauseVideo:NULL];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerReadyForDisplayDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMovieDurationAvailableNotification object:nil];
}

-(void)clean
{
    [self pauseVideo:NULL];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerReadyForDisplayDidChangeNotification object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMovieDurationAvailableNotification object:nil];
}

-(void)readyToGo:(id)sender
{
    if (!ready) {
        bufferTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(setBufferStatus:) userInfo:nil repeats:YES];
        [waitView setHidden:YES];
        [waitView stopAnimating];
        //[videoPlayerView setDuration:[movieController duration]];
        [videoPlayerView setReadyToPlay:YES];
        ready = YES;
        //[thumb setHidden:YES];
    }
}

-(void)setDuration:(id)sender
{
    [videoPlayerView setDuration:[movieController duration]];
}

-(void)moviePlayBackDidFinish:(NSNotification *)notification {
    //[self performSelector:@selector(togglePlay:) withObject:NULL];
    [self pauseVideo:NULL];
}

-(void)togglePlay:(id)sender
{
    [thumb setHidden:YES];
    if (!playing) {
         [self performSelector:@selector(playVideo:) withObject:NULL];
    } else {
        [self performSelector:@selector(pauseVideo:) withObject:NULL];
    }
}

-(void)pauseVideo:(id)sender
{
    [movieController pause];
    playing = NO;
    [videoPlayerView setPlaying:playing];
    if (progressTimer.isValid) {
        [progressTimer invalidate];
    }
}

-(void)playVideo:(id)sender
{
    [movieController play];
    playing = YES;
    [videoPlayerView setPlaying:playing];
    progressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(setProgress:) userInfo:nil repeats:YES];
}

-(void)setProgress:(id)sender
{
    NSTimeInterval current = [movieController currentPlaybackTime];
    NSTimeInterval duration = [movieController duration];
    progress = current/duration;
    [videoPlayerView setProgress:progress];
}

-(void)setBufferStatus:(id)sender
{
    NSTimeInterval playable = [movieController playableDuration];
    NSTimeInterval duration = [movieController duration];
    float buffered = (playable/duration);
    if (buffered < 0 | playable == 0) {
        buffered = 0;
    }
    [videoPlayerView setBuffered:buffered];
    if (buffered >= 0.99) {
        if ([bufferTimer isValid]) {
            [bufferTimer invalidate];
        }
    }
}

-(void)moveProgress:(NSNumber*)value
{
    if (playing) {
        [movieController pause];
    }
    [movieController setCurrentPlaybackTime:([movieController duration] * [value floatValue])];
    [self setProgress:NULL];
    if (playing) {
        [movieController play];
    }
}

-(void)share
{
    if (delegate != nil) {
        [delegate performSelector:@selector(showShareSelect)];
    }
}

///////

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [videoPlayerView setFrame:CGRectMake(0,0,oldFrame.size.height, oldFrame.size.width)];
        [movieController.view setFrame:videoPlayerView.frame];
        [videoPlayerView setPortrait:NO];
        [waitView setFrame:videoPlayerView.frame];
        //[movieController.view setFrame:videoPlayerView.frame];
        [[wbHeader sharedHeader] setHidden:YES];
        [[wbFooter sharedFooter] setHidden:YES];
    } else {
        [videoPlayerView setFrame:CGRectMake(0,0,oldFrame.size.width, (oldFrame.size.width/16)*9)];
        [movieController.view setFrame:videoPlayerView.frame];
        [videoPlayerView setPortrait:YES];
        [waitView setFrame:videoPlayerView.frame];
        //[movieController.view setFrame:videoPlayerView.frame];
        [[wbHeader sharedHeader] setHidden:NO];
        [[wbFooter sharedFooter] setHidden:NO];
    }
    [videoPlayerView setNeedsDisplay];
    [movieController.view setNeedsDisplay];
}
//

//- (BOOL)prefersStatusBarHidden
//{
//    return YES;
//}
//
//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}
//
- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait|UIInterfaceOrientationLandscapeRight;
}

@end
