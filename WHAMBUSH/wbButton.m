//
//  wbButton.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 04/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbButton.h"

@implementation wbButton

//@synthesize payload;
@synthesize preserveWidth;
@synthesize buttonImage;
@synthesize buttonImageView;
@synthesize cropButtonImage;
@synthesize imageScale;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        preserveWidth = NO;
        cropButtonImage = NO;
        imageScale = 1;
        [self setClipsToBounds:YES];
    }
    DPRINTCLASS;
    return self;
}

-(NSString*)description
{
    return [super description];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)sizeToFit
{
    float oldWidth = 0;
    if (preserveWidth) {
        oldWidth = self.frame.size.width;
    }
    wbLabel *tmp = [[wbLabel alloc] init];
    if ([self titleLabel] != nil) {
        [tmp setText:[[self titleLabel] text]];
        [tmp setFont:[[self titleLabel] font]];
    } else {
        [tmp setAttributedText:[self attributedTitleForState:UIControlStateNormal]];
    }
    [tmp setNumberOfLines:[[self titleLabel] numberOfLines]];
    [tmp setEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [tmp sizeToFit];
    
    //    [super sizeToFit];
    if (preserveWidth) {
        [self setFrame:CGRectMake(self.frame.origin.x,self.frame.origin.y,oldWidth+[self titleEdgeInsets].left+[self titleEdgeInsets].right, tmp.frame.size.height+[self titleEdgeInsets].top+[self titleEdgeInsets].bottom)];
        
    } else {
        [self setFrame:CGRectMake(self.frame.origin.x,self.frame.origin.y,tmp.frame.size.width+[self titleEdgeInsets].left+[self titleEdgeInsets].right, tmp.frame.size.height+[self titleEdgeInsets].top+[self titleEdgeInsets].bottom)];
    }
    
    tmp = nil;
}

-(void)setTitle:(NSString *)title forState:(UIControlState)state
{
    [super setTitle:title forState:state];
    //[[self titleLabel] setText:title];
    
}

-(void)setButtonImage:(UIImage *)_buttonImage
{
    buttonImage = _buttonImage;
    
    if (buttonImageView == nil) {
        buttonImageView = [[UIImageView alloc] init];
    }
   
    float width;
    float height;
    
    if (cropButtonImage){
        height = buttonImage.size.height*imageScale;
        width  = buttonImage.size.width*imageScale;
        [self setClipsToBounds:YES];
    } else {
        if (self.frame.size.height < buttonImage.size.height) {
            height = self.frame.size.height*imageScale;
            width  = (buttonImage.size.width)/(buttonImage.size.height/height);
        } else {
            height = buttonImage.size.height*imageScale;
            width  = buttonImage.size.width*imageScale;
        }
        
        if (self.frame.size.width < width) {
            width = self.frame.size.width*imageScale;
            height  = (buttonImage.size.height)/(buttonImage.size.width/width);
        }
    }
    
    [buttonImageView setFrame:CGRectMake(0, 0, width, height)];
    [buttonImageView setImage:buttonImage];
    [buttonImageView setCenter:CGPointMake(self.frame.size.width/2,self.frame.size.height/2)];
    
    [self addSubview:buttonImageView];
    
}


@end
