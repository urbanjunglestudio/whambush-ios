//
//  wbFollowListCell.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbBaseCell.h"
#import "wbRoundedButton.h"

@interface wbFollowListCell : wbBaseCell
{
    wbRoundedButton *followButton;
    BOOL isFollowing;
    UIActivityIndicatorView *ai;
}

@property BOOL hideWhenUnfollowPressed;
@property id delegate;
@property (retain,nonatomic) NSIndexPath* indexPath;

@property (retain,nonatomic) wbUser* user;

@end
