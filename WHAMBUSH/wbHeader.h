//
//  wbHeader.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 04/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbHeader : UIView
{
    CGRect headerBarRect;
    UIView *headerBar;
    UIButton *backButton;
    wbButton *changeCountryButton;
    wbButton *logoButton;
}

//+(id)myHeader;

+(id)sharedHeader;

@property (retain,nonatomic) UIView *uploadView;

@property (nonatomic) BOOL headerBackButton;
@property (nonatomic) BOOL showChangeCountryButton;
@property (nonatomic) BOOL green;
@property (nonatomic,retain) NSString *logo;
@property (nonatomic,retain) NSString *userTitle;
@property (nonatomic) BOOL countryChangeActive;
@end
