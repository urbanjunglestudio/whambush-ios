//
//  wbActivityTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbActivityTableView.h"

@implementation wbActivityTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        //[self setCanCancelContentTouches:NO];
        
        [self setHidden:NO];
        super.hideHeader = YES;
        
        allRead = YES;
    }
    DPRINTCLASS;
    return self;
}

-(void)forceRefresh
{
    [ai setHidden:NO];
    [[self tableFooterView] setHidden:NO];
    [self refresh:nil];
}

- (void)drawRect:(CGRect)rect
{
    if (refreshController == nil) {
        refreshController = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [refreshController addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[UIImage ch_imageNamed:@"refresh_bg_image"]]];
        [self addSubview:refreshController];
    }
    
    
    UIView *tableFooter =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 100)];
    [tableFooter setBackgroundColor:kBKGUICOLOR];
    if (/*super.hideHeader*/ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [ai startAnimating];
        [ai setHidesWhenStopped:NO];
        [ai setCenter:CGPointMake(self.frame.size.width/2,kFOLLOWLINEH/2)];
        [tableFooter addSubview:ai];
    }
    
    
    if(dataArray == nil) {
        [[wbData sharedData] getActivityData:self];
    }
    [self setTableFooterView:tableFooter];
    
}


-(void)executeRefresh
{
        [[wbData sharedData] refreshActivityData:self];
}

-(void)resultsReady
{
    //NSLog(@"%@\n%@",dataArray,resultArray);
    
    NSMutableArray *newRows = [[NSMutableArray alloc] init];
    
    for (long i = [self numberOfRowsInSection:0]; i < numberOfResults; i++) {
        NSIndexPath *newReloadRows = [NSIndexPath indexPathForRow:i inSection:0];
        [newRows addObject:newReloadRows];
        newReloadRows = nil;
    }
    
    
    [self beginUpdates];
    [self insertRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationFade];
    [self endUpdates];
    
    
    [self setHidden:NO];
    
    [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    
    DMSG;
    kHIDEWAIT;
    DCMSG([self tableFooterView]);
    [ai setHidden:YES];
    
    [[self tableFooterView] setHidden:NO];
    
    DNSLog(@"unread: %ld",(long)[[[wbAPI sharedAPI] num_unread_activities] integerValue] );
    
    if ([[[wbAPI sharedAPI] num_unread_activities] integerValue] > 0) {
        allRead = NO;
    }
}

// table methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (resultArray != nil) {
        return numberOfResults;
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    wbBaseCell *cell;// = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[wbBaseCell alloc] init];//WithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [cell setBackgroundColor:kTRANSPARENTUICOLOR];
    
    if ([resultArray count]-8 == [indexPath row] || [resultArray count] == [indexPath row]) {
        if (numberOfResults < totalNumberOfResults) {
            DMSG;
            [ai setHidden:NO];
            [self performSelectorInBackground:@selector(getMoreResults) withObject:nil];
        }
    }
    
    if  ([resultArray count] > ([indexPath row])){
        wbActivityCell *activityView = [[wbActivityCell alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 80)];
        [[wbData sharedData] saveWhambushUser:[[resultArray objectAtIndex:[indexPath row]] objectForKey:@"user"]];
        [activityView setAllRead:allRead];
        [activityView setData:[resultArray objectAtIndex:[indexPath row]]];
        [cell addSubview:activityView];
        
        if (([[[resultArray objectAtIndex:[indexPath row]] objectForKey:@"is_read"] boolValue] == YES || [resultArray count] == [indexPath row]+1) && allRead == NO) {
            [self markAllRead];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DCMSG([resultArray objectAtIndex:[indexPath row]]);
    NSDictionary *targetData = [[resultArray objectAtIndex:[indexPath row]] objectForKey:@"target"];
    NSDictionary *userData = [[resultArray objectAtIndex:[indexPath row]] objectForKey:@"user"];
    if (![targetData isKindOfClass:[NSNull class]]) {

        [[wbData sharedData] setSingleVideoData:(NSMutableDictionary*)targetData];
        
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            
            [kROOTVC performSelectorOnMainThread:@selector(startSingleVideo:) withObject:self waitUntilDone:NO];
        }];
        
        [op start];
    } else {
        wbUser *user = [[wbData sharedData] getWhambushUser:[userData objectForKey:@"url"]];
        [kROOTVC performSelector:@selector(startUserPage:user:) withObject:nil withObject:user];
    }
    [self deselectRowAtIndexPath:indexPath animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}


-(void)markAllRead
{
    if (!allRead) {
        allRead = YES;
        [[wbAPI sharedAPI] getDataWithURL:[[wbAPI sharedAPI] urlWithEndpoint:kACTIVITYREADAPI] delegate:self selectorName:@"activityAllReadDone:"];
    }
}

-(void)activityAllReadDone:(id)data
{
    allRead = YES;
    [[wbAPI sharedAPI] setNum_unread_activities:@"0"];
    //[[wbData sharedData] setNum_of_unreadActivities:[NSNumber numberWithLong:0]];
}


@end

