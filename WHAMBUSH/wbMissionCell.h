//
//  wbBubbleCell.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/6/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbDefines.h"
#import "wbSpeechBubbleView.h"
#import "wbAPI.h"
#import "wbBaseCell.h"
#import "wbRoundedButton.h"
#import "wbMission.h"

@interface wbMissionCell : UIView//wbBaseCell
{
    wbSpeechBubbleView  *sb;
    wbLabel *missionTitleLabel;
    UIImage *doneTickImg;
    UIImageView *doneTickImageView;
    //wbLabel *missionTxtLabel;
    UITextView *missionTxtView;
    wbLabel *missionEndTimeLabel;
    wbButton *missionEndTimeButton;
    wbRoundedButton *missionButton;
    UIImage *iconImg;
    UIImageView *iconImgView;
    UIImage *playImg;
    wbButton *playButton;
    
    
    UIView *content;
    
    UIImage *gorillaHeadImg;
    UIImageView *gorillaHeadImgView;
    
    NSString* missionSubjectString;
    NSString* missionTxtString;
    NSString* startTimeTxtString;
    NSString* endTimeTxtString;
}

@property (nonatomic,retain) NSDictionary *cellArray;
@property BOOL isArchived;
@property id delegate;

@end
