//
//  wbFlagSelectionView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 27/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbFlagSelectionView : UIView <UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSInteger selectedRow;
//    UIView *hideView;
    UIToolbar *hideView;
    UIButton *cancelBtn;
    UIView *theBox;
    UIButton *sendBtn;
}


@property NSInteger videoId;

@end

