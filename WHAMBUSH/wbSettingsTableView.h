//
//  wbSettingsTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 12/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbBaseTableView.h"
#import "wbBaseCell.h"
#import "wbRoundedButton.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "wbBirthdaySelectView.h"
#import "wbInsertTextView.h"
#import "wbCountryPickerView.h"

@interface wbSettingsTableView : wbBaseTableView <UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,MFMailComposeViewControllerDelegate>
{
    NSURL *linkToBeOpen;
    MFMailComposeViewController *mail;

    wbBirthdaySelectView *selectBirthdayView;
    
    wbInsertTextView *insertTxtView;

    wbCountryPickerView *pickCountry;

}

@property (nonatomic,retain) NSDictionary *selectedCountry;

@end
