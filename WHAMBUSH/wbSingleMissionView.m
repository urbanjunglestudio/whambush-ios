//
//  wbSingleMissionView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbSingleMissionView.h"

@implementation wbSingleMissionView

@synthesize controller;
@synthesize mission;
@synthesize missionTimer;
@synthesize hideByShare;
@synthesize videoPlayerController;

#define kTimerRowHeight 35
#define kVideoRowHeight (self.frame.size.width/16)*9
#define kActionRowHeight 40
#define kEnterRowHeight 35
#define kWhoRowHeight 50
#define kDescRowHeight [self descriptionRow].frame.size.height
#define kInfoRowHeight 40
#define kMissionVideoRowHeight 96

#define kFirstVideoRow 7

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setDelegate:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        [self setClipsToBounds:YES];
        [self setHidden:NO];
        [self setCanCancelContentTouches:YES];
        refreshing = NO;
        number_of_videos = 0;
        togleEndTime = NO;
        hideByShare = NO;
        missionVideoCells = [NSMutableDictionary new];
        missionVideos = [NSMutableArray new];
        allowLoad = YES;
    }
    return self;
}
- (UIEdgeInsets)layoutMargins
{
    return UIEdgeInsetsZero;
}

-(void)setGAIViewName
{
    [[wbAPI sharedAPI] registerGoogleAnalytics:[NSString stringWithFormat:@"Mission:id=%@",[mission mission_id]]];
}

-(void)scrollTableTop
{
    [self setContentOffset:CGPointZero animated:YES];
}


-(void)setMission:(wbMission *)_mission
{
    mission = _mission;
    [mission setDelegate:self];
    [self waitForMissionReady];
}

-(void)waitForMissionReady
{
    if ([mission ready]) {
        [self setGAIViewName];
        [self setDataSource:self];
        number_of_videos = [[mission number_of_submissions] integerValue];
        if (number_of_videos > 0) {
            kSHOWWAIT;
            videoUrl = [[wbAPI sharedAPI] urlWithEndpoint:[NSString stringWithFormat:kMISSIONVIDEOSAPI,[mission mission_id]]];
            [self getMissionVideosWithURL:videoUrl];
        }
        [self reloadData];
    } else {
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self waitForMissionReady];
        });
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    if (refreshController == nil) {
        refreshController = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [refreshController addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        UIImageView *rBkg = [[UIImageView alloc] init];
        [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[UIImage ch_imageNamed:@"refresh_bg_image"]]];
        NSURL *refreshPicURL = [mission mission_image_2_url];
        [rBkg setImageWithURLRequest:[NSMutableURLRequest requestWithURL:refreshPicURL] placeholderImage:[UIImage ch_imageNamed:@"refresh_bg_image"] success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            if  (image != nil) {
                refreshBkgImage = image;
                [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[self scaleImage:image withScale:1]]];
            }
            
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
        
        /*[rBkg setImageWithURL:refreshPicURL placeholderImage:[UIImage ch_imageNamed:@"refresh_bg_image"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            if  (image != nil) {
                refreshBkgImage = image;
                [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[self scaleImage:image withScale:1]]];
            }
        }];*/
        [self addSubview:refreshController];
    }
}

-(UIImage*)scaleImage:(UIImage*)image withScale:(CGFloat)scale
{
    CGFloat widht = self.frame.size.width*scale;
    CGSize scaleSize = CGSizeMake(widht, widht*(image.size.height/image.size.width));
    UIGraphicsBeginImageContextWithOptions(scaleSize, YES, 0.0);
    [image drawInRect:CGRectMake((self.frame.size.width-scaleSize.width)/2, 0, scaleSize.width, scaleSize.height) blendMode:kCGBlendModeNormal alpha:0.3];
    UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resizedImage;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (refreshBkgImage != nil && !refreshing) {
        float offset = self.contentOffset.y;
        if (offset < 0) {
            CGFloat scale = 1+fabs(offset)/190;
            //NSLog(@"%f",scale);
            [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[self scaleImage:refreshBkgImage withScale:scale]]];
        }
    }
}
-(void)refresh:(UIRefreshControl *)refreshControl
{
    refreshing = YES;
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [self setScrollEnabled:NO];
    if (videoUrl != nil) {
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            [self performSelectorOnMainThread:@selector(doRefresh:) withObject:refreshController waitUntilDone:NO];
        }];
        [op start];
    } else {
        refreshing = NO;
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [self setScrollEnabled:YES];
        [self setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}
-(void)doRefresh:(UIRefreshControl*)refreshControl
{
    [refreshControl beginRefreshing];
    [self beginUpdates];
    for (NSInteger i = kFirstVideoRow; i < [missionVideos count]+kFirstVideoRow; i++) {
        [self deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    [missionVideos removeAllObjects];
    [self endUpdates];
    [self getMissionVideosWithURL:videoUrl];
    [refreshControl endRefreshing];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] == 0) {
        return kTimerRowHeight;
    } else if ([indexPath row] == 1) {
        return kVideoRowHeight;
    } else if ([indexPath row] == 2) {
        return kActionRowHeight;
    } else if ([indexPath row] == 3) {
        if ([mission isActive] &&  ![[wbAPI sharedAPI] uploading]) {
            return kEnterRowHeight;
        } else {
            return 0;
        }
    } else if ([indexPath row] == 4) {
        return kWhoRowHeight;
    } else if ([indexPath row] == 5) {
        return kDescRowHeight;
    } else if ([indexPath row] == 6) {
        return kInfoRowHeight;
    } else { // submission videos
        return kMissionVideoRowHeight;
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return kFirstVideoRow+[missionVideos count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![mission ready]) {
        return nil;
    }
    
    NSString *identifier = [NSString stringWithFormat:@"mission_%@_%ld_%ld",[mission mission_id],(long)[indexPath row],(long)[indexPath section]];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if  ([indexPath row] == 1) {
        cell = nil;
    }
    if (cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setFrame:CGRectMake(0, 0, self.frame.size.width, [self tableView:self heightForRowAtIndexPath:indexPath])];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setClipsToBounds:YES];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        [cell setBackgroundColor:kTRANSPARENTUICOLOR];
        [cell setClipsToBounds:YES];
    }
    if ([indexPath row] == 0) {
        [cell addSubview:[self timerRow]];
    } else if ([indexPath row] == 1) {
        [cell addSubview:[self videoRow]];
    } else if ([indexPath row] == 2) {
        [cell addSubview:[self actionRow]];
    } else if ([indexPath row] == 3) {
        [cell addSubview:[self enterRow]];
    } else if ([indexPath row] == 4) {
        [cell addSubview:[self whoRow]];
    } else if ([indexPath row] == 5) {
        [cell addSubview:[self descriptionRow]];
    } else if ([indexPath row] == 6) {
        [cell addSubview:[self infoRow]];
    } else if ([indexPath row] > kFirstVideoRow-1 ) { // submission videos
        [cell addSubview:[self missionVideoRowWithId:[NSNumber numberWithInteger:[indexPath row]-kFirstVideoRow]]];
    }
    
    
    return cell;
}

///////////////////
-(UIView*)timerRow
{
    if (timerLabel == nil) {
        timerLabel = [[wbLabel alloc] init];
        [timerLabel setUserInteractionEnabled:YES];
    }
    [self updateTime];

    if (endTime == nil) {
        endTime = [wbButton buttonWithType:UIButtonTypeSystem];
        [endTime setFrame:CGRectMake(0, 0, timerLabel.frame.size.width, timerLabel.frame.size.height)];
        [endTime setBackgroundColor:kTRANSPARENTUICOLOR];
        [endTime setShowsTouchWhenHighlighted:YES];
        [endTime addTarget:self action:@selector(toggleEndTime) forControlEvents:UIControlEventTouchUpInside];
    }

    if ([mission isActive]) {
        missionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
        [endTime setUserInteractionEnabled:YES];
    } else {
        [endTime setUserInteractionEnabled:NO];
    }
    [timerLabel addSubview:endTime];
    return timerLabel;
}

-(void)updateTime
{
    if (togleEndTime) {
        [timerLabel setAttributedText:[mission endTimeAString]];
    } else {
        [timerLabel setAttributedText:[mission getMissionEndString]];
    }
    [timerLabel sizeToFit];
    [timerLabel setCenter:CGPointMake(self.frame.size.width/2, kTimerRowHeight/2)];
    
}

-(void)toggleEndTime
{
    togleEndTime = !togleEndTime;
    [self updateTime];
    [endTime setFrame:CGRectMake(0, 0, timerLabel.frame.size.width, timerLabel.frame.size.height)];
    
}

///////////////////
-(UIView*)videoRow
{
    if (videoRowView == nil) {
        videoRowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kVideoRowHeight)];
    }
    if ([mission linked_video_id] != nil) {
        if (videoPlayerController == nil) {
            videoPlayerController = [[wbVideoPlayerViewController alloc] init];
        }
        [videoPlayerController setVideoURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://view.vzaar.com/%@/video",[[mission linked_video_dictionary] valueForKey:@"external_id"]]]];
        if ([[[mission mission_image_1_url] absoluteString] isEqualToString:@""]) {
            [videoPlayerController setVideoThumbURL:[NSURL URLWithString:[[mission linked_video_dictionary] valueForKey:@"thumbnail_url"]]];
        } else {
            [videoPlayerController setVideoThumbURL:[mission mission_image_1_url]];
        }
        [videoPlayerController.view setHidden:NO];
        [(wbVideoPlayerView*)videoPlayerController.view setShowShare:NO];
        [videoPlayerController setDelegate:nil];
        [videoRowView addSubview:videoPlayerController.view];
    } else {
        if (noVideoImageView == nil) {
            noVideoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kVideoRowHeight)];
        }
        UIImage *placeHolder;
        if ([[[mission created_by] userId] integerValue] == 1 ) {
            placeHolder = [UIImage imageNamed:@"default1.jpg"];
        } else {
            placeHolder = [UIImage imageNamed:@"default3.jpg"];
        }
        [noVideoImageView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.width*(placeHolder.size.height/placeHolder.size.width))];
        __weak typeof(self) weakSelf = self;
        [noVideoImageView setImageWithURLRequest:[NSMutableURLRequest requestWithURL:[mission mission_image_1_url]] placeholderImage:placeHolder success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
            __strong typeof(self) strongSelf = weakSelf;
            if (image != nil) {
                [strongSelf->noVideoImageView setFrame:CGRectMake(0, 0, strongSelf.frame.size.width, strongSelf.frame.size.width*(image.size.height/image.size.width))];
                [strongSelf->noVideoImageView setImage:image];
            }
        } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
            
        }];
        /*[noVideoImageView setImageWithURL:[mission mission_image_1_url] placeholderImage:placeHolder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
            if (image != nil) {
                [noVideoImageView setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.width*(image.size.height/image.size.width))];
            }
        }];*/

        [videoRowView addSubview:noVideoImageView];
    }
    return videoRowView;
}
///////////////////
-(UIView*)actionRow
{
    if (actionRowView == nil) {
        actionRowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kActionRowHeight)];
    }
    
    if (shareBtn == nil) {
        shareBtn = [wbButton buttonWithType:UIButtonTypeSystem];
        [shareBtn setFrame:CGRectMake(self.frame.size.width-self.frame.size.width/5, 0, self.frame.size.width/5 ,kActionRowHeight)];//(0, 0, self.frame.size.width/5 ,kActionRowHeight)];
        [shareBtn setShowsTouchWhenHighlighted:YES];
        //[shareBtn setImageScale:1.2];
        [shareBtn setButtonImage:[UIImage imageNamed:@"share_grey"]];
        [shareBtn addTarget:self action:@selector(showShareSelect) forControlEvents:UIControlEventTouchUpInside];
        [actionRowView addSubview:shareBtn];
    }
    
    /*if (cameraBtn == nil) {
        cameraBtn = [wbButton buttonWithType:UIButtonTypeSystem];
        [cameraBtn setFrame:CGRectMake(self.frame.size.width-self.frame.size.width/5, 0, self.frame.size.width/5 ,kActionRowHeight)];
        [cameraBtn setShowsTouchWhenHighlighted:YES];
        [cameraBtn setImageScale:1.4];
        [cameraBtn addTarget:self action:@selector(startCamera) forControlEvents:UIControlEventTouchUpInside];
        [actionRowView addSubview:cameraBtn];
    }
    if ([mission isActive]) {
        [cameraBtn setButtonImage:[UIImage imageNamed:@"camera_grey"]];
        [cameraBtn setEnabled:YES];
    } else {
        [cameraBtn setButtonImage:[UIImage imageNamed:@"camera_grey_no"]];
        [cameraBtn setEnabled:NO];
    }*/

    if (likeBtn == nil) {
        likeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [likeBtn setTintColor:kTRANSPARENTUICOLOR];
        [actionRowView addSubview:likeBtn];
    }
    [likeBtn setBackgroundImage:[UIImage ch_imageNamed:@"banana_inactive.png"] forState:UIControlStateNormal];
    [likeBtn setBackgroundImage:[UIImage ch_imageNamed:@"banana_active.png"] forState:UIControlStateSelected];
    [likeBtn setTitle:@"like" forState:UIControlStateDisabled];
    [likeBtn addTarget:self action:@selector(doLikeAction:) forControlEvents:UIControlEventTouchUpInside];
    [likeBtn setSelected:[[mission has_liked] boolValue]];
    [likeBtn setFrame:CGRectMake(self.frame.size.width/5/*CGRectGetMaxX(shareBtn.frame)*/, 0, kActionRowHeight, kActionRowHeight)];
    
    if (dislikeBtn == nil) {
        dislikeBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [dislikeBtn setTintColor:kTRANSPARENTUICOLOR];
        [actionRowView addSubview:dislikeBtn];
    }
    [dislikeBtn setBackgroundImage:[UIImage ch_imageNamed:@"shit_inactive.png"] forState:UIControlStateNormal];
    [dislikeBtn setBackgroundImage:[UIImage ch_imageNamed:@"shit_active.png"] forState:UIControlStateSelected];
    [dislikeBtn setTitle:@"dislike" forState:UIControlStateDisabled];
    [dislikeBtn addTarget:self action:@selector(doLikeAction:) forControlEvents:UIControlEventTouchUpInside];
    [dislikeBtn setSelected:[[mission has_disliked] boolValue]];
    [dislikeBtn setFrame:CGRectMake(CGRectGetMinX(shareBtn.frame)-[dislikeBtn backgroundImageForState:UIControlStateNormal].size.width, 0, kActionRowHeight, kActionRowHeight)];
    
    DCMSG([mission like_count]);
    
    if (likeDislikeCount == nil) {
        likeDislikeCount = [[wbLabel alloc] init];
        [likeDislikeCount setFont:kFONT(24)];
        [likeDislikeCount setBackgroundColor:kTRANSPARENTUICOLOR];
        [likeDislikeCount setTextAlignment:NSTextAlignmentCenter];
        [actionRowView addSubview:likeDislikeCount];
    }
    [likeDislikeCount setText:[mission like_count_string]];
    [likeDislikeCount setEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
    [likeDislikeCount sizeToFit];
    [likeDislikeCount setCenter:CGPointMake(self.frame.size.width/2, kActionRowHeight/2)];
    
    BOOL bOrS;
    if ([[mission like_count] integerValue] < 0) {
        bOrS = NO;
        [likeDislikeCount setTextColor:kREDUICOLOR];
    } else {
        bOrS = YES;
        [likeDislikeCount setTextColor:kGREENUICOLOR];
    }

    if (ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [actionRowView addSubview:ai];
        [ai setFrame:CGRectMake(0, 0, kActionRowHeight, kActionRowHeight)];
        [ai setHidden:YES];
        [ai setHidesWhenStopped:YES];
    }

    if (likeBubble != nil) {
        [likeBubble removeFromSuperview];
    }
    likeBubble = [[wbAPI sharedAPI] giveBubleWithBanana:bOrS frame:CGRectMake(CGRectGetMinX(likeBtn.frame), 0, self.frame.size.width-(CGRectGetMinX(likeBtn.frame)+(self.frame.size.width-CGRectGetMaxX(dislikeBtn.frame))), kActionRowHeight)];
    [actionRowView addSubview:likeBubble];
    
    return actionRowView;
}

-(void)showShareSelect
{
    if (activityViewController == nil) {
        NSArray *excludedServices = [NSArray arrayWithObjects:
                                     //UIActivityTypePostToFacebook,
                                     //UIActivityTypePostToTwitter,
                                     //UIActivityTypePostToWeibo,
                                     //UIActivityTypeMessage,
                                     //UIActivityTypeMail,
                                     UIActivityTypePrint,
                                     //UIActivityTypeCopyToPasteboard,
                                     UIActivityTypeAssignToContact,
                                     UIActivityTypeSaveToCameraRoll,
                                     //UIActivityTypeAddToReadingList,
                                     UIActivityTypePostToFlickr,
                                     UIActivityTypePostToVimeo,
                                     //UIActivityTypePostToTencentWeibo,
                                     UIActivityTypeAirDrop,
                                     nil];
        
        NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"MISSIONS_SHARE_MESSAGE", @""),[mission title]];
        NSURL *shareURL = [mission mission_url];
        activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[shareString, shareURL] applicationActivities:nil];
        [activityViewController setExcludedActivityTypes:excludedServices];
        [activityViewController setValue:NSLocalizedString(@"GENERAL_APP_NAME", @"") forKey:@"subject"];
        
    }
    hideByShare = YES;
    [kROOTVC presentViewController:activityViewController animated:YES completion:nil];
}

-(void)doLikeAction:(id)sender
{
    UIButton *button = sender;
    [ai setCenter:button.center];
    [ai startAnimating];
    [ai setHidden:NO];
    if ([button isSelected]) {
        if ([[button titleForState:UIControlStateDisabled] isEqualToString:@"dislike"]) {
            [mission undislikeMission];
        } else {
            [mission unlikeMission];
        }
    } else {
        if ([[button titleForState:UIControlStateDisabled] isEqualToString:@"dislike"]) {
            [mission dislikeMission];
        } else {
            [mission likeMission];
        }
    }
}

-(void)updateReady
{
    if  ([mission ready]) {
        [ai stopAnimating];
        [ai setHidden:YES];
        
        (void)[self actionRow];
    } else {
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self updateReady];
        });
    }
}

///////////////////
-(UIView*)enterRow
{
    if (cameraBtn == nil) {
        cameraBtn = [wbButton buttonWithType:UIButtonTypeSystem];
        [cameraBtn setFrame:CGRectMake(0, 0, self.frame.size.width ,kActionRowHeight)];
        [cameraBtn setShowsTouchWhenHighlighted:YES];
        [cameraBtn addTarget:self action:@selector(startCamera) forControlEvents:UIControlEventTouchUpInside];
        [cameraBtn setTitle:NSLocalizedString(@"MISSIONS_CREATE_MISSION_VIDEO", @"") forState:UIControlStateNormal];
        [cameraBtn setTitle:NSLocalizedString(@"MISSIONS_COMPLETED", @"") forState:UIControlStateDisabled];
        [[cameraBtn titleLabel] setFont:kFONTHelvetica(15)];
        [cameraBtn setTitleColor:kBKGUICOLOR forState:UIControlStateNormal];
        [cameraBtn setTitleColor:kBKGUICOLOR forState:UIControlStateDisabled];
   }
    if ([[mission submissions_left] integerValue] > 1) {
        NSString *string = [NSString stringWithFormat:NSLocalizedString(@"MISSIONS_SUBMISSIONS_LEFT", @""),[[mission submissions_left] integerValue]];
       [cameraBtn setTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedString(@"MISSIONS_CREATE_MISSION_VIDEO", @""), string] forState:UIControlStateNormal];
    } else {
        [cameraBtn setTitle:NSLocalizedString(@"MISSIONS_CREATE_MISSION_VIDEO", @"") forState:UIControlStateNormal];
    }
    [cameraBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 4, 0)];
    if ([[mission submissions_left] integerValue] == 0) {
        [cameraBtn setEnabled:NO];
        [cameraBtn setBackgroundColor:kYELLOWUICOLOR];
    } else {
        [cameraBtn setEnabled:YES];
        [cameraBtn setBackgroundColor:kGREENUICOLOR];
    }
    
    return cameraBtn;
}

-(void)startCamera
{
    if ([[mission submissions_left] integerValue] > 0) {
        hideByShare = YES;
        [[wbAPI sharedAPI] setMissionData:[mission mission_dictionary]];
        [kROOTVC performSelector:@selector(startCamera:) withObject:nil];
    }
}

///////////////////

-(UIView*)whoRow
{
    if (whoRowView == nil) {
        whoRowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kWhoRowHeight)];
    }
    if (userPicture == nil) {
        userPicture = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kWhoRowHeight-4, kWhoRowHeight-4)];
        [[mission created_by] userProfilePictureImageView:userPicture];
        [userPicture setCenter:CGPointMake(15+kWhoRowHeight/2, kWhoRowHeight/2)];
        [whoRowView addSubview:userPicture];
    }
    if (username == nil) {
        username = [[wbButton alloc] init];
        [[mission created_by] getUserButton:username];
        [username sizeToFit];
        [username setFrame:CGRectMake(CGRectGetMaxX(userPicture.frame)+6, kWhoRowHeight/2, username.frame.size.width, username.frame.size.height)];
        [whoRowView addSubview:username];
    }
    if (title == nil) {
        title = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-CGRectGetMaxX(userPicture.frame), 0)];
        [title setText:[[mission title] uppercaseString]];
        [title setFont:kFONT(20)];
        [title setTextColor:kWHITEUICOLOR];
        [title setEdgeInsets:UIEdgeInsetsMake(0, 0, -6, 15)];
        [title setNumberOfLines:1];
        [title setPreserveWidth:YES];
        [title sizeToFit];
        [title setFrame:CGRectMake(username.frame.origin.x, CGRectGetMinY(username.frame)-title.frame.size.height, title.frame.size.width, title.frame.size.height)];
        [whoRowView addSubview:title];
    }
    return whoRowView;
}
///////////////////
-(UIView*)descriptionRow
{
    if (description == nil) {
        description = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 0)];
        [description setText:[mission text]];
        [description setFont:kFONTHelveticaReg(15)];
        [description setTextColor:kWHITEUICOLOR];
        [description setDataDetectorTypes:UIDataDetectorTypeLink];
        if ([[mission text] length] > 0) {
            [description setTextContainerInset:UIEdgeInsetsMake(6, 6, 6, 6)];
            [description sizeToFit];
        }
        [description setScrollEnabled:NO];
        [description setEditable:NO];
        [description setUserInteractionEnabled:YES];
        [description setTintColor:kGREENUICOLOR];
        [description setBackgroundColor:kTRANSPARENTUICOLOR];
    }
    return description;
}
///////////////////
-(UIView*)infoRow
{
    if (infoRowView == nil) {
        infoRowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kWhoRowHeight)];
    }
    if (videoLabel == nil) {
        videoLabel = [[wbLabel alloc] init];
        if ([mission max_submissions] > 0) {
            [videoLabel setText:[NSString stringWithFormat:NSLocalizedString(@"MISSIONS_VIDEO_COUNT_COUNT", @""),[mission number_of_submissions_string],[mission max_submissions_string]]];
        } else {
            [videoLabel setText:[NSString stringWithFormat:NSLocalizedString(@"MISSIONS_VIDEO_COUNT", @""),[[mission number_of_submissions_string] integerValue]]];
        }
        [videoLabel setFont:kFONTHelveticaReg(15)];
        [videoLabel setTextColor:kWHITEUICOLOR];
        [videoLabel sizeToFit];
        [videoLabel setCenter:CGPointMake(15+videoLabel.frame.size.width/2, kInfoRowHeight/2)];
        [infoRowView addSubview:videoLabel];
    }
    if (rulesButton == nil) {
        rulesButton = [[wbButton alloc] init];
        [rulesButton setTitle:NSLocalizedString(@"MISSIONS_RULES_LINK_TEXT", @"") forState:UIControlStateNormal];
        [[rulesButton titleLabel] setFont:kFONTHelvetica(15)];
        [rulesButton setTitleColor:kWHITEUICOLOR forState:UIControlStateNormal];
        [rulesButton sizeToFit];
        [rulesButton setCenter:CGPointMake(self.frame.size.width-(15+rulesButton.frame.size.width/2), kInfoRowHeight/2)];
        [rulesButton addTarget:self action:@selector(openRulesLink) forControlEvents:UIControlEventTouchUpInside];
        [infoRowView addSubview:rulesButton];
    }
    return infoRowView;
}

-(void)openRulesLink
{
    /*NSString *string = NSLocalizedString(@"GENERAL_WARNING_TITLE", @"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string
                                                    message:NSLocalizedString(@"SETTINGS_CONFIRM_BROWSER", @"")
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"GENERAL_CANCEL",@"")
                                          otherButtonTitles:NSLocalizedString(@"GENERAL_OK",@""),nil];
    [alert setTag:0];
    [alert show];*/
    if (wv == nil) {
        wv = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
        UIButton *close = [UIButton buttonWithType:UIButtonTypeSystem];
        [close setFrame:CGRectMake(wv.frame.size.width-40, 30, 30, 30)];
        [close addTarget:self action:@selector(closeWebView) forControlEvents:UIControlEventTouchUpInside];
        [close setBackgroundImage:[UIImage imageNamed:@"cancel_profile_pic"] forState:UIControlStateNormal];
        [close setTintColor:kTRANSPARENTUICOLOR];
        if (aiweb == nil) {
            aiweb = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        }
        [aiweb startAnimating];
        [aiweb setHidden:NO];
        [aiweb setCenter:wv.center];
        [wv addSubview:aiweb];
        [wv addSubview:close];
    }
    [wv setBackgroundColor:kBKGUICOLORAlpha];
    [wv setOpaque:NO];
    [wv setDelegate:self];
    [wv loadRequest:[NSMutableURLRequest requestWithURL:[mission mission_rules_url_ios]]];
    [wv setHidden:NO];
    [[[[UIApplication sharedApplication] delegate] window] addSubview:wv];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
        if (buttonIndex == 1) {
            [self closeWebView];
            [[UIApplication sharedApplication]  openURL:rulesLinkUrl];
        }
}

-(void)closeWebView
{
    allowLoad = YES;
    [wv setHidden:YES];
    [wv removeFromSuperview];
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    if (!allowLoad) {
        NSLog(@"url: %@",[request URL]);
        rulesLinkUrl = [request URL];
        DCMSG([rulesLinkUrl pathComponents]);
        if ([[[rulesLinkUrl pathComponents] objectAtIndex:1] isEqualToString:@"u"]) {
            [self closeWebView];
            [[mission created_by] openUserFeed:self];
            return NO;
        } else if ([[[rulesLinkUrl pathComponents] objectAtIndex:1] isEqualToString:@"m"]) {
            [self closeWebView];
            [self scrollTableTop];
            return NO;
        } else {
            NSString *string = NSLocalizedString(@"GENERAL_WARNING_TITLE", @"");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:string
                                                            message:NSLocalizedString(@"SETTINGS_CONFIRM_BROWSER", @"")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"GENERAL_CANCEL",@"")
                                                  otherButtonTitles:NSLocalizedString(@"GENERAL_OK",@""),nil];
            [alert setTag:0];
            [alert show];
        }
    }
    return allowLoad;
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    allowLoad = NO;
    [aiweb stopAnimating];
    [aiweb setHidden:YES];
}

///////////////////
-(UIView*)missionVideoRowWithId:(NSNumber*)videoid
{
    if ([missionVideos count] == [videoid integerValue] + 1) {
        if (next != nil) {
            [self getMissionVideosWithURL:next];
        }
    }

    wbVideoCell *videoCell = [[wbVideoCell alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kMissionVideoRowHeight)];
    [missionVideoCells setObject:videoCell forKey:videoid];
    [videoCell setCellContent:[missionVideos objectAtIndex:[videoid integerValue]]];
    return [missionVideoCells objectForKey:videoid];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([indexPath row] >= kFirstVideoRow) {
        
        [[wbData sharedData] setSingleVideoData:[missionVideos objectAtIndex:[indexPath row]-kFirstVideoRow]];
        
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        [op setCompletionBlock:^(void){
            [kROOTVC performSelectorOnMainThread:@selector(startSingleVideo:) withObject:self waitUntilDone:NO];
        }];
        
        [op start];
        
        DMSG;
        [self deselectRowAtIndexPath:indexPath animated:NO];
    }
}

-(void)getMissionVideosWithURL:(NSURL*)url
{
    [[wbAPI sharedAPI] getJSONWithURL:url response:^(id data) {
        NSInteger oldCount = [missionVideos count];
        [missionVideos addObjectsFromArray:[data objectForKey:@"results"]];
        if (![[data objectForKey:@"next"] isKindOfClass:[NSNull class]]) {
            next = [NSURL URLWithString:[data objectForKey:@"next"]];
        } else {
            next = nil;
        }
        [self beginUpdates];
        for (NSInteger i = oldCount+kFirstVideoRow; i < [missionVideos count]+kFirstVideoRow; i++) {
            [self insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
        }
        [self endUpdates];
        if (refreshing) {
            [self setContentOffset:CGPointZero animated:YES];
        }
        refreshing = NO;
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        [self setScrollEnabled:YES];
        kHIDEWAIT;
    }];
}

///////////////////
///////////////////


@end
