//
//  wbRoundedButton.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 11/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbRoundedButton.h"

@implementation wbRoundedButton

@synthesize buttonColor;
@synthesize filled;
@synthesize fontSize;
@synthesize buttonWidth;
@synthesize buttonText;
@synthesize cornerRadius;
@synthesize helvetica;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //[self setBackgroundColor:[UIColor yellowColor]];
        // Initialization code
        buttonColor = kGREENUICOLOR;
        fontSize = 20;
        cornerRadius = 8;
        [self setTintColor:kTRANSPARENTUICOLOR];
        helvetica = YES;
        [self setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
        [self setContentVerticalAlignment:UIControlContentVerticalAlignmentFill];
    }
    DPRINTCLASS;
    return self;
}

-(NSString*)description
{
    return [super description];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    if (filled) {
        [self setBackgroundImage:[self borders:self.frame.size color:buttonColor fill:YES] forState:UIControlStateNormal];
        [self setBackgroundImage:[self borders:self.frame.size color:buttonColor fill:NO] forState:UIControlStateHighlighted];
        [self setTitleColor:kBKGUICOLOR forState:UIControlStateNormal];
        [self setTitleColor:buttonColor forState:UIControlStateHighlighted];

    } else {
        [self setBackgroundImage:[self borders:self.frame.size color:buttonColor fill:NO] forState:UIControlStateNormal];
        [self setBackgroundImage:[self borders:self.frame.size color:buttonColor fill:YES] forState:UIControlStateHighlighted];
        [self setTitleColor:buttonColor forState:UIControlStateNormal];
        [self setTitleColor:kBKGUICOLOR forState:UIControlStateHighlighted];

    }
    if (helvetica) {
        [self.titleLabel setFont:kFONTHelvetica(fontSize)];
    } else {
        [self.titleLabel setFont:kFONT(fontSize)];
    }

    [super drawRect:rect];
}

-(void)sizeToFit
{
    wbLabel *tmp = [[wbLabel alloc] init];
    [tmp setText:[[self titleLabel] text]];
    //float bottomFix;
    //float topFix;
    float marginal;
    if (helvetica) {
        [tmp setFont:kFONTHelvetica(fontSize)];
        marginal = 20;
        //bottomFix = 0;
        //topFix = 0;
    } else {
        [tmp setFont:kFONT(fontSize)];
        marginal = 5;
        //bottomFix = 0;
        //topFix = 0;
    }
    [tmp setNumberOfLines:[[self titleLabel] numberOfLines]];
    [tmp setEdgeInsets:UIEdgeInsetsMake((fontSize/4.0), (fontSize/2.0), (fontSize/4.0), (fontSize/2.0))];
    [tmp sizeToFit];
    
    
//    [super sizeToFit];
    [self setFrame:CGRectMake(self.frame.origin.x,self.frame.origin.y,tmp.frame.size.width+self.titleEdgeInsets.left+self.titleEdgeInsets.right+marginal, tmp.frame.size.height+self.titleEdgeInsets.top+self.titleEdgeInsets.bottom)];

    tmp = nil;
}

-(UIImage*)borders:(CGSize)size color:(UIColor*)color fill:(BOOL)fill
{
    // variables to make things happend
    float fontFix = 0;
    if (!helvetica) {
        fontFix = 3;
    }

    float width = size.width-1;
    float height = size.height-1;
    cornerRadius = size.height/2.1;
    float xx = 1;
    float yy = 1;
    float rr = cornerRadius-fontFix/2;
    //start graphic context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width,size.height),0.0,0.0);
        
    //this gets the graphic context
    CGContextRef context = UIGraphicsGetCurrentContext();
        
    //you can stroke and/or fill
    UIBezierPath *countBkg = [UIBezierPath bezierPath];
        
    //draw "buble"
    [countBkg moveToPoint:CGPointMake(xx+rr, yy)];
    [countBkg addLineToPoint:CGPointMake(width-rr, yy)];
    [countBkg addCurveToPoint:CGPointMake(width, yy+rr) controlPoint1:CGPointMake(width-rr/2,yy) controlPoint2:CGPointMake(width, yy+rr/2)];
    [countBkg addLineToPoint:CGPointMake(width, height-fontFix-rr)];
    [countBkg addCurveToPoint:CGPointMake(width-rr, height-fontFix) controlPoint1:CGPointMake(width,height-fontFix-(rr/2)) controlPoint2:CGPointMake(width-rr/2, height-fontFix)];
    [countBkg addLineToPoint:CGPointMake(xx+rr, height-fontFix)];
    [countBkg addCurveToPoint:CGPointMake(xx, height-fontFix-rr) controlPoint1:CGPointMake(xx+rr/2,height-fontFix) controlPoint2:CGPointMake(xx, height-fontFix-(rr/2))];
    [countBkg addLineToPoint:CGPointMake(xx, yy/2+(height-fontFix)/2)];
    [countBkg addLineToPoint:CGPointMake(xx, yy+rr)];
        
    [countBkg addCurveToPoint:CGPointMake(xx+rr, yy) controlPoint1:CGPointMake(xx,yy+rr/2) controlPoint2:CGPointMake(xx+rr/2, yy)];
    [countBkg closePath];
    
    CGContextSetStrokeColorWithColor(context, color.CGColor);
    [countBkg setLineWidth:1.0];
    [countBkg stroke];
    if (fill) {
        CGContextSetFillColorWithColor(context, color.CGColor);
        [countBkg fill];
    }
    
    //create image
    UIImage *bezierImage = UIGraphicsGetImageFromCurrentImageContext();
        
    UIGraphicsEndImageContext();
    
    context = nil;
    countBkg = nil;
    
    return bezierImage;
}

-(void)setButtonWidth:(float)_buttonWidth
{
    CGRect tmpRect = self.frame;
    tmpRect.size.width = _buttonWidth;
    [self setFrame:tmpRect];
}

-(float)buttonWidth
{
    return self.frame.size.width;
}

-(void)setButtonText:(NSString *)_buttonText
{
    if (!helvetica) {
        _buttonText = [_buttonText uppercaseString];
    }
    [[self titleLabel] setText:_buttonText];
    [self setTitle:_buttonText forState:UIControlStateNormal];
    [self setTitle:_buttonText forState:UIControlStateHighlighted];
    [self setTitle:_buttonText forState:UIControlStateSelected];
    [self setTitle:_buttonText forState:UIControlStateDisabled];
    [self setTitle:_buttonText forState:UIControlStateReserved];
}

-(void)setTitle:(NSString *)title forState:(UIControlState)state
{
    if (helvetica) {
        [super setTitle:title forState:state];
        [[self titleLabel] setText:title];
    } else {
        [super setTitle:[title uppercaseString] forState:state];
        [[self titleLabel] setText:[title uppercaseString]];
    }

}

-(void)dealloc
{
    buttonColor = nil;
    buttonText = nil;
}

@end
