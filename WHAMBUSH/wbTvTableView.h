//
//  wbTvTableView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/03/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseTableView.h"

typedef NS_ENUM(NSUInteger, channelTypeDef) { kCOUNTRYCHANNEL = 0, kSUBCHANNEL, kARTISTCHANNEL };

@interface wbTvTableView : wbBaseTableView <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger numberOfChannels;
    NSMutableArray *channelArray;
    channelTypeDef channelType;
}
@property (nonatomic) NSInteger channelId;

@end
