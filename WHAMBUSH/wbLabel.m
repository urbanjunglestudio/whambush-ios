//
//  wbLabel.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 18/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbLabel.h"

@implementation wbLabel

@synthesize edgeInsets;
@synthesize preserveWidth;

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        preserveWidth = NO;
    }
    DPRINTCLASS;
    return self;
}


-(NSString*)description
{
    return [super description];
}

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
}

-(void)sizeToFit
{
    
    CGRect newFrame = self.frame;
    
    if (preserveWidth) {
        
        float oldWidth = self.frame.size.width - (edgeInsets.left + edgeInsets.right);
        
        newFrame.size.width = oldWidth;
        
        [self setFrame:newFrame];
    
        [super sizeToFit];
    
        newFrame = self.frame;

        newFrame.size.width = oldWidth + (edgeInsets.left + edgeInsets.right);
    } else {
        [super sizeToFit];
        newFrame.size.width = self.frame.size.width + edgeInsets.left + edgeInsets.right;
    }
    newFrame.size.height = 3 + self.frame.size.height + edgeInsets.top + edgeInsets.bottom;
    
    [self setFrame:newFrame];
}

- (UIImage *)labelAsImage:(CGSize)_size
{
    // Create a "canvas" (image context) to draw in.
    UIGraphicsBeginImageContextWithOptions(_size, 0.0, 0.0);
    
    // Make the CALayer to draw in our "canvas".
    //[[self layer] renderInContext: UIGraphicsGetCurrentContext()];
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // Fetch an UIImage of our "canvas".
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Stop the "canvas" from accepting any input.
    UIGraphicsEndImageContext();
    
    // Return the image.
    return image;
}

@end
