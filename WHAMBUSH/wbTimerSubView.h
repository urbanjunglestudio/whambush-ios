//
//  wbTimerSubView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/18/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbDefines.h" 

@interface wbTimerSubView : UIView
{
    struct clips {
        int *viewIndex;
        float startAngle;
        float stopAngle;
    };
    CGRect timerRect;
    UILabel* timeInSeconds;
    UIColor* timerColor;
    UIImageView *roundThing;
    NSMutableArray *clipsArray;
    
    UIImageView *circle;
}

@property BOOL recording;
@property (nonatomic) float seconds;
@property (nonatomic) float lineWidth;

-(void)setClipPoint;
-(void)removeClipPoint;


@end
