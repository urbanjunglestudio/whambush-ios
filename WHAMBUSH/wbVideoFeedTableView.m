//
//  wbVideoFeedTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 31/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbVideoFeedTableView.h"

@implementation wbVideoFeedTableView

//@synthesize currentFeed;
@synthesize isTv;
//@synthesize hideHeader;
@synthesize currentSelection;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        //[self setCanCancelContentTouches:NO];
        
        [self setHidden:NO];
        
        //refresh = NO;
        
//        UIRefreshControl *refreshControler = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
//        [refreshControler addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
//        [self addSubview:refreshControler];
   
        //gettingMoreResults = NO;
        isTv = NO;
        //hideHeader = NO;

        //deltaY = 0;
        //prevContentOffset = 0;
        //currentFeed = super.currentFeed;
        currentSelection = nil;
    }
    DPRINTCLASS;
    return self;
}

//-(NSDictionary*)currentFeed
//{
//    return super.currentFeed;
//}

//-(void)setCurrentFeed:(NSDictionary *)_currentFeed
//{
//    currentFeedType = [_currentFeed objectForKey:@"type"];
//    currentFeed = _currentFeed;
//    super.endpoint = [_currentFeed objectForKey:@"endpoint"];
//    refresh = NO;
//    [self setGAIViewName];
//}

-(void)setGAIViewName
{
    // Google analytics
    NSString *gaString;
    if ([self currentFeed] != nil) {
        if ([[[self currentFeed] objectForKey:@"type"] isEqualToString:@"user"]) {
            gaString = [NSString stringWithFormat:@"UserFeed:user=%@",[[self currentFeed] objectForKey:@"userId"]];
        } else if ([[[self currentFeed] objectForKey:@"type"] isEqualToString:@"search"]) {
            gaString = [NSString stringWithFormat:@"SearchFeed:feed=%@",[[self currentFeed] objectForKey:@"name"]];
        } else if ([[[self currentFeed] objectForKey:@"type"] isEqualToString:@"mission"]) {
            gaString = [NSString stringWithFormat:@"MissionFeed:feed=%@",[[self currentFeed] objectForKey:@"missionId"]];
        } else {
            gaString = [NSString stringWithFormat:@"Main:feed=%@",[[self currentFeed] objectForKey:@"name"]];
        }
        [[wbAPI sharedAPI] registerGoogleAnalytics:gaString];
    }
    //DMSG;
}

-(void)forceRefresh
{
    [ai setHidden:NO];
    [[self tableFooterView] setHidden:NO];
    [self refresh:nil];
}

-(void)afterDelete
{
    dataArray = nil;
    [self setNeedsDisplay];
}

 - (void)drawRect:(CGRect)rect
{
    if (refreshController == nil) {
        refreshController = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [refreshController addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[UIImage ch_imageNamed:@"refresh_bg_image"]]];
        [self addSubview:refreshController];
        
    }
    
    if  (isTv) {
        super.hideHeader = YES;
    }
    
   
    UIView *tableFooter =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 100)];
    [tableFooter setBackgroundColor:kBKGUICOLOR];
    if (/*super.hideHeader*/ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [ai startAnimating];
        [ai setHidesWhenStopped:NO];
        [ai setCenter:CGPointMake(self.frame.size.width/2,kFOLLOWLINEH/2)];
        [tableFooter addSubview:ai];
    }
    
    //tableHeader =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kTXTBOXH)];
    //[tableHeader setBackgroundColor:kBKGUICOLOR];
    
    if (noVideos == nil) {
        noVideos = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    if ([[wbAPI sharedAPI] is_guest]) {
        [noVideos setTitle:[NSLocalizedString(@"MAIN_NO_VIDEOS_IN_FEED_GUEST", @"") uppercaseString] forState:UIControlStateNormal];
    } else {
        [noVideos setTitle:[NSLocalizedString(@"MAIN_NO_VIDEOS_IN_FEED", @"") uppercaseString] forState:UIControlStateNormal];
        [noVideos setEnabled:NO];
    }
    [[noVideos titleLabel] setNumberOfLines:2];
    [[noVideos titleLabel] setTextAlignment:NSTextAlignmentCenter];
    [[noVideos titleLabel] setFont:kFONTLeague(20)];
    [noVideos setTitleColor:kBOTTOMUICOLOR forState:UIControlStateNormal];
    [noVideos setBackgroundColor:kTRANSPARENTUICOLOR];
    [noVideos addTarget:self action:@selector(showAskToLoginRegisterOnView) forControlEvents:UIControlEventTouchUpInside];
    [noVideos sizeToFit];
    [noVideos setCenter:tableFooter.center];
    [noVideos setHidden:YES];
    
    [tableFooter addSubview:noVideos];
    //[tableFooter setHidden:YES];

    if (isTv) {
        [[wbHeader sharedHeader] setLogo:@"_tv_"];
        UIImageView *tvHeader = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width,kTVCELLH)];
        //NSLog(@"%@",super.currentFeed);
        [tvHeader setImageWithURL:[NSURL URLWithString:[[self currentFeed] objectForKey:@"picture_url"]] placeholderImage:[UIImage ch_imageNamed:@"placeholder_banner"]];
        if (tvHeaderLabel == nil) {
            tvHeaderLabel = [[UILabel alloc] init];
        }
//        [tvHeaderLabel setFont:kFONT(30)];
//        [tvHeaderLabel setText:[[tvHeaderLabel text] uppercaseString]];
//        [tvHeaderLabel setBackgroundColor:kTRANSPARENTUICOLOR];
//        [tvHeaderLabel sizeToFit];
//        [tvHeaderLabel setFrame:CGRectMake(10, tvHeader.frame.size.height-textLabel.frame.size.height, textLabel.frame.size.width, textLabel.frame.size.height)];
        [tvHeader addSubview:tvHeaderLabel];
        [self setTableHeaderView:tvHeader];
    }

    if(dataArray == nil) {
        if ([self currentFeed] != nil) {
            if ([currentFeedType isEqualToString:@"user"] || [currentFeedType isEqualToString:@"mission"] || [currentFeedType isEqualToString:@"tv"]) {
                [[wbAPI sharedAPI] getDataWithEndpoint:[[self currentFeed] objectForKey:@"endpoint"] delegate:self];
                if ([currentFeedType isEqualToString:@"user"]) {
                    wbUser *_user = [[wbData sharedData] getWhambushUser:[[self currentFeed] objectForKey:@"userUrl"]];
                    [[wbHeader sharedHeader] setLogo:[_user username]];
                }
            } else {
                [[wbData sharedData] getFeedData:[[self currentFeed] objectForKey:@"name"] delegate:self];
            }
            [[wbData sharedData] getFeedData:[[self currentFeed] objectForKey:@"name"] delegate:self];
        } else {
            [[wbData sharedData] getFeedData:@"default" delegate:self];
        }
    }
    [self setTableFooterView:tableFooter];
    
}


-(void)executeRefresh
{
    deltaY = 0;
    if ([[[self currentFeed] objectForKey:@"type"] isEqualToString:@"normal"]) {
        [[wbData sharedData] refreshFeedData:[[self currentFeed] objectForKey:@"name"] delegate:self];
    } else {
        [self setCurrentFeed:[self currentFeed]];
        [[wbAPI sharedAPI] getDataWithEndpoint:[[self currentFeed] objectForKey:@"endpoint"] delegate:self];
    }
}

-(void)resultsReady
{
    if (isTv) {
        NSMutableAttributedString *name = [[NSMutableAttributedString alloc] initWithString:[[[self currentFeed] objectForKey:@"name"] uppercaseString]];
        [name addAttribute:NSForegroundColorAttributeName value:kWHITEUICOLOR range:(NSRange){0,[name length]}];
        [name addAttribute:NSFontAttributeName value:kFONT(20) range:NSMakeRange(0, [name length])];

        NSMutableAttributedString *separator = [[NSMutableAttributedString alloc] initWithString:[NSLocalizedString(@"MAIN_TV_EPISODE_SEPARATOR", @"") uppercaseString]];
        [separator addAttribute:NSForegroundColorAttributeName value:kWHITEUICOLOR range:(NSRange){0,[separator length]}];
        [separator addAttribute:NSFontAttributeName value:kFONT(20) range:NSMakeRange(0, [separator length])];
        
        NSMutableAttributedString *count = [[NSMutableAttributedString alloc] initWithString:[[[dataArray valueForKey:@"count"] stringValue] uppercaseString]];
        [count addAttribute:NSForegroundColorAttributeName value:kGREENUICOLOR range:(NSRange){0,[count length]}];
        [count addAttribute:NSFontAttributeName value:kFONT(20) range:NSMakeRange(0, [count length])];
        
        NSMutableAttributedString *episodes = [[NSMutableAttributedString alloc] initWithString:[NSLocalizedString(@"MAIN_TV_EPISODE_STRING", @"") uppercaseString]];
        [episodes addAttribute:NSForegroundColorAttributeName value:kGREENUICOLOR range:(NSRange){0,[episodes length]}];
        [episodes addAttribute:NSFontAttributeName value:kFONT(20) range:NSMakeRange(0, [episodes length])];
        
        NSMutableAttributedString *space = [[NSMutableAttributedString alloc] initWithString:@" "];
        [space addAttribute:NSForegroundColorAttributeName value:kGREENUICOLOR range:(NSRange){0,[space length]}];
        [space addAttribute:NSFontAttributeName value:kFONT(20) range:NSMakeRange(0, [space length])];
        
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] init];
        [text appendAttributedString:name];
        [text appendAttributedString:space];
        [text appendAttributedString:separator];
        [text appendAttributedString:space];
        [text appendAttributedString:count];
        [text appendAttributedString:space];
        [text appendAttributedString:episodes];
        
        [tvHeaderLabel setAttributedText:text];
//        [tvHeaderLabel setText:[ NSString stringWithFormat:@"%@ %@ %@ %@",
//                                [[self currentFeed] objectForKey:@"name"],
//                                NSLocalizedString(@"MAIN_TV_EPISODE_SEPARATOR", @""),
//                                [dataArray objectForKey:@"count"],
//                                NSLocalizedString(@"MAIN_TV_EPISODE_STRING", @"")
//                                ]];
        //[tvHeaderLabel setFont:kFONT(30)];
        //[tvHeaderLabel setTextColor:kWHITEUICOLOR];
        //[tvHeaderLabel setText:[[tvHeaderLabel text] uppercaseString]];
        [tvHeaderLabel setBackgroundColor:kTRANSPARENTUICOLOR];
        [tvHeaderLabel sizeToFit];
        [tvHeaderLabel setFrame:CGRectMake(10, [self tableHeaderView].frame.size.height-(tvHeaderLabel.frame.size.height+10), tvHeaderLabel.frame.size.width, tvHeaderLabel.frame.size.height)];
    }
    DCMSG([[wbData sharedData] feeds]);
    [super setAllFeeds:[[wbData sharedData] feeds]];
    [super setShowFeedSelectSearch:YES];
    
    DMSG;

    if ([[wbAPI sharedAPI] is_guest] && ([[[self currentFeed] objectForKey:@"name"] isEqualToString:@"MAIN_FEED_MY_VIDEOS"] || [[[self currentFeed] objectForKey:@"name"] isEqualToString:@"MAIN_FEED_FOLLOW"])) {
        totalNumberOfResults = 0;
        numberOfResults = 0;
    }
    NSMutableArray *newRows = [[NSMutableArray alloc] init];
    
    for (long i = [self numberOfRowsInSection:0]; i < numberOfResults; i++) {
        NSIndexPath *newReloadRows = [NSIndexPath indexPathForRow:i inSection:0];
        [newRows addObject:newReloadRows];
        newReloadRows = nil;
    }
    
    
    [self beginUpdates];
    [self insertRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationFade];
    [self endUpdates];
    
    
    [self setHidden:NO];
    
    [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    if (totalNumberOfResults == 0 && !([currentFeedType isEqualToString:@"user"] || [currentFeedType isEqualToString:@"search"])) {
        [noVideos setHidden:NO];
    } else {
        [noVideos setHidden:YES];
    }

    [self setGAIViewName];
    
    DMSG;
    kHIDEWAIT;
    DCMSG([self tableFooterView]);
    [ai setHidden:YES];

    [[self tableFooterView] setHidden:NO];
}

// table methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (resultArray != nil) {
        return numberOfResults;
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    wbBaseCell *cell;// = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[wbBaseCell alloc] init];//WithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    [cell setBackgroundColor:kTRANSPARENTUICOLOR];
    
    if ([resultArray count]-8 == [indexPath row] || [resultArray count] == [indexPath row]) {
        if (numberOfResults < totalNumberOfResults) {
            DMSG;
            [ai setHidden:NO];
            [self performSelectorInBackground:@selector(getMoreResults) withObject:nil];
        }
    }
    
    if  ([resultArray count] > ([indexPath row])){
        //[ai removeFromSuperview];
        wbVideoCell *ccell;
        //NSNumber *videoid = [[resultArray objectAtIndex:([indexPath row])] objectForKey:@"id"];
        //if ([[[wbData sharedData] videoCells] objectForKey:videoid] == nil) {
            ccell = [[wbVideoCell alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 96)];
          //  [[[wbData sharedData] videoCells] setObject:ccell forKey:videoid];
            if ([[[resultArray objectAtIndex:([indexPath row])] objectForKey:@"video_type"] integerValue] == 2 && ![[[self currentFeed] objectForKey:@"name" ] isEqualToString:@"WhambushTV"]) {
                [ccell setIsTV:YES];
            }
            [ccell setCellContent:[resultArray objectAtIndex:([indexPath row])]];
        //} else {
        //    ccell = [[[wbData sharedData] videoCells] objectForKey:videoid];
        //}
        [cell addSubview:ccell];
        [cell bringSubviewToFront:ccell];
        [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
        [cell setClipsToBounds:YES];
        if ([currentFeedType isEqualToString:@"mission"]) {
            if ([[[self currentFeed] objectForKey:@"is_ranked"] boolValue]) {
                [ccell setRankNumber:[indexPath row]+1];
            }
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentSelection = indexPath;
    
    [[wbData sharedData] setSingleVideoData:[resultArray objectAtIndex:[indexPath row]]];
    
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
        [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
    }];
    [op setCompletionBlock:^(void){
        
        [kROOTVC performSelectorOnMainThread:@selector(startSingleVideo:) withObject:self waitUntilDone:NO];
    }];
    
    [op start];
    
    DMSG;
    [self deselectRowAtIndexPath:indexPath animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    float height;
    if (numberOfResults > [indexPath row]) {
        height =  96;
        if ([indexPath row] < numberOfResults) {
            if ([[[resultArray objectAtIndex:[indexPath row]] valueForKey:@"deleted"] boolValue]) {
                height = 0;
            }
        }
    } else {
        height = 0;
    }
    return height;
}

-(void)refreshRow
{
    DCMSG([self subviews]);
    if (currentSelection != nil) {
        [self beginUpdates];
        [self reloadRowsAtIndexPaths:[NSArray arrayWithObject:currentSelection] withRowAnimation:UITableViewRowAnimationFade];
        [self endUpdates];
        currentSelection = nil;
    }
    DCMSG([self subviews]);
    [self bringSubviewToFront:feedSelect];
    [self bringSubviewToFront:feedSelectButton];
    DCMSG([self subviews]);
}


@end
