//
//  wbPBJCameraViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 08/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PBJVision.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "wbCameraView.h"

@interface wbPBJCameraViewController : UIViewController <PBJVisionDelegate,UIGestureRecognizerDelegate,UIAlertViewDelegate>
{
    wbCameraView *cameraOverlay;

    UIView *_previewView;
    AVCaptureVideoPreviewLayer *_previewLayer;
    //PBJFocusView *_focusView;
    //GLKViewController *_effectsViewController;
    
    UILabel *_instructionLabel;
    UIView *_gestureView;

    UITapGestureRecognizer *_recordTapGestureRecognizer;
    
    BOOL isFirstRecord;
    BOOL isRecording;
    
    ALAssetsLibrary *_assetLibrary;
    __block NSDictionary *_currentVideo;
    
    NSTimer *theTimer;
    
    UIView *waitView;
    
    BOOL exit;
}
@end
