//
//  wbCommentCell.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/10/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbCommentCell.h"

@implementation wbCommentCell

@synthesize cellContent;
@synthesize controller;
@synthesize totalNumberOfComments;
@synthesize currentCommentNumber;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setClipsToBounds:NO];
    }
    DPRINTCLASS;
    return self;
}


-(void)setCellContent:(NSMutableDictionary *)_cellContent
{
    cellContent = _cellContent;
    DCMSG(cellContent);
    if (cellContent != nil) {
       //[self getUserData:[[_cellContent objectForKey:@"user"] objectForKey:@"url"]];
        if (user == nil) {
            user = [[wbData sharedData] getWhambushUser:[[_cellContent objectForKey:@"user"] objectForKey:@"url"]];
        }
    }
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // Drawing code
    if ([user userReady]) {
        DCMSG(user);
        wbSpeechBubbleView  *sb = [[wbSpeechBubbleView alloc] initWithFrame:CGRectMake(6, 6,  255/*self.frame.size.width-kFOLLOWLINEH*/, [self getHeight]-5)];
        [sb setBubbleAlpha:0.85];
        
        if (numberOfCommentsLabel == nil) {
            numberOfCommentsLabel = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        }
        [numberOfCommentsLabel setText:[NSString stringWithFormat:@"%ld/%ld",(long)currentCommentNumber,(long)totalNumberOfComments]];
        [numberOfCommentsLabel setFont:kFONTHelvetica(12)];
        [numberOfCommentsLabel setTextColor:kBOTTOMUICOLOR];
        [numberOfCommentsLabel setEdgeInsets:UIEdgeInsetsMake(2, 0, 0, 12)];
        [numberOfCommentsLabel sizeToFit];
        [numberOfCommentsLabel setCenter:CGPointMake(CGRectGetMaxX(sb.frame)-numberOfCommentsLabel.frame.size.width/2, CGRectGetMinY(sb.frame)+numberOfCommentsLabel.frame.size.height/2)];
#define kHeadSize 45
        head = [[UIImageView alloc] init];//WithImage:[user profilePicture]];
        [head setFrame:CGRectMake(0, 0, kHeadSize, kHeadSize)];
        [user userProfilePictureImageView:head];
//        }
        [head setCenter:CGPointMake(255+((self.frame.size.width-255)/2), self.frame.size.height-4-(kHeadSize/2))];
        
        if (headButton == nil) {
            headButton = [wbButton buttonWithType:UIButtonTypeSystem];
        }
        //[headButton setPayload:controller];
        //[headButton addTarget:user action:@selector(openUserFeed:) forControlEvents:UIControlEventTouchUpInside];
        [user userButton:headButton controller:controller txt:NO];
        [headButton setFrame:head.frame];
        
        [self addSubview:sb];
        [self addSubview:head];
        [self addSubview:headButton];
        [self addSubview:numberOfCommentsLabel];

        if (comment == nil) {
            comment = [self comment:[NSString stringWithFormat:@"%@",[cellContent valueForKey:@"comment"]]];
        }
        [comment setFrame:CGRectMake(sb.frame.origin.x, sb.frame.origin.y, comment.frame.size.width, comment.frame.size.height) ];
            
        if (author == nil) {
            author = [self author];
        }
        [author setFrame:CGRectMake(CGRectGetMinX(sb.frame), CGRectGetMaxY(comment.frame), author.frame.size.width, author.frame.size.height) ];
            
        if (when == nil) {
            when = [self when:[NSString stringWithFormat:@" %@ %@",NSLocalizedString(@"SINGLE_COMMENT_DELIM", @""),[[wbAPI sharedAPI] parsePostedTime:[cellContent valueForKey:@"post_date"]]]];
        }
        [when setCenter:CGPointMake(CGRectGetMaxX(author.frame)+CGRectGetWidth(when.frame)/2, CGRectGetMidY(author.frame))];
            
        [self addSubview:comment];
        [self addSubview:author];
        [self addSubview:when];
       
//        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
//        [self addGestureRecognizer:tapGesture];
        
    } else {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [ai startAnimating];
        [ai setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
        [self addSubview:ai];
        [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(setNeedsDisplay) userInfo:nil repeats:NO];
    }


}


-(wbLabel*)comment:(NSString*)text
{
    wbLabel *commentLabel = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, 255, 0)];
    [commentLabel setFont:kFONTHelvetica(15)];
    [commentLabel setTextColor:kWHITEUICOLOR];
    [commentLabel setEdgeInsets:UIEdgeInsetsMake(4, 6, 2, 16)];
    [commentLabel setText:text];
    [commentLabel setNumberOfLines:0];
    [commentLabel setPreserveWidth:YES];
    [commentLabel sizeToFit];
    return commentLabel;
}

-(wbButton*)author
{
//    wbButton *authorButton = [user userButton:controller];
    wbButton *authorButton = [wbButton buttonWithType:UIButtonTypeSystem];
    [user userButton:authorButton controller:controller txt:YES];
    [authorButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 0)];
    [authorButton sizeToFit];
    
    return authorButton;
}

-(wbLabel*)when:(NSString*)text
{
    wbLabel *whenLabel = [[wbLabel alloc] init];
    [whenLabel setFont:kFONTHelvetica(10)];
    [whenLabel setText:text];
    [whenLabel setTextColor:kLIGHTGRAYUICOLOR];
    [whenLabel setNumberOfLines:1];
    [whenLabel setEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [whenLabel sizeToFit];
    return whenLabel;
    
}

-(NSInteger)getHeight
{
    return [self getHeight:cellContent];
}

-(NSInteger)getHeight:(NSMutableDictionary*)_cellContent
{
    NSString *textString = [NSString stringWithFormat:@"%@",[_cellContent objectForKey:@"comment"]];
    wbLabel *text = [[wbLabel alloc] initWithFrame:CGRectMake(0, 0, 255, 700)];
    [text setNumberOfLines:0];
    [text setEdgeInsets:UIEdgeInsetsMake(4, 6, 2, 16)];
    [text setFont:kFONTHelvetica(15)];
    [text setText:textString];
    [text setPreserveWidth:YES];
    [text sizeToFit];

    NSInteger commentHeight = text.frame.size.height + 30;
    text = nil;
    textString = nil;
    
    return commentHeight;
}


//- (void)handleTapGesture:(UITapGestureRecognizer *)tapGesture
//{
//    NSLog(@"tapGesture:");
//    
//    CGRect targetRectangle = CGRectMake(self.frame.size.width/2, 0, 0, 0);
//    
//    if (menu == nil) {
//        menu = [[UIMenuController alloc] init];
//    }
//    [menu setTargetRect:targetRectangle inView:self];
//    
//    UIMenuItem *menuItem1 = [[UIMenuItem alloc] initWithTitle:@"Report" action:@selector(reportAction:)];
//    UIMenuItem *menuItem2 = [[UIMenuItem alloc] initWithTitle:@"Delete" action:@selector(deleteAction:)];
//    [menu setMenuItems:@[menuItem1,menuItem2]];
//    [menu update];
//    
//    [menu setMenuVisible:YES animated:YES];
//    DCMSG(menu);
//}
//
//- (BOOL)canBecomeFirstResponder
//{
//    return YES;
//}
//
//- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
//{
//    BOOL result = NO;
//    if(@selector(copy:) == action || @selector(deleteAction:) == action || @selector(reportAction:) == action) {
//        result = YES;
//    }
//    return YES;
//}
//
//// UIMenuController Methods
//
//// Default copy method
//- (void)copy:(id)sender
//{
//    DCMSG(@"copy");
//    [[UIPasteboard generalPasteboard] setString:[comment text]];
//}
//
//// Our custom method
//- (void)deleteAction:(id)sender
//{
//    DCMSG(@"deleteAction");
//}
//- (void)reportAction:(id)sender
//{
//    DCMSG(@"reportAction");
//}

@end
