//
//  wbSingleMissionViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbSingleMissionViewController.h"

@interface wbSingleMissionViewController ()

@end

@implementation wbSingleMissionViewController

@synthesize mission;

//-(void)loadView
//{
//    missionView = [[wbSingleMissionView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
//    [missionView setController:self];
//    [self setView:missionView];
//}

-(void)setMission:(wbMission *)_mission
{
    mission = _mission;
    [missionView setMission:mission];
}

-(void)startCamera {
    [missionView performSelector:@selector(startCamera)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    missionView = [[wbSingleMissionView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [missionView setController:self];
    [missionView setMission:mission];

    [self.view addSubview:missionView];
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    videoPlayerController = [missionView videoPlayerController];
    [[wbHeader sharedHeader] setLogo:[super headerLogo]];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [missionView closeWebView];
    if ([missionView hideByShare]) {
        [missionView setHideByShare:NO];
    } else {
        [[missionView missionTimer] invalidate];
    }
    [[UIApplication sharedApplication] endIgnoringInteractionEvents];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        if (iOS8) {
            [[[[UIApplication sharedApplication] delegate] window] addSubview:videoPlayerController.view];
        } else {
            [self.view addSubview:videoPlayerController.view];
            [self.view bringSubviewToFront:videoPlayerController.view];
        }
        //[singleVideoView setFrame:CGRectMake(0, 0, [[wbAPI sharedAPI] landscapeFrame].size.width, [[wbAPI sharedAPI] landscapeFrame].size.height)];
        //[singleVideoView setNeedsDisplay];
        [missionView setHidden:YES];
        [videoPlayerController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    } else {
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
        [self setNeedsStatusBarAppearanceUpdate];
        [videoPlayerController willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
        //[videoPlayerController.view removeFromSuperview];
        //[singleVideoView setFrame:[[wbAPI sharedAPI] contentRect]];
        //[singleVideoView setNeedsDisplay];
        [missionView setHidden:NO];
        //(void)[(wbSingleMissionView*)[self view] performSelector:@selector(videoRow)];
        [missionView beginUpdates];
        [missionView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [missionView endUpdates];

    }
}

-(BOOL)shouldAutorotate
{
    if (videoPlayerController != nil) {
        DMSG;
        return YES;
    }
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
