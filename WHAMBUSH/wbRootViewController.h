//
//  wbRootViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/28/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbAPI.h"
#import "wbDefines.h"
#import "vzaarAPI.h"

#import "wbStartViewController.h"

#import "wbVideoFeedViewController.h"
#import "wbSingleVideoViewController.h"
#import "wbUserViewController.h"
#import "wbSettingsViewController.h"
#import "wbMissionViewController.h"
#import "wbNewMissionViewController.h"
#import "wbSingleMissionViewController.h"   
#import "wbTvViewController.h"

#import "wbMissionCountrySelectViewController.h"

#import "wbCameraRollViewController.h"
#import "wbUploadViewController.h"
#import "wbLoginRegisterViewController.h"

#import "wbProfilePicPickerViewController.h"
#import "wbPBJCameraViewController.h"

@interface wbRootViewController : UIViewController
{
    wbVideoFeedViewController *videoFeedController;
    //wbCameraViewController *cameraViewController;
    wbPBJCameraViewController *cameraViewController;
    //wbMissionViewController *missionController;
    wbUserViewController *userViewController;
    wbVideoFeedViewController *tvFeedController;
    wbTvViewController *tvChannelsController;
    wbTvViewController *defaultTvViewController;
    wbSettingsViewController *settingsController;
    
    NSMutableArray *viewStack;
    
    NSMutableArray *homeViewStack;
    NSMutableArray *missionViewStack;
    NSMutableArray *tvViewStack;
    NSMutableArray *userViewStack;
    
    UIView *containerView;
    NSInteger tvHomeIndex;
}

@property (nonatomic,retain) NSNumber *defaultTvChannel;
@property (nonatomic,retain,readonly)     wbNewMissionViewController *missionController;


-(void)loginOK;

-(void)askCountry;

//-(void)startLogin:(id)sender;
//-(void)startRegister:(id)sender;
//-(void)newUserRegistered:(id)sender;

//-(void)startVideoFeed:(id)sender;
-(void)startNewVideoFeed:(id)sender feed:(NSDictionary*)feed;
-(void)startUserPage:(id)sender user:(wbUser*)user;
-(void)startSettings:(id)sender;
-(void)startSingleVideo:(id)sender;
-(void)startTvChannelView:(NSNumber*)channelId;
//-(void)startMissions:(id)sender;
//-(void)startUserProfile:(id)sender;
-(void)startCamera:(id)sender;
-(void)closeCamera:(id)sender;
-(void)startCameraRoll:(id)sender;
-(void)startProfilePicGallery:(id)sender;
-(void)startProfilePicCamera:(id)sender;
//-(void)nilCameraRoll;
-(void)startUpload:(id)sender;

-(void)logout:(id)sender;
//-(void)login:(id)sender;

-(void)goBack:(id)sender;
-(void)goHome:(id)sender;

-(void)changeViewStack:(NSString*)stack;

-(void)hideSettings;

@end
