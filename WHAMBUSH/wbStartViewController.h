//
//  wbStartViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 23/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"
#import "wbStartView.h"

@interface wbStartViewController : wbBaseViewController

@end
