//
//  wbCameraRollViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/26/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbCameraRollViewController.h"

@interface wbCameraRollViewController ()

@end

@implementation wbCameraRollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self setVideoQuality:UIImagePickerControllerQualityTypeIFrame1280x720];
        [self setDelegate:self];
    }
    return self;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSURL* url = [info objectForKey:UIImagePickerControllerMediaURL];
    
    [[wbAPI sharedAPI] setVideoURL:url];
    //[kROOTVC performSelector:@selector(closeCamera:) withObject:self];
    [kROOTVC performSelector:@selector(startUpload:) withObject:self];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    //[kROOTVC performSelector:@selector(goBack:) withObject:self];
//    [self dismissViewControllerAnimated:YES completion:^(void){
//        DNSLog(@"-Back from %@",[self description]);
//    }];
//    [kROOTVC performSelector:@selector(nilCameraRoll)];
    [kROOTVC performSelector:@selector(closeCamera:) withObject:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    [self setMediaTypes:[[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil]];
    [self setVideoMaximumDuration:30.0];
    [self setAllowsEditing:YES];
    
    // Google analytics
    NSString *viewName;
    if ([[[wbAPI sharedAPI] uploadData] objectForKey:@"missionData"] != nil) {
        viewName = [NSString stringWithFormat:@"Gallery:mission=%@",[[[[wbAPI sharedAPI] uploadData] objectForKey:@"missionData"] objectForKey:@"id"]];
    } else {
        viewName = @"Gallery";
    }
    [[wbAPI sharedAPI] registerGoogleAnalytics:viewName];
}

-(void)viewDidAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    [super viewDidAppear:animated];
    DMSG;
    [self setNeedsStatusBarAppearanceUpdate];

    kHIDEWAIT
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

/*- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}*/

- (BOOL) shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
   return UIInterfaceOrientationPortrait;
//     return UIInterfaceOrientationLandscapeRight;
}

@end
