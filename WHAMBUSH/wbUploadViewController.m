//
//  wbUploadViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/27/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbUploadViewController.h"

@interface wbUploadViewController ()

@end

@implementation wbUploadViewController

//@synthesize uploadView;
@synthesize uploadSetupData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        scrolled = NO;
    }
    DCMSG(self);
    return self;
}

-(void)loadView
{
    [[wbData sharedData] performSelector:@selector(getAllMissionFeeds) withObject:nil];
    DCMSG(@"uploadvc load view");
    wbUploadView *uploadView = [[wbUploadView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect] controller:self];
    if (uploadSetupData != nil) {
        [uploadView setUploadData:uploadSetupData];        
    }
    
    [self setView:uploadView];
    [[wbFooter sharedFooter] setUploadView:uploadView];
    [[wbHeader sharedHeader] setUploadView:uploadView];
}

- (void)viewDidLoad
{
    DCMSG(@"uploadvc did load view");
    [super viewDidLoad];
}

-(void)goBack:(id)sender
{
    [self.view setHidden:YES];
    [[wbAPI sharedAPI] clearUploadData];
    [[wbAPI sharedAPI] cleanTmpDir];

    [kROOTVC performSelector:@selector(goBack:) withObject:self];
}



-(void)viewDidAppear:(BOOL)animated
{
    [self.view setFrame:[[wbAPI sharedAPI] contentRect]];
    
    DCMSG(@"uploadvc did appear view");

    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    kHIDEWAIT
    [super viewDidAppear:animated];

    NSString *gaString;
    
    if ([[[wbAPI sharedAPI] uploadData] objectForKey:@"missionData"] != nil) {
        DCMSG([[[wbAPI sharedAPI] uploadData] objectForKey:@"missionData"]);
        gaString = [NSString stringWithFormat:@"Upload:mission=%@",[[[[wbAPI sharedAPI] uploadData] objectForKey:@"missionData"] objectForKey:@"id"]];
    } else {
        gaString = @"Upload";
    }
    
    [[wbAPI sharedAPI] registerGoogleAnalytics:gaString];
    [[wbHeader sharedHeader] setShowChangeCountryButton:NO];

}

//////
- (BOOL) shouldAutorotate
{
    return NO;
}
//
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
