//
//  wbRankTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 16/02/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbRankTableView.h"

@implementation wbRankTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        
        resultArray = [[NSMutableArray alloc] init];
        nextPageNumber = 1;
        numberOfResults = 0;
    }
    DPRINTCLASS;
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    UIView *tableFooter =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 100)];
    [tableFooter setBackgroundColor:kBKGUICOLOR];
    [self setTableFooterView:tableFooter];
    
    if (ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [ai startAnimating];
        [ai setCenter:CGPointMake(self.frame.size.width/2,kFOLLOWLINEH/2)];
        [tableFooter addSubview:ai];
    }
    if (numberOfResults == 0) {
        [self getRankingData];
    }
}


-(void)getRankingData//:(NSInteger)page
{
    [[self tableFooterView] setHidden:NO];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@",kGAMEAPI,@"rank"];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions: NSJSONReadingMutableContainers];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
    
    [manager.requestSerializer setValue:@"tofuhead.eu" forHTTPHeaderField:@"X-Coronium-APP-ID"];
    [manager.requestSerializer setValue:@"4a1739e8-6aac-47ea-86e2-1c1c22c3e4c2" forHTTPHeaderField:@"X-Coronium-API-KEY"];
    
    NSString *country;
    //if ([[[[wbUser sharedUser] countryDictionary] valueForKey:@"supported"] boolValue]) {
        country = [[wbUser sharedUser] country];
    //} else {
    //    country = @"ZZ";
    //}
    
    NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:
                                [[wbUser sharedUser] username],@"username",
                                [NSNumber numberWithInteger:nextPageNumber],@"page_number",
                                country,@"country",
                                nil];
    
    [manager POST:urlString parameters:parameters
          success:^(AFHTTPRequestOperation *operation, id responseObject) {
              //DCMSG(responseObject);
              NSDictionary *resultDictionary = [responseObject objectForKey:@"result"];
              
              totalNumberOfResults  = [[resultDictionary valueForKey:@"count"] integerValue];
              nextPageNumber = [[resultDictionary valueForKey:@"next"] integerValue];
              
              [resultArray addObjectsFromArray:[resultDictionary objectForKey:@"results"]];
              numberOfResults = [resultArray count];
              
              [[self tableFooterView] setHidden:YES];
              [self reloadData];
              NSInteger rankNumber = [[[resultDictionary objectForKey:@"user"]  objectForKey:@"rank_local"]integerValue];
              [[wbData sharedData] setRankNumber:rankNumber];
              [[[wbUser sharedUser ] rankLabel] setText:[NSString stringWithFormat:NSLocalizedString(@"USER_RANK",@""),[NSString stringWithFormat:NSLocalizedString(@"USER_COUNT",@""),rankNumber]]];
              [[[wbUser sharedUser ] rankLabel] sizeToFit];

          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              DCMSG(error);
              //[[wbAPI sharedAPI] showErrorWithTxt:@"Ranking error... Try again later"];
              [[self tableFooterView] setHidden:YES];
          }];
    
}

-(UIView*)row:(NSDictionary*)data row:(NSInteger)row
{
    CGFloat mult = 1;
    if (row == 1) {
        mult = 1.2;
    } else if (row == 2) {
        mult = 1.2;
    } else if (row == 3) {
        mult = 1.2;
    }
    wbLabel *number = [[wbLabel alloc] init];
    [number setText:[NSString stringWithFormat:@"%ld",(long)row]];
    //[number setText:[NSString stringWithFormat:@"%@",[data objectForKey:@"rank"]]];
    [number setFont:kFONT(25*mult)];
    [number setTextColor:kGREENUICOLOR];
    [number setEdgeInsets:UIEdgeInsetsMake(6, 12, 0, 6)];
    [number setFrame:CGRectMake(0, 0, 0, 0)];
    [number sizeToFit];
    
    wbLabel *name = [[wbLabel alloc] init];
    [name setText:[NSString stringWithFormat:@"%@",[data objectForKey:@"username"]]];
    [name setFont:kFONTHelvetica(20*mult)];
    [name setTextColor:kWHITEUICOLOR];
    [name setEdgeInsets:UIEdgeInsetsMake(4, 6, 0, 0)];
    [name setFrame:CGRectMake(CGRectGetMaxX(number.frame), 0, 0, 0)];
    [name sizeToFit];
    
    wbLabel *score = [[wbLabel alloc] init];
    [score setText:[NSString stringWithFormat:@"%@",[data objectForKey:@"score"]]];
    [score setFont:kFONT(25*mult)];
    [score setTextColor:kYELLOWUICOLOR];
    [score setEdgeInsets:UIEdgeInsetsMake(6, 0, 0, 12)];
    [score sizeToFit];
    [score setFrame:CGRectMake(self.frame.size.width-score.frame.size.width, 0, score.frame.size.width, score.frame.size.height)];
    
    UIView *rowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, kFOLLOWLINEH)];
    [rowView addSubview:number];
    [rowView addSubview:name];
    [rowView addSubview:score];

    return rowView;
    
}

///////
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = [NSString stringWithFormat:@"%ld_%ld",(long)indexPath.section,(long)indexPath.row];
    
    wbBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if ([resultArray count]-8 == [indexPath row] || [resultArray count] == [indexPath row]) {
        if (numberOfResults < totalNumberOfResults) {
            DMSG;
            [self performSelectorInBackground:@selector(getRankingData) withObject:nil];
        }
    }
    
    if  (cell == nil) {
        cell = [[wbBaseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
        
        [cell setBackgroundColor:kTRANSPARENTUICOLOR];
        //[cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
        [cell addSubview:[self row:[resultArray objectAtIndex:[indexPath row]]row:[indexPath row]+1]];
    }
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfResults;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kFOLLOWLINEH+2;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger userId = [[[resultArray objectAtIndex:[indexPath row]] valueForKey:@"userId"] integerValue];
    if (userId != [[[wbUser sharedUser] userId] integerValue]) {
        wbUser *user = [[wbData sharedData] getWhambushUserWithId:userId];
        if (user != nil) {
            //kSHOWWAIT;
            [kROOTVC performSelector:@selector(startUserPage:user:) withObject:self withObject:user];
        }
    }
    [self deselectRowAtIndexPath:indexPath animated:NO];
}

@end
