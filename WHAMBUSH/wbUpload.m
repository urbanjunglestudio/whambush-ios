//
//  wbUpload.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 06/02/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbUpload.h"

@implementation wbUpload

@synthesize controller;
@synthesize uploadArray;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        if (progView == nil) {
            progView = [[wbUploadProgressView alloc] initWithFrame:CGRectMake(0,0,[[wbFooter sharedFooter] cameraButton].frame.size.width,[[wbFooter sharedFooter] cameraButton].frame.size.height)];
        }
        [progView setDelegate:self];
        saved = NO;
    }
    return self;
}

+ (id)sharedUpload
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//}

-(void)setUploadArray:(NSDictionary *)_uploadArray
{
    DCMSG(_uploadArray);
    uploadArray = _uploadArray;
    
    title       = [_uploadArray objectForKey:@"title"];
    description = [_uploadArray objectForKey:@"description"];
    tagStrings  = [_uploadArray objectForKey:@"tagStrings"];
    missionData = [_uploadArray objectForKey:@"missionData"];
    isMission  = [[_uploadArray objectForKey:@"isMission"] boolValue];
    descriptionSet = [[_uploadArray objectForKey:@"descriptionSet"] boolValue];
    partnerVideo = [[_uploadArray objectForKey:@"partnerVideo"] boolValue];
}

-(void)doUpload
{

    if ([[wbAPI sharedAPI] checkIfNetworkAvailable]) {
        //[[wbAPI sharedAPI] setUploading:YES];
        [[wbAPI sharedAPI] setUploadProgressView:progView];

        if (vAPI == nil) {
            vAPI = [[vzaarAPI alloc] init];
        }
        
        UIBackgroundTaskIdentifier bgTask = 0;
        bgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
            [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        }];
        [[wbAPI sharedAPI] setBgTask:bgTask];
        
        
        //[controller performSelector:@selector(closeUploadView:) withObject:NULL];
        [progView setNeedsDisplay];
        [kROOTVC performSelector:@selector(goHome:) withObject:nil];
        
        [[wbFooter sharedFooter] setNeedsDisplay];

        kHIDEWAIT
       
        [vAPI setDelegate:self];
        [vAPI uploadFile:[[[wbAPI sharedAPI] uploadData] objectForKey:@"url"]];
    } else {
        kHIDEWAIT
        [self saveVideo:[[[wbAPI sharedAPI] uploadData] objectForKey:@"url"]];
    }
}

-(void)setProgress:(NSNumber*)value {
    float floatValue = [value floatValue];
    [progView setProgress:floatValue];
}

-(void)s3UpdateDidComplete:(id)request {
    DCMSG(request);
    
    NSString *tags;
    if ([tagStrings count] > 1) {
        DCMSG(tagStrings);
        [tagStrings removeObjectAtIndex:0];
        DCMSG(tagStrings);
        tags = [tagStrings componentsJoinedByString:@","];
    } else {
        tags = @"";
    }
    
    DCMSG(tagStrings);
    DCMSG(tags);
    
    NSString *descriptioString;
    if (descriptionSet) {
        descriptioString = description;
    } else {
        descriptioString = @"";
    }
    
    NSNumber *videoType = [NSNumber numberWithInt:0];
    if ([[[wbUser sharedUser] userType] intValue] == 1) {
        if (partnerVideo) {
            videoType = [NSNumber numberWithInt:1];
        }
    }
    
    info = [[NSDictionary alloc] initWithObjectsAndKeys:
            title,@"name",
            [descriptioString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]],@"description",
            tags,@"tags",
            videoType,@"video_type",
            nil];
    
    [vAPI notifyVzaar:info];
}

-(void)finalizeUpload:(NSNumber*)id
{
    NSInteger videoid = [id integerValue];
    DNSLog(@"videoid: %ld",(long)videoid);
    if (videoid > 0) {
        DMSG;
        if (isMission) {
            [[wbAPI sharedAPI] finalizeUpload:info vzaarId:videoid missionId:[[missionData valueForKey:@"id"] integerValue] delegate:self];
            
        } else {
            [[wbAPI sharedAPI] finalizeUpload:info vzaarId:videoid delegate:self];
        }
    } else {
        DMSG;
        DCMSG([[wbAPI sharedAPI] uploadData]);
        
        [self uploadDone:@NO];
    }
    
}

-(void)uploadDone:(NSNumber*)ok
{
    if (![ok boolValue]) {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"UPLOAD_ERROR_UPLOAD_FAIL", @"")];
        [self saveVideo:[[[wbAPI sharedAPI] uploadData]objectForKey:@"url"]];
    }
    [progView hide];
    [[wbAPI sharedAPI] setUploadProgressView:nil];
    [[wbAPI sharedAPI] clearUploadData];
    [self cleanTmp];
    UIBackgroundTaskIdentifier bgTask = [[wbAPI sharedAPI] bgTask];
    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
}


-(void)s3UpdateDidFail:(id)request {
    if(!cancelled) {
        DCMSG(request);
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"UPLOAD_ERROR_UPLOAD_FAIL", @"")];
        DCMSG([[wbAPI sharedAPI] uploadData]);
        [self saveVideo:[[[wbAPI sharedAPI] uploadData]objectForKey:@"url"]];
        
        [[wbAPI sharedAPI] setUploadProgressView:nil];
        
        UIBackgroundTaskIdentifier bgTask = [[wbAPI sharedAPI] bgTask];
        [[UIApplication sharedApplication] endBackgroundTask:bgTask];
        
        [self showUploadView];
    }
    DMSG;
    cancelled = NO;
}


-(void)saveVideo:(NSURL*)url
{
    if (!saved) {
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];

        [library writeVideoAtPathToSavedPhotosAlbum:url
                                    completionBlock:^(NSURL *assetURL, NSError *error)
         {
             if (!error)
             {
                 DNSLog(@"SAVE DONE");
                 //[[wbAPI sharedAPI] showNoteWithTxt:NSLocalizedString(@"UPLOAD_SAVE_OK", @"")];
                 //[self cleanTmp:url];
                 saved = YES;
             }
             else
             {
                 //[[wbAPI sharedAPI] performSelectorInBackground:@selector(showErrorWithTxt:) withObject:NSLocalizedString(@"UPLOAD_ERROR_SAVE_FAIL", @"")];
                 [[wbAPI sharedAPI] performSelectorOnMainThread:@selector(showErrorWithTxt:) withObject:NSLocalizedString(@"UPLOAD_ERROR_SAVE_FAIL", @"") waitUntilDone:NO];
                 DNSLog(@"SAVE FAIL");
                 DCMSG(error);
                 //[self cleanTmp:url];
                 saved = NO;
             }
         }
         ];
        library = nil;
    }
}

-(void)cancelUpload
{
    cancelled = YES;
    DMSG;
    [vAPI cancelUpload];
    
    [[wbAPI sharedAPI] setUploadProgressView:nil];
    [progView hide];
    
    UIBackgroundTaskIdentifier bgTask = [[wbAPI sharedAPI] bgTask];
    [[UIApplication sharedApplication] endBackgroundTask:bgTask];
    
    [self showUploadView];
}

-(void)cleanTmp
{
    NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
    
    for (NSString *file in tmpDirectory) {
        if (!([file rangeOfString:@"mov"].length == 0)) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
        }
    }
}

-(void)showUploadView
{
    DCMSG(@"upload showuploadview");
    [kROOTVC performSelector:@selector(startUploadWithData:) withObject:uploadArray];
}



@end
