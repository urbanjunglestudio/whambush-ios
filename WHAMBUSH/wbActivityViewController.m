//
//  wbActivityViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbActivityViewController.h"

@interface wbActivityViewController ()

@end

@implementation wbActivityViewController

-(void)loadView
{
    wbActivityTableView *activityView = [[wbActivityTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [self setView:activityView];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    //[self.view performSelector:@selector(markAllRead) withObject:nil];
    [[wbData sharedData] setNum_of_unreadActivities:[NSNumber numberWithInteger:[[[wbAPI sharedAPI] num_unread_activities] integerValue]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
