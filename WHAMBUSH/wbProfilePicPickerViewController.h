//
//  wbProfilePicCameraViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 16/02/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DZImageEditing/DZImageEditingController.h"
#import "wbProfilePictureOverlay.h"

@interface wbProfilePicPickerViewController : UIImagePickerController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,DZImageEditingControllerDelegate>
{
    BOOL frontCamera;
    //UIButton *flipCamera;
    //CGRect cropRect;
    DZImageEditingController *editor;
    CGFloat tmpScale;
}
@property BOOL isCamera;
@property (retain,nonatomic) id parent;

@end
