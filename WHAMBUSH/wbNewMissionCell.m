//
//  wbNewMissionCell.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/11/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbNewMissionCell.h"

@implementation wbNewMissionCell

@synthesize cellBkg;
@synthesize mission;

-(void)blur:(UIImage*)image
{
    CIFilter *gaussianBlurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [gaussianBlurFilter setDefaults];
    CIImage *inputImage = [CIImage imageWithCGImage:[image CGImage]];
    [gaussianBlurFilter setValue:inputImage forKey:kCIInputImageKey];
    [gaussianBlurFilter setValue:@10 forKey:kCIInputRadiusKey];
    
    CIImage *outputImage = [gaussianBlurFilter outputImage];
    CIContext *context   = [CIContext contextWithOptions:nil];
    CGImageRef cgimg     = [context createCGImage:outputImage fromRect:[inputImage extent]];  // note, use input image extent if you want it the same size, the output image extent is larger
    UIImage *newImage       = [UIImage imageWithCGImage:cgimg];
    CGImageRelease(cgimg);
    [cellBkg performSelectorOnMainThread:@selector(setImage:) withObject:newImage waitUntilDone:NO];
}


/////////////////////////////////
-(void)drawRect:(CGRect)rect
{
    if ([mission ready] && [user userReady]) {
        
        if (cellBkg == nil) {
            cellBkg = [[UIImageView alloc] init];
            [self addSubview:cellBkg];
            [cellBkg setClipsToBounds:NO];
        }

            UIImage *placeHolder;
            if ([[[mission created_by] userId] integerValue] == 1 ) {
                placeHolder = [UIImage imageNamed:@"default1.jpg"];
            } else {
                placeHolder = [UIImage imageNamed:@"default3.jpg"];
            }
            [cellBkg setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.width*(placeHolder.size.height/placeHolder.size.width))];
            if ([mission mission_2_image] != nil) {
                [cellBkg setImage:[mission mission_2_image]];
            } else {
                __weak typeof(self) weakSelf = self;
                [cellBkg setImageWithURLRequest:[NSMutableURLRequest requestWithURL:[mission mission_image_2_url]] placeholderImage:placeHolder success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                    __strong typeof(self) strongSelf = weakSelf;
                    
                    if (image != nil) {
                        strongSelf->y = (self.frame.size.width*(image.size.height/image.size.width)-self.frame.size.height)/2;
                        [weakSelf.cellBkg setFrame:CGRectMake(0, -strongSelf->y, weakSelf.frame.size.width, weakSelf.frame.size.width*(image.size.height/image.size.width))];
                        [weakSelf.cellBkg setImage:image];
                    }
                } failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                    
                }];
                /*[cellBkg setImageWithURL:[mission mission_image_2_url] placeholderImage:placeHolder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                    if (image != nil) {
                         y = (self.frame.size.width*(image.size.height/image.size.width)-self.frame.size.height)/2;
                        [cellBkg setFrame:CGRectMake(0, -y, self.frame.size.width, self.frame.size.width*(image.size.height/image.size.width))];
                    }
                }];*/
            [cellBkg setAlpha:0.4];
           
        }
        
        if (checked == nil) {
            checked = [[UIImageView alloc] init];
            [self addSubview:checked];
        }
        if ([[mission submissions_left] integerValue] > 0) {
            [checked setImage:[UIImage imageNamed:@"country_select.png"]];
        } else {
            [checked setImage:[UIImage imageNamed:@"country_select_done.png"]];
        }
        [checked setFrame:CGRectMake(0, 0, checked.image.size.width, checked.image.size.height)];
        [checked setCenter:CGPointMake((self.frame.size.width-15)-checked.image.size.width/2, self.frame.size.height/2)];
        
        if (profilePicture == nil) {
            profilePicture = [[UIImageView alloc] init];
            [self addSubview:profilePicture];
        }
        [profilePicture setFrame:CGRectMake(15, 13, 65, 65)];
        [user userProfilePictureImageView:profilePicture];
        
        if (ends == nil) {
            ends = [[wbLabel alloc] init];
            [self addSubview:ends];
        }
        if ([mission isActive]) {
            [ends setText:[NSString stringWithFormat:@"%@ %@ %@",[mission ends],NSLocalizedString(@"SINGLE_COMMENT_DELIM", @""),[NSString stringWithFormat:NSLocalizedString(@"MISSIONS_VIDEO_COUNT", @""),[[mission number_of_submissions] integerValue]]]];
        } else {
            [ends setText:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:NSLocalizedString(@"MISSIONS_VIDEO_COUNT", @""),[[mission number_of_submissions] integerValue]]]];
        }
        [ends setFont:kFONTHelveticaReg(10)];
        [ends setTextColor:kLIGHTGRAYUICOLOR];
        [ends sizeToFit];
        [ends setFrame:CGRectMake(CGRectGetMaxX(profilePicture.frame)+15, CGRectGetMaxY(profilePicture.frame)-ends.frame.size.height, ends.frame.size.width, ends.frame.size.height)];
        
        if (username == nil) {
            username = [wbButton new];
            //username = [wbButton buttonWithType:UIButtonTypeSystem];
            [self addSubview:username];
        }
        [user userButton:username controller:nil txt:YES];
        [username setTitleEdgeInsets:UIEdgeInsetsMake(-6, 0, 0, 0)];
        [username sizeToFit];
        [username setFrame:CGRectMake(ends.frame.origin.x, CGRectGetMinY(ends.frame)-username.frame.size.height, username.frame.size.width, username.frame.size.height)];
        
        
        if (title == nil) {
            title = [[wbLabel alloc] init];
            [self addSubview:title];
        }
        [title setText:[[mission title] uppercaseString]];
        [title setTextColor:kWHITEUICOLOR];
        [title setFont:kFONT(20)];
        [title setEdgeInsets:UIEdgeInsetsMake(4, 0, 0, 0)];
        [title sizeToFit];
        [title setFrame:CGRectMake(ends.frame.origin.x, CGRectGetMinY(username.frame)-title.frame.size.height, self.frame.size.width-(95+10+checked.frame.size.width), title.frame.size.height)];
        
        //[title setBackgroundColor:kYELLOWUICOLOR];
        
        if (bOrS == nil) {
            bOrS = [[UIImageView alloc] init];
            [self addSubview:bOrS];
        }
        if ([[mission like_count] integerValue] < 0) {
            [bOrS setImage:[UIImage imageNamed:@"shit_inactive"]];
        } else {
            [bOrS setImage:[UIImage imageNamed:@"banana_inactive"]];
        }
        [bOrS setFrame:CGRectMake(CGRectGetMidX(profilePicture.frame)-bOrS.image.size.width+6, CGRectGetMaxY(profilePicture.frame), bOrS.image.size.width, bOrS.image.size.height)];
        
        if (count == nil) {
            count = [[wbLabel alloc] init];
            [self addSubview:count];
        }
        [count setText:[mission like_count_string]];
        [count setFont:kFONTHelvetica(15)];
        [count setTextColor:kLIGHTGRAYUICOLOR];
        [count setEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 0)];
        [count sizeToFit];
        [count setFrame:CGRectMake(CGRectGetMidX(profilePicture.frame), CGRectGetMaxY(profilePicture.frame), count.frame.size.width, count.frame.size.height)];
        
    } else {
        if (ai == nil) {
            ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [ai setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
            [self addSubview:ai];
        }
        
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [ai removeFromSuperview];
            [self setNeedsDisplay];
        });
    }
    
}

-(void)resetBkg
{
    [cellBkg setFrame:CGRectMake(cellBkg.frame.origin.x, -y, cellBkg.frame.size.width, cellBkg.frame.size.height)];
}

-(void)setMission:(wbMission *)_mission
{
    if ([_mission ready]) {
        mission = _mission;
        user = [mission created_by];
        if (cellBkg != nil) {
            [cellBkg setFrame:CGRectMake(cellBkg.frame.origin.x, -y, cellBkg.frame.size.width, cellBkg.frame.size.height)];
        }
        //[[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self setNeedsDisplay];
    } else {
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self setMission:_mission];
        });
    }
}

@end
