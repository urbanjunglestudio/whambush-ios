//
//  wbUpload.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 06/02/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "vzaarAPI.h"
#import "wbUploadProgressView.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface wbUpload : NSObject
{
    wbUploadProgressView *progView;
    vzaarAPI *vAPI;

    NSString *title;
    NSMutableArray *tagStrings;
    BOOL descriptionSet;
    NSString *description;
    BOOL isMission;
    NSDictionary *missionData;
    BOOL partnerVideo;
    
    BOOL cancelled;
    BOOL saved;
    
    UIButton *cancelBtn;
    NSDictionary *info;
}

+(id)sharedUpload;

@property (nonatomic,retain) id controller;
@property (nonatomic,retain) NSDictionary* uploadArray;

-(void)saveVideo:(NSURL*)url;
-(void)cleanTmp;

@end
