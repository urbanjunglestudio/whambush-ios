//
//  wbTvViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/03/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbTvViewController.h"

@interface wbTvViewController ()

@end

@implementation wbTvViewController

@synthesize openChannelWithId;

-(void)loadView
{
    wbTvTableView *tvView = [[wbTvTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    //[videoFeedView setBackButton:backButton];
    [tvView setChannelId:openChannelWithId];
    [self setView:tvView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    wbTvTableView *tvView = (wbTvTableView*)[self view];
    if ([tvView channelId] == 0) {
        [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
    } else {
        [[wbHeader sharedHeader] setShowChangeCountryButton:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
