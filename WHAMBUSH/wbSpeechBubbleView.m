//
//  wbSpeechBubbleView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/10/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbSpeechBubbleView.h"

@implementation wbSpeechBubbleView

@synthesize rightBuble;
@synthesize bubbleAlpha;
@synthesize bubbleColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
       [self setBackgroundColor:kTRANSPARENTUICOLOR];
//         [self setBackgroundColor:kWHITEUICOLOR];
        rightBuble = YES;
        bubbleAlpha = 1.0;
        bubbleColor = kTOPUICOLOR;
    }
    DPRINTCLASS;
    return self;
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIImageView *bubble = [[UIImageView alloc] initWithImage:[self bubble:CGSizeMake(self.bounds.size.width, self.bounds.size.height)]];
    
    if (!rightBuble) {
        [bubble setTransform:CGAffineTransformMakeScale(-1, 1)];
    }
    [bubble setAlpha:bubbleAlpha];
    [self addSubview:bubble];
}


-(UIImage*)bubble:(CGSize)size
{
    // variables to make things happend
    float width = size.width-10;
    float height = size.height;
    float xx = 0;
    float yy = 0;
    float rr = 7;
    
    //start graphic context
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.width,size.height),0.0,0.0);
    
    //this gets the graphic context
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //you can stroke and/or fill
    UIBezierPath *bubble = [UIBezierPath bezierPath];
    
    //draw "bubble"
    [bubble moveToPoint:CGPointMake(xx+rr, yy)];
    [bubble addLineToPoint:CGPointMake(width-rr, yy)];
    [bubble addCurveToPoint:CGPointMake(width, yy+rr) controlPoint1:CGPointMake(width-rr/2,yy) controlPoint2:CGPointMake(width, yy+rr/2)];
    
    [bubble addLineToPoint:CGPointMake(width, height-2*rr)];
    [bubble addLineToPoint:CGPointMake(width+10, height-2*rr)]; //point
    [bubble addLineToPoint:CGPointMake(width, height-rr)];

    [bubble addCurveToPoint:CGPointMake(width-rr, height) controlPoint1:CGPointMake(width,height-rr/2) controlPoint2:CGPointMake(width-rr/2, height)];
    [bubble addLineToPoint:CGPointMake(xx+rr, height)];
    [bubble addCurveToPoint:CGPointMake(xx, height-rr) controlPoint1:CGPointMake(xx+rr/2,height) controlPoint2:CGPointMake(xx, height-rr/2)];
    [bubble addLineToPoint:CGPointMake(xx, yy/2+height/2)];
    [bubble addLineToPoint:CGPointMake(xx, yy+rr)];
    
    [bubble addCurveToPoint:CGPointMake(xx+rr, yy) controlPoint1:CGPointMake(xx,yy+rr/2) controlPoint2:CGPointMake(xx+rr/2, yy)];
    [bubble closePath];
    
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:39.0/255.0 green:41.0/255.0 blue:53.0/255.0 alpha:bubbleAlpha].CGColor);
    [bubble setLineWidth:1.0];
    [bubble stroke];
    
    CGContextSetFillColorWithColor(context, bubbleColor.CGColor);
    [bubble fill];
    
    //create image
    UIImage *bezierImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    bubble = nil;
    context = nil;
    
    return bezierImage;
}
@end
