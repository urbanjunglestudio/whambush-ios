//
//  wbBaseViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 18/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"

@interface wbBaseViewController ()

@end

@implementation wbBaseViewController

//@synthesize transitionAnimate;
//@synthesize viewStackIndex;
@synthesize headerLogo;
@synthesize index;
@synthesize tabTitle;
@synthesize tabLabel;
@synthesize hasBack;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(statusBarChange:)
                                                     name:UIApplicationDidChangeStatusBarFrameNotification
                                                   object:nil];
        
        //transitionAnimate = YES;
        [self setEdgesForExtendedLayout:UIRectEdgeNone];
    }
    DPRINTCLASS;
    return self;
}


- (void)viewDidLoad
{
    //headerLogo = [[wbHeader sharedHeader] logo];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    //[[wbAPI sharedAPI] setCurrentViewController:self];
//    wbHeader *header = [wbHeader myHeader];
//    [header setLogo:headerLogo];
//    [header setHeaderBackButton:[[wbHeader sharedHeader] headerBackButton]];
//    [self.view addSubview:header];
//
}

-(void)viewDidAppear:(BOOL)animated
{
//    if (![headerLogo isKindOfClass:[NSString class]]) {
        DNSLog(@"%@",headerLogo);
//    }
    if (headerLogo == nil) {
        headerLogo = [[wbHeader sharedHeader] logo];
        if (headerLogo == nil) {
            headerLogo = @"_wb_";
        }
        DNSLog(@"-> %@",headerLogo);
    }
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [super viewDidAppear:animated];
    
    if (hasBack) {
        [[wbHeader sharedHeader] setGreen:YES];
        [[wbHeader sharedHeader] setHeaderBackButton:YES];
    }
    

    if ([[wbData sharedData] launchedFromPush]) {
        [kROOTVC performSelectorOnMainThread:@selector(launchFromPush) withObject:nil waitUntilDone:NO];
    }
    
}

//

-(void)setTabTitle:(NSString *)_tabTitle
{
    tabTitle = _tabTitle;
    if (tabLabel == nil) {
        tabLabel = [[wbLabel alloc] init];
    }
    [tabLabel setText:tabTitle];
}

-(void)statusBarChange:(id)sender
{
    //DCMSG(sender);
    
//    [[self.view subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    [self.view setNeedsDisplay];
    [[wbFooter sharedFooter] setNeedsDisplay];
}

-(void)refreshView
{
//    DCMSG([self.view description]);
//    [[self.view subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    [self.view setNeedsDisplay];
}

-(void)scrollTableTop
{
    if ([self.view respondsToSelector:@selector(scrollTableTop)]) {
        [self.view performSelector:@selector(scrollTableTop)];
    }
}


//
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL) shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [[wbData sharedData] clearMemory];
}

@end
