//
//  wbUserData.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 8/26/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "wbDefines.h"
#import "wbButton.h"
#import "wbLabel.h"

@interface wbUser : NSObject
{
    //wbButton *userButton;
    BOOL triedOnce;
    NSData *profilePicData;
    UIImage *profilePicture;
}

@property (nonatomic,retain,strong) NSDictionary *userDetails;

@property (nonatomic,retain,strong) NSString *birthday;
@property (nonatomic,retain,strong) NSString *birthdayString;
@property (nonatomic,retain,readonly,strong) NSString *country;
@property (nonatomic,retain,strong) NSDictionary *countryDictionary;
@property (nonatomic,retain,strong) NSString *activation_state;

@property (nonatomic,retain,readonly,strong) NSString *descriptionTxt;

@property (nonatomic,retain,strong) NSString *email;
@property (nonatomic,retain,readonly,strong) NSNumber *userId;
@property (nonatomic,retain,readonly,strong) NSString *userscore;
@property (nonatomic,retain,readonly,strong) NSNumber *num_dislikes;
@property (nonatomic,retain/*,readonly*/,strong) NSNumber *num_followers;
@property (nonatomic,retain/*,readonly*/,strong) NSNumber *num_followings;
@property (nonatomic,retain,readonly,strong) NSNumber *num_likes;
@property (nonatomic,retain,readonly,strong) NSNumber *num_videos;
@property /*(readonly)*/ BOOL is_following;
@property (nonatomic,retain,strong) NSString *profilePictureURL;
@property (nonatomic,retain,readonly,strong) NSString *userRank;
@property (nonatomic,retain,readonly,strong) NSString *userURL;
@property (nonatomic,retain,strong) NSString *username;
//@property (nonatomic,retain,readonly,strong) UIImage *profilePicture;

//@property (nonatomic,retain,readonly,strong) UIImageView *profilePictureUserView;
//@property (nonatomic,retain,readonly,strong) UIImageView *profilePictureUserViewSmall;
@property (nonatomic,retain,readonly,strong) NSDictionary *userFeed;
@property (nonatomic,retain,readonly,strong) NSDictionary *userMyFeed;
@property (nonatomic,retain,readonly,strong) NSNumber *userType;

@property (nonatomic,retain,readonly) wbLabel *userFollowingLabel;
@property (nonatomic,retain,readonly) wbLabel *userFollowerLabel;

@property (nonatomic,retain,readonly) wbLabel *rankLabel;
@property (nonatomic,retain,readonly) wbLabel *videosLabel;

@property (nonatomic,readonly) BOOL userReady;

+(id)sharedUser;

-(void)getUserData:(NSString*)path;
-(void)updateUserData;
//-(wbButton*)userButton:(id)controller;

-(void)getUserButton:(wbButton*)userButton;
-(void)getUserButton:(wbButton*)userButton fontSize:(NSInteger)fontSize;
-(void)userButton:(wbButton*)userButton controller:(id)controller txt:(BOOL)txt;
-(void)userButton:(wbButton*)userButton controller:(id)controller txt:(BOOL)txtBool fontSize:(NSInteger)fontSize;

-(void)openUserFeed:(id)sender;

-(void)printUser;

-(void)setNewBirthday:(NSString*)_bday;
-(void)setNewDescription:(NSString*)_desc;
-(void)setNewEmail:(NSString*)_email;

-(void)userProfilePictureImageView:(UIImageView*)imageView;
-(void)updateProfilePicture:(UIImage*)image;

-(void)flushUser;


@end
