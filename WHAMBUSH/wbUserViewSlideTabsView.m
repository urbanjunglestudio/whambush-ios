//
//  wbUserViewSlideTabsView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 09/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbUserViewSlideTabsView.h"

@implementation wbUserViewSlideTabsView

@synthesize titleStrings;
@synthesize titleColor;
@synthesize titleFont;
@synthesize currentViewController;
@synthesize defaultTab;
@synthesize views;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //
        tabLabels = [[NSMutableArray alloc] init];
        defaultTab = 0;
        currentTab = 0;
        //[[wbData sharedData] setUserProfileTabs:self];
    }
    return self;
}

#define kHH self.frame.size.height/2
#define kHW self.frame.size.width/2

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    NSInteger count = [views count];

    if (slider == nil) {
        slider = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width, self.frame.size.height)];
        [slider setClipsToBounds:YES];
    }
    
    [tabLabels removeAllObjects];
    
    for (int i = 0; i < count; i++) {
        wbLabel *tab = [(wbBaseViewController*)[views objectAtIndex:i] tabLabel];
        [tab setCenter:CGPointMake(0, 10*kHH)];
        [tab setFont:titleFont];
        [tab setTextColor:kWHITEUICOLOR];
        [tab sizeToFit];
        [tabLabels addObject:tab];
        [slider addSubview:tab];
    }
    
    if (greenLine == nil) {
        greenLine = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height-2, [(wbLabel*)[tabLabels objectAtIndex:defaultTab] frame].size.width, 2)];
    }
    [greenLine setCenter:CGPointMake(self.frame.size.width/2,greenLine.center.y)];
    [greenLine setBackgroundColor:titleColor];
    
    UIImageView *leftGradient = [[UIImageView alloc] initWithImage:[self getGradient]];
    [leftGradient setFrame:CGRectMake(0, 0,self.frame.size.height*2, self.frame.size.height)];
    
    UIImageView *rightGradient = [[UIImageView alloc] initWithImage:[self getGradient]];
    [rightGradient setTransform:CGAffineTransformRotate([rightGradient transform],M_PI)];
    [rightGradient setFrame:CGRectMake(self.frame.size.width-(self.frame.size.height*2), 0,self.frame.size.height*2, self.frame.size.height)];

    [self addSubview:slider];
    [self addSubview:greenLine];
    [self addSubview:leftGradient];
    [self addSubview:rightGradient];
    
    [self moveToTab:defaultTab animate:NO];
    //currentTab = defaultTab;
}


-(void)moveToTab:(NSInteger)index
{
    [self moveToTab:index animate:YES];
}

-(void)moveToTab:(NSInteger)index animate:(BOOL)animate
{
    DNSLog(@">>%ld",(long)index);
    
    NSInteger direction;
    if (index > currentTab) {
        direction = 0;
        DNSLog(@"Right");
    } else {
        direction = 1;
        DNSLog(@"Left");
    }
    
    NSInteger leftIndex;
    NSInteger rightIndex;
    if (index == 0) {
        rightIndex = index + 1;
        leftIndex = [tabLabels count] - 1;
    } else if (index == [tabLabels count] - 1) {
       rightIndex = 0;
       leftIndex  = index - 1;
    } else {
        leftIndex = index - 1;
        rightIndex = index + 1;
    }
    NSInteger leftLeftIndex;
    if (leftIndex == 0) {
        leftLeftIndex = [tabLabels count] - 1;
    } else {
        leftLeftIndex = leftIndex - 1;
    }
    NSInteger rightRightIndex;
    if (rightIndex == [tabLabels count] - 1) {
        rightRightIndex = 0;
    } else {
        rightRightIndex = rightIndex + 1;
    }
    
    DNSLog(@"--> %ld %ld %ld %ld %ld",(long)leftLeftIndex,(long)leftIndex,(long)index,(long)rightIndex,(long)rightRightIndex);
    
    wbLabel *leftLeftTab = [tabLabels objectAtIndex:leftLeftIndex];
    wbLabel *leftTab = [tabLabels objectAtIndex:leftIndex];
    wbLabel *middleTab = [tabLabels objectAtIndex:index];
    wbLabel *rightTab = [tabLabels objectAtIndex:rightIndex];
    wbLabel *rightRightTab = [tabLabels objectAtIndex:rightRightIndex];

    if (leftLeftIndex == rightRightIndex) {
        if (!(leftLeftIndex == leftIndex || rightRightIndex == rightIndex)) {
            if (direction) {
                [leftLeftTab setCenter:CGPointMake(kHW+(-3*kHW), kHH)];
                [leftLeftTab setTextColor:kTRANSPARENTUICOLOR];
            } else {
                [rightRightTab setCenter:CGPointMake(kHW+(3*kHW), kHH)];
                [rightRightTab setTextColor:kTRANSPARENTUICOLOR];
            }
        }
    } else {
        if (leftLeftIndex != leftIndex) {
            [leftLeftTab setCenter:CGPointMake(kHW+(-3*kHW), kHH)];
            [leftLeftTab setTextColor:kTRANSPARENTUICOLOR];
        }
        if (rightRightIndex != rightIndex) {
            [rightRightTab setCenter:CGPointMake(kHW+(3*kHW), kHH)];
            [rightRightTab setTextColor:kTRANSPARENTUICOLOR];
        }
    }
    
    
    // animations settings
    if (animate) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.15];
    }
    
    [leftTab setCenter:CGPointMake(kHW+(-1*kHW), kHH)];
    [leftTab setTextColor:kWHITEUICOLOR];
    [rightTab setCenter:CGPointMake(kHW+(1*kHW), kHH)];
    [rightTab setTextColor:kWHITEUICOLOR];
    [middleTab setCenter:CGPointMake(kHW, kHH)];
    [middleTab setTextColor:titleColor];
    
    [greenLine setFrame:CGRectMake(0, self.frame.size.height-2, [(wbLabel*)[tabLabels objectAtIndex:index] frame].size.width, 2)];
    [greenLine setCenter:CGPointMake(self.frame.size.width/2,greenLine.center.y)];
    
    
    if (animate) {
        [UIView commitAnimations];
    }

    currentTab = index;
    defaultTab = currentTab;
    
}



-(UIImage*)getGradient
{
    CGFloat r0,g0,b0,a0;
    [[self backgroundColor] getRed:&r0 green:&g0 blue:&b0 alpha:&a0];
    CGSize size = CGSizeMake(self.frame.size.height, self.frame.size.height);
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
    size_t gradientNumberOfLocations = 2;
    CGFloat gradientLocations[2] = { 0.0, 1.0 };
    CGFloat gradientComponents[8] = { r0, g0, b0, 1, r0, g0, b0, 0, };
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents (colorspace, gradientComponents, gradientLocations, gradientNumberOfLocations);
    
    CGContextDrawLinearGradient(context, gradient, CGPointMake(0, 0), CGPointMake(size.width,0), 0);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorspace);
    UIGraphicsEndImageContext();

    return image;
}

@end
