//
//  wbSingleMissionView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseTableView.h"
#import "wbVideoPlayerViewController.h"
#import "wbVideoCell.h"

@interface wbSingleMissionView : wbBaseTableView <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate,UIAlertViewDelegate,UIWebViewDelegate>
{
    UIRefreshControl *refreshController;
    UIImage *refreshBkgImage;
    BOOL refreshing;
    NSURL *videoUrl;
    NSInteger number_of_videos;
    wbLabel *timerLabel;
    wbButton *endTime;

    UIView* videoRowView;
    //wbVideoPlayerViewController *videoPlayerController;
    UIImageView *noVideoImageView;
    
    BOOL togleEndTime;
    UIImage *likeBtnImg;
    UIImage *dislikeBtnImg;
    wbButton *shareBtn;
    UIButton *likeBtn;
    UIButton *dislikeBtn;
    UIImageView *likeBubble;
    UIView *actionRowView;
    wbLabel *likeDislikeCount;
    UIActivityIndicatorView *ai;
    UIActivityViewController *activityViewController;
    
    wbButton *cameraBtn;
    
    UIView *whoRowView;
    
    UIImageView *userPicture;
    wbLabel *title;
    wbButton *username;
    
    UITextView *description;
    
    UIView *infoRowView;
    wbLabel *videoLabel;
    wbButton *rulesButton;
    
    NSMutableArray *missionVideos;
    NSMutableDictionary *missionVideoCells;
    NSURL *next;
    
    UIWebView *wv;
    BOOL allowLoad;
    UIActivityIndicatorView *aiweb;
    NSURL *rulesLinkUrl;

}
@property id controller;
@property (retain,nonatomic) wbMission *mission;
@property (retain,nonatomic) NSTimer *missionTimer;
@property BOOL hideByShare;
@property (nonatomic,strong) wbVideoPlayerViewController *videoPlayerController;

-(void)closeWebView;

@end
