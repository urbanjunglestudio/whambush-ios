//
//  wbUploadProgressView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 23.05.14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbLabel.h"

@interface wbUploadProgressView : UIView
{
    UIButton *cancelButton;
    UIImageView *rotatingCircle;
    UIProgressView *progressBar;
}


@property (nonatomic) float progress;
@property (nonatomic,retain) id delegate;

-(void)hide;

@end
