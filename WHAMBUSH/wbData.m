    //
//  wbData.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/11/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbData.h"
#import <sys/utsname.h>

@implementation wbData

@synthesize singleVideoData;
//@synthesize hasMissions;
//@synthesize hasUserpage;
@synthesize launchedFromPush;
@synthesize pushData;
@synthesize thumbSize;

@synthesize feeds;
@synthesize missionFeeds;

@synthesize num_of_unreadActivities;

@synthesize supportMessage;
@synthesize activityString;
@synthesize activityLabel;

@synthesize rankNumber;

//@synthesize videoCells;

@synthesize rootTVChannel;
@synthesize selectedMissionCountry;

@synthesize launchWithMission;

-(id) init
{
    self = [super init];
//    hasMissions = NO;
//    hasUserpage = NO;
    launchedFromPush = NO;
//    fetching = [[NSMutableDictionary alloc] init];
//    videoCells = [[NSMutableDictionary alloc] init];
    rankNumber = 0;
    numberOfCountries = 1000;
    DPRINTCLASS;
    return self;
}

+ (id)sharedData
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(NSString*)description
{
    return [super description];
}


-(void)setNum_of_unreadActivities:(NSNumber*)_num_of_unreadActivities
{
    num_of_unreadActivities = _num_of_unreadActivities;
    if (activityLabel == nil) {
        activityLabel = [[wbLabel alloc] init];
    }
    activityString = [NSString stringWithFormat:NSLocalizedString(@"USER_ACTIVITY",@""),[NSString stringWithFormat:NSLocalizedString(@"USER_COUNT",@""),[num_of_unreadActivities integerValue]]];
    [activityLabel setText:activityString];
    [activityLabel sizeToFit];
}

///////////////////////////////

-(void)getTvChannels
{
    [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:kCHANNELSAPI]
                             response:^(id resultArray){
                                 [self saveTvChannels:[resultArray objectForKey:@"results"] firstTime:YES];
                                 DCMSG(resultArray);
                             }];
    
}

-(void)saveTvChannels:(NSArray*)channels firstTime:(BOOL)first
{
    if ([[wbUser sharedUser] userReady] || [[wbAPI sharedAPI] is_guest]) {
        if (tvChannels == nil) {
            tvChannels = [[NSMutableDictionary alloc] init];
        }
        if (tvChannelsData == nil) {
            tvChannelsData = [[NSMutableDictionary alloc] init];
            [tvChannelsData setObject:channels forKey:@0];
            rootTVChannel = 0;
            [kROOTVC performSelector:@selector(setDefaultTvChannel:) withObject:[NSNumber numberWithInteger:rootTVChannel]];
        }
        for (int i = 0; i < [channels count]; i++) {
            NSMutableDictionary *channel = [[NSMutableDictionary alloc] initWithDictionary:[channels objectAtIndex:i]];
            if (first) {
                [channel setObject:@NO forKey:@"has_parent"];
            } else {
                [channel setObject:@YES forKey:@"has_parent"];
            }
            if (/*![[channel objectForKey:@"channel_type"] boolValue]*/first) {
                if ([[channel objectForKey:@"country"] isEqualToString:[[[wbAPI sharedAPI] currentCountry] objectForKey:@"country"]] ||
                    (![[[[wbAPI sharedAPI] currentCountry] valueForKey:@"supported"] boolValue] && [[channel objectForKey:@"country"] isEqualToString:@"ZZ"])) {
                    rootTVChannel = [[channel objectForKey:@"id"] integerValue];
                    [kROOTVC performSelector:@selector(setDefaultTvChannel:) withObject:[NSNumber numberWithInteger:rootTVChannel]];
                }
            }
            [tvChannelsData setObject:channel forKey:[channel objectForKey:@"id"]];
            if ([[channel objectForKey:@"children"] count] > 0) {
                [self saveTvChannels:[channel objectForKey:@"children"] firstTime:NO];
            }
        }
        //NSLog(@"%@\n%ld",tvChannelsData,(long)rootTVChannel);
    } else {
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self saveTvChannels:channels firstTime:first];
        });
    }
}

-(void)getTvChannelWithId:(NSInteger)channelId delegate:(id)delegate
{
    if (tvChannelsData != nil) {
        //DCMSG(tvChannelsData);
        [delegate performSelector:@selector(gotTvChannel:) withObject:[tvChannelsData objectForKey:[NSNumber numberWithInteger:channelId]]];
    } else {
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self getTvChannelWithId:channelId delegate:delegate];
        });
    }
}

///////////////////////////////

-(void)getAllCountries:(NSString*)next
{
    if (allCountriesData == nil || next != nil) {
        NSURL *endpoint;
        if (next == nil) {
            endpoint = [[wbAPI sharedAPI] urlWithEndpoint:kCOUNTRIESAPI];
        } else {
            endpoint = [NSURL URLWithString:next];
        }
        [[wbAPI sharedAPI] getJSONWithURL:endpoint
                                 response:^(id resultArray){
                                     if (allCountriesData == nil) {
                                         allCountriesData = [[NSMutableArray alloc] init];
                                     }
                                     numberOfCountries = [[resultArray valueForKey:@"count"] integerValue];
                                     [allCountriesData addObjectsFromArray:[resultArray objectForKey:@"results"]];
                                     if ([[resultArray objectForKey:@"next"] isKindOfClass:[NSString class]]) {
                                         [self getAllCountries:[resultArray objectForKey:@"next"]];
                                     }
                                 }];
    }
}

-(void)getAllCountries
{
    if (allCountriesData == nil) {
        [self getAllCountries:nil];
    }
}

-(void)getCountriesData:(id)delegate
{
    if (allCountriesData != nil && [allCountriesData count] >= numberOfCountries) {
        [delegate performSelector:@selector(gotCountries:) withObject:allCountriesData];
    } else {
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self getCountriesData:delegate];
        });
    }
    
}

///////////////////////////////
-(void)getAllMissionFeeds
{
    if ([[wbUser sharedUser] userReady] || [[wbAPI sharedAPI] is_guest]) {
        if ([[[[wbAPI sharedAPI] currentCountry] valueForKey:@"supported"] boolValue]) {
            [self getAllCountryMissionFeeds:[[[wbAPI sharedAPI] currentCountry] objectForKey:@"country"]];
        } else {
            [self getAllCountryMissionFeeds:@"ZZ"];
        }
    } else {
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self getAllMissionFeeds];
        });
    }
}

-(void)getAllCountryMissionFeeds:(NSString*)country
{
    if (missionsData == nil) {
        missionsData = [[NSMutableDictionary alloc] init];
    } else {
        [missionsData removeAllObjects];
    }
    
    
    selectedMissionCountry = country;
    
    NSString *missionCountry;
    missionCountry = [NSString stringWithFormat:@"%@,ZZ",country];

    NSMutableDictionary *active = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                   @"MISSIONS_ACTIVE_FEED",@"name",
                                   @"1",@"id",
                                   @"" ,@"icon_url",
                                   [NSString stringWithFormat:@"%@?country=%@",kACTIVEMISSIONAPI,missionCountry],@"endpoint",
                                   @"star.png",@"icon",
                                   @"missions",@"type",
                                   @"1",@"default",
                                   nil];
    NSMutableDictionary *archive = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                    @"MISSIONS_ARCHIVE_FEED",@"name",
                                    @"2",@"id",
                                    @"" ,@"icon_url",
                                    [NSString stringWithFormat:@"%@&country=%@",kOLDMISSIONAPI,missionCountry],@"endpoint",
                                    @"latest.png",@"icon",
                                    @"missions",@"type",
                                    @"0",@"default",
                                    nil];
    NSDictionary *defaultM  = [[NSDictionary alloc] initWithDictionary:active];
    if (missionFeeds == nil) {
        missionFeeds = [[NSMutableDictionary alloc] init ];
    } else {
        [missionFeeds removeAllObjects];
    }
    [missionFeeds setObject:defaultM forKey:@"default"];
    [missionFeeds setObject:archive forKey:@"MISSIONS_ARCHIVE_FEED"];
    [missionFeeds setObject:active forKey:@"MISSIONS_ACTIVE_FEED"];
                    //WithObjectsAndKeys:defaultM,@"default",active,@"MISSIONS_ACTIVE_FEED",archive,@"MISSIONS_ARCHIVE_FEED",nil];
    
    DCMSG(missionFeeds);
    
    [self getAllMissions];
}


-(void)getAllMissions
{
    //missionsData = [[NSMutableDictionary alloc] init];
    NSAssert([missionFeeds objectForKey:@"default"] != nil, @"default missing");
    
    for (NSString* key in missionFeeds) {
        [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:[[missionFeeds objectForKey:key] objectForKey:@"endpoint"]]
                                 response:^(id resultArray){
                                     //NSAssert([missionFeeds objectForKey:@"default"] != nil, @"default missing");
                                     if (![key isEqualToString:@"default"]) {
                                         [missionsData setObject:[NSMutableDictionary dictionaryWithDictionary:resultArray] forKey:[[missionFeeds objectForKey:key] objectForKey:@"name"]];
                                         for (NSDictionary *dict in [resultArray objectForKey:@"results"]) {
                                             [self saveMission:dict];
                                         }
                                     }
                                 }];
    }
}

-(void)saveMission:(NSDictionary*)data
{
    if (whambushMissions == nil) {
        whambushMissions = [[NSMutableDictionary alloc] init];
    }
    
    [self saveWhambushUser:[data objectForKey:@"added_by"]];
    if ([whambushMissions objectForKey:[NSNumber numberWithInteger:[[data objectForKey:@"id"] integerValue]]] == nil) {
        wbMission *mission = [[wbMission alloc] init];
        [mission setMissionWithDictionary:data];
        [whambushMissions setObject:mission forKey:[NSNumber numberWithInteger:[[data objectForKey:@"id"] integerValue]]];
    } else {
        [(wbMission*)[whambushMissions objectForKey:[NSNumber numberWithInteger:[[data objectForKey:@"id"] integerValue]]] setMissionWithDictionary:data];
    }
}

-(wbMission*)getMissionWithId:(NSNumber*)missionid
{
    if (whambushMissions == nil) {
        whambushMissions = [[NSMutableDictionary alloc] init];
    }
    if ([whambushMissions objectForKey:missionid] == nil) {
        wbMission *mission = [[wbMission alloc] init];
        [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:[NSString stringWithFormat:@"%@%@/",kACTIVEMISSIONAPI,missionid]]
                                 response:^(id data){
                                     [mission setMissionWithDictionary:data];
                                     [whambushMissions setObject:mission forKey:[NSNumber numberWithInteger:[[data objectForKey:@"id"] integerValue]]];
                                 }];
        return mission;
    } else {
        return [whambushMissions objectForKey:missionid];
    }
}

-(void)refreshMissionData:(NSString*)name delegate:(id)delegate
{
    if ([name isEqualToString:@"default"]) {
        if ([missionFeeds objectForKey:@"default"]) {
            name = [[missionFeeds objectForKey:@"default"] objectForKey:@"name"];
        }
    } 
    
    NSString *endpoint = [[missionFeeds objectForKey:name] objectForKey:@"endpoint"];
    
    //NSAssert(endpoint != nil, @"Endpoint nil");
    if (endpoint != nil) {
        [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:endpoint]
                                 response:^(id resultArray){
                                     [missionsData setObject:resultArray forKey:name];
                                     for (NSDictionary *dict in [resultArray objectForKey:@"results"]) {
                                         [self saveMission:dict];
                                     }
                                     [self getMissionsData:name delegate:delegate];
                                 }];
    } else {
        [self getMissionsData:name delegate:delegate];
    }

}


-(void)getMissionsData:(NSString*)name delegate:(id)delegate
{
    if ([name isEqualToString:@"default"] || name == nil) {
        if ([missionFeeds objectForKey:@"default"]) {
            name = [[missionFeeds objectForKey:@"default"] objectForKey:@"name"];
        } else {
            name = @"MISSIONS_ACTIVE_FEED";
        }
    }
    
    if ([missionsData objectForKey:name]) {
        if ([delegate respondsToSelector:@selector(setCurrentFeed:)]) {
            [delegate performSelector:@selector(setCurrentFeed:) withObject:[missionFeeds objectForKey:name]];
        }
        [delegate performSelector:@selector(dataReady:) withObject:[missionsData objectForKey:name]];
    } else {
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self getMissionsData:name delegate:delegate];
        });
    }
    
}

///////////////////////////////

///////////////////
//
//-(void)getRanking
//{
//    NSString *urlString = @"http://178.62.88.135/1/code/rank";
//
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions: NSJSONReadingMutableContainers];
//    
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
//    
//    [manager.requestSerializer setValue:@"178.62.88.135" forHTTPHeaderField:@"X-Coronium-APP-ID"];
//    [manager.requestSerializer setValue:@"4a1739e8-6aac-47ea-86e2-1c1c22c3e4c2" forHTTPHeaderField:@"X-Coronium-API-KEY"];
//    
//    NSDictionary *parameters = [NSDictionary dictionaryWithObject:[[wbUser sharedUser] username] forKey:@"username"];
//    
//    [manager POST:urlString parameters:parameters
//         success:^(AFHTTPRequestOperation *operation, id responseObject) {
//             DCMSG(responseObject);
//         }
//         failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//             DCMSG(error);
//         }];
//    
//}
//
///////////////////

-(void)getAllFeeds
{
    [allFeedData removeAllObjects];
    [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:kFEEDSAPI]
                             response:^(id resultArray){
                                 //DCMSG(resultArray);
                                 
                                 NSArray *rawFeeds = [resultArray objectForKey:@"results"];
                                 NSInteger count = [[resultArray valueForKey:@"count"] integerValue];
                                 NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
                                 for (int i = 0; i <  count; i++) {
                                     NSMutableDictionary *content = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                                                     [[rawFeeds objectAtIndex:i] objectForKey:@"name"],@"name",
                                                                     [[rawFeeds objectAtIndex:i] objectForKey:@"id"],@"id",
                                                                     [[rawFeeds objectAtIndex:i] objectForKey:@"icon_url"],@"icon_url",
                                                                      [[[rawFeeds objectAtIndex:i] objectForKey:@"endpoint_url"]
                                                                      stringByTrimmingCharactersInSet:[NSCharacterSet  whitespaceAndNewlineCharacterSet]] ,@"endpoint",
                                                                     [self parseIcon:[[[rawFeeds objectAtIndex:i] objectForKey:@"default_icon"]integerValue]],@"icon",
                                                                     @"normal",@"type",
                                                                     nil];
                                     if ([[resultArray objectForKey:@"default"] integerValue] == [[[rawFeeds objectAtIndex:i] objectForKey:@"id"] integerValue]) {
                                         [content setObject:@"1" forKey:@"default"];
                                         [data setObject:content forKey:@"default"];
  
                                     } else {
                                         [content setObject:@"0" forKey:@"default"];
                                     }
                                     [data setObject:content forKey:[content objectForKey:@"name"]];
                                 }
                                 [self setFeeds:data];
                             }];
}


-(void)setFeeds:(NSMutableDictionary*)_feeds
{
    feeds = _feeds;
    DCMSG(feeds);
    if (allFeedData == nil) {
        allFeedData = [[NSMutableDictionary alloc] init];
    }
    for (NSString* key in feeds) {
        if (![key isEqualToString:@"default"]) {
            if ([[wbAPI sharedAPI] checkIfNetworkAvailable]) {
                [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:[[feeds objectForKey:key] objectForKey:@"endpoint"]]
                                     response:^(id resultArray){
                                         [allFeedData setObject:resultArray forKey:key];
                                          //DCMSG(allFeedData);
                                     }];
            }
        }
    }
}

//-(NSMutableDictionary*)videoResultArray:(NSMutableDictionary*)resultArray
//{
//    NSMutableDictionary *tmpDictionary = [[NSMutableDictionary alloc] initWithDictionary:resultArray];
//    //[tmpDictionary removeObjectForKey:@"results"];
//    NSArray *data = [resultArray objectForKey:@"results"];
//    NSInteger count = [[resultArray objectForKey:@"results"] count];
//    NSMutableArray *tmpArray = [[NSMutableArray alloc] init];
//    for (int i = 0; i < count; i++) {
//        [tmpArray addObject:[wbVideo initVideoObjectWithData:[data objectAtIndex:i]]];
//    }
//    [tmpDictionary setObject:tmpArray forKey:@"videos"];
//    
//    return tmpDictionary;
//}

-(void)refreshFeedData:(NSString*)name delegate:(id)delegate
{
    NSString *endpoint = [[feeds objectForKey:name] objectForKey:@"endpoint"];
    [allFeedData removeObjectForKey:name];
    
    [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:endpoint]
                             response:^(id resultArray){
                                 [allFeedData setObject:resultArray forKey:name];
                                 if (delegate != nil) {
                                     [self getFeedData:name delegate:delegate];
                                 }
                             }];
    
}

-(void)getFeedData:(NSString*)name delegate:(id)delegate
{
    if ([name isEqualToString:@"default"]) {
        if ([feeds objectForKey:@"default"]) {
            name = [[feeds objectForKey:@"default"] objectForKey:@"name"];            
        }
    }
    
    if ([allFeedData objectForKey:name]) {
        [delegate performSelector:@selector(setCurrentFeed:) withObject:[feeds objectForKey:name]];
        [delegate performSelector:@selector(dataReady:) withObject:[allFeedData objectForKey:name]];
    } else {
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //code to be executed on the main queue after delay
            [self getFeedData:name delegate:delegate];
        });
    }
}

///////////////////////////////


-(void)getAllActivity
{
    [self refreshActivityData:nil];
}

-(void)refreshActivityData:(id)delegate
{
    [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:kACTIVITYAPI]
                             response:^(id resultArray){
                                 activityData = [NSMutableDictionary dictionaryWithDictionary:resultArray];
                                 //DCMSG(activityData);
                                 if (delegate != nil) {
                                     [self getActivityData:delegate];
                                 }
                             }];
}

-(void)getActivityData:(id)delegate
{
    if (activityData != nil) {
        [delegate performSelector:@selector(dataReady:) withObject:activityData];
    } else {
        [self performSelector:@selector(getActivityData:) withObject:delegate afterDelay:1];
    }
}

///////////////////////////////


-(NSString*)parseIcon:(NSInteger)number
{
    NSString *iconName;
    switch (number) {
        case 1:
            iconName = @"following.png";
            break;
        case 2:
            iconName = @"popular.png";
            break;
        case 3:
            iconName = @"latest.png";
            break;
        case 4:
            iconName = @"myvideos.png";
            break;
        case 5:
            iconName = @"star.png";
            break;
        case 6:
            iconName = @"eye.png";
            break;
        case 7:
            iconName = @"banana_active.png";
            break;
        case 8:
            iconName = @"shit_active.png";
            break;
        case 9:
            iconName = @"wait.png";
            break;
        case 10:
            iconName = @"random.png";
            break;
        case 11:
            iconName = @"tv_feed.png";
            break;
            
        default:
            iconName = @"default_icon.png";
            break;
    }
    return iconName;
}

///////////////////////////////


-(void)saveWhambushUser:(NSDictionary*)userArray
{
    if (whambushUsers == nil) {
        whambushUsers = [[NSMutableDictionary alloc] init];
        if (![[wbAPI sharedAPI] is_guest]) {
            [whambushUsers setObject:[wbUser sharedUser] forKey:[[wbUser sharedUser] userURL]];
        }
    }
    
    if ([whambushUsers objectForKey:[userArray objectForKey:@"url"]] == nil)
    {
        wbUser *user = [[wbUser alloc] init];
        //[user setUserDetails:userArray];
        [user performSelectorOnMainThread:@selector(setUserDetails:) withObject:userArray waitUntilDone:YES];
        [whambushUsers setObject:user forKey:[NSString stringWithString:[userArray objectForKey:@"url"]]];
    }
}

-(wbUser*)getWhambushUser:(NSString*)path
{
    //DCMSG(whambushUsers);
    if (whambushUsers == nil) {
        whambushUsers = [[NSMutableDictionary alloc] init];
        if (![[wbAPI sharedAPI] is_guest]) {
            [whambushUsers setObject:[wbUser sharedUser] forKey:[[wbUser sharedUser] userURL]];
        }
    }
    
    if ([whambushUsers objectForKey:path] == nil)
    {
        wbUser *user = [[wbUser alloc] init];
        [user getUserData:path];
        [whambushUsers setObject:user forKey:[NSString stringWithString:path]];
        return user;
    } else {
        return [whambushUsers objectForKey:path];
    }
}

-(wbUser*)getWhambushUserWithId:(NSInteger)userid
{
    NSString *path = [NSString stringWithFormat:@"%@/users/%ld/",kWHAMBUSHAPIURL,(long)userid];
    return [self getWhambushUser:path];
}

-(wbUser*)getWhambushUserWithUsername:(NSString*)username
{
    wbUser *user = [[wbUser alloc] init];
    [[wbAPI sharedAPI] getJSONWithURL:[[wbAPI sharedAPI] urlWithEndpoint:[NSString stringWithFormat:kUSERSEARCHAPI,username]] response:^(id data){
        [user setUserDetails:[[data objectForKey:@"results"] firstObject]];
        [self saveWhambushUser:[[data objectForKey:@"results"] firstObject]];
    }];

    return user;
    //NSString *path = [NSString stringWithFormat:@"%@/users/%ld/",kWHAMBUSHAPIURL,(long)userid];
    //return [self getWhambushUser:path];
}

//-(void)saveVideo:(NSDictionary*)data
//{
//    if (allVideoData == nil) {
//        allVideoData = [[NSMutableDictionary alloc] init];
//    }
//    if ([allVideoData objectForKey:[data objectForKey:@"id"]] == nil) {
//        [allVideoData setObject:[wbVideo initVideoObjectWithData:data] forKey:[data objectForKey:@"id"]];
//    }
//}
//
//-(wbVideo*)getVideoWithId:(NSNumber*)videoid
//{
//    if (allVideoData == nil) {
//        allVideoData = [[NSMutableDictionary alloc] init];
//    }
//    if ([allVideoData objectForKey:@"vidoid"] == nil) {
//        [allVideoData setObject:[wbVideo initVideoObjectWithId:[videoid integerValue]] forKey:videoid];
//    }
//    return [allVideoData objectForKey:videoid];
//}
//


//////////////

-(void)clearUserData
{
    [whambushUsers removeAllObjects];
    whambushUsers= nil;
}


-(void)clearData
{
    
/*    [whambushUsers removeAllObjects];
    whambushUsers = nil;
    
    [singleVideoData removeAllObjects];
    singleVideoData = nil;
    
    [activityData removeAllObjects];
    activityData = nil;
    
    [missionsData removeAllObjects];
    missionsData = nil;
    
    [allFeedData removeAllObjects];
    allFeedData = nil;
    
//    [fetching removeAllObjects];
//    fetching = nil;
    
    [feeds removeAllObjects];
    feeds = nil;
    
    [missionFeeds removeAllObjects];
    missionFeeds = nil;
    
//    hasMissions = NO;
//    hasUserpage = NO;
    supportMessage = @"";
 */
}

-(void)clearMemory
{
   /* if (whambushUsers != nil) {
        DCMSG(@"clearMemory - users");
        whambushUsers = nil;
    }*/
}

-(NSDictionary*)tvFeed
{
    NSMutableDictionary *feed = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                 [NSString stringWithFormat:@"%@?type=tv",kVIDEOAPI],@"endpoint",
                                 [NSNumber numberWithInteger:18],@"id",
                                 [NSNumber numberWithInteger:0],@"default",
                                 @"WhambushTV",@"name",
                                 @"tv_feed.png",@"icon",
                                 @"TV",@"type",
                                 nil];
        
    return feed;
}

//-(NSString*)deviceType
//{
//    struct utsname systemInfo;
//    uname(&systemInfo);
//    
//    //NSString *version = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
//    //NSString *devicesystemv = [UIDevice currentDevice].systemVersion;
//
//    return [NSString stringWithFormat:@"%@ %@",[NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding],[UIDevice currentDevice].systemVersion];
//}

-(void)createSupportMessage:(NSDictionary*)loginInfo
{
    NSString *tmp = [NSString stringWithFormat:@"\n\n-----info to support don't remove-----\n %@\n %@\n %@\n %@\n %@\n %@\n-----------------",
                     [loginInfo objectForKey:@"username"],
                     [loginInfo objectForKey:@"manufacturer"],
                     [loginInfo objectForKey:@"os"],
                     [loginInfo objectForKey:@"os_version"],
                     [loginInfo objectForKey:@"device"],
                     [loginInfo objectForKey:@"whambush_version"]];
    supportMessage = tmp;
}
//username,@"username",
//password,@"password",
//@"Apple",@"manufacturer",
//@"iOS",@"os",
//[UIDevice currentDevice].systemVersion,@"os_version",
//[NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding],@"device",
//[[NSUserDefaults standardUserDefaults] objectForKey:@"version"],@"whambush_version",
//guest_id,@"guest_id",


@end
