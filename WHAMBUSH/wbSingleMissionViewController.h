//
//  wbSingleMissionViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"
#import "wbMission.h"
#import "wbSingleMissionView.h"

@interface wbSingleMissionViewController : wbBaseViewController
{
    wbVideoPlayerViewController *videoPlayerController;
    wbSingleMissionView *missionView;
}

@property (nonatomic,retain) wbMission *mission;
@end
