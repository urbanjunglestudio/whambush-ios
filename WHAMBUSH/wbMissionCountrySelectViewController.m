//
//  wbMissionCountrySelectViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 14/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbMissionCountrySelectViewController.h"

@implementation wbMissionCountrySelectViewController

@synthesize missionView;

-(void)loadView
{
    wbMissionCountrySelectTableView *missionCSView = [[wbMissionCountrySelectTableView alloc] initWithFrame:[[wbAPI sharedAPI] contentRect]];
    [missionCSView setMissionView:missionView];
    [self setView:missionCSView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    super.headerLogo = @"_mission_";
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[wbHeader sharedHeader] setHeaderBackButton:NO];
    [[wbHeader sharedHeader] setShowChangeCountryButton:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
