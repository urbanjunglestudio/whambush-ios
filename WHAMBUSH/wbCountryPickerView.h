//
//  wbCountryPickerView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 01/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbCountryPickerView : UIView <UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSUInteger numberOfRows;
    UIToolbar *hideView;
    UIButton *cancelBtn;
    UIView *theBox;
    UIButton *sendBtn;
    NSDictionary *country;
    BOOL picked;
    NSMutableArray *allCountries;
    NSMutableArray *supportedCountries;
    NSMutableArray *otherCountries;
    NSInteger supportedIndex;
    NSInteger selectedRowNumber;
}

@property id delegate;
@property (retain,nonatomic) NSDictionary *selectedCountry;
-(void)show;
-(void)hide;

@end
