//
//  wbWaitView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 28/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbWaitView : UIView
{
    UIActivityIndicatorView *ai;
}

+ (id)sharedWait;

-(void)showLandscape;

-(void)show;
-(void)hide;

@end
