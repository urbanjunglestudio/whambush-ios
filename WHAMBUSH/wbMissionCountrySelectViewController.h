//
//  wbMissionCountrySelectViewController.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 14/04/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbBaseViewController.h"
#import "wbMissionCountrySelectTableView.h"

@interface wbMissionCountrySelectViewController : wbBaseViewController

@property id missionView;

@end
