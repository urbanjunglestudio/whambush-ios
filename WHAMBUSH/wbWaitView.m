//
//  wbWaitView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 28/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbWaitView.h"

@implementation wbWaitView

- (id)init
{
    CGRect frame = [[wbAPI sharedAPI] portraitFrame];
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.35]];
        [self setHidden:YES];
    }
    return self;
}

+ (id)sharedWait
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(void)show
{
    CGRect newFrame = [[wbAPI sharedAPI] portraitFrame];
    newFrame.size.height = newFrame.size.height - [[wbAPI sharedAPI] footerBarHeight];
    [self setFrame:newFrame];
    [self showWait];
}

-(void)showLandscape
{
    [self setFrame:[[wbAPI sharedAPI] landscapeFrame]];
    [self showWait];
}

-(void)showWait
{
//    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait) {
//        [self setFrame:[[wbAPI sharedAPI] portraitFrame]];
//    } else {
//        [self setFrame:[[wbAPI sharedAPI] landscapeFrame]];
//    }
    DNSLog(@"show wait");
    [self setHidden:NO];
    [self setNeedsDisplay];
    //DCMSG([[[[UIApplication sharedApplication] delegate] window] subviews]);
    [[[[UIApplication sharedApplication] delegate] window] addSubview:self];
    [[[[UIApplication sharedApplication] delegate] window] bringSubviewToFront:self];
}

-(void)hide
{
    //DNSLog(@"hide wait");
    [self setHidden:YES];
    //[self removeFromSuperview];
}


-(NSString*)description
{
    return [super description];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // Drawing code
    UIImage *gorillaShapeImg = [UIImage ch_imageNamed:@"gorilla_shape.png"];
    UIImageView *gorillaShape = [[UIImageView alloc] initWithImage:gorillaShapeImg];
    [gorillaShape setCenter:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))];
    [gorillaShape setAlpha:0.75];
    [self addSubview:gorillaShape];
    
    if (ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [ai setHidden:NO];
    }
    [ai setCenter:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))];
    [ai startAnimating];
    [self addSubview:ai];
}



@end
