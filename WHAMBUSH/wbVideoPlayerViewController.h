//
//  wbVideoPlayerViewController
//
//  Created by Jari Kalinainen on 24/03/14.
//  Copyright (c) 2014 TofuHead. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "wbVideoPlayerView.h"

@interface wbVideoPlayerViewController : UIViewController
{
    BOOL playing;
    wbVideoPlayerView *videoPlayerView;
    NSTimer *progressTimer;
    NSTimer *bufferTimer;
    UIActivityIndicatorView *waitView;
    MPMoviePlayerController *movieController;
    float progress;
    BOOL ready;
    CGRect oldFrame;
    UIImageView *thumb;
}

@property (nonatomic,retain) NSURL *videoURL;
@property (nonatomic,retain) NSURL *videoThumbURL;
@property (nonatomic) CGRect viewRect;
@property (nonatomic,retain) id delegate;

-(void)clean;

@end
