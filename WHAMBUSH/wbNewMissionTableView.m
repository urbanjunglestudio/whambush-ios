//
//  wbNewMissionTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 13/10/15.
//  Copyright © 2015 Jari Kalinainen. All rights reserved.
//

#import "wbNewMissionTableView.h"

@implementation wbNewMissionTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        [self setClipsToBounds:YES];
        [self setHidden:NO];
        prevY = 0;
    }
    DPRINTCLASS;
    return self;
}

-(void)setGAIViewName
{
    [[wbAPI sharedAPI] registerGoogleAnalytics:[[self currentFeed] objectForKey:@"type"]];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    if (refreshController == nil) {
        refreshController = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [refreshController addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[UIImage ch_imageNamed:@"refresh_bg_image"]]];
        [self addSubview:refreshController];
    }
    if (dataArray == nil) {
        [[wbData sharedData] getMissionsData:@"default" delegate:self];
    }
}

-(void)forceRefresh
{
    dataArray = nil;
    resultArray = nil;
    numberOfResults = 0;
    refresh = NO;
    super.hideHeader = NO;
    [feedSelect removeFromSuperview];
    feedSelect = nil;
    //[self reloadData];
    [self setNeedsDisplay];
    kSHOWWAIT;
}

-(void)executeRefresh
{
    deltaY = 0;
    NSLog(@"%@",self.currentFeed);
    if ([[[self currentFeed] objectForKey:@"type"] isEqualToString:@"missions"]) {
        [[wbData sharedData] refreshMissionData:[[self currentFeed] objectForKey:@"name"] delegate:self];
    } else {
        [self setCurrentFeed:[self currentFeed]];
        [[wbAPI sharedAPI] getDataWithEndpoint:[[self currentFeed] objectForKey:@"endpoint"] delegate:self];
    }
}

-(void)resultsReady
{
    if ([self numberOfRowsInSection:0] > 0) {
        for (NSDictionary *dict in [dataArray objectForKey:@"results"]) {
            [[wbData sharedData] saveMission:dict];
        }
    }
    [super setAllFeeds:[[wbData sharedData] missionFeeds]];
    [super setShowFeedSelectSearch:NO];
    
    //NSMutableArray *newRows = [[NSMutableArray alloc] init];
    
    //numberOfResults = 20;
    [self reloadData];
    [self beginUpdates];
    NSLog(@"%ld",(long)[self numberOfRowsInSection:0]);
    for (long i = [self numberOfRowsInSection:0]; i < numberOfResults; i++) {
        NSIndexPath *newReloadRows = [NSIndexPath indexPathForRow:i inSection:0];
        //[newRows addObject:newReloadRows];
        //newReloadRows = nil;
        [self insertRowsAtIndexPaths:@[newReloadRows] withRowAnimation:UITableViewRowAnimationAutomatic];
        //[self reloadRowsAtIndexPaths:@[newReloadRows] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    //[self insertRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationFade];
    
    [self endUpdates];
    
    
    [self setHidden:NO];
    
    [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    [self setGAIViewName];
    
    
    DMSG;
    kHIDEWAIT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 256/2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (dataArray == nil) {
        return 0;
    } else {
        return numberOfResults;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([resultArray count]-8 == [indexPath row] || [resultArray count] == [indexPath row]) {
        if (numberOfResults < totalNumberOfResults) {
            DMSG;
            [self performSelectorInBackground:@selector(getMoreResults) withObject:nil];
        }
    }
    
    NSString *identifier = [NSString stringWithFormat:@"%@_%@_%ld_%ld",[[wbData sharedData] selectedMissionCountry],[[self currentFeed] objectForKey:@"name"],(long)[indexPath row],(long)[indexPath section]];
    
    wbNewMissionCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil){
        cell = [[wbNewMissionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setFrame:CGRectMake(0, 0, self.frame.size.width, 256/2)];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [cell setBackgroundColor:kBKGUICOLOR];
        [cell setClipsToBounds:YES];
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
    NSNumber *mission_id = [[resultArray objectAtIndex:[indexPath row]] objectForKey:@"id"];
    wbMission *mission = [[wbData sharedData] getMissionWithId:mission_id];
    
    [cell setMission:mission];
    }
    
    return cell;
}

// parallax me :D
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    for (wbNewMissionCell *cell in [self visibleCells]) {
        float offset = cell.cellBkg.frame.origin.y + (self.contentOffset.y - prevY)/20;
        //NSLog(@"%f : %f : %f",self.contentOffset.y, offset, self.contentOffset.y - prevY);
        if (offset > 0) {
            offset = 0;
        } else if (fabsf(offset) > (cell.cellBkg.frame.size.height - cell.frame.size.height)) {
            offset =  cell.frame.size.height - cell.cellBkg.frame.size.height;
        }
        
        CGRect frame = CGRectMake(cell.cellBkg.frame.origin.x, offset, cell.cellBkg.frame.size.width, cell.cellBkg.frame.size.height);
        cell.cellBkg.frame = frame;
    }
    
    prevY = self.contentOffset.y;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentRow = indexPath;
    NSNumber *mission_id = [[resultArray objectAtIndex:[indexPath row]] objectForKey:@"id"];
    wbMission *mission = [[wbData sharedData] getMissionWithId:mission_id];
    [kROOTVC performSelectorOnMainThread:@selector(startSingleMission:) withObject:mission waitUntilDone:NO];
}

-(void)refreshRow
{
    if (currentRow != nil) {
        [[self cellForRowAtIndexPath:currentRow] setNeedsDisplay];
    }
}

@end
