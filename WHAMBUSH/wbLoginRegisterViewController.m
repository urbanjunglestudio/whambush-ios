//
//  wbLoginRegisterViewController.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 09/10/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbLoginRegisterViewController.h"

@interface wbLoginRegisterViewController ()

@end

@implementation wbLoginRegisterViewController

@synthesize selectedCountry;
@synthesize showSettings;

-(id) init
{
    DPRINTCLASS;
    self = [super init];
    if (self) {
        //
        //selectedCountry = @"";
        
        pickCountry = [[wbCountryPickerView alloc] initWithFrame:[[wbAPI sharedAPI] portraitFrame]];
        [pickCountry setSelectedCountry:[[wbAPI sharedAPI] currentCountry]];
        [pickCountry setDelegate:self];
        showSettings = NO;
    }
    return self;
}

+ (id)sharedLRVC
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

-(NSString*)description
{
    return [super description];
}


-(void)showAskToLoginRegisterOnView:(id)view
{
    [kROOTVC performSelector:@selector(hideSettings)];
    NSString *settingsTxt;
    if (showSettings) {
        settingsTxt = NSLocalizedString(@"USER_SETTINGS", @"");
    }
    
    IBActionSheet *actionSheet = [[IBActionSheet alloc] initWithTitle:NSLocalizedString(@"GENERAL_GUEST_USER_LOGIN_REGISTER",@"") delegate:self cancelButtonTitle:NSLocalizedString(@"GENERAL_CANCEL", @"") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"GENERAL_LOGIN",@""),NSLocalizedString(@"REGISTER_REGISTER_BUTTON",@""),settingsTxt, nil];

    [actionSheet setTitleBackgroundColor:kTOPUICOLOR];
    [actionSheet setTitleTextColor:kLIGHTGRAYUICOLOR];
    [actionSheet setButtonTextColor:kLIGHTGRAYUICOLOR];
    [actionSheet setButtonBackgroundColor:kTOPUICOLOR forButtonAtIndex:0];
    [actionSheet setButtonBackgroundColor:kTOPUICOLOR forButtonAtIndex:1];
    if (showSettings) {
        [actionSheet setButtonBackgroundColor:kBOTTOMUICOLOR forButtonAtIndex:2];
        [actionSheet setButtonBackgroundColor:kREDUICOLOR forButtonAtIndex:3];
    } else {
        [actionSheet setButtonBackgroundColor:kREDUICOLOR forButtonAtIndex:2];
    }
    [actionSheet setAlpha:1];
    [actionSheet showInView:[[[UIApplication sharedApplication] delegate] window]];
}

-(void)closeAlert
{
    [currentAlertView close];
}


-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DNSLog(@"%ld",(long)buttonIndex);
    if (buttonIndex == 2 && showSettings) {
        [kROOTVC performSelector:@selector(startSettings:) withObject:self];
    } else if (buttonIndex == 0) {
        [self showLoginScreen];
    } else if (buttonIndex == 1) {
        [self showRegisterScreen];
    }
    showSettings = NO;
}


-(void)showLoginScreen
{
    emailField = nil;
    userField = nil;
    passField = nil;
    passCheckField = nil;
    activeField = nil;

    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"GENERAL_CANCEL", @""), NSLocalizedString(@"GENERAL_LOGIN", @""), nil]];
    [alertView setDelegate:self];
    [alertView setContainerView:[self loginView]];
    [alertView setTag:0];
    [alertView setButtonTextColor:kLIGHTGRAYUICOLOR];
    [alertView show];
}

-(void)showRegisterScreen
{
    emailField = nil;
    userField = nil;
    passField = nil;
    passCheckField = nil;
    activeField = nil;
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:NSLocalizedString(@"GENERAL_CANCEL", @""), NSLocalizedString(@"REGISTER_REGISTER_BUTTON", @""), nil]];
    [alertView setDelegate:self];
    [alertView setContainerView:[self registerView]];
    [alertView setTag:1];
    [alertView setButtonTextColor:kWHITEUICOLOR];
    [alertView show];

    [[[[UIApplication sharedApplication] delegate] window] addSubview:pickCountry];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DNSLog(@"%ld, %ld",(long)buttonIndex,(long)alertView.tag);
    if ([alertView tag] == 0 && buttonIndex == 1) {
        DNSLog(@"%@",[alertView textFieldAtIndex:0] );
        DNSLog(@"%@",[alertView textFieldAtIndex:1]);
        
        NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
            [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
        }];
        
        [op setCompletionBlock:^(void){
            dispatch_sync(dispatch_get_main_queue(), ^{
                NSString *guest_id = [[wbAPI sharedAPI] getSettingWithKey:@"guest_id"];
                [[wbAPI sharedAPI] authenticate:[[alertView textFieldAtIndex:0] text] password:[[alertView textFieldAtIndex:1] text] guest_id:guest_id];
            });
        }];
        [op start];    
    } else {
        kHIDEWAIT;
    }
}

- (void)customIOS7dialogButtonTouchUpInside:(id)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    currentAlertView = alertView;
    
    DNSLog(@"%ld, %@",(long)buttonIndex,(CustomIOS7AlertView*)alertView);
    if ([(CustomIOS7AlertView*)alertView tag] == 0) {
        if (buttonIndex == 1) {
            [self doLogin];
        } else {
            kHIDEWAIT;
            [alertView close];
            [activeField resignFirstResponder];
        }
    } else {
        if (buttonIndex == 1) {
            [self checkRegister];
        } else {
            kHIDEWAIT;
            [alertView close];
            [activeField resignFirstResponder];
        }
    }
}

-(void)doLogin
{
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
        [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
    }];
    
    [op setCompletionBlock:^(void){
        dispatch_sync(dispatch_get_main_queue(), ^{
            NSString *guest_id = [[wbAPI sharedAPI] getSettingWithKey:@"guest_id"];
            [[wbAPI sharedAPI] authenticate:userField.text password:passCheckField.text guest_id:guest_id];
        });
    }];
    [op start];
}

-(void)checkRegister
{
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
        [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
    }];
    [op setCompletionBlock:^(void){
        dispatch_sync(dispatch_get_main_queue(), ^{
            if ([self checkForRegisterErrors]) {
                
                NSDictionary *userArray = [NSDictionary dictionaryWithObjectsAndKeys:
                                           [NSString stringWithFormat:@"%@",emailField.text],@"email",
                                           [NSString stringWithFormat:@"%@",userField.text],@"username",
                                           [NSString stringWithFormat:@"%@",passField.text],@"password1",
                                           [NSString stringWithFormat:@"%@",passField.text],@"password2",
                                           [selectedCountry objectForKey:@"country"],@"country",
                                           nil];
                
                [[wbAPI sharedAPI] createUser:userArray];
                //kHIDEWAIT
            } else {
                kHIDEWAIT;
            }
        });
    }];
    
    [op start];
}

-(BOOL)checkForRegisterErrors
{
    if (emailField.text.length < 1 | userField.text.length < 1) {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"REGISTER_ERROR_EMPTY_FIELDS",@"")];
        return NO;
    }
    else if (![passField.text isEqualToString:passCheckField.text]){
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"REGISTER_ERROR_PASSWORD_MISMATCH",@"")];
        return NO;
    }
    else if (![[wbAPI sharedAPI] validateEmailWithString:emailField.text]) {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"REGISTER_ERROR_BROKEN_EMAIL",@"")];
        return NO;
    }
    else if (passField.text.length < 4 ) {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"REGISTER_ERROR_PASSWORD_TOO_SHORT",@"")];
        return NO;
    }
    else if (selectedCountry == nil) {
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"REGISTER_ERROR_EMPTY_COUNTRY",@"")];
        return NO;
    }
    
    return YES;
}

//- (BOOL)validateEmailWithString:(NSString*)email
//{
//    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:email];
//}

-(UIView*)loginView
{
    
    UIView *rv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 150)];
    
    
    UIButton *closekb = [UIButton buttonWithType:UIButtonTypeSystem];
    [closekb setFrame:rv.frame];
    [closekb setBackgroundColor:kTRANSPARENTUICOLOR];
    [closekb addTarget:activeField action:@selector(resignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [rv addSubview:closekb];
    
    wbLabel *regLabel = [[wbLabel alloc] init];
    [regLabel setText:[NSLocalizedString(@"GENERAL_LOGIN", @"") uppercaseString]];
    [regLabel setTextColor:kGREENUICOLOR];
    [regLabel setFont:kFONT(30)];
    [regLabel sizeToFit];
    [regLabel setCenter:CGPointMake(CGRectGetMidX(rv.frame), 26)];
    
    //Textfields
#define firstX (rv.frame.size.width/2-(kTXTBOXW/2))
    CGRect usernameRect = CGRectMake(firstX,CGRectGetMaxY(regLabel.frame)+12,
                                     (kTXTBOXW),
                                     kTXTBOXH);
    
    
    CGRect passwordCheckRect = CGRectMake(firstX,CGRectGetMaxY(usernameRect)+1,
                                          kTXTBOXW,
                                          kTXTBOXH);
    if (userField == nil) {
        userField = [self giveTxtFieldWithRect:usernameRect placeholder:NSLocalizedString(@"GENERAL_USERNAME",@"") secure:NO tag:0];
    }
    CAShapeLayer *passwordMaskLayerT = [[CAShapeLayer alloc] init];
    UIBezierPath *passwordMaskPathWithRadiusTop = [UIBezierPath bezierPathWithRoundedRect:userField.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    passwordMaskLayerT.frame     = userField.bounds;
    passwordMaskLayerT.path      = passwordMaskPathWithRadiusTop.CGPath;
    passwordMaskLayerT.fillColor = [UIColor whiteColor].CGColor;
    [userField.layer setMask:passwordMaskLayerT];
    [userField setDelegate:self];
    
    if (passCheckField == nil) {
        passCheckField = [self giveTxtFieldWithRect:passwordCheckRect placeholder:NSLocalizedString(@"GENERAL_PASSWORD",@"") secure:YES tag:1];
    }
    CAShapeLayer *passwordMaskLayerB = [[CAShapeLayer alloc] init];
    UIBezierPath *passwordMaskPathWithRadiusBottom = [UIBezierPath bezierPathWithRoundedRect:passCheckField.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    passwordMaskLayerB.frame     = passCheckField.bounds;
    passwordMaskLayerB.path      = passwordMaskPathWithRadiusBottom.CGPath;
    passwordMaskLayerB.fillColor = kBOTTOMUICOLOR.CGColor;//[UIColor whiteColor].CGColor;
    [passCheckField.layer setMask:passwordMaskLayerB];
    [passCheckField setDelegate:self];
    
    
    [rv addSubview:userField];
    [rv addSubview:passCheckField];
    [rv addSubview:regLabel];
    
    return rv;
}

-(UIView*)registerView
{

    UIView *rv = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 265+kTXTBOXH)];
    
    
    UIButton *closekb = [UIButton buttonWithType:UIButtonTypeSystem];
    [closekb setFrame:rv.frame];
    [closekb setBackgroundColor:kTRANSPARENTUICOLOR];
    [closekb addTarget:activeField action:@selector(resignFirstResponder) forControlEvents:UIControlEventTouchUpInside];
    [rv addSubview:closekb];
    
    wbLabel *regLabel = [[wbLabel alloc] init];
    [regLabel setText:[NSLocalizedString(@"REGISTER_REGISTER_BUTTON", @"") uppercaseString]];
    [regLabel setTextColor:kGREENUICOLOR];
    [regLabel setFont:kFONT(30)];
    [regLabel sizeToFit];
    [regLabel setCenter:CGPointMake(CGRectGetMidX(rv.frame), 30)];
    
    //Textfields
    #define firstX (rv.frame.size.width/2-(kTXTBOXW/2))
    CGRect usernameRect = CGRectMake(firstX,CGRectGetMaxY(regLabel.frame)+12,
                                     (kTXTBOXW),
                                     kTXTBOXH);
    
    CGRect emailRect = CGRectMake(firstX,CGRectGetMaxY(usernameRect)+1,
                                  kTXTBOXW,
                                  kTXTBOXH);
    
    CGRect countryRect = CGRectMake(firstX,CGRectGetMaxY(emailRect)+1,
                                  kTXTBOXW,
                                  kTXTBOXH);
    
    CGRect passwordRect = CGRectMake(firstX,CGRectGetMaxY(countryRect)+1,
                                     kTXTBOXW,
                                     kTXTBOXH);
    
    CGRect passwordCheckRect = CGRectMake(firstX,CGRectGetMaxY(passwordRect)+1,
                                          kTXTBOXW,
                                          kTXTBOXH);
    if (userField == nil) {
        userField = [self giveTxtFieldWithRect:usernameRect placeholder:NSLocalizedString(@"GENERAL_USERNAME",@"") secure:NO tag:0];
   }
    CAShapeLayer *passwordMaskLayerT = [[CAShapeLayer alloc] init];
    UIBezierPath *passwordMaskPathWithRadiusTop = [UIBezierPath bezierPathWithRoundedRect:userField.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    passwordMaskLayerT.frame     = userField.bounds;
    passwordMaskLayerT.path      = passwordMaskPathWithRadiusTop.CGPath;
    passwordMaskLayerT.fillColor = [UIColor whiteColor].CGColor;
    [userField.layer setMask:passwordMaskLayerT];
    [userField setDelegate:self];
    
    if (emailField == nil) {
        emailField = [self giveTxtFieldWithRect:emailRect placeholder:NSLocalizedString(@"GENERAL_EMAIL",@"") secure:NO tag:1];
    }
    [emailField setDelegate:self];
    if (passField == nil) {
        passField = [self giveTxtFieldWithRect:passwordRect placeholder:NSLocalizedString(@"GENERAL_PASSWORD",@"") secure:YES tag:2];
    }
    
    //country
    if (countryButton == nil) {
        countryButton = [UIButton buttonWithType:UIButtonTypeSystem];
    }
    [countryButton setBackgroundColor:kBOTTOMUICOLOR];
    [countryButton setFrame:countryRect];
    [countryButton setTitle:NSLocalizedString(@"GENERAL_COUNTRY", @"") forState:UIControlStateNormal];
    [countryButton setTitleColor:kWHITEUICOLOR forState:UIControlStateNormal];
    [countryButton setTitleColor:kWHITEUICOLOR forState:UIControlStateSelected];
    [[countryButton titleLabel] setFont:kFONTHelvetica(18)];
    [countryButton addTarget:self action:@selector(countryAction) forControlEvents:UIControlEventTouchUpInside];
    [countryButton setTintColor:kTRANSPARENTUICOLOR];
    
    [self setSelectedCountry:[[wbAPI sharedAPI] currentCountry]];
    
    UIImage *arrowImg = [[wbAPI sharedAPI] changeColorForImage:[UIImage ch_imageNamed:@"feed_arrow.png"] toColor:kBKGUICOLOR];
    UIImageView *downArrowImgView = [[UIImageView alloc] initWithImage:arrowImg];
    [downArrowImgView setFrame:CGRectMake(countryButton.frame.size.width-25, 0, arrowImg.size.width/2, arrowImg.size.height/2)];
    [downArrowImgView setCenter:CGPointMake(downArrowImgView.center.x, countryButton.frame.size.height/2)];
    [downArrowImgView setUserInteractionEnabled:NO];
    
    [passField setDelegate:self];
    if (passCheckField == nil) {
        passCheckField = [self giveTxtFieldWithRect:passwordCheckRect placeholder:NSLocalizedString(@"REGISTER_CONFIRM_PASSWORD",@"") secure:YES tag:3];
    }
    CAShapeLayer *passwordMaskLayerB = [[CAShapeLayer alloc] init];
    UIBezierPath *passwordMaskPathWithRadiusBottom = [UIBezierPath bezierPathWithRoundedRect:passCheckField.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(10.0, 10.0)];
    passwordMaskLayerB.frame     = passCheckField.bounds;
    passwordMaskLayerB.path      = passwordMaskPathWithRadiusBottom.CGPath;
    passwordMaskLayerB.fillColor = kBOTTOMUICOLOR.CGColor;//[UIColor whiteColor].CGColor;
    [passCheckField.layer setMask:passwordMaskLayerB];
    [passCheckField setDelegate:self];
    
    //terms&cond
    NSMutableAttributedString *firstLine = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"REGISTER_TERMS_CONDITIONS", @"")];
    NSMutableAttributedString *rowStr = [[NSMutableAttributedString alloc] initWithAttributedString:firstLine];
    [rowStr addAttribute:NSForegroundColorAttributeName value:kNOTSOLIGHTGRAYUICOLOR range:(NSRange){0,[rowStr length]}];
    
    UIButton *termButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [termButton setAttributedTitle:rowStr forState:UIControlStateNormal];
    [termButton.titleLabel setFont:kFONTHelvetica(12)];
    [termButton.titleLabel setNumberOfLines:0];
    [termButton setFrame:CGRectMake(0, CGRectGetMaxY(passCheckField.frame)+10,rv.frame.size.width, 40)];
    [termButton addTarget:self action:@selector(openTermsUrl) forControlEvents:UIControlEventTouchUpInside];
    [termButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [termButton setBackgroundColor:kTRANSPARENTUICOLOR];
    

    
    [rv addSubview:userField];
    [rv addSubview:emailField];
    [rv addSubview:countryButton];
    [countryButton addSubview:downArrowImgView];
    [rv addSubview:passField];
    [rv addSubview:passCheckField];
    [rv addSubview:regLabel];
    [rv addSubview:termButton];

    return rv;
}

//text field
-(UITextField *)giveTxtFieldWithRect:(CGRect)tfRect placeholder:(NSString*)placeholder secure:(BOOL)secure tag:(NSInteger)tag
{
    UITextField *tmp = [self giveTxtFieldWithRect:tfRect placeholder:placeholder secure:secure];
    [tmp setTag:tag];
    return tmp;
}

-(UITextField *)giveTxtFieldWithRect:(CGRect)tfRect placeholder:(NSString*)placeholder secure:(BOOL)secure
{
    UITextField *txtField = [[UITextField alloc]initWithFrame:tfRect];
    [txtField setTextColor:kWHITEUICOLOR];
    [txtField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [txtField setBackgroundColor:/*UIColorFromHexRGB(0x434947)*/kBOTTOMUICOLOR];
    [txtField setBorderStyle:UITextBorderStyleNone];
    [txtField setFont:kFONTHelvetica(18)];
    [txtField setUserInteractionEnabled:YES];
    [txtField setSecureTextEntry:secure];
    [txtField setKeyboardAppearance:UIKeyboardAppearanceDark];
    [txtField setKeyboardType:UIKeyboardTypeEmailAddress];
    [txtField setAutocorrectionType:UITextAutocorrectionTypeNo];
    [txtField setTintColor:kWHITEUICOLOR];
    [txtField setAutoresizesSubviews:YES];
    [txtField setReturnKeyType:UIReturnKeyNext];
    [txtField setContentVerticalAlignment:UIControlContentVerticalAlignmentCenter];
    [txtField setTextAlignment:NSTextAlignmentCenter];
    [txtField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:placeholder attributes:@{NSForegroundColorAttributeName:kLIGHTGRAYUICOLOR}]];
    return txtField;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO;
}


- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
}

-(void)openTermsUrl
{
    [[UIApplication sharedApplication]  openURL:[NSURL URLWithString:NSLocalizedString(kTERMSCONDLINK, @"")]];
}

-(void)countryAction
{
    DCMSG(@"countryAction");
    [activeField resignFirstResponder];
    [pickCountry show];
}

-(void)setSelectedCountry:(NSDictionary *)_selectedCountry
{
    selectedCountry = _selectedCountry;
    [countryButton setTitle:[selectedCountry objectForKey:@"name"] forState:UIControlStateSelected];
    [countryButton setSelected:YES];
    
}

@end
