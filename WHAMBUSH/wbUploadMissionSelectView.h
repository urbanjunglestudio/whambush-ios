//
//  wbUploadMissionSelectView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07.06.14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbUploadMissionSelectView : UIView <UIPickerViewDelegate,UIPickerViewDataSource>
{
    NSInteger selectedRow;
    UIToolbar *hideView;
    UIButton *cancelBtn;
    UIView *theBox;
    UIButton *okBtn;
    NSMutableArray *missionArray;
}

@property id delegate;
@property (nonatomic,retain) NSDictionary *missions;

@end
