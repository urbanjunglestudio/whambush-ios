//
//  wbBaseCell.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/10/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbBaseCell : UITableViewCell

@property (nonatomic,retain) NSMutableDictionary *cellContent;
@property (nonatomic,retain) NSMutableArray *contentArray;

@property UITableViewCellStyle cellStyle;
@property NSString* cellReuseIdentifier;

@end

