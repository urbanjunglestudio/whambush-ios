//
//  wbAPI.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 8/26/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "wbUser.h"
#import "wbDefines.h"
#import "SSKeychain.h"
#import "Reachability.h"
#import <AVFoundation/AVFoundation.h>
#import "wbWaitView.h"
#import "AFNetworking.h"
#import "GAIDictionaryBuilder.h"
#import "KeychainItemWrapper.h"

@interface wbAPI : NSObject <UITextFieldDelegate, UIAlertViewDelegate>
{
    NSInteger page_size;
    //UIView *waitView;
    NSDictionary *newUserArray;
    UIView *uploadProgressView;
    BOOL no_page;
    BOOL firstTimeFail;
    BOOL newUser;
}

@property (retain,readonly) NSNumber *iPhoneVersion;

@property (nonatomic,retain) NSDictionary *currentCountry;

@property (readonly) BOOL is_guest;
@property (readonly) BOOL is_authenticated;
@property (readonly) CGRect portraitFrame;
@property (readonly) CGRect landscapeFrame;

@property (nonatomic,retain) id currentViewController;

@property (nonatomic,strong) UIViewController *rootViewController;
@property (nonatomic,strong) NSMutableDictionary *uploadData;

@property (nonatomic) UIBackgroundTaskIdentifier bgTask;

@property (nonatomic,retain) NSString* pushToken;

@property (nonatomic,retain,readonly) NSString* token;
@property (nonatomic,retain,readonly) NSString* vzaar_secret;
@property (nonatomic,retain,readonly) NSString* vzaar_key;
//@property (nonatomic,retain,readonly) NSString* guest_id;

@property (nonatomic,retain) NSString *num_active_missions;
@property (nonatomic,retain) NSString *num_incomplete_profile;
@property (nonatomic,retain) NSString *num_unread_activities;
@property (readonly) BOOL uploading;
@property (nonatomic,readonly) NSNumber *GPUCamera;

+(id)sharedAPI;

//////////////////////////
-(float)statusBarHeight;
-(float)footerBarHeight;
-(float)headerBarHeight;
-(CGRect)headerBarRect;
-(CGRect)footerBarRect;
-(CGRect)contentRect;
//////////////////////////

//////////////////////////
-(BOOL)checkIfNetworkAvailable;
-(NSURL*)urlWithEndpoint:(NSString*)endpoint;
//////////////////////////

//////////////////////////
-(void)authenticate:(NSString *)username password:(NSString *)password guest_id:(NSString*)guest_id;
-(void)tryLogin;
-(void)createUser:(NSDictionary *)userArray;
-(void)logout;
//////////////////////////

//////////////////////////
//-(void)saveCurrentCountry:(id)country;
-(void)saveSettingWithKey:(NSString*)key andData:(id)data;
-(id)getSettingWithKey:(NSString*)key;
-(void)removeSettingWithKey:(NSString*)key;
//////////////////////////

//////////////////////////
-(void)getJSONWithURL:(NSURL *)url response:(void (^)(id json))jsonResponse;
-(void)postJSONWithURL:(NSURL*)url paramenters:(NSDictionary*)parameters response:(void (^)(id json))jsonResponse errorResponse:(void (^)(id errorResponse))errorResponse;
//////////////////////////

//////////////////////////
-(void)getDataWithEndpoint:(NSString*)endpoint delegate:(id)delegate;
-(void)getDataWithURL:(NSString*)urlStr delegate:(id)delegate;
-(void)getDataWithURL:(NSURL*)url delegate:(id)delegate selectorName:(NSString*)selectorName;
//////////////////////////

//////////////////////////
-(void)showErrorWithTxt:(NSString*)errorTxt;
-(void)showNoteWithTxt:(NSString*)noteTxt;
//////////////////////////

//////////////////////////
-(void)setMissionData:(NSDictionary*)missionData;
-(void)setVideoURL:(NSURL*)url;
-(void)clearUploadData;
-(void)finalizeUpload:(NSDictionary*)info vzaarId:(NSInteger)videoid missionId:(NSInteger)missionId delegate:(id)delegate;
-(void)finalizeUpload:(NSDictionary*)info vzaarId:(NSInteger)videoid delegate:(id)delegate;
//////////////////////////

//////////////////////////
-(void)addComment:(NSString*)comment videoid:(NSInteger)videoid  delegate:(id)delegate;
//////////////////////////

//////////////////////////
-(void)like:(BOOL)like videoid:(NSString*)videoid delegate:(id)delegate;
-(void)unlike:(BOOL)like videoid:(NSString*)videoid delegate:(id)delegate;
//////////////////////////

//////////////////////////
-(NSString*)beautifyTime:(NSString*)timeString;
-(NSString*)parsePostedTime:(NSString*)timeString;
-(NSString*)parseMissionEndTime:(NSString*)timeString;
//////////////////////////

//////////////////////////
-(void)reportVideo:(NSInteger)_videoId reasonCode:(NSInteger)_reasonCode;
//////////////////////////

-(void)followUser:(wbUser*)user delegate:(id)delegate;
//-(void)followUser:(NSInteger)userId delegate:(id)delegate;
-(void)unfollowUser:(wbUser*)user delegate:(id)delegate;
//-(void)unfollowUser:(NSInteger)userId delegate:(id)delegate;

//////////////////////////
-(void)updateUserDetails:(NSDictionary*)details;
-(void)updateUserEmail:(NSDictionary*)details delegate:(id)delegate;
-(void)updateUserProfilePicture:(NSString*)imagePath delegate:(id)delegate;
//////////////////////////

//////////////////////////
-(UIImage*)doGradient:(CGSize)size;
-(UIImage*)doGradient:(UIImage*)image size:(CGSize)size;
-(UIImage*)fixThumbnail:(UIImage*)thumbnail size:(CGSize)size;
-(UIImage*)changeColorForImage:(UIImage*)image toColor:(UIColor*)color;
//////////////////////////

-(void)cleanTmpDir;

-(UIView*)getUploadProgressView;
-(void)setUploadProgressView:(UIView*)view;

-(void)sendPushNotificationToken:(NSString*)token;

-(void)removeVideo:(NSInteger)videoId delegate:(id)delegate;
-(void)removeComment:(NSNumber*)commentId delegate:(id)delegate;

-(void)registerGoogleAnalytics:(NSString*)name;

-(BOOL)validateEmailWithString:(NSString*)email;

-(UIImage*)doDotWithColor:(UIColor*)color border:(BOOL)border;

-(UIImageView*)giveBubleWithBanana:(BOOL)banana frame:(CGRect)frame;

//////////////////////////
-(NSString*)valueToString:(NSNumber*)value;
-(UIImage*)blur:(UIImage*)image;

@end
