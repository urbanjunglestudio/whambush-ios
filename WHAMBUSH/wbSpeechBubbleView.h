//
//  wbSpeechBubbleView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/10/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbDefines.h"

@interface wbSpeechBubbleView : UIView

@property BOOL rightBuble;
@property float bubbleAlpha;
@property (nonatomic,retain) UIColor *bubbleColor;

@end
