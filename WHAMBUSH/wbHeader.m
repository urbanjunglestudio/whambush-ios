//
//  wbHeader.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 04/12/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbHeader.h"

@implementation wbHeader

@synthesize uploadView;
@synthesize headerBackButton;
@synthesize green;
@synthesize logo;
@synthesize userTitle;
@synthesize showChangeCountryButton;
@synthesize countryChangeActive;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        headerBackButton = NO;
        green = YES;
        //[UIView setAnimationsEnabled:NO];
        countryChangeActive = YES;
        logo = @"_wb_";
    }
    DPRINTCLASS;
    return self;
}

/*+(id)myHeader
{
    id _myObject = [[self alloc] initWithFrame:CGRectMake(0, 0, [[wbAPI sharedAPI] portraitFrame].size.width,[[wbAPI sharedAPI] headerBarHeight])];
    return _myObject;
}*/

+ (id)sharedHeader
{
    static dispatch_once_t pred = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&pred, ^{
        _sharedObject = [[self alloc] initWithFrame:CGRectMake(0, 0, [[wbAPI sharedAPI] portraitFrame].size.width,[[wbAPI sharedAPI] headerBarHeight])];
    });
    return _sharedObject;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    // Drawing code
    headerBarRect = CGRectMake(0, 0, [[wbAPI sharedAPI] portraitFrame].size.width,[[wbAPI sharedAPI] headerBarHeight]);
    if (headerBar == nil) {
        headerBar = [[UIView alloc] initWithFrame:[self frame]];        
    }
    [headerBar setBackgroundColor:kTOPUICOLOR];
    
    if (logoButton == nil) {
        logoButton = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    [logoButton setFrame:CGRectMake(0, [[wbAPI sharedAPI] statusBarHeight], self.frame.size.width, self.frame.size.height-[[wbAPI sharedAPI] statusBarHeight])];
    [logoButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [logoButton addTarget:kROOTVC action:@selector(scrollToTop:) forControlEvents:UIControlEventTouchUpInside];
    [self setLogo:logo];
    
    [headerBar addSubview:logoButton];
    
    UIImage *buttonImg;
    if (green) {
        buttonImg = [UIImage ch_imageNamed:@"back.png"];
    } else {
        buttonImg = [UIImage ch_imageNamed:@"back_upload.png"];
    }
    if (backButton == nil) {
        backButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [headerBar addSubview:backButton];
        [backButton setHidden:!headerBackButton];
        [backButton addTarget:[wbHeader sharedHeader] action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    }
    [backButton setBackgroundImage:buttonImg forState:UIControlStateNormal];
    [backButton setShowsTouchWhenHighlighted:YES];
    [backButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [backButton setClipsToBounds:YES];
    
    [backButton setFrame:CGRectMake(0,0,buttonImg.size.width,buttonImg.size.height)];
    [backButton setCenter:CGPointMake(buttonImg.size.width/2, [[wbAPI sharedAPI] headerBarHeight]/2+10)];
    
    UIImage *changeCountryImg = [UIImage ch_imageNamed:@"globe.png"];
    if (changeCountryButton == nil) {
        changeCountryButton = [wbButton buttonWithType:UIButtonTypeSystem];
    }
    countryChangeActive = YES;
    //[changeCountryButton setBackgroundImage:changeCountryImg forState:UIControlStateNormal];
    [changeCountryButton setShowsTouchWhenHighlighted:YES];
    [changeCountryButton setBackgroundColor:kTRANSPARENTUICOLOR];
    [changeCountryButton setClipsToBounds:NO];
    [changeCountryButton setFrame:CGRectMake(0,0,[[wbAPI sharedAPI] headerBarHeight]-[[wbAPI sharedAPI] statusBarHeight],[[wbAPI sharedAPI] headerBarHeight]-[[wbAPI sharedAPI] statusBarHeight])];
    [changeCountryButton setCenter:CGPointMake((self.frame.size.width)-changeCountryButton.frame.size.width/2, [[wbAPI sharedAPI] headerBarHeight]/2+10)];
    [changeCountryButton setButtonImage:changeCountryImg];
    [changeCountryButton addTarget:self action:@selector(changeCountryAction) forControlEvents:UIControlEventTouchUpInside];

    UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headerBar.frame)-1, self.frame.size.width, 1)];
    [bottomLine setBackgroundColor:kBKGUICOLOR];
    [headerBar addSubview:bottomLine];

    [headerBar bringSubviewToFront:backButton];
    [self addSubview:headerBar];
    [self bringSubviewToFront:headerBar];
    
}

-(void)backAction
{
    if (uploadView != nil) {
        [uploadView performSelector:@selector(goBack:) withObject:nil];
    } else {
        [kROOTVC performSelector:@selector(goBack:) withObject:nil];
    }
}

-(void)setHeaderBackButton:(BOOL)_headerBackButton
{
    headerBackButton = _headerBackButton;
    if (_headerBackButton) {
        [backButton setHidden:NO];
        //[headerBar addSubview:backButton];
    } else {
        [backButton setHidden:YES];
        //[backButton removeFromSuperview];
    }
    [backButton addTarget:[wbHeader sharedHeader] action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    [headerBar bringSubviewToFront:backButton];

}

-(void)changeCountryAction
{
    DCMSG(@"changeCountryAction");
    [self setShowChangeCountryButton:NO];
    if ([logo isEqualToString:@"_tv_"]) {
        [kROOTVC performSelector:@selector(goHome:) withObject:nil];
    }
    if ([logo isEqualToString:@"_wb_"]) {
        [kROOTVC performSelector:@selector(startMissionCountrySelect:) withObject:nil];
    }
}

-(void)setShowChangeCountryButton:(BOOL)_showChangeCountryButton
{
    showChangeCountryButton = _showChangeCountryButton;
    if (_showChangeCountryButton) {
        [self setCountryChangeActive:YES];
        [headerBar addSubview:changeCountryButton];
    } else {
        [changeCountryButton removeFromSuperview];
    }
    //[self setNeedsDisplay];
}

-(void)setGreen:(BOOL)_green
{
    green = _green;
    UIImage *buttonImg;
    if (green) {
        buttonImg = [UIImage ch_imageNamed:@"back.png"];
    } else {
        buttonImg = [UIImage ch_imageNamed:@"back_upload.png"];
    }
    [backButton setBackgroundImage:buttonImg forState:UIControlStateNormal];
    [headerBar bringSubviewToFront:backButton];

}

-(void)setLogo:(NSString*)_logo
{
//    logo = @"Whambush";//_logo;
    logo = _logo;
    UIImage *logoImg;
    wbLabel *userLogo;
    if ([logo isEqualToString:@"_wb_"]) {
        logoImg = [UIImage ch_imageNamed:@"logo.png"];
    } else if ([logo isEqualToString:@"_upload_"]) {
        logoImg = [UIImage ch_imageNamed:@"logo_upload.png"];
        [self setGreen:NO];
    } else if ([logo isEqualToString:@"_mission_"]) {
        logoImg = [UIImage ch_imageNamed:@"missions_logo.png"];
    } else if ([logo isEqualToString:@"_tv_"]) {
        logoImg = [UIImage ch_imageNamed:@"logotv.png"];
    } else {
        if (logo != nil) {
            userLogo = [[wbLabel alloc] init];
            //[userLogo setBackgroundColor:kREDUICOLOR];
//            NSMutableAttributedString *logoString = [[NSMutableAttributedString alloc] initWithString:logo];
//            [logoString addAttribute:NSFontAttributeName value:kFONT(30) range:NSMakeRange(0, logo.length)];
//            [logoString addAttribute:NSFontAttributeName value:kFONT(36) range:NSMakeRange(0, 1)];
//            [userLogo setAttributedText:logoString];
            [userLogo setAttributedText:[self createHeaderWithTxt:logo]];
            [userLogo sizeToFit];
//            [userLogo setTextColor:[UIColor colorWithPatternImage:[[wbAPI sharedAPI] doGradient:userLogo.frame.size]]];
            [userLogo setTextColor:kGREENUICOLOR];
            [userLogo setCenter:CGPointMake(logoButton.frame.size.width/2, logoButton.frame.size.height/2 + 5)];
        }
    }
    if (logoImg != nil) {
        [[logoButton subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [logoButton setButtonImage:logoImg];
    }
    if (userLogo != nil) {
        [logoButton setButtonImage:logoImg];
        [[logoButton subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [logoButton addSubview:userLogo];
    }
    [logoButton setNeedsDisplay];
}

-(NSMutableAttributedString*)createHeaderWithTxt:(NSString*)txt
{
    NSMutableAttributedString *finalTxt = [[NSMutableAttributedString alloc] init];
    NSMutableArray *letterArray = [NSMutableArray array];
    NSString *letters = txt;
    [letters enumerateSubstringsInRange:NSMakeRange(0, [letters length])
                                options:(NSStringEnumerationByComposedCharacterSequences)
                             usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                                 [letterArray addObject:substring];
                             }];
    
    for (int i = 0; i < [letterArray count]; i++) {
        NSMutableAttributedString *tmp = [[NSMutableAttributedString alloc] initWithString:[[letterArray objectAtIndex:i] uppercaseString]];
        if ([[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:[[letterArray objectAtIndex:i] characterAtIndex:0]]) {
            [tmp addAttribute:NSFontAttributeName value:kFONT(36) range:NSMakeRange(0, 1)];
        } else {
            [tmp addAttribute:NSFontAttributeName value:kFONT(30) range:NSMakeRange(0, 1)];
        }
        [finalTxt appendAttributedString:tmp];
    }
    return finalTxt;
}

-(void)setCountryChangeActive:(BOOL)_countryChangeActive
{
    countryChangeActive = _countryChangeActive;
    [changeCountryButton setUserInteractionEnabled:countryChangeActive];
}


///////////////////////
/*- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    UIView *hitView = [super hitTest:point withEvent:event];
    
    // If the hitView is THIS view, return nil and allow hitTest:withEvent: to
    // continue traversing the hierarchy to find the underlying view.
    if (hitView == self || hitView == [wbHeader sharedHeader]) {
        return nil;
    }
    // Else return the hitView (as it could be one of this view's buttons):
    return hitView;
}*/

@end
