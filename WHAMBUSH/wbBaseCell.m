//
//  wbBaseCell.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 30/10/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbBaseCell.h"

@implementation wbBaseCell

@synthesize cellContent;
@synthesize contentArray;
@synthesize cellStyle;
@synthesize cellReuseIdentifier;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        //cellStyle = style;
        //cellReuseIdentifier = reuseIdentifier;
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setSelectionStyle:UITableViewCellSelectionStyleDefault];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = kTOPUICOLOR;
        [self setSelectedBackgroundView:bgColorView];
    }
    DPRINTCLASS;
    return self;
}
/*
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
*/
@end
