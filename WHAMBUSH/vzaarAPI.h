//
//  vzaarAPI.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 10/15/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "OAMutableURLRequest.h"
#import "wbDefines.h"
//#import "ASIFormDataRequest.h"
#import "AFNetworking.h"
#import "KDURLRequestSerialization+OAuth.h"
#import "XMLDictionary.h"
#import "AFAmazonS3manager.h"


@interface vzaarAPI : NSObject {
    NSString *guid;
    NSString *https;
    NSString *signature;
    NSString *acl;
    NSString *key;
    NSString *accesskeyid;
    NSString *bucket;
    NSString *expirationdate;
    NSString *policy;
    NSURL *fileURL;
    //ASIFormDataRequest *s3Uploader;
    AFAmazonS3Manager *s3Manager;
}

@property (nonatomic,retain) id delegate;

//authorize to vzaar
//-(void)authorizeVzaar:(SEL)selector;

//test method
-(void)testAuth;

//upload file to vzaar
-(void)uploadFile:(NSURL*)url;

//notify vzaar about upploaded file
-(void)notifyVzaar:(NSDictionary*)info;

//cancel upload
-(void)cancelUpload;

@end
