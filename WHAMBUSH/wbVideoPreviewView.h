//
//  wbVideoPreviewView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 24.06.14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbVideoPlayerViewController.h"

@interface wbVideoPreviewView : UIView
{
    UIToolbar *hideView;
    wbVideoPlayerViewController *player;
    UIButton *exitButton;
}

@property (nonatomic,retain) NSURL* videoUrl;
@property id delegate;

@end
