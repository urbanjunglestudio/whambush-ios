//
//  vzaarAPI.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 10/15/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "vzaarAPI.h"
#import "KDURLRequestSerialization+OAuth.h"

@implementation vzaarAPI

@synthesize delegate;

-(id)init
{
    if (self = [super init])
    {
        //delegate = self;
    }
    return self;
}

-(void)getFromVzaarWithEndpoint:(NSString*)endpoint parameters:(NSDictionary*)parameters response:(void (^)(id response))response errorResponse:(void (^)(id errorResponse))errorResponse
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    KDHTTPRequestSerializer *reqSerializer = [KDHTTPRequestSerializer serializer];
    [reqSerializer setUseOAuth:YES];
    [reqSerializer setOAuthToken:kVZAARSECRET];
    [reqSerializer setOAuthTokenSecret:kVZAARTOKEN];
    
    manager.requestSerializer = reqSerializer;
    //manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    
    NSString *urlStr = [NSString stringWithFormat:@"%@/%@",kVZAARAPI,endpoint];
    [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithXMLParser:responseObject];
        DCMSG(dictionary);
        
        response(dictionary);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DCMSG(error);
    }];
    
}


-(void)authorizeVzaar:(NSString*)selectorName
{
    NSString *endpoint = @"videos/signature";
    
    [self getFromVzaarWithEndpoint:endpoint parameters:nil response:^(id content){
        guid = [content objectForKey:@"guid"];
        https = [content objectForKey:@"https"];
        signature = [content objectForKey:@"signature"];
        acl = [content objectForKey:@"acl"];
        key = [content objectForKey:@"key"];
        accesskeyid = [content objectForKey:@"accesskeyid"];
        bucket = [content objectForKey:@"bucket"];
        expirationdate = [content objectForKey:@"expirationdate"];
        policy = [content objectForKey:@"policy"];
        if (selectorName != nil) {
            SEL selector = NSSelectorFromString(selectorName);
            IMP imp = [self methodForSelector:selector];
            void (*func)(id, SEL) = (void *)imp;
            func(self, selector);
        }
    } errorResponse:^(id data){
        [[wbAPI sharedAPI] showErrorWithTxt:NSLocalizedString(@"GENERAL_ERROR_AUTHORIZEVZAAR_FAILED",@"")];
    }];
}

-(BOOL)isAuthenticated
{
    if (guid == nil) {
        return NO;
    }
    if (https == nil) {
        return NO;
    }
    if (signature == nil) {
        return NO;
    }
    if (acl == nil) {
        return NO;
    }
    if (key == nil) {
        return NO;
    }
    if (accesskeyid == nil) {
        return NO;
    }
    if (bucket == nil) {
        return NO;
    }
    if (expirationdate == nil) {
        return NO;
    }
    if (policy == nil) {
        return NO;
    }
    return YES;
}

-(void)testAuth
{
    if([self isAuthenticated]) {
        DNSLog(@"YES");
    } else {
        [self authorizeVzaar:@"testAuth"];
        [self getFromVzaarWithEndpoint:@"test/whoami" parameters:nil response:^(id data){
            DCMSG(data);
        } errorResponse:nil];
    }
}

-(void)uploadFile:(NSURL*)url;
{
    fileURL = url;
    [self authorizeVzaar:@"doUpload"];
}


-(void)doUpload
{
    DCMSG(@"upload file");
    
    NSDictionary *parametersDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    bucket,@"bucket",
                                    key,@"key",
                                    accesskeyid,@"AWSAccessKeyId",
                                    acl,@"acl",
                                    policy,@"policy",
                                    signature,@"Signature",
                                    @"201",@"success_action_status",
                                    nil];
    
    NSString *url;
    //if (https) {
        url = [NSString stringWithFormat:@"https://%@.s3.amazonaws.com",bucket];
    //} else {
    //    url = [NSString stringWithFormat:@"http://%@.s3.amazonaws.com",bucket];
    //}

    s3Manager = [[AFAmazonS3Manager alloc] initWithBaseURL:[NSURL URLWithString:url]];

    [s3Manager postObjectWithFile:[fileURL path]
                  destinationPath:@""
                       parameters:parametersDict
                         progress:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite) {
                             DNSLog(@"%f%% Uploaded", (totalBytesWritten / (totalBytesExpectedToWrite * 1.0f) * 100));
                             [delegate performSelectorOnMainThread:@selector(setProgress:) withObject:[NSNumber numberWithFloat:(totalBytesWritten / (totalBytesExpectedToWrite * 1.0f))] waitUntilDone:NO];
                         }
                          success:^(id responseObject) {
                              DNSLog(@"Upload Complete");
                              [delegate performSelector:@selector(s3UpdateDidComplete:) withObject:[NSDictionary dictionaryWithXMLParser:responseObject]];
                          }
                          failure:^(NSError *error) {
                              DNSLog(@"Error: %@", error);
                              [delegate  performSelector:@selector(s3UpdateDidFail:) withObject:error];
                          }];
 }

-(void)cancelUpload
{
    DNSLog(@"Cancel upload");
    [s3Manager.operationQueue cancelAllOperations];
}

-(void)notifyVzaar:(NSDictionary*)info
{
    DCMSG(@"notify vzaar");
    
    NSData *data = [[NSString stringWithFormat:@"i(%@)%@-WHAMBUSH",[[wbUser sharedUser] username],[info objectForKey:@"name"]] dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *newTitle = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
   // NSString *newTitle = [NSString stringWithFormat:@"(%@)%@-WHAMBUSH",[[wbUser sharedUser] username],goodValue];
    NSString *newDesc = [NSString stringWithFormat:@"www.WHAMBUSH.com"];
    
    DCMSG(newTitle);
    
    NSMutableDictionary *content = [[NSMutableDictionary alloc] init];
    [content setObject:newDesc forKey:@"description"];
    [content setObject:newTitle forKey:@"title"];
    [content setObject:@"" forKey:@"labels"];
    [content setObject:guid forKey:@"guid"];
    [content setObject:@"6" forKey:@"profile"];
    [content setObject:@"true" forKey:@"transcoding"];
    NSString *encoding = [NSString stringWithFormat:@"<width>%@</width><bitrate>%@</bitrate>",[[[wbAPI sharedAPI] uploadData] objectForKey:@"width"],kBITRATE];
    [content setObject:encoding forKey:@"encoding"];
    
    DCMSG(@"Vzaar notify");
    
    NSMutableString *xmlStr = [[NSMutableString alloc] init];
    
    [xmlStr appendString:@"<vzaar-api><video>"];
    
    if (content) {
        for (NSString* arrayKey in content) {
            id value = [content objectForKey:arrayKey];
            [xmlStr appendFormat:@"<%@>%@</%@>",arrayKey,value,arrayKey];
        }
    }
    [xmlStr appendString:@"</video></vzaar-api>"];

    DCMSG(xmlStr);

    NSData *xmlData = [xmlStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    KDHTTPRequestSerializer *reqSerializer = [KDHTTPRequestSerializer serializer];
    [reqSerializer setUseOAuth:YES];
    [reqSerializer setOAuthToken:kVZAARSECRET];
    [reqSerializer setOAuthTokenSecret:kVZAARTOKEN];
    [reqSerializer setValue:xmlStr forHTTPHeaderField:@"Body"];
    [reqSerializer setValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    
    manager.requestSerializer = reqSerializer;
    manager.responseSerializer = [AFXMLParserResponseSerializer serializer];

    NSString *urlStr = [NSString stringWithFormat:@"%@/%@",kVZAARAPI,@"videos"];

    
    NSMutableURLRequest *request = [manager.requestSerializer requestWithMethod:@"POST" URLString:urlStr parameters:nil error:nil];
    [request setHTTPBody:xmlData];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithXMLParser:responseObject];
        DCMSG(dictionary);
        [delegate performSelector:@selector(finalizeUpload:) withObject:[NSNumber numberWithLong:(long)[[dictionary valueForKey:@"video"] integerValue]]];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DNSLog(@"Error req: %@",operation.request);
        DNSLog(@"Error rsp: %@",operation.response);
        [delegate performSelector:@selector(finalizeUpload:) withObject:[NSNumber numberWithInt:0]];
    }];
    
    [[[NSOperationQueue alloc] init] addOperation:operation];
    
        //return operation;



}

//-(NSDictionary *)sendPostRequestToURL:(NSURL *)url parameters:(NSDictionary *)parameters error:(NSError **)error
//{
//
//	OAConsumer *consumer = [[OAConsumer alloc] initWithKey:@"" secret:@""];
//	OAToken *token = [[OAToken alloc] initWithKey:kVZAARSECRET secret:kVZAARTOKEN];
//    
//	NSMutableString *xml = [[NSMutableString alloc] init];
//	
//	[xml appendString:@"<vzaar-api>\n<video>\n"];
//	
//	if (parameters) {
//        for (NSString* arrayKey in parameters) {
//            id value = [parameters objectForKey:arrayKey];
//            [xml appendFormat:@"<%@>%@</%@>\n",arrayKey,value,arrayKey];
//        }
//    }
//	[xml appendString:@"</video>\n</vzaar-api>"];
//	DCMSG(xml);
//    NSData *xmlData = [xml dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
//    
//	OAMutableURLRequest *request = [[OAMutableURLRequest alloc] initWithURL:url
//																   consumer:consumer
//																	  token:token
//																	  realm:nil
//														  signatureProvider:nil];
//    
//	[request setHTTPMethod:@"POST"];
//    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[xmlData length]] forHTTPHeaderField:@"Content-Length"];
//    [request setValue:@"application/xml" forHTTPHeaderField:@"Content-Type"];
//	[request prepare];
//    [request setHTTPBody:xmlData];
//	[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
//
//	
//    NSURLResponse* response;
//    NSError *parseError = nil;
//    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:error];
//    NSString *stringResponse = [[NSString alloc] initWithData:responseData encoding:[NSString defaultCStringEncoding]];
//    //NSDictionary* xmlResponse = [XMLReader dictionaryForXMLData:responseData error:&parseError];
//    
//    NSInteger statuscode = [(NSHTTPURLResponse*)response statusCode];
//    
//    responseData = nil;
//    response = nil;
//    request = nil;
//    xmlData = nil;
//    xml = nil;
//    consumer = nil;
//    token = nil;
//    
//    if (statuscode == 200 | statuscode == 201 | parseError != nil) {
//        DNSLog(@"Status code: %ld : %@", (long)statuscode,stringResponse);
//        return nil;
//    }
//    else
//    {
//        DNSLog(@"Status code: %ld -> %s:%d", (long)statuscode,__FILE__,__LINE__);
//        DNSLog(@"Response Failed: %@", stringResponse);
//        return nil;
//    }
//}
//
//-(NSDictionary *)sendGetRequestToURL:(NSURL *)url parameters:(NSDictionary *)parameters error:(NSError **)error {
//	
//	OAConsumer *consumer = [[OAConsumer alloc] initWithKey:@"" secret:@""];
//	OAToken *token = [[OAToken alloc] initWithKey:kVZAARSECRET secret:kVZAARTOKEN];
//	DCMSG(kVZAARSECRET);
//    DCMSG(kVZAARTOKEN);
//    
//    NSMutableString *parameterString = [NSMutableString string];
//
//    if (parameters) {
//        BOOL first = YES;
//        for (NSString* arrayKey in parameters) {
//            if (first) {
//                [parameterString appendString:@"?"];
//                first = NO;
//            }
//            id value = [parameters objectForKey:arrayKey];
//            [parameterString appendFormat:@"%@=%@",arrayKey,value];
//        }
//        DCMSG(parameterString);
//    }
//	
//	OAMutableURLRequest *request = [[OAMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",[url absoluteString],parameterString]] consumer:consumer token:token realm:nil signatureProvider:nil];
//    
//	[request setHTTPMethod:@"GET"];
//	[request prepare];
//    //DMSG;
//    
//    parameterString = nil;
//    consumer = nil;
//    token = nil;
//    
//	return [self sendPreparedRequest:request error:error];
//}
//
//-(NSDictionary *)sendPreparedRequest:(OAMutableURLRequest *)request error:(NSError **)error {
//    
//	NSHTTPURLResponse *response = nil;
//	NSError *connectionError = nil;
//	
//	NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&connectionError];
//
//	   
//	if (([response statusCode] != 200 && [response statusCode] != 201) || response == nil || connectionError != nil) {
//        DCMSG(response);
//        response = nil;
//        connectionError = nil;
//        responseData = nil;
//        
//		return nil;
//	} else {
//		// All was OK in the URL, let's try and parse the XML.
//		
//		NSError *parseError = nil;
//        
//		//NSDictionary *dict = [XMLReader dictionaryForXMLData:responseData error:&parseError];
//		
//        if (parseError != nil) {
//			if (error != NULL) {
//				*error = parseError;
//			}
//			return nil;
//		} else {
//			return nil;
//		}
//	}
//}
//
//-(void)beginS3UploadWithHTTPMethod:(NSString *)httpMethod headers:(NSDictionary *)headers url:(NSURL *)url path:(NSString*)path delegate:(id)_delegate
//{
//	DNSLog(@"s3url: %@",url);
//	DNSLog(@"httpmethod: %@",httpMethod);
//    
//	s3Uploader = [[ASIFormDataRequest alloc] initWithURL:url];
//	
//	DNSLog(@"HEADERS:\n%@",headers);
//	//do headers
//    [s3Uploader addRequestHeader:@"x-amz-acl:" value:[headers objectForKey:@"acl"]];
//    
//    //post data
//    if (headers) {
//        for (NSString* arrayKey in headers) {
//            id value = [headers objectForKey:arrayKey];
//            [s3Uploader setPostValue:value forKey:arrayKey];
//        }
//    }
//    [s3Uploader addFile:path forKey:@"file"];
//    
//	[s3Uploader setRequestMethod:httpMethod];
//	[s3Uploader setShowAccurateProgress:YES];
//	[s3Uploader setUploadProgressDelegate:_delegate];
//	[s3Uploader setUseSessionPersistence:NO];
//	[s3Uploader setDelegate:_delegate];
//	[s3Uploader setDidFinishSelector: @selector(s3UpdateDidComplete:)];
//	[s3Uploader setDidFailSelector: @selector(s3UpdateDidFail:)];
//	
//	NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//	[queue addOperation:s3Uploader];
//	
//}


@end
