//
//  wbFooter.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 04/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbLoginRegisterViewController.h"
#import "wbUpload.h"

@interface wbFooter : UIView
{
    CGRect footerBarRect;

    //UIImage *selectedImage;
    
    UIImage *userButtonImg;
    UIImage *userGuestButtonImg;
    //UIImage *userButtonSelectImg;
    UIImage *tvButtonImg;
    UIImage *homeButtonImg;
    //UIImage *homeButtonSelectImg;
    UIImage *missionButtonImg;
    //UIImage *missionButtonSelectImg;
    UIImage *cameraButtonImg;
    UIImage *cameraButtonPressImg;
    UIView *uploadProgress;
}

+(id)sharedFooter;

@property (retain,nonatomic) UIView *uploadView;

@property (retain,nonatomic) wbButton *homeButton;
@property (retain,nonatomic) wbButton *missionButton;
@property (retain,nonatomic) wbButton *cameraButton;
@property (retain,nonatomic) wbButton *tvButton;
@property (retain,nonatomic) wbButton *userButton;

@property (nonatomic) BOOL emptyFooter;
@property (nonatomic,retain) UIImage *footerImage;

@property (readonly,getter=footerBarHeight) float footerBarHeight;
@property (readonly,getter=statusBarHeight) float statusBarHeight;

//@property (nonatomic) float fixVal;

@end
