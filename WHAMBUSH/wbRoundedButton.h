//
//  wbRoundedButton.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 11/12/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbButton.h"

@interface wbRoundedButton : wbButton
{
}

@property (nonatomic,retain) UIColor *buttonColor;
@property BOOL filled;
@property NSInteger fontSize;

@property (nonatomic) float buttonWidth;
@property (nonatomic,retain) NSString* buttonText;
@property NSInteger cornerRadius;
@property BOOL helvetica;
@end


