//
//  wbTimerSubView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 9/18/13.
//  Copyright (c) 2013 Jari Kalinainen. All rights reserved.
//

#import "wbTimerSubView.h"

@implementation wbTimerSubView


@synthesize recording;
@synthesize seconds;
@synthesize lineWidth;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        
        timerRect = CGRectMake(self.bounds.origin.x+1,self.bounds.origin.y+1, self.bounds.size.width-2, self.bounds.size.height-2);
        
        seconds = kVIDEOMAXTIME;
        timeInSeconds = [[UILabel alloc] initWithFrame:timerRect];
        [timeInSeconds setCenter:CGPointMake(CGRectGetMidX(timerRect), CGRectGetMidY(timerRect)+3)];
        [timeInSeconds setBackgroundColor:kTRANSPARENTUICOLOR];
        [timeInSeconds setText:[NSString stringWithFormat:@"%.f",seconds]];
        timerColor = [UIColor greenColor];
        recording = NO;
        
        roundThing = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [roundThing setBackgroundColor:kTRANSPARENTUICOLOR];
        [roundThing setImage:[self drawTimer:seconds]];
        lineWidth = 6;
    }
    return self;
}

#define kTIMERLINEWIDTH lineWidth
#define kCIRCLESIZE (self.frame.size.width-(2*lineWidth))

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{

    circle = [self drawCircle:kCIRCLESIZE-1];
    [circle setCenter:CGPointMake(CGRectGetMidX(timerRect), CGRectGetMidY(timerRect))];
    [self addSubview:circle];

    [self addSubview:roundThing];
    
    //Time label
    [timeInSeconds setText:[NSString stringWithFormat:@"%.f",seconds]];
    [timeInSeconds setFont:kFONT(4*lineWidth)];
    //    [timeLabel sizeToFit];
    [timeInSeconds setTextColor:timerColor];
    [timeInSeconds setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:timeInSeconds];
}

-(void)setSeconds:(float)_seconds
{
    if (!isnan(_seconds)) {
        seconds  = _seconds;
    }
    [roundThing setImage:[self drawTimer:seconds]];
    [timeInSeconds setText:[NSString stringWithFormat:@"%.f",seconds]];
    [self drawTimerLine:seconds];
}

-(float)getAngle:(float)_seconds
{
    return ((kVIDEOMAXTIME-_seconds)/(float)kVIDEOMAXTIME)*360.00;
    
}

-(UIImageView*)drawCircle:(float)radius
{
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(radius,radius),0.0,0.0);
    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0,0,radius,radius)];
    [kBKGUICOLOR setFill];
    //[kYELLOWUICOLOR setFill];
    [ovalPath fill];
    
    UIImage *roundImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIImageView *roundImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, radius, radius)];
    [roundImgView setBackgroundColor:kTRANSPARENTUICOLOR];
    [roundImgView setImage:roundImg];
    
    return roundImgView;
    
}


-(UIImage*)drawTimer:(float)_seconds
{
    if (_seconds < 0) _seconds = 0.0;
    
    float angle = [self getAngle:_seconds];
    
#define kGFix 5.0
#define kGtoY ((float)kVIDEOMAXTIMEf-kGFix)
#define kYtoR ((float)kVIDEOMAXTIMEf/3.0)
    
    //seconds = roundf(10.0f * seconds)/10.0f;
    if (_seconds >= kGtoY) {
        timerColor = kGREENUICOLOR;
    } else if (_seconds >= kYtoR) {
        float red   = (173+(72*(((kGtoY) - _seconds)/(kGtoY-kYtoR))))/255;
        float green = (194+(18*(((kGtoY) - _seconds)/(kGtoY-kYtoR))))/255;
        float blue  =  (52-(10*(((kGtoY) - _seconds)/(kGtoY-kYtoR))))/255;
        //DNSLog(@"G2Y: 245:%.f,212:%.f,42:%.f",red*255,green*255,blue*255);
        timerColor = [[UIColor alloc] initWithRed:red green:green blue:blue alpha:0.9];
    } else {
        float red   = (245-(41*((kYtoR - _seconds)/kYtoR)))/255;
        float green = (212-(127*((kYtoR - _seconds)/kYtoR)))/255;
        float blue  = (42+(32*((kYtoR - _seconds)/kYtoR)))/255;
        //DNSLog(@"Y2R: 204:%.f,85:%.f,74:%.f",red*255,green*255,blue*255);
        timerColor = [[UIColor alloc] initWithRed:red green:green blue:blue alpha:0.9];
    }
    
    //CGContextRef context = UIGraphicsGetCurrentContext();
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(roundThing.frame.size.width,roundThing.frame.size.height),0.0,0.0);
    
    UIBezierPath* ovalPath = [UIBezierPath bezierPath];
    [ovalPath addArcWithCenter:CGPointMake(CGRectGetMidX(timerRect), CGRectGetMidY(timerRect)) radius:CGRectGetWidth(timerRect)/2-lineWidth/2 startAngle:angle*M_PI/180 endAngle:360*M_PI/180 clockwise:YES];
    //[ovalPath addLineToPoint:CGPointMake(CGRectGetMidX(timerRect), CGRectGetMidY(timerRect))];
    //[ovalPath closePath];
    
    //[timerColor setFill];
    //[ovalPath fill];
    [timerColor setStroke];
    ovalPath.lineWidth = kTIMERLINEWIDTH;
    [ovalPath stroke];
 
    UIImage *roundImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundImg;
   
    //Round thing
//    CGRect circleRect = CGRectMake((self.bounds.size.width/2)-25,(self.bounds.size.height/2)-25,50,50);
//    
//    CGContextSetFillColorWithColor(context, kBKGUICOLOR.CGColor);
//    CGContextFillEllipseInRect(context, circleRect);
    
}

-(UIImage*)drawTimerLine:(float)_seconds
{
#define kPINENDSIZE 3.0
    
    if (isnan(_seconds)) _seconds = 0;
    
    float angle = [self getAngle:_seconds];
    if (angle == 0) return [[UIImage alloc] init];
    float startX = 0;//cosf(angle*M_PI/180)*kCIRCLESIZE;
    float startY = 0;//sinf(angle*M_PI/180)*kCIRCLESIZE;
    float stopX = cosf(angle*M_PI/180)*CGRectGetWidth(timerRect)/2;
    float stopY = sinf(angle*M_PI/180)*CGRectGetWidth(timerRect)/2;
    float pinX = cosf(angle*M_PI/180)*((CGRectGetWidth(timerRect)/2)-kPINENDSIZE/2);
    float pinY = sinf(angle*M_PI/180)*((CGRectGetWidth(timerRect)/2)-kPINENDSIZE/2);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(timerRect)+startX, CGRectGetMidY(timerRect)+startY);
    CGPoint stopPoint = CGPointMake(CGRectGetMidX(timerRect)+stopX, CGRectGetMidY(timerRect)+stopY);
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(roundThing.frame.size.width,roundThing.frame.size.height),0.0,0.0);
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:startPoint];
    
    [bezierPath addCurveToPoint:stopPoint controlPoint1:stopPoint controlPoint2:stopPoint];
    [kBKGUICOLOR setStroke];
    bezierPath.lineWidth = 1;
    [bezierPath stroke];

    UIBezierPath* ovalPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(CGRectGetMidX(timerRect)+(pinX-(kPINENDSIZE/2.0)), CGRectGetMidY(timerRect)+(pinY-(kPINENDSIZE/2.0)),kPINENDSIZE,kPINENDSIZE)];
    [kBKGUICOLOR setFill];
    [ovalPath fill];
    
    
    UIImage *lineImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return lineImg;

}

-(void)setClipPoint
{
    UIImageView *clipPoint = [[UIImageView alloc] initWithFrame:roundThing.frame];
    [clipPoint setBackgroundColor:kTRANSPARENTUICOLOR];
    [clipPoint setImage:[self drawTimerLine:seconds]];
    [self addSubview:clipPoint];
    [self insertSubview:clipPoint belowSubview:circle];
    if (clipsArray == nil) {
        clipsArray = [[NSMutableArray alloc] init];
    }
    [clipsArray addObject:clipPoint];
}

-(void)removeClipPoint
{
    [[clipsArray lastObject] removeFromSuperview];
    [clipsArray removeLastObject];
}

@end
