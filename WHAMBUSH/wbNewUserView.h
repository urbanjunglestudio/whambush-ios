//
//  wbNewUserView.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 07/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "wbUserViewSlideViewController.h"
#import "wbInsertTextView.h"
#import "wbRoundedButton.h"
#import "IBActionSheet.h"

@interface wbNewUserView : UIScrollView <UIScrollViewDelegate,IBActionSheetDelegate>
{
    UIImageView *profilePicture;
    //UIImageView *countryFlag;
    wbLabel *userDescription;
    wbRoundedButton *settingsButton;
    wbRoundedButton *followButton;
    wbLabel *bananaCount;
    wbUserViewSlideViewController *slideViewController;
    wbButton *profilePictureButton;
    
    BOOL isMe;
}
@property (nonatomic,retain) UIImageView *countryFlag;
@property (nonatomic,retain) wbUser *user;
@property (nonatomic,retain) wbInsertTextView *insertTxtView;


@end
