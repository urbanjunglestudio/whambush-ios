//
//  wbBirthdaySelectView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 10/01/14.
//  Copyright (c) 2014 Jari Kalinainen. All rights reserved.
//

#import "wbBirthdaySelectView.h"

@implementation wbBirthdaySelectView

@synthesize birthday;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:kTRANSPARENTUICOLOR];
        [self setUserInteractionEnabled:NO];
        [self setHidden:YES];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];

        startYear = [components year]-100;
            bDay = [components day];
            bMonth = [components month];
            bYear = [components year];
    }
    DPRINTCLASS;
    return self;
}

-(void)setBirthday:(NSString *)_birthday
{
    if ([birthday isKindOfClass:[NSString class]]) {
        if ([birthday length] > 4) {
            NSArray *bdayComponent = [_birthday componentsSeparatedByString:@"-"];
            if (bdayComponent != nil) {
                NSInteger year = [[bdayComponent objectAtIndex:0] integerValue];
                if (startYear > year) {
                    startYear = year-1;
                }
            }
        }
    }
    birthday = _birthday;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [[self subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
//    if (![birthday isEqualToString:@"<null>"]) {
    if (![birthday isKindOfClass:[NSNull class]]) {
        NSArray *bdayComponent = [birthday componentsSeparatedByString:@"-"];
        bDay = [[bdayComponent objectAtIndex:2] integerValue];
        bMonth = [[bdayComponent objectAtIndex:1] integerValue];
        bYear = [[bdayComponent objectAtIndex:0] integerValue];
    }

    // Drawing code
    if (hideView == nil) {
        hideView = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    [hideView setBackgroundColor:kTRANSPARENTUICOLOR];
    [hideView setAlpha:0.0];
    [hideView setBarStyle:UIBarStyleBlack];
    
//    hideView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//    [hideView setBackgroundColor:[UIColor blackColor]];
//    [hideView setAlpha:0];
 
    theBox = [[UIView alloc] initWithFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, 182+40)];
    [theBox setBackgroundColor:kSEPRATORUICOLORAlpha];
 
    UIPickerView *selection = [[UIPickerView alloc] init];
    [selection setFrame:CGRectMake(0, 40, self.frame.size.width, 180)];
    [selection setBackgroundColor:kSEPRATORUICOLORAlpha];
    [selection setDelegate:self];
    [selection setDataSource:self];
    [selection setShowsSelectionIndicator:YES];
    [selection setTintColor:kWHITEUICOLOR];
    [selection selectRow:bDay-1 inComponent:0 animated:NO];
    [selection selectRow:bMonth-1 inComponent:1 animated:NO];
    [selection selectRow:bYear-1-startYear inComponent:2 animated:NO];
    
    cancelBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [cancelBtn setFrame:hideView.frame];
    [cancelBtn setBackgroundColor:[UIColor clearColor]];
    [cancelBtn addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
    [hideView addSubview:cancelBtn];
    
    UIImage *sendImg = [UIImage ch_imageNamed:@"done_comment_default.png"];
    UIImage *sendImgActive = [UIImage ch_imageNamed:@"done_comment_active.png"];
    sendBtn = [UIButton buttonWithType:UIButtonTypeSystem];
    [sendBtn setFrame:CGRectMake(self.frame.size.width-(sendImg.size.width+2), 2, sendImg.size.width, sendImg.size.height)];
    [sendBtn setBackgroundColor:[UIColor clearColor]];
    [sendBtn setBackgroundImage:sendImg forState:UIControlStateNormal];
    [sendBtn setBackgroundImage:sendImgActive forState:UIControlStateHighlighted];
    [sendBtn setShowsTouchWhenHighlighted:YES];
    [sendBtn addTarget:self action:@selector(createBirthday:) forControlEvents:UIControlEventTouchUpInside];
    
    [theBox addSubview:selection];
    [theBox addSubview:sendBtn];
    [self addSubview:hideView];
    [self addSubview:theBox];
}

// Handle the selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            bDay = row+1;
            break;
        case 1:
            bMonth = row+1;
            break;
        case 2:
            bYear = row+1+startYear;
            break;
            
        default:
            break;
    }
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger numRows;
    
    switch (component) {
        case 0:
            numRows = 31;
            break;
        case 1:
            numRows = 12;
            break;
        case 2:
            numRows = 100;
            break;
        default:
            numRows = 0;
            break;
    }
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}


// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    int sectionWidth;
    switch (component) {
        case 0:
            sectionWidth = 50;
            break;
        case 1:
            sectionWidth = 150;
            break;
        case 2:
            sectionWidth = 100;
            break;
        default:
            sectionWidth = 0;
            break;
    }
    
    return sectionWidth;
}
-(NSAttributedString*)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *txt;
    switch (component) {
        case 0:
        {
            txt = [NSString stringWithFormat:@"%ld",(long)row+1];
        }
            break;
        case 1:
        {
            NSDateFormatter *formatString = [[NSDateFormatter alloc] init];
            [formatString setDateFormat:@"MM"];
            NSDate *monthDate = [formatString dateFromString:[NSString stringWithFormat:@"%02ld",(long)row+1]];
            [formatString setDateFormat:@"MMMM"];
            txt = [formatString stringFromDate:monthDate];
        }
            break;
        case 2:
        {
            txt = [NSString stringWithFormat:@"%ld",(long)(row+startYear+1)];
        }
            break;
        default:
            break;
    }
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:[txt capitalizedString] attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];

    return attString;
}

-(void)createBirthday:(id)sender
{
    NSBlockOperation *op = [NSBlockOperation blockOperationWithBlock:^(void){
        [[wbWaitView sharedWait] performSelectorOnMainThread:@selector(show) withObject:NULL waitUntilDone:YES];
    }];
    
    DNSLog(@"%ld-%02ld-%02ld",(long)bYear,(long)bMonth,(long)bDay);
    NSString *bdayString = [NSString stringWithFormat:@"%ld-%02ld-%02ld",(long)bYear,(long)bMonth,(long)bDay];
    if (![bdayString isEqualToString:birthday]) {
        if ([delegate respondsToSelector:@selector(updateBirthday:)]) {
            [op setCompletionBlock:^(void){
                [delegate performSelectorOnMainThread:@selector(updateBirthday:) withObject:bdayString waitUntilDone:YES];
            }];
            DMSG;
            [op start];
        } else {
            DMSG;
            kHIDEWAIT;
        }
    } else {
        [self hide];
    }
    
    //bdayString = nil;
}


//show/hide
-(void)show
{
    [delegate setScrollEnabled:NO];
    [self setHidden:NO];
    [self setUserInteractionEnabled:YES];
    // get a rect for the textView frame
	CGRect containerFrame = theBox.frame;
    containerFrame.origin.y = self.bounds.size.height - theBox.frame.size.height;//-20;//-50;
	
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
	
	// set views with new info
    hideView.alpha = 0.97;
    theBox.alpha = 0.90;
	theBox.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
}

-(void)hide
{
    DMSG;
    [self setUserInteractionEnabled:NO];
    // get a rect for the textView frame
	CGRect containerFrame = theBox.frame;
    containerFrame.origin.y = self.bounds.size.height;
	
    // animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationCurve:7];
	
	// set views with new info
    hideView.alpha = 0.0;
    theBox.alpha = 0.0;
	theBox.frame = containerFrame;
    
	// commit animations
	[UIView commitAnimations];
    [delegate setScrollEnabled:YES];
    [self setHidden:YES];
}

@end
