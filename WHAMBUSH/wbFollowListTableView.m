//
//  wbFollowListTableView.m
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 15/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import "wbFollowListTableView.h"

@implementation wbFollowListTableView

@synthesize user;
@synthesize isFollowers;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setDelegate:self];
        [self setDataSource:self];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self setSeparatorColor:kSEPRATORUICOLOR];
        [self setSeparatorInset:UIEdgeInsetsZero];
        [self setBackgroundColor:kBKGUICOLOR];
        //[self setCanCancelContentTouches:NO];
        [self setHidden:NO];
        [self setHideHeader:YES];
        
        isMe = NO;
        isFollowers = NO;
        
    }
    DPRINTCLASS;
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code

    if (refreshController == nil) {
        refreshController = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        [refreshController addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
        [refreshController setBackgroundColor:[UIColor colorWithPatternImage:[UIImage ch_imageNamed:@"refresh_bg_image"]]];
        [self addSubview:refreshController];
    }
    
    UIView *tableFooter =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 100)];
    [tableFooter setBackgroundColor:kBKGUICOLOR];
    [self setTableFooterView:tableFooter];
    
    if (ai == nil) {
        ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [ai startAnimating];
        [ai setCenter:CGPointMake(self.frame.size.width/2,kFOLLOWLINEH/2)];
        [tableFooter addSubview:ai];
    }
}


-(void)resultsReady
{
    
    [self saveUsers:resultArray];
    
    NSMutableArray *newRows = [[NSMutableArray alloc] init];
    
    for (long i = [self numberOfRowsInSection:0]; i < numberOfResults; i++) {
        NSIndexPath *newReloadRows = [NSIndexPath indexPathForRow:i inSection:0];
        [newRows addObject:newReloadRows];
        newReloadRows = nil;
    }
    
    [self beginUpdates];
    [self insertRowsAtIndexPaths:newRows withRowAnimation:UITableViewRowAnimationFade];
    [self endUpdates];
    
    [self setHidden:NO];
    if (ai == nil) {
        ai = [[UIActivityIndicatorView alloc] init];
        [ai setHidden:YES];
    }
    [ai removeFromSuperview];

    kHIDEWAIT;
}

-(void)saveUsers:(NSArray*)users
{
    for (int i = 0; i < [users count]; i++) {
        [[wbData sharedData] saveWhambushUser:[users objectAtIndex:i]];
    }
}

-(void)setUser:(wbUser *)_user
{
    user = _user;
    if ([[user userId] integerValue] == [[[wbUser sharedUser] userId] integerValue]) {
        isMe = YES;
    }
    [self setIsFollowers:isFollowers];
}

 -(void)setIsFollowers:(BOOL)_isFollowers
{
    isFollowers = _isFollowers;
    if (user != nil) {
        if (isFollowers) {
            super.endpoint = [NSString stringWithFormat:@"follows/?type=followers&user=%ld",[[user userId] longValue]];
        } else {
            super.endpoint = [NSString stringWithFormat:@"follows/?type=following&user=%ld",[[user userId] longValue]];
        }
        [self execute];
    }
}

-(void)execute
{
    if (user != nil && super.endpoint != nil) {
        [[wbAPI sharedAPI] getDataWithEndpoint:super.endpoint delegate:self];
    }
}

-(void)update
{
    //[self refresh:nil];
    [self reloadData];
}

-(void)removeLine:(NSIndexPath*)indexPath
{
    [self beginUpdates];
    
//    [resultArray setObject:[NSNull null] atIndexedSubscript:[indexPath row]];
//    [self reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [resultArray removeObjectAtIndex:[indexPath row]];
    numberOfResults = [resultArray count];

    [self deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self endUpdates];
}


///////
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *identifier = [NSString stringWithFormat:@"fllw_%d_%d",indexPath.section,indexPath.row];
    
    wbFollowListCell *cell ;//= [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if  (cell == nil) {
        cell = [[wbFollowListCell alloc] init];//WithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    if ([resultArray count]-8 == [indexPath row] || [resultArray count] == [indexPath row]) {
        if (numberOfResults < totalNumberOfResults) {
            DMSG;
            [self performSelectorInBackground:@selector(getMoreResults) withObject:nil];
        }
    }
    
    [cell setBackgroundColor:kTRANSPARENTUICOLOR];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //    if  ([resultArray count] > ([indexPath row])){
    [cell setUser:[[wbData sharedData] getWhambushUser:[[resultArray objectAtIndex:[indexPath row]] objectForKey:@"url"]]];
    [cell setDelegate:self];
    if (!isMe) {
        [cell setHideWhenUnfollowPressed:NO];
    } else {
        [cell setHideWhenUnfollowPressed:!isFollowers];
    }
    [cell setIndexPath:indexPath];
    //      }
    [ai removeFromSuperview];

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfResults;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return kFOLLOWLINEH;
}

@end
