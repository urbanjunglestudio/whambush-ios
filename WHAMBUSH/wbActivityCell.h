//
//  wbActivityCell.h
//  WHAMBUSH
//
//  Created by Jari Kalinainen on 26/01/15.
//  Copyright (c) 2015 Jari Kalinainen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface wbActivityCell : UIView
{
    UIImageView *profilePicture;
    wbLabel *activityLabel;
    wbLabel *postedLabel;
    UIImageView *unreadDot;
    wbUser *who;
    NSInteger videoId;
    NSString *activityString;
    BOOL isGuest;
    BOOL isRead;
    NSInteger action; //0 = like, 1 = follow, 2 = comment
    wbButton *userButton;
    UIView *textContainer;
}

@property (nonatomic,retain) NSDictionary *data;
@property BOOL allRead;

@end
