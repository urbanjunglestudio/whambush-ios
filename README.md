# whambush-ios
Whambush iOS app

How to use:
- pull the repository and cd to root of it
- install cocoapods to your enviroment (https://cocoapods.org)
  - `sudo gem install cocoapods`
- Install pods `pod install`
- Open generated WHAMBUSH.xcworkspace (workspace) with Xcode6 (or newer)
- Build project
  - All set if no errors :)
